
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustDetailsForBillGen]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 -- =============================================                    
 -- Author  : NEERAJ KANOJIYA                  
 -- Create date  : 25-MARCHAR-2015                    
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S DETAILS FOR ASSIGN METER.       
 -- Create date  : 8-APRIL-2015                    
 -- Description  : REMOVE ACTIVE STATUS ID FILTER
 
 -- =============================================                    
 ALTER PROCEDURE [dbo].[USP_GetCustDetailsForBillGen]                    
 (                    
 @XmlDoc xml                    
 )                    
 AS                    
 BEGIN                    
  DECLARE @GlobalAccountNumber VARCHAR(50)                  
  SELECT         
	@GlobalAccountNumber = C.value('(GolbalAccountNumber)[1]','VARCHAR(50)')                                                                
	FROM @XmlDoc.nodes('CustomerRegistrationBE') as T(C)         
	
 		  
		  SELECT 
				   A.GlobalAccountNumber AS CustomerID  
				  ,dbo.fn_GetCustomerFullName_New(A.Title,A.FirstName,A.MiddleName,A.LastName) AS  FirstNameLandlord -- Modified By -Padmini                   
				  --,dbo.fn_GetCustomerFullName(GlobalAccountNumber) AS Name               
				  ,A.GlobalAccountNumber AS AccountNo
				  ,(A.AccountNo+ ' - '+ A.GlobalAccountNumber) AS GlobalAccNoAndAccNo
				  ,CASE WHEN A.MeterNumber IS NULL THEN 'N/A' ELSE A.MeterNumber END AS MeterNumber 
				  ,RT.RouteName AS RouteName 
				  ,A.RouteSequenceNo AS RouteSequenceNumber                            
				  ,A.ReadCodeID AS ReadCodeID                  
				  ,A.TariffId AS ClassID                       
				  --,dbo.fn_GetCustomerTotalDueAmount(GlobalAccountNumber) AS OutStandingAmount
				  ,A.OutStandingAmount AS OutStandingAmount
				  ,(SELECT DBO.fn_GetMeterDials_ByAccountNO(@GlobalAccountNumber)) AS MeterDials 
				  ,(dbo.fn_GetCustomerServiceAddress(A.GlobalAccountNumber)) AS FullServiceAddress  
				  ,A.CustomerTypeId AS CustomerTypeId
				  ,1 AS IsSuccessful  
				  ,A.ServiceUnitName 
				  ,A.ServiceCenterName
				  ,A.CycleName
				  --,A.BookNo      
				  ,A.OldAccountNo  
				  ,A.BookId AS BookNo
				  ,A.CycleId
		  FROM [UDV_CustomerDescription] AS A    
		  LEFT JOIN Tbl_MRoutes AS RT ON A.RouteSequenceNo=RT.RouteID  
		  WHERE (GlobalAccountNumber = @GlobalAccountNumber OR A.OldAccountNo=@GlobalAccountNumber)
				--AND A.ActiveStatusId=1		                        
		  FOR XML PATH('CustomerRegistrationBE'),TYPE		               
 END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerPendingBillDetailsByAccountNo]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- =============================================                  
 -- Author  :	Karteek                
 -- Create date  : 08 May 2015               
 -- Description  : THIS PROCEDURE WILL GET CUSTOMER'S AND BILL DETAILS BY ACCOUNT # 
 -- =============================================                  
ALTER PROCEDURE [dbo].[USP_GetCustomerPendingBillDetailsByAccountNo]                  
(                  
	@XmlDoc xml                  
)                  
AS                  
 BEGIN                  
	DECLARE	@AccountNo VARCHAR(50)
 
	SELECT       
		@AccountNo = C.value('(AccountNo)[1]','VARCHAR(50)')                     
	FROM @XmlDoc.nodes('CustomerDetailsBe') as T(C)              
    
	DECLARE	@GlobalAccountNo VARCHAR(50)
			,@Name VARCHAR(200)
			,@ServiceAddress VARCHAR(MAX)
			,@BookGroup VARCHAR(50)
			,@BookName VARCHAR(50)
			,@Tariff VARCHAR(50)
			,@OutstandingAmount DECIMAL(18,2)
			,@OldAccountNo VARCHAR(50)
			,@AccountNo1 VARCHAR(50)
	
    SELECT @GlobalAccountNo = GlobalAccountNumber 
			,@Name = dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)
			,@ServiceAddress = dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName    
											,Service_Landmark    
											,Service_City,''    
											,Service_ZipCode) 
			,@OutstandingAmount = OutStandingAmount 
			,@BookGroup = CycleName
			,@BookName = BookId +' ( '+ BookCode +' )'
			,@Tariff = ClassName
			,@OldAccountNo=OldAccountNo
			,@AccountNo1 = AccountNo
    FROM UDV_CustomerDescription
    WHERE GlobalAccountNumber = @AccountNo OR OldAccountNo = @AccountNo
    
    DECLARE @LastPaidDate DATETIME, @LastPaidAmount DECIMAL(18,2)
    
    SELECT TOP(1) @LastPaidDate = RecievedDate
		,@LastPaidAmount = PaidAmount 
	FROM Tbl_CustomerPayments WHERE AccountNo = @GlobalAccountNo
	ORDER BY CustomerPaymentID DESC
    
    SELECT 
    (
		SELECT CONVERT(VARCHAR(20),@LastPaidDate,106) AS LastPaymentDate
			,@LastPaidAmount AS LastPaidAmount
			,@GlobalAccountNo AS AccountNo
			,(@AccountNo1+' - '+@GlobalAccountNo) AS AccNoAndGlobalAccNo
			,@Name AS Name
			,@ServiceAddress AS ServiceAddress
			,@OutstandingAmount AS OutStandingAmount
			,@BookGroup AS BookGroup 
			,@BookName AS BookName
			,@Tariff AS Tariff
			,@OldAccountNo AS OldAccountNo
		FOR XML PATH('Customerdetails'),TYPE 
    )
    ,
    (
		SELECT 
			 CB.BillNo
			,CustomerBillId AS CustomerBillID
			,(CASE CB.ReadCodeId WHEN 1 THEN 'D' WHEN 2 THEN 'R' WHEN 3 THEN 'A' ELSE 'M' END) AS ReadType
			,CB.BillYear        
			,CB.BillMonth        
			,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS BillingMonthName
			,CONVERT(VARCHAR(15),CB.BillGeneratedDate,106) AS LastBillGeneratedDate
			,ISNULL(CB.PreviousReading,'0') AS PreviousReading
			,ISNULL(CB.PresentReading,'0') AS PresentReading
			,Usage AS Consumption
			,Dials AS MeterDials
			,NetEnergyCharges
			,NetFixedCharges
			,TotalBillAmount
			,VAT AS Vat
			,TotalBillAmountWithTax
			,NetArrears
			,TotalBillAmountWithArrears 
			,CB.AccountNo AS AccountNo
			,CB.PaidAmount AS LastPaidAmount
		FROM Tbl_CustomerBills CB 
		WHERE CB.AccountNo = @GlobalAccountNo AND ISNULL(CB.PaymentStatusID,2) = 2
		AND (ISNULL(TotalBillAmountWithTax,0) > (ISNULL(PaidAmount,0) + ISNULL(AdjustmentAmmount,0)))
		ORDER BY CB.CustomerBillId DESC
		FOR XML PATH('CustomerBillDetails'),TYPE 
    )
    FOR XML PATH(''),ROOT('CustomerDetailsInfoByXml')            
    
 END     

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[USP_InsertBillGenerationDetails_New_ServicePurpose]
--( @XmlDoc Xml)
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Xml data reading
	
	DECLARE @GlobalAccountNumber  VARCHAR(50)     
			,@Month INT        
			,@Year INT         
			,@Date DATETIME         
			,@MonthStartDate DATE     
			,@PreviousReading VARCHAR(50)        
			,@CurrentReading VARCHAR(50)        
			,@Usage DECIMAL(20)        
			,@RemaningBalanceUsage INT        
			,@TotalAmount DECIMAL(18,2)= 0        
			,@TaxValue DECIMAL(18,2)        
			,@BillingQueuescheduleId INT        
			,@PresentCharge INT        
			,@FromUnit INT        
			,@ToUnit INT        
			,@Amount DECIMAL(18,2)    
			,@TaxId INT        
			,@CustomerBillId INT = 0        
			,@BillGeneratedBY VARCHAR(50)        
			,@LastDateOfBill DATETIME        
			,@IsEstimatedRead BIT = 0        
			,@ReadCodeId INT        
			,@BillType INT        
			,@LastBillGenerated DATETIME      
			,@FeederId VARCHAR(50)      
			,@CycleId VARCHAR(MAX)     -- CycleId 
			,@BillNo VARCHAR(50)    
			,@PrevCustomerBillId INT    
			,@AdjustmentAmount DECIMAL(18,2)    
			,@PreviousDueAmount DECIMAL(18,2)    
			,@CustomerTariffId VARCHAR(50)    
			,@BalanceUnits INT    
			,@RemainingBalanceUnits INT    
			,@IsHaveLatest BIT = 0    
			,@ActiveStatusId INT    
			,@IsDisabled BIT    
			,@BookDisableType INT    
			,@IsPartialBill BIT    
			,@OutStandingAmount DECIMAL(18,2)    
			,@IsActive BIT    
			,@NoFixed BIT = 0    
			,@NoBill BIT = 0     
			,@ClusterCategoryId INT=NULL  
			,@StatusText VARCHAR(50)    
			,@BU_ID VARCHAR(50)  
			,@BillingQueueScheduleIdList VARCHAR(MAX)  
			,@RowsEffected INT
			,@IsFromReading BIT
			,@TariffId INT
			,@EnergyCharges DECIMAL(18,2) 
			,@FixedCharges  DECIMAL(18,2)
			,@CustomerTypeID INT
			,@ReadType Int
			,@TaxPercentage DECIMAL(18,2)=5  
			,@TotalBillAmountWithTax  DECIMAL(18,2)
			,@AverageUsageForNewBill  DECIMAL(18,2)
			,@IsEmbassyCustomer INT
			,@TotalBillAmountWithArrears  DECIMAL(18,2) 
			,@CusotmerNewBillID INT
			,@InititalkWh INT
			,@NetArrears DECIMAL(18,2)
			,@BookNo VARCHAR(30)
			,@GetPaidMeterBalance DECIMAL(18,2)
			,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)
			,@MeterNumber VARCHAR(50)
			,@ActualUsage DECIMAL(18,2)
			,@RegenCustomerBillId INT
			,@PaidAmount  DECIMAL(18,2)
			,@PrevBillTotalPaidAmount Decimal(18,2)
			,@PreviousBalance Decimal(18,2)
			,@OpeningBalance Decimal(18,2)
			,@CustomerFullName VARCHAR(MAX)
			,@BusinessUnitName VARCHAR(100)
			,@TariffName VARCHAR(50)
			,@ReadDate DateTime
			,@Multiplier INT
			,@Service_HouseNo VARCHAR(100)
			,@Service_Street VARCHAR(100)
			,@Service_City VARCHAR(100)
			,@ServiceZipCode VARCHAR(100)
			,@Postal_HouseNo VARCHAR(100)
			,@Postal_Street VARCHAR(100)
			,@Postal_City VARCHAR(100)
			,@Postal_ZipCode VARCHAR(100)
			,@Postal_LandMark VARCHAR(100)
			,@Service_LandMark VARCHAR(100)
			,@OldAccountNumber VARCHAR(100)
			,@IspartialClose INT
			,@TariffCharges varchar(max)
			,@CusotmerPreviousBillNo varchar(50)
			,@DisableDate Datetime
			,@BillingComments varchar(max)
			,@TotalBillAmount Decimal(18,2)
			,@PaidMeterDeductedAmount Decimal(18,2)
			,@IsFirstmonthBill Bit
			,@SetupDate DATETIME
			,@ConnectionDate DATETIME

	DECLARE @CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()     

	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)        

	SET  @CycleId = (SELECT STUFF((SELECT '|' + CAST(CycleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
     SET  @BillingQueueScheduleIdList = (SELECT STUFF((SELECT '|' + CAST(BillingQueueScheduleId AS VARCHAR(50)) 
         FROM Tbl_BillingQueueSchedule 
         WHERE BillGenarationStatusId=5
         ORder by BillingQueueScheduleId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,1,'')  )
        
        set @CycleId=REPLACE(@CycleId, ' ', '')
        set @BillingQueueScheduleIdList=REPLACE(@BillingQueueScheduleIdList, ' ', '')
	 
 
	
	--===========================Creating table for CycleID and BillingQueueSheduleID List=============================
	CREATE TABLE #tmpCustomerBillsDetails(ID INT IDENTITY(1,1), CycleID VARCHAR(MAX),BillingQueueScheduleID VARCHAR(MAX)) 
	    
	INSERT INTO #tmpCustomerBillsDetails(CycleID ,BillingQueueScheduleID)  
	SELECT Value1 AS CycleID, Value2 AS BillingQueueScheduleID  
	FROM  [dbo].[fn_splitTwo](@CycleId,@BillingQueueScheduleIdList,'|')  
	--===========================---------------------------------------------------------------============================= 
 
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(ID) FROM #tmpCustomerBillsDetails)  
	WHILE(@index<@rwCount) --loop
	BEGIN
	 
		-- Set all values null
		SET @CycleId = NULL
		SET @BillingQueueScheduleId = NULL
		SET @BU_ID = NULL
		
		-- increase the index value
		SET @index=@index+1
		
		SET @CycleId=(SELECT CycleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		SET @BillingQueueScheduleId=(SELECT BillingQueueScheduleID FROM #tmpCustomerBillsDetails WHERE ID=@index)  
		
		--=======================Filtering customer data based on bu=============================  
		DECLARE @CustomerMaster TABLE(
			GlobalAccountNumber VARCHAR(10)  
			,TariffId INT  
			,OutStandingAmount DECIMAL(18,2)  
			,SortOrder INT  
			,ActiveStatusId INT  
			,MeterNumber  VARCHAR(50)  
			,BU_ID VARCHAR(50)  
			,SU_ID VARCHAR(50)  
			,ServiceCenterId VARCHAR(50)  
			,PoleId VARCHAR(50)  
			,OldAccountNo VARCHAR(50)  
			,BookNo VARCHAR(50) 
			,CustomerTypeID INT
			,IsEmbassyCustomer INT
			,Highestconsumption decimal(18,2)
			,ClusterCategoryId INT
			,InititalkWh INT
			,ReadCodeID INT
			,OpeningBalance	Decimal(18,2)
			,CustomerFullName varchar(max)
			,BusinessUnitName  varchar (100)   -- New Table      
			,ServiceHouseNo  varchar (max)-- New Table   
			,ServiceStreet varchar (max) -- New Table         
			,ServiceCity varchar (max)       -- New Table   
			,ServiceZipCode varchar (max)  -- New Table          
			,TariffName varchar (50)	   -- New Table  
			,Postal_ZipCode  varchar (max)	
			,Postal_HouseNo   varchar (max)	 
			,Postal_StreetName   varchar (max)	  
			,Postal_City   varchar (max)
			,Postal_LandMark VARCHAR(max)
			,Service_LandMark VARCHAR(max)
			,SetupDate DATETIME
			,ConnectionDate DATETIME
		)        
		-- Get Customer info for master data
		INSERT INTO @CustomerMaster  
		(  
			GlobalAccountNumber  
			,TariffId  
			,OutStandingAmount  
			,SortOrder  
			,ActiveStatusId  
			,MeterNumber  
			,BU_ID  
			,SU_ID  
			,ServiceCenterId    
			,PoleId  
			,OldAccountNo  
			,BookNo     
			,CustomerTypeID
			,IsEmbassyCustomer
			,Highestconsumption
			,ClusterCategoryId
			,InititalkWh
			,ReadCodeID
			,OpeningBalance
			,CustomerFullName
			,BusinessUnitName      
			,ServiceHouseNo   
			,ServiceStreet        
			,ServiceCity   
			,ServiceZipCode           
			,TariffName  
			,Postal_ZipCode 	
			,Postal_HouseNo  
			,Postal_StreetName   	  
			,Postal_City 
			,Postal_LandMark
			,Service_LandMark
			,SetupDate  
			,ConnectionDate
		)  
		SELECT CD.GlobalAccountNumber  
			,CD.TariffId  
			,CD.OutStandingAmount  
			,CD.SortOrder  
			,CD.ActiveStatusId  
			,CD.MeterNumber  
			,BU.BU_ID  
			,CD.SU_ID  
			,CD.ServiceCenterId    
			,CD.PoleId  
			,CD.OldAccountNo  
			,CD.BookNo
			,CD.CustomerTypeID
			,CD.IsEmbassyCustomer
			,CD.Highestconsumption
			,CD.ClusterCategoryId
			,CD.InitialBillingKWh
			,CD.ReadCodeID
			,CD.OpeningBalance
			, dbo.fn_GetCustomerFullName_New(Cd.Title,CD.FirstName,CD.MiddleName,CD.LastName)--,CD.CustomerFullName
			,CD.BusinessUnitName      
			,CD.Service_HouseNo   
			,CD.Service_StreetName        
			,CD.Service_City   
			,CD.Service_ZipCode           
			,CD.ClassName  
			,CD.Postal_ZipCode 	
			,CD.Postal_HouseNo  
			,CD.Postal_StreetName   	  
			,CD.Postal_City
			,Cd.Postal_Landmark
			,Cd.Service_Landmark 
			,CD.SetupDate 
			,CD.ConnectionDate
		FROM Tbl_Cycles C(NOLOCK)
			INNER JOIN Tbl_ServiceCenter SC(NOLOCK) ON SC.ServiceCenterId=C.ServiceCenterId
			INNER JOIN Tbl_ServiceUnits SU(NOLOCK) ON SU.SU_ID=SC.SU_ID
			INNER JOIN Tbl_BussinessUnits BU(NOLOCK) ON BU.BU_ID=SU.BU_ID 
			INNER JOIN UDV_CustDetailsForBillGen CD (NOLOCK) ON CD.BU_ID=BU.BU_ID AND CD.CycleId=C.CycleId --AND CD.CycleId=@CycleId 
		WHERE C.CycleId=@CycleId 		
		
		-- Filter the customer accounts
		DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50),BillingMonth INT,BillingYear INT,BillQueCustomerId INT,BillingQueuescheduleId INT)        
		DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)        
    
     
		DELETE FROM @FilteredAccounts   
		INSERT INTO @FilteredAccounts        
		SELECT DISTINCT C.AccountNo,[Month],[Year],BillQueueCustomerId,BillingQueuescheduleId        
		FROM Tbl_BillingQueeCustomers C(NOLOCK) WHERE
		C.BillingQueuescheduleId = @BillingQueuescheduleId    
		ORDER BY AccountNo,[Month],[Year]
		
	 
 
				
		SELECT TOP(1) @Year = BillingYear,@Month = BillingMonth,@GlobalAccountNumber = AccountNo 
		FROM @FilteredAccounts ORDER BY AccountNo ASC   

		WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @GlobalAccountNumber ORDER BY AccountNo ASC))        
		BEGIN

			BEGIN TRY		    
						SELECT @ActiveStatusId = ActiveStatusId,@OutStandingAmount = ISNULL(OutStandingAmount,0)
						,@TariffId=TariffId,@CustomerTypeID=CustomerTypeID,@ClusterCategoryId=ClusterCategoryId,
						@IsEmbassyCustomer=IsEmbassyCustomer,@InititalkWh=InititalkWh
						,@ReadCodeId=ReadCodeID,@BookNo=BookNo,@MeterNumber=MeterNumber
						,@OpeningBalance=isnull(OpeningBalance,0)
						,@CustomerFullName=CustomerFullName
						,@BusinessUnitName =BusinessUnitName
						,@TariffName =TariffName
 						,@Service_HouseNo =ServiceHouseNo
						,@Service_Street =ServiceStreet
						,@Service_City =ServiceCity
						,@ServiceZipCode  =ServiceZipCode
						,@Postal_HouseNo =ServiceHouseNo
						,@Postal_Street =Postal_StreetName
						,@Postal_City  =Postal_City
						,@Postal_ZipCode  =Postal_ZipCode
						,@Postal_LandMark =Postal_LandMark
						,@Service_LandMark =Service_LandMark
						,@OldAccountNumber=OldAccountNo
						,@SetupDate=SetupDate
						,@ConnectionDate=ConnectionDate 
						
						FROM @CustomerMaster WHERE GlobalAccountNumber = @GlobalAccountNumber 
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------


						IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
						BEGIN
							
							 
							SELECT @OutStandingAmount=ISNULL(NetArrears,0)+ISNULL(AdjustmentAmmount,0)
							,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
							FROM Tbl_CustomerBills(NOLOCK)  
							WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
							DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId


							DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
							SELECT @PaidAmount=SUM(PaidAmount)
							FROM Tbl_CustomerBillPayments(NOLOCK)  
							WHERE BillNo=@CusotmerPreviousBillNo

							IF @PaidAmount IS NOT NULL
							BEGIN
								SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
								UPDATE Tbl_CustomerBillPayments
								SET BillNo=NULL
								WHERE BillNo=@RegenCustomerBillId
							END

							----------------------Update Readings as is billed =0 ----------------------------
							UPDATE Tbl_CustomerReadings 
							SET IsBilled =0 
								,BillNo=NULL
							WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							-- Insert Paid Meter Payments
							Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
							Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) from Tbl_PaidMeterPaymentDetails 
							WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 
								    
							update	Tbl_PaidMeterDetails
							SET OutStandingAmount=isnull(OutStandingAmount,0) +	
							ISNULL(@PreviousPaidMeterDeductedAmount,0)
							WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1
							
							DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
							
							DELETE FROM Tbl_CustomerBillFixedCharges WHERE CustomerBillId = @RegenCustomerBillId
					
							DELETE FROM Tbl_CustomerBillPDFFiles WHERE CustomerBillId = @RegenCustomerBillId

							SET @PreviousPaidMeterDeductedAmount=NULL
							
							update CUSTOMERS.Tbl_CustomerActiveDetails
							SET OutStandingAmount=@OutStandingAmount
							where GlobalAccountNumber=@GlobalAccountNumber
							----------------------------------------------------------------------------------

							--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
							DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
							-------------------------------------------------------------------------------------
 					END
					
					  ;WITH CTE (PreviousReading,PresentReading,GlobalAccountNumber,DuplicateCount)
						AS
						(
						SELECT PreviousReading,PresentReading,GlobalAccountNumber,
						ROW_NUMBER() OVER(PARTITION BY PreviousReading,PresentReading,GlobalAccountNumber 
						ORDER BY GlobalAccountNumber) AS DuplicateCount
						FROM Tbl_CustomerReadings
						where GlobalAccountNumber=@GlobalAccountNumber
						and IsBilled=0
						)
						DELETE 
						FROM CTE
						WHERE DuplicateCount > 1	 
						
						
							select   @IspartialClose=IsPartialBill,
									 @BookDisableType=DisableTypeId,
									 @DisableDate=DisableDate
							from Tbl_BillingDisabledBooks	  
							where BookNo=@BookNo 	and IsActive=1

						   IF	isnull(@ActiveStatusId,0) =3 	  or	 isnull(@ActiveStatusId,0) =4   or isnull(@CustomerTypeID,0) = 3  
						   BEGIN

						   GOTO Loop_End;
						   END    

							IF   isnull(@BookDisableType,0)=1	 
							IF isnull(@IspartialClose,0)<>1
								   GOTO Loop_End;
					
					DECLARE @IsInActiveCustomer BIT = 0
					
					IF(isnull(@ActiveStatusId,0) =2 or isnull(@BookDisableType,0)=2)
									BEGIN
							SET @IsInActiveCustomer = 1
						END

					IF @ReadCodeId=2 
						BEGIN
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @PreviousReading=(CASE WHEN @IsInActiveCustomer = 0 THEN PreviousReading ELSE NULL END),
								@CurrentReading = (CASE WHEN @IsInActiveCustomer = 0 THEN PresentReading ELSE NULL END),
								@Usage=(CASE WHEN @IsInActiveCustomer = 0 THEN Usage ELSE NULL END),
										@LastBillGenerated=LastBillGenerated,
										@PreviousBalance =Previousbalance,
										@BalanceUnits=BalanceUsage,
										@IsFromReading=IsFromReading,
										@PrevCustomerBillId=CustomerBillId,
										@PreviousBalance=PreviousBalance,
										@Multiplier=Multiplier,
										@ReadDate=ReadDate
								FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
						--END

								IF @IsFromReading=1
								BEGIN
										SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
							
							IF @Usage < ISNULL(@BalanceUnits,0)
										BEGIN
											SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
												+convert(varchar(100),@BalanceUnits)+' , @Usage :'
												+convert(varchar(100),@Usage)
											SET @ActualUsage=@Usage
											SET @Usage=0
									SET @RemaningBalanceUsage=ISNULL(@BalanceUnits,0)-@Usage
											SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
											SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
										END
										ELSE
										BEGIN
											SET @ActualUsage=@Usage
									SET @Usage=@Usage-ISNULL(@BalanceUnits,0)
											SET @RemaningBalanceUsage=0
											SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
												+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
												+ convert(varchar(100),@RemaningBalanceUsage)
										END
								END
								ELSE
								BEGIN
										SET @ReadType=5 -- As per the master table "Tbl_MReadCodes", Average billing
										SET @ActualUsage=@Usage
										SET @RemaningBalanceUsage=@Usage+@BalanceUnits
										SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
								END
							END
						ELSE  
							BEGIN
								SET @IsEstimatedRead =1

								SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate
										,@PrevCustomerBillId=CustomerBillId, 
											  @PreviousBalance=PreviousBalance
								FROM Tbl_CustomerBills (NOLOCK)      
								WHERE AccountNo = @GlobalAccountNumber       
								ORDER BY CustomerBillId DESC 
								
								IF @PreviousBalance IS NULL
								BEGIN
									SET @PreviousBalance=@OpeningBalance
								END

					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
								SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
							
								IF(@IsInActiveCustomer = 1)
									BEGIN
										SET @Usage = 0
									END
								
							DECLARE @BillingRule INT
							
							SELECT @BillingRule=BillingRule FROM tbl_EstimationSettings(NOLOCK) WHERE BillingYear=@Year AND BillingMonth=@Month 
									AND CycleId=@CycleId AND ClusterCategoryId=@ClusterCategoryId AND ActiveStatusID=1 AND TariffId=@TariffId
									
							IF(@BillingRule = 1)
								BEGIN
									SET @ReadType=3 -- Estimate customer
								END
							ELSE
								BEGIN
								SET @ReadType=1 -- Direct
								END
					--	END
					--ELSE
					--	BEGIN
					--		SET @Usage = 0
					--		SET @ReadType=1 -- Direct
					--	END
								SET @ActualUsage=@Usage
							END

						IF @Usage<>0
							SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
						ELSE
							SET @EnergyCharges=0
						 
						SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 

						DECLARE @tblFixedCharges AS BillFixedCharges 
						DECLARE @tblUpdatedFixedCharges AS BillFixedCharges 
						------------------------------------------------------------------------------------ 
			
						IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
							BEGIN
								SET @FixedCharges=0
							END
						ELSE 
							BEGIN
										IF @BookDisableType = 2 or isnull(@ActiveStatusId,0) =2 -- Temparary Close
										BEGIN
											SET	 @EnergyCharges=0
						END	
											
										 SET @IsFirstmonthBill=  (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)
						
					INSERT INTO @tblFixedCharges(ChargeId ,Amount)
					SELECT ClassID,Amount 
					FROM dbo.fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@ConnectionDate);
					
										 SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
									
 							 END
						------------------------------------------------------------------------------------------------------------------------------------
					SELECT @GetPaidMeterBalance=ISNULL(dbo.fn_GetPaidMeterCustomer_Balance_New(@GlobalAccountNumber),0)
			
						IF @GetPaidMeterBalance>0
						BEGIN
							IF @GetPaidMeterBalance<=@FixedCharges
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=0
								SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
								SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
							END
							ELSE
							BEGIN
								SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
								SET @PaidMeterDeductedAmount=@FixedCharges
								SET @FixedCharges=0
							END
						
						END
				
					INSERT INTO @tblUpdatedFixedCharges(ChargeId,Amount)
					SELECT ChargeId,Amount 
					FROM dbo.[fn_GetCustomerBillFixedCharges](@tblFixedCharges,@GetPaidMeterBalance)
						------------------------
						-- Caluclate tax here
					
					SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE 5 END) 
					SET @TaxValue = @TaxPercentage * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )
 
							IF @PrevCustomerBillId IS NULL 
							BEGIN
							 SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) FROM Tbl_BillAdjustments 
							 WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
										 
							END
							ELSE
							BEGIN
								SELECT @AdjustmentAmount = SUM(TotalAmountEffected) FROM Tbl_BillAdjustments 
								WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
							
							END
						
						IF @AdjustmentAmount IS NULL
							SET @AdjustmentAmount=0
						
						set @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
						
						--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)
						
						SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
						
						SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
						SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)
						
						
						
						SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
							FROM Tbl_CustomerBills (NOLOCK)      
							WHERE AccountNo = @GlobalAccountNumber
							Group BY CustomerBillId       
							ORDER BY CustomerBillId DESC 
						 
						if @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
							SET @AverageUsageForNewBill=@Usage
						
						if @RemainingBalanceUnits IS NULL
						 set @RemainingBalanceUnits=0
						 
						 -------------------------------------------------------------------------------------------------------
						SET    @TariffCharges= (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))

						FROM Tbl_LEnergyClassCharges 
						WHERE ClassID =@TariffId
						AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
						FOR XML PATH(''), TYPE)
						.value('.','NVARCHAR(MAX)'),1,0,'')  ) 
						 
						-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------
						
						 select @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
						-----------------------------------------------------------------------------------------------------------------------
						--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------
						--- Need to verify all fields before insert
						
						SET  @BillGeneratedBY =( select  top 1 CreatedBy from Tbl_BillingQueueSchedule where BillingQueueScheduleId=@BillingQueueScheduleId)
						INSERT INTO Tbl_CustomerBills
						(
							[AccountNo]   --@GlobalAccountNumber     
							,[TotalBillAmount] --@TotalBillAmountWithTax      
							,[ServiceAddress] --@EnergyCharges 
							,[MeterNo]   -- @MeterNumber    
							,[Dials]     --   
							,[NetArrears] --   @NetArrears    
							,[NetEnergyCharges] --  @EnergyCharges     
							,[NetFixedCharges]   --@FixedCharges     
							,[VAT]  --     @TaxValue 
							,[VATPercentage]  --  @TaxPercentage    
							,[Messages]  --      
							,[BU_ID]  --      
							,[SU_ID]  --    
							,[ServiceCenterId]  
							,[PoleId] --       
							,[BillGeneratedBy] --       
							,[BillGeneratedDate]        
							,PaymentLastDate        --
							,[TariffId]  -- @TariffId     
							,[BillYear]    --@Year    
							,[BillMonth]   --@Month     
							,[CycleId]   -- @CycleId
							,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears
							,[ActiveStatusId]--        
							,[CreatedDate]--GETDATE()        
							,[CreatedBy]        
							,[ModifedBy]        
							,[ModifiedDate]        
							,[BillNo]  --      
							,PaymentStatusID        
							,[PreviousReading]  --@PreviousReading      
							,[PresentReading]   --  @CurrentReading   
							,[Usage]     --@Usage   
							,[AverageReading] -- @AverageUsageForNewBill      
							,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax      
							,[EstimatedUsage] --@Usage       
							,[ReadCodeId]  --  @ReadType    
							,[ReadType]  --  @ReadType
							,AdjustmentAmmount -- @AdjustmentAmount  
							,BalanceUsage    --@RemaningBalanceUsage
							,BillingTypeId -- 
							,ActualUsage
							,LastPaymentTotal	 --	  @PrevBillTotalPaidAmount
							,PreviousBalance--@PreviousBalance
							,TariffRates
						)        
						SELECT GlobalAccountNumber
							,@TotalBillAmount   
							,dbo.fn_GetCustomerServiceAddress(C.GlobalAccountNumber) AS ServiceDetails    
							,@MeterNumber    
							,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = C.MeterNumber) AS MeterDials      
							,@NetArrears       
							,@EnergyCharges 
							,@FixedCharges
							,@TaxValue 
							,@TaxPercentage        
							,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(BU_ID))        
							,BU_ID
							,SU_ID
							,ServiceCenterId
							,PoleId        
							,@BillGeneratedBY        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,
							(
								SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P 
								WHERE P.AccountNo = C.GlobalAccountNumber 
								ORDER BY RecievedDate DESC
							)         
							,TariffId
							,@Year
							,@Month
							,@CycleId
							,@TotalBillAmountWithArrears 
							,1 --ActiveStatusId        
							,(SELECT dbo.fn_GetCurrentDateTime())        
							,@BillGeneratedBY        
							,NULL --ModifedBy
							,NULL --ModifedDate
							,(SELECT dbo.fn_GenerateBillNo(2,C.BookNo,@Month,@Year))  --BillNo      
							,2 -- PaymentStatusID   
							,@PreviousReading        
							,@CurrentReading        
							,@Usage        
							,ISNULL(@AverageUsageForNewBill,0)
							,@TotalBillAmountWithTax             
							,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)         
							,@ReadType
							,@ReadType       
							,@AdjustmentAmount    
							,@RemaningBalanceUsage   
							,2 -- BillingTypeId 
							,@ActualUsage
							,@PrevBillTotalPaidAmount
							,case when (select COUNT(0) from Tbl_CustomerBills where AccountNo=@GlobalAccountNumber)>0  THEN @OutStandingAmount  else @OpeningBalance END
							,@TariffCharges
						FROM @CustomerMaster C        
						WHERE GlobalAccountNumber = @GlobalAccountNumber  
						
						set @CusotmerNewBillID = SCOPE_IDENTITY() 
						----------------------- Update Customer Outstanding ------------------------------

						-- Insert Paid Meter Payments
						  IF isnull(@PaidMeterDeductedAmount,0) >0
						   BEGIN
						   
						   INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
						   SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        
						   
						   update Tbl_PaidMeterDetails
						   SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
						   where AccountNo=@GlobalAccountNumber
						   
						   END

						update CUSTOMERS.Tbl_CustomerActiveDetails
						SET OutStandingAmount=@TotalBillAmountWithArrears
						where GlobalAccountNumber=@GlobalAccountNumber
						----------------------------------------------------------------------------------
						----------------------Update Readings as is billed =1 ----------------------------

					IF(@IsInActiveCustomer = 0)
						BEGIN
							UPDATE Tbl_CustomerReadings 
							SET IsBilled = 1 
								,BillNo=@CusotmerNewBillID
							WHERE GlobalAccountNumber=@GlobalAccountNumber
								AND IsBilled = 0
						END
			 
						--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
						
						INSERT INTO TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
						SELECT @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ChargeId 
						FROM  @tblUpdatedFixedCharges  
						
						INSERT INTO Tbl_CustomerBillFixedCharges
						(
							 CustomerBillId 
							,FixedChargeId 
							,Amount 
							,BillMonth 
							,BillYear 
							,CreatedDate 
						)
						SELECT
							 @CusotmerNewBillID
							,ChargeId
							,Amount
							,@Month
							,@Year
							,(SELECT dbo.fn_GetCurrentDateTime())  
						FROM @tblUpdatedFixedCharges  

						DELETE FROM @tblFixedCharges  
						DELETE FROM @tblUpdatedFixedCharges

						------------------------------------------------------------------------------------
						--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------
						Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1     
						WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId 
						
						---------------------------------------------------------------------------------------
						Update Tbl_BillAdjustments SET EffectedBillId=@CusotmerNewBillID  WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId    

						--------------Save Bill Deails of customer name,BU-----------------------------------------------------
						INSERT INTO Tbl_BillDetails
									(CustomerBillID,
									 BusinessUnitName,
									 CustomerFullName,
									 Multiplier,
									 Postal_City,
									 Postal_HouseNo,
									 Postal_Street,
									 Postal_ZipCode,
									 ReadDate,
									 ServiceZipCode,
									 Service_City,
									 Service_HouseNo,
									 Service_Street,
									 TariffName,
									 Postal_LandMark,
									 Service_Landmark,
									 OldAccountNumber)
								VALUES
									(@CusotmerNewBillID,
									@BusinessUnitName,
									@CustomerFullName,
									@Multiplier,
									@Postal_City,
									@Postal_HouseNo,
									@Postal_Street,
									@Postal_ZipCode,
									@ReadDate,
									@ServiceZipCode,
									@Service_City,
									@Service_HouseNo,
									@Service_Street,
									@TariffName,
									@Postal_LandMark,
									@Service_LandMark,
									@OldAccountNumber)
						---------------------------------------------------Set Variables to NULL-
						SET @TotalAmount = NULL
						SET @CusotmerNewBillID=NULL
						SET @CusotmerNewBillID=NULL
						SET @EnergyCharges = NULL
						SET @FixedCharges = NULL
						SET @TaxValue = NULL
						SET @PreviousReading  = NULL      
						SET @CurrentReading   = NULL     
						SET @Usage   = NULL
						SET @ReadType =NULL
						SET @BillType   = NULL     
						SET @AdjustmentAmount    = NULL
						SET @RemainingBalanceUnits   = NULL 
						SET @TotalBillAmountWithArrears=NULL
						SET @BookNo=NULL
						SET @AverageUsageForNewBill=NULL	
						SET @IsHaveLatest=0
						SET @ActualUsage=NULL
						SET @RegenCustomerBillId =NULL
						SET @PaidAmount  =NULL
						SET @PrevBillTotalPaidAmount =NULL
						SET @PreviousBalance =NULL
						SET @OpeningBalance =NULL
						SET @CustomerFullName  =NULL
						SET @BusinessUnitName  =NULL
						SET @TariffName  =NULL
						SET @ReadDate  =NULL
						SET @Multiplier  =NULL
						SET @Service_HouseNo  =NULL
						SET @Service_Street  =NULL
						SET @Service_City  =NULL
						SET @ServiceZipCode =NULL
						SET @Postal_HouseNo  =NULL
						SET @Postal_Street =NULL
						SET @Postal_City  =NULL
						SET @Postal_ZipCode  =NULL
						SET @Postal_LandMark =NULL
						SET	@Service_LandMark =NULL
						SET @OldAccountNumber=NULL
						SET @IspartialClose=NULL
						SET @BookDisableType =NULL
						SET @TariffCharges=NULL
						SET @CusotmerPreviousBillNo=NULL
						SET @PrevCustomerBillId=NULL
						SET @DisableDate=NULL
						SET @BillingComments=NULL
						SET @TotalBillAmount=NULL
						SET @PaidMeterDeductedAmount=NULL
						SET @IsEmbassyCustomer=NULL
						SET @TaxPercentage=NULL
						SET  @IsFirstmonthBill =NULL
						SET  @SetupDate =NULL
						SET @PaidMeterDeductedAmount=NULL
   			 	-----------------------------------------------------------------------------------------
						
						--While Loop continue checking
						IF(@GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))        
							BREAK;        
						ELSE        
						BEGIN     
						  
						   Loop_End:
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue;        
						END
			END TRY
			BEGIN CATCH
			 
						   SET @GlobalAccountNumber = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @GlobalAccountNumber ORDER BY AccountNo ASC)        
						  
						   IF(@GlobalAccountNumber IS NULL) break;        
						   Continue      

			END CATCH
		END

		Update Tbl_BillingQueueSchedule SET BillGenarationStatusId = 1     
		WHERE CycleId = @CycleId  AND BillingMonth=@Month AND BillingYear=@Year 
		
		   
	END
 
END

-----------------------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GenerateBillForCustomer]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================  
-- Author:  <NEERAJ KANOJIYA>  
-- Create date: <26-MAR-2015>  
-- Description: <This procedure is a copy of base proc of bill gen. This proc is for individual customer bill generation.>  
-- =============================================  
--Exec USP_BillGenaraton  
ALTER PROCEDURE [dbo].[USP_GenerateBillForCustomer]  
( 
	@XmlDoc Xml
)  
AS  
  BEGIN  
    
 DECLARE @GlobalAccountNumber  VARCHAR(50)       
	,@Month INT          
	,@Year INT           
	,@Date DATETIME           
	,@MonthStartDate DATE       
	,@PreviousReading VARCHAR(50)          
	,@CurrentReading VARCHAR(50)          
	,@Usage DECIMAL(20)          
	,@RemaningBalanceUsage INT          
	,@TotalAmount DECIMAL(18,2)= 0          
	,@TaxValue DECIMAL(18,2)          
	,@BillingQueuescheduleId INT          
	,@PresentCharge INT          
	,@FromUnit INT          
	,@ToUnit INT          
	,@Amount DECIMAL(18,2)      
	,@TaxId INT          
	,@CustomerBillId INT = 0          
	,@BillGeneratedBY VARCHAR(50)          
	,@LastDateOfBill DATETIME          
	,@IsEstimatedRead BIT = 0          
	,@ReadCodeId INT          
	,@BillType INT          
	,@LastBillGenerated DATETIME        
	,@FeederId VARCHAR(50)        
	,@CycleId VARCHAR(MAX)     -- CycleId   
	,@BillNo VARCHAR(50)      
	,@PrevCustomerBillId INT      
	,@AdjustmentAmount DECIMAL(18,2)      
	,@PreviousDueAmount DECIMAL(18,2)      
	,@CustomerTariffId VARCHAR(50)      
	,@BalanceUnits INT      
	,@RemainingBalanceUnits INT      
	,@IsHaveLatest BIT = 0      
	,@ActiveStatusId INT      
	,@IsDisabled BIT      
	,@BookDisableType INT      
	,@IsPartialBill BIT      
	,@OutStandingAmount DECIMAL(18,2)      
	,@IsActive BIT      
	,@NoFixed BIT = 0      
	,@NoBill BIT = 0       
	,@ClusterCategoryId INT=NULL    
	,@StatusText VARCHAR(50)      
	,@BU_ID VARCHAR(50)    
	,@BillingQueueScheduleIdList VARCHAR(MAX)    
	,@RowsEffected INT  
	,@IsFromReading BIT  
	,@TariffId INT  
	,@EnergyCharges DECIMAL(18,2)   
	,@FixedCharges  DECIMAL(18,2)  
	,@CustomerTypeID INT  
	,@ReadType Int  
	,@TaxPercentage DECIMAL(18,2)=5    
	,@TotalBillAmountWithTax  DECIMAL(18,2)  
	,@AverageUsageForNewBill  DECIMAL(18,2)  
	,@IsEmbassyCustomer INT  
	,@TotalBillAmountWithArrears  DECIMAL(18,2)   
	,@CusotmerNewBillID INT  
	,@InititalkWh INT  
	,@NetArrears DECIMAL(18,2)  
	,@BookNo VARCHAR(30)  
	,@GetPaidMeterBalance DECIMAL(18,2)  
	,@GetPaidMeterBalanceAfterBill DECIMAL(18,2)  
	,@MeterNumber VARCHAR(50)  
	,@ActualUsage DECIMAL(18,2)  
	,@RegenCustomerBillId INT  
	,@PaidAmount  DECIMAL(18,2)  
	,@PrevBillTotalPaidAmount Decimal(18,2)  
	,@PreviousBalance Decimal(18,2)  
	,@OpeningBalance Decimal(18,2)  
	,@CustomerFullName VARCHAR(MAX)  
	,@BusinessUnitName VARCHAR(100)  
	,@TariffName VARCHAR(50)  
	,@ReadDate DateTime  
	,@Multiplier INT  
	,@Service_HouseNo VARCHAR(100)  
	,@Service_Street VARCHAR(100)  
	,@Service_City VARCHAR(100)  
	,@ServiceZipCode VARCHAR(100)  
	,@Postal_HouseNo VARCHAR(100)  
	,@Postal_Street VARCHAR(100)  
	,@Postal_City VARCHAR(100)  
	,@Postal_ZipCode VARCHAR(100)  
	,@Postal_LandMark VARCHAR(100)  
	,@Service_LandMark VARCHAR(100)  
	,@OldAccountNumber VARCHAR(100)  
	,@ServiceUnitId VARCHAR(50)  
	,@PoleId Varchar(50)  
	,@ServiceCenterId VARCHAR(50)  
	,@IspartialClose INT  
	,@TariffCharges varchar(max)  
	,@CusotmerPreviousBillNo varchar(50)  
	,@DisableDate DATETIME 
	,@BillingComments Varchar(max) 
	,@TotalBillAmount decimal(18,2)
	,@TotalPaidAmount DECIMAL(18,2)
	,@PaidMeterDeductedAmount DECIMAL(18,2)
	,@AdjustmentAmountEffected DECIMAL(18,2)
	,@IsFirstmonthBill Bit
	,@SetupDate DATETIME
	,@ConnectionDate DATETIME
	,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()
	,@IsFirstRptTblData BIT=0   
  
	DECLARE @GeneratedCustomerBills TABLE(CustomerBillID INT)          
   
	SELECT @GlobalAccountNumber = C.value('(AccountNo)[1]','VARCHAR(50)')   
		,@Month = C.value('(BillMonth)[1]','VARCHAR(50)')   
		,@Year = C.value('(BillYear)[1]','VARCHAR(50)')   
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)         
      
	SELECT @TotalPaidAmount = ISNULL(SUM(CBP.PaidAmount),0) 
	FROM Tbl_CustomerBills CB 
	JOIN Tbl_CustomerBillPayments CBP ON CB.BillNo = CBP.BillNo
		AND BillMonth=@Month AND BillYear = @Year AND AccountNo=@GlobalAccountNumber
	 
	SELECT @AdjustmentAmountEffected=ISNULL(SUM(BA.AmountEffected),0)
	FROM Tbl_CustomerBills AS CB
	JOIN Tbl_BillAdjustments AS BA ON CB.CustomerBillId=BA.CustomerBillId
		AND CB.AccountNo=@GlobalAccountNumber AND CB.BillMonth=@Month AND CB.BillYear=@Year
	JOIN Tbl_BillAdjustmentDetails BID ON BA.BillAdjustmentId =BID.BillAdjustmentId
       
	IF(@GlobalAccountNumber !='' AND @TotalPaidAmount <= 0 AND @AdjustmentAmountEffected <= 0)  
		BEGIN     
			SELECT   
				 @ActiveStatusId = ActiveStatusId 
				,@OutStandingAmount = ISNULL(OutStandingAmount,0)  
				,@TariffId=TariffId
				,@CustomerTypeID=CustomerTypeID
				,@ClusterCategoryId=ClusterCategoryId 
				,@IsEmbassyCustomer=IsEmbassyCustomer
				,@InititalkWh=InitialBillingKWh  
				,@ReadCodeId=ReadCodeID
				,@BookNo=BookNo
				,@MeterNumber=MeterNumber  
				,@OpeningBalance=isnull(OpeningBalance,0)  
				,@CustomerFullName=dbo.fn_GetCustomerFullName_New(Title,FirstName,MiddleName,LastName)  
				,@BusinessUnitName =BusinessUnitName  
				,@TariffName =ClassName  
				,@Service_HouseNo =Service_HouseNo  
				,@Service_Street =Service_StreetName  
				,@Service_City =Service_City  
				,@ServiceZipCode  =Service_ZipCode  
				,@Postal_HouseNo =Postal_HouseNo  
				,@Postal_Street =Postal_StreetName  
				,@Postal_City  =Postal_City  
				,@Postal_ZipCode  =Postal_ZipCode  
				,@Postal_LandMark =Postal_LandMark  
				,@Service_LandMark =Service_LandMark  
				,@OldAccountNumber=OldAccountNo  
				,@BU_ID=BU_ID  
				,@ServiceUnitId=SU_ID  
				,@PoleId=PoleID  
				,@TariffId=ClassID  
				,@ServiceCenterId=ServiceCenterId  
				,@CycleId=CycleId
				,@SetupDate=SetupDate
				,@ConnectionDate=ConnectionDate  
			FROM UDV_CustDetailsForBillGen WHERE GlobalAccountNumber = @GlobalAccountNumber   

			----------------------------------------COPY START --------------------------------------------------------   
			--SELECT @ServiceCenterId AS ServiceCenterId
			--SELECT TOP 1 SC_ID FROM Tbl_ReportBillingStatisticsBySCId WHERE SC_ID=@ServiceCenterId
				IF EXISTS(SELECT TOP 1 SC_ID FROM Tbl_ReportBillingStatisticsBySCId WHERE SC_ID=@ServiceCenterId)
					BEGIN
					--PRINT 'SC'
							IF EXISTS(SELECT TOP 1 CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
							BEGIN
								--PRINT 'RPT'
								------------------------------------------6-Aug-2015 (ID-077) starts--------------------------------------------
								--Assigning variable befor Bill gen
								--------------------------------------------------------------------------------------
								DECLARE  @BillMonthList VARCHAR(MAX)
								,@BillYearList VARCHAR(MAX)
								,@LastMonth INT
								,@LastYear INT
								,@CreatedBy VARCHAR(50)
								,@BU_ID_RPT VARCHAR(50)
								,@BusinessUnitName_RPT VARCHAR(50)
								,@SU_ID_RPT VARCHAR(50)
								,@ServiceUnitName_RPT VARCHAR(50)
								,@ServiceCenterId_RPT VARCHAR(50)
								,@ServiceCenterName_RPT VARCHAR(50)
								,@TariffId_RPT INT
								,@ClassName_RPT VARCHAR(50)
								,@CustomerTypeId_RPT INT
								,@CustomerType_RPT VARCHAR(50)
								,@BillYear_RPT INT
								,@BillMonth_RPT INT
								,@AccountNo_RPT VARCHAR(50)
								,@ReadCodeID_RPT INT
								,@Usage_RPT decimal(18,2)
								,@NetFixedCharges_RPT decimal(18,2)
								,@NetEnergyCharges_RPT decimal(18,2)
								,@VATPercentage_RPT decimal(18,2)
								,@NetArrears_RPT decimal(18,2)
								,@VAT_RPT decimal(18,2)
								,@TotalBillAmount_RPT decimal(18,2)
								,@TotalBillAmountWithTax_RPT decimal(18,2)
								,@TotalBillAmountWithArrears_RPT decimal(18,2)
								,@ReadType_RPT INT
								,@PaidAmount_RPT decimal(18,2)
								,@OutStandingAmount_RPT decimal(18,2)
								,@ClosingBalance_RPT decimal(18,2)
								,@BillNo_RPT VARCHAR(50)
								,@NoOfStubs_RPT VARCHAR(50)
								,@GlobalAccountNumber_RPT VARCHAR(50)
								SELECT TOP 1
									 @BU_ID_RPT = CB.BU_ID
									,@BusinessUnitName_RPT=BU.BusinessUnitName
									,@SU_ID_RPT=CB.SU_ID
									,@ServiceUnitName_RPT=SU.ServiceUnitName
									,@ServiceCenterId_RPT=CB.ServiceCenterId
									,@ServiceCenterName_RPT=SC.ServiceCenterName
									,@TariffId_RPT=CB.TariffId
									,@ClassName_RPT=TC.ClassName
									,@CustomerTypeId_RPT=CPD.CustomerTypeId
									,@CustomerType_RPT=CT.CustomerType
									,@BillYear_RPT=CB.BillYear
									,@BillMonth_RPT=CB.BillMonth
									,@AccountNo_RPT=CB.AccountNo
									,@ReadCodeID_RPT=CPD.ReadCodeID
									,@Usage_RPT=CB.Usage
									,@NetFixedCharges_RPT=CB.NetFixedCharges
									,@NetEnergyCharges_RPT=CB.NetEnergyCharges
									,@VATPercentage_RPT=CB.VATPercentage
									,@NetArrears_RPT=CB.NetArrears
									,@VAT_RPT=CB.VAT
									,@TotalBillAmount_RPT=CB.TotalBillAmount
									,@TotalBillAmountWithTax_RPT=CB.TotalBillAmountWithTax
									,@TotalBillAmountWithArrears_RPT=CB.TotalBillAmountWithArrears
									,@ReadType_RPT=CB.ReadType
									,@PaidAmount_RPT=CB.PaidAmount
									,@OutStandingAmount_RPT=CAD.OutStandingAmount
									,@TotalBillAmountWithTax_RPT =CB.TotalBillAmountWithTax 
									,@BillNo_RPT=CB.BillNo
									,@NoOfStubs_RPT=(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) 
									FROM Tbl_CustomerBills CB 
									INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
									INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
									INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
									INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
									INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
									INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
									INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
									INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
									INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
									WHERE	BillMonth = @Month 
									AND		BillYear = @Year
									AND		CB.AccountNo=@GlobalAccountNumber
								--select @ServiceCenterId_RPT as ServiceCenterId_RPT
								--select @ServiceCenterId as ServiceCenterId
								--------------------------------------------------------------------------------------
								--DEBIT AMOUNTS
								--------------------------------------------------------------------------------------
								--set @ServiceCenterId_RPT = @ServiceCenterId
								
								--SELECT @NetFixedCharges_RPT,@TotalBillAmount_RPT,@NetEnergyCharges_RPT,@TotalBillAmountWithTax_RPT
								--		,@TotalBillAmountWithTax_RPT, @ClosingBalance_RPT,@ServiceCenterId
										
								--PRINT '1'
									UPDATE Tbl_ReportBillingStatisticsBySCId
									SET	FixedCharges =FixedCharges-@NetFixedCharges_RPT
										,TotalAmountBilled =TotalAmountBilled-@TotalBillAmount_RPT			
										,EnergyBilled=EnergyBilled-@NetEnergyCharges_RPT
										,RevenueBilled=RevenueBilled-@TotalBillAmountWithTax_RPT
										,OpeningBalance=OpeningBalance-@TotalBillAmountWithTax_RPT
										,ClosingBalance=ClosingBalance-@ClosingBalance_RPT
									WHERE SC_ID=@ServiceCenterId	
							
									--PRINT '2'
									UPDATE Tbl_ReportBillingStatisticsByTariffId
									SET	FixedCharges =FixedCharges-@NetFixedCharges_RPT
										,TotalAmountBilled =TotalAmountBilled-@TotalBillAmount_RPT					
										,EnergyBilled=EnergyBilled-@NetEnergyCharges_RPT
										,RevenueBilled=RevenueBilled-@TotalBillAmountWithTax_RPT
										,OpeningBalance=OpeningBalance-@TotalBillAmountWithTax_RPT
										,ClosingBalance=ClosingBalance-@ClosingBalance_RPT
								WHERE TariffId=@TariffId_RPT	
								------------------------------------------6-Aug-2015 (ID-077) ends--------------------------------------------
						END
					END
				ELSE
					SET @IsFirstRptTblData=1
		
			--------------------------------------------------------COPY CUSTUMER WISE-------------------------------------------------

			IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills(NOLOCK) WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month)
				BEGIN

					SELECT @OutStandingAmount=ISNULL(NetArrears,0)--+ISNULL(AdjustmentAmmount,0)
						,@RegenCustomerBillId=CustomerBillId,@CusotmerPreviousBillNo=BillNo	
					FROM Tbl_CustomerBills(NOLOCK)  
					WHERE AccountNo = @GlobalAccountNumber AND BillYear=@Year AND BillMonth=@Month
					
					DELETE FROM Tbl_CustomerBills WHERE  CustomerBillId=@RegenCustomerBillId

					DELETE FROM Tbl_BillDetails where  CustomerBillID= @RegenCustomerBillId
					
					--SELECT @PaidAmount=SUM(PaidAmount)
					--FROM Tbl_CustomerBillPayments(NOLOCK)  
					--WHERE BillNo=@CusotmerPreviousBillNo

					--IF @PaidAmount IS NOT NULL
					--	BEGIN
					--		SET @OutStandingAmount=@OutStandingAmount+@PaidAmount
							
					--		UPDATE Tbl_CustomerBillPayments
					--		SET BillNo=NULL
					--		WHERE BillNo=@RegenCustomerBillId
					--	END

					----------------------Update Readings as is billed =0 ----------------------------
					UPDATE Tbl_CustomerReadings 
					SET IsBilled =0 
						,BillNo=NULL
					WHERE GlobalAccountNumber=@GlobalAccountNumber AND BillNo=@RegenCustomerBillId
					-- Insert Paid Meter Payments

					Declare @PreviousPaidMeterDeductedAmount decimal(18,2)
					
					Select  @PreviousPaidMeterDeductedAmount=isnull(Amount,0) 
					from Tbl_PaidMeterPaymentDetails 
					WHERE AccountNo=@GlobalAccountNumber AND isnull(BillNo,0)=@RegenCustomerBillId 

					update	Tbl_PaidMeterDetails
					SET OutStandingAmount=isnull(OutStandingAmount,0) +	ISNULL(@PreviousPaidMeterDeductedAmount,0)
					WHERE AccountNo=@GlobalAccountNumber  and ActiveStatusId=1

					DELETE FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo=@GlobalAccountNumber AND BillNo = @RegenCustomerBillId
					
					DELETE FROM Tbl_CustomerBillFixedCharges WHERE CustomerBillId = @RegenCustomerBillId
					
					DELETE FROM Tbl_CustomerBillPDFFiles WHERE CustomerBillId = @RegenCustomerBillId

					SET @PreviousPaidMeterDeductedAmount=NULL

					update CUSTOMERS.Tbl_CustomerActiveDetails
					SET OutStandingAmount=@OutStandingAmount
					where GlobalAccountNumber=@GlobalAccountNumber
					----------------------------------------------------------------------------------

					
					--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------
					DELETE FROM TBL_Customer_Additionalcharges WHERE AccountNo=@GlobalAccountNumber AND CustomerBillId=@RegenCustomerBillId
					-------------------------------------------------------------------------------------
				END
			

			select  @IspartialClose=IsPartialBill,
				@BookDisableType=DisableTypeId,
				@DisableDate=DisableDate
			from Tbl_BillingDisabledBooks	  
			where BookNo=@BookNo and IsActive=1

			IF	isnull(@ActiveStatusId,0) =3 or	isnull(@ActiveStatusId,0) =4 or isnull(@CustomerTypeID,0) = 3  
				BEGIN
					Return;
				END    

			IF isnull(@BookDisableType,0)=1	 
				IF isnull(@IspartialClose,0)<>1
					Return;
					
			DECLARE @IsInActiveCustomer BIT = 0
			
			IF(isnull(@ActiveStatusId,0) =2 or isnull(@BookDisableType,0)=2)
				BEGIN
					SET @IsInActiveCustomer = 1
				END

			IF @ReadCodeId=2 
				BEGIN
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @PreviousReading=(CASE WHEN @IsInActiveCustomer = 0 THEN PreviousReading ELSE NULL END),
								@CurrentReading = (CASE WHEN @IsInActiveCustomer = 0 THEN PresentReading ELSE NULL END),
								@Usage=(CASE WHEN @IsInActiveCustomer = 0 THEN Usage ELSE NULL END),
								@LastBillGenerated=LastBillGenerated,
								@PreviousBalance =Previousbalance,
								@BalanceUnits=BalanceUsage,
								@IsFromReading=IsFromReading,
								@PrevCustomerBillId=CustomerBillId,
								@PreviousBalance=PreviousBalance,
								@Multiplier=Multiplier,
								@ReadDate=ReadDate
							FROM fn_GetNormalCustomerConsumption(@GlobalAccountNumber,@OpeningBalance)
						--END

					IF @IsFromReading=1
						BEGIN
							SET @ReadType=2 -- As per the master table "Tbl_MReadCodes", Reading billing
							
							IF @Usage < ISNULL(@BalanceUnits,0)
								BEGIN
									SET @BillingComments= '\n Balance Units Greater than Usage(@Usage<@BalanceUnits) @BalanceUnits:  '
														+convert(varchar(100),@BalanceUnits)+' , @Usage :'
														+convert(varchar(100),@Usage)
									SET @ActualUsage=@Usage
									SET @Usage=0
									SET @RemaningBalanceUsage=ISNULL(@BalanceUnits,0)-@Usage
									SET @BillingComments= @BillingComments + '\n Remaining Units :'+ convert(varchar(100),@RemaningBalanceUsage)
									SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
								END
							ELSE
								BEGIN
									SET @ActualUsage=@Usage
									SET @Usage=@Usage-ISNULL(@BalanceUnits,0)
									SET @RemaningBalanceUsage=0
									SET @BillingComments='\n Having Balance Usage - Balance Units Before Deduction:'
														+ convert(varchar(100),@BalanceUnits) + ', Balance Usage After Deduction '
														+ convert(varchar(100),@RemaningBalanceUsage)														
								END
						END
					ELSE
						BEGIN
							SET @ReadType=5-- As per the master table "Tbl_MReadCodes", Average billing
							SET @ActualUsage=@Usage
							SET @RemaningBalanceUsage=@Usage+@BalanceUnits
							SET @BillingComments='\n Average Billing  Customer  Before Billing Balance Units :'
												+ convert(varchar(100),@BalanceUnits)
												+' After Bill Balance Units' + convert(varchar(100),@RemaningBalanceUsage)	
						END
				END
			ELSE  
				BEGIN
					SET @IsEstimatedRead =1

					SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate
							,@PrevCustomerBillId=CustomerBillId, 
					@PreviousBalance=PreviousBalance
					FROM Tbl_CustomerBills (NOLOCK)      
					WHERE AccountNo = @GlobalAccountNumber       
					ORDER BY CustomerBillId DESC 

					IF @PreviousBalance IS NULL
						BEGIN
							SET @PreviousBalance=@OpeningBalance
						END
					
					--IF(@IsInActiveCustomer = 0)
					--	BEGIN	
							SELECT @Usage=dbo.fn_GetDirectCustomerConsumption(@GlobalAccountNumber,@CycleId,@ClusterCategoryId,@Year,@Month,@InititalkWh,@TariffId)
							
							IF(@IsInActiveCustomer = 1)
								BEGIN
									SET @Usage = 0
								END
								
							DECLARE @BillingRule INT
							
							SELECT @BillingRule=BillingRule FROM tbl_EstimationSettings(NOLOCK) WHERE BillingYear=@Year AND BillingMonth=@Month 
									AND CycleId=@CycleId AND ClusterCategoryId=@ClusterCategoryId AND ActiveStatusID=1 AND TariffId=@TariffId
									
							IF(@BillingRule = 1)
								BEGIN
									SET @ReadType=3 -- Estimate customer
								END
							ELSE
								BEGIN
									SET @ReadType=1 -- Direct
								END
					--	END
					--ELSE
					--	BEGIN
					--		SET @Usage = 0
					--		SET @ReadType=1 -- Direct
					--	END
					SET @ActualUsage=@Usage
				END

			IF @Usage<>0
				SELECT @EnergyCharges = dbo.fn_GetEnergyChanges(@GlobalAccountNumber,@Usage,@Month,@Year,@TariffId) 
			ELSE
				SET @EnergyCharges=0

			SET @MonthStartDate = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01') 

			DECLARE @tblFixedCharges AS BillFixedCharges 
			DECLARE @tblUpdatedFixedCharges AS BillFixedCharges 
			------------------------------------------------------------------------------------ 
			
			IF  isnull(@IspartialClose,0) =   1	 --or  isnull(@ActiveStatusId,0) =2
				BEGIN
					SET @FixedCharges=0
				END
			ELSE 
				BEGIN
					IF @BookDisableType = 2  or isnull(@ActiveStatusId,0) =2-- Temparary Close
						BEGIN
							SET	@EnergyCharges=0
						END	
							
					SET @IsFirstmonthBill= (case when @PrevCustomerBillId IS NULL THEN 1 else 0 end)
						
					INSERT INTO @tblFixedCharges(ChargeId ,Amount)
					SELECT ClassID,Amount 
					FROM dbo.fn_GetFixedCharges_New(@TariffId,@MonthStartDate,@GlobalAccountNumber,@IsFirstmonthBill,@ConnectionDate);
					
					SELECT @FixedCharges=SUM(Amount) FROM @tblFixedCharges	
					
				END
			------------------------------------------------------------------------------------------------------------------------------------
			SELECT @GetPaidMeterBalance=ISNULL(dbo.fn_GetPaidMeterCustomer_Balance_New(@GlobalAccountNumber),0)
			
			IF @GetPaidMeterBalance>0
				BEGIN
					IF @GetPaidMeterBalance<=@FixedCharges
						BEGIN
							SET @GetPaidMeterBalanceAfterBill=0
							SET @FixedCharges=@FixedCharges-@GetPaidMeterBalance
							SET @ReadType=4 -- As per the master table "Tbl_MReadCodes", Minimum billing
							SET @PaidMeterDeductedAmount=@GetPaidMeterBalance
						END
					ELSE
						BEGIN
							SET @GetPaidMeterBalanceAfterBill=@GetPaidMeterBalance-@FixedCharges
							SET @PaidMeterDeductedAmount=@FixedCharges
							SET @FixedCharges=0
						END
						
				END
				
			INSERT INTO @tblUpdatedFixedCharges(ChargeId,Amount)
			SELECT ChargeId,Amount 
			FROM dbo.[fn_GetCustomerBillFixedCharges](@tblFixedCharges,@GetPaidMeterBalance)
			------------------------
			-- Caluclate tax here

			SET @TaxPercentage = (CASE WHEN isnull(@IsEmbassyCustomer,0)=1 THEN 0 ELSE 5 END) 
			SET @TaxValue = @TaxPercentage * ((ISNULL(@FixedCharges,0)+ISNULL(@EnergyCharges,0)) / 100 )

			IF	@PrevCustomerBillId IS NULL 
				BEGIN
					SELECT @AdjustmentAmount = SUM(isnull(TotalAmountEffected,0)) 
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = 0 
				END
			ELSE
				BEGIN
					SELECT @AdjustmentAmount = SUM(TotalAmountEffected) 
					FROM Tbl_BillAdjustments 
					WHERE AccountNo = @GlobalAccountNumber AND isnull(CustomerBillId,0) = @PrevCustomerBillId    
				END

			IF @AdjustmentAmount IS NULL
				SET @AdjustmentAmount=0

			SET @NetArrears=@OutStandingAmount -- Removed as it is updating from outstaning  -@AdjustmentAmount
			--SET @TotalAmount=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@OutStandingAmount,0)-ISNULL(@AdjustmentAmount,0)

			SET @TotalBillAmountWithTax=ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)+ISNULL(@TaxValue,0)
			SET @TotalBillAmount =ISNULL(@EnergyCharges,0)+ISNULL(@FixedCharges,0)
			SET @TotalBillAmountWithArrears=@TotalBillAmountWithTax+ISNULL(@OutStandingAmount,0)-- Removed as it is updating from outstaning  -ISNULL(@AdjustmentAmount,0)

			SELECT TOP(3) @AverageUsageForNewBill=AVG(Usage) 
			FROM Tbl_CustomerBills (NOLOCK)      
			WHERE AccountNo = @GlobalAccountNumber
			Group BY CustomerBillId       
			ORDER BY CustomerBillId DESC 

			IF @AverageUsageForNewBill IS NULL OR @AverageUsageForNewBill=0.00
				SET @AverageUsageForNewBill=@Usage

			IF @RemainingBalanceUnits IS NULL
				SET @RemainingBalanceUnits=0

			-------------------------------------------------------------------------------------------------------
			SET @TariffCharges = (SELECT STUFF((SELECT TOP 3  CAST(isnull(convert(varchar(50),Amount),'--') +'  '  AS VARCHAR(50))
									FROM Tbl_LEnergyClassCharges 
									WHERE ClassID =@TariffId
									AND IsActive = 1 AND CONVERT(DATE,@MonthStartDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
									FOR XML PATH(''), TYPE)
									.value('.','NVARCHAR(MAX)'),1,0,'')  ) 

			-------------------------------------Get Customer Total Payments Done For the Previous Bill----------------------------

			SELECT @PrevBillTotalPaidAmount=dbo.fn_Get_Customer_LastBill_TotalPayment(@GlobalAccountNumber,@PrevCustomerBillId,@LastBillGenerated)
			
			-----------------------------------------------------------------------------------------------------------------------

			----------------------------------------------------------COPY END-------------------------------------------------------------------  
			--- Need to verify all fields before insert  
			INSERT INTO Tbl_CustomerBills  
			(  
				 [AccountNo]   --@GlobalAccountNumber       
				,[TotalBillAmount] --@TotalBillAmountWithTax        
				,[ServiceAddress] --@EnergyCharges   
				,[MeterNo]   -- @MeterNumber      
				,[Dials]     --     
				,[NetArrears] --   @NetArrears      
				,[NetEnergyCharges] --  @EnergyCharges       
				,[NetFixedCharges]   --@FixedCharges       
				,[VAT]  --     @TaxValue   
				,[VATPercentage]  --  @TaxPercentage      
				,[Messages]  --        
				,[BU_ID]  --        
				,[SU_ID]  --      
				,[ServiceCenterId]    
				,[PoleId] --         
				,[BillGeneratedBy] --         
				,[BillGeneratedDate]          
				,PaymentLastDate        --  
				,[TariffId]  -- @TariffId       
				,[BillYear]    --@Year      
				,[BillMonth]   --@Month       
				,[CycleId]   -- @CycleId  
				,[TotalBillAmountWithArrears]   --     @TotalBillAmountWithArrears  
				,[ActiveStatusId]--          
				,[CreatedDate]--GETDATE()          
				,[CreatedBy]          
				,[ModifedBy]          
				,[ModifiedDate]          
				,[BillNo]  --        
				,PaymentStatusID          
				,[PreviousReading]  --@PreviousReading        
				,[PresentReading]   --  @CurrentReading     
				,[Usage]     --@Usage     
				,[AverageReading] -- @AverageUsageForNewBill        
				,[TotalBillAmountWithTax] -- @TotalBillAmountWithTax        
				,[EstimatedUsage] --@Usage         
				,[ReadCodeId]  --  @ReadType      
				,[ReadType]  --  @ReadType  
				,AdjustmentAmmount -- @AdjustmentAmount    
				,BalanceUsage    --@RemaningBalanceUsage  
				,BillingTypeId --   
				,ActualUsage  
				,LastPaymentTotal  --   @PrevBillTotalPaidAmount  
				,PreviousBalance--@PreviousBalance
				,TariffRates  
			)          
			Values
			( 
				 @GlobalAccountNumber  
				,@TotalBillAmount     
				,dbo.fn_GetCustomerServiceAddress(@GlobalAccountNumber)   
				,@MeterNumber      
				,(select TOP 1 mt.MeterDials from Tbl_MeterInformation Mt WHERE Mt.MeterNo = @MeterNumber)   
				,@NetArrears         
				,@EnergyCharges   
				,@FixedCharges  
				,@TaxValue   
				,@TaxPercentage          
				,(SELECT dbo.fn_GetDistrictMessages_ByBu_Id(@BU_ID))          
				,@BU_ID  
				,@ServiceUnitId  
				,@ServiceCenterId  
				,@PoleId          
				,@BillGeneratedBY          
				,(SELECT dbo.fn_GetCurrentDateTime())          
				,(SELECT TOP(1) RecievedDate  FROM Tbl_CustomerPayments P WHERE P.AccountNo = @GlobalAccountNumber  
				ORDER BY RecievedDate DESC) --@LastDateOfBill          
				,@TariffId  
				,@Year  
				,@Month  
				,@CycleId  
				,@TotalBillAmountWithArrears   
				,1 --ActiveStatusId          
				,(SELECT dbo.fn_GetCurrentDateTime())          
				,@BillGeneratedBY          
				,NULL --ModifedBy  
				,NULL --ModifedDate  
				,(SELECT dbo.fn_GenerateBillNo(2,@BookNo,@Month,@Year))  --BillNo        
				,2 -- PaymentStatusID     
				,@PreviousReading          
				,@CurrentReading          
				,@Usage          
				,@AverageUsageForNewBill  
				,@TotalBillAmountWithTax               
				,(CASE @IsEstimatedRead WHEN 1 THEN @Usage END)           
				,@ReadType  
				,@ReadType         
				,@AdjustmentAmount      
				,@RemaningBalanceUsage     
				,2 -- BillingTypeId   
				,@ActualUsage  
				,@PrevBillTotalPaidAmount  
				,@PreviousBalance
				,@TariffCharges  
			)  
			
			SET @CusotmerNewBillID = SCOPE_IDENTITY()   
			----------------------- Update Customer Outstanding ------------------------------  

			-- Insert Paid Meter Payments  
			IF isnull(@PaidMeterDeductedAmount,0) >0
				BEGIN
					INSERT INTO Tbl_PaidMeterPaymentDetails(AccountNo,MeterNo,Amount,BillNo,CreatedBy,CreatedDate)  
					SELECT @GlobalAccountNumber,@MeterNumber,@PaidMeterDeductedAmount,@CusotmerNewBillID,(SELECT dbo.fn_GetCurrentDateTime()),@BillGeneratedBY        

					UPDATE Tbl_PaidMeterDetails
					SET OutStandingAmount=OutStandingAmount-@PaidMeterDeductedAmount
					WHERE AccountNo=@GlobalAccountNumber
				END

			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails  
			SET OutStandingAmount=@TotalBillAmountWithArrears  
			WHERE GlobalAccountNumber=@GlobalAccountNumber  
			----------------------------------------------------------------------------------  
			----------------------Update Readings as is billed =1 ----------------------------  
			
			IF(@IsInActiveCustomer = 0)
				BEGIN
					UPDATE Tbl_CustomerReadings 
					SET IsBilled = 1 
						,BillNo=@CusotmerNewBillID
					WHERE GlobalAccountNumber=@GlobalAccountNumber
						AND IsBilled = 0
				END

			--------------------------------Insert Detailed Aditional Chages in TBL_Customer_Additionalcharges---------------------------------------------------  

			INSERT INTO TBL_Customer_Additionalcharges(AccountNo,CustomerBillID,TariffId,Amount,ChargeId)  
			SELECT @GlobalAccountNumber,@CusotmerNewBillID,@TariffId,Amount,ChargeId 
			FROM  @tblUpdatedFixedCharges  
			
			INSERT INTO Tbl_CustomerBillFixedCharges
			(
				 CustomerBillId 
				,FixedChargeId 
				,Amount 
				,BillMonth 
				,BillYear 
				,CreatedDate 
			)
			SELECT
				 @CusotmerNewBillID
				,ChargeId
				,Amount
				,@Month
				,@Year
				,(SELECT dbo.fn_GetCurrentDateTime())  
			FROM @tblUpdatedFixedCharges  

			DELETE FROM @tblFixedCharges  
			DELETE FROM @tblUpdatedFixedCharges

			------------------------------------------------------------------------------------  

			--------------------Update Biling Shedule Tbl_BillingQueeCustomers -------------------  
			--Update Tbl_BillingQueeCustomers SET BillGenarationStatusId = 1       
			--WHERE AccountNo = @GlobalAccountNumber AND BillingQueuescheduleId = @BillingQueuescheduleId   

			---------------------------------------------------------------------------------------  
			UPDATE Tbl_BillAdjustments 
			SET EffectedBillId=@CusotmerNewBillID  			
			WHERE AccountNo = @GlobalAccountNumber AND CustomerBillId = @PrevCustomerBillId      

			--------------Save Bill Deails of customer name,BU-----------------------------------------------------  
			INSERT INTO Tbl_BillDetails  
			(
				CustomerBillID,  
				BusinessUnitName,  
				CustomerFullName,  
				Multiplier,  
				Postal_City,  
				Postal_HouseNo,  
				Postal_Street,  
				Postal_ZipCode,  
				ReadDate,  
				ServiceZipCode,  
				Service_City,  
				Service_HouseNo,  
				Service_Street,  
				TariffName,  
				Postal_LandMark,  
				Service_Landmark,  
				OldAccountNumber
			)  
			VALUES  
			(
				@CusotmerNewBillID,  
				@BusinessUnitName,  
				@CustomerFullName,  
				@Multiplier,  
				@Postal_City,  
				@Postal_HouseNo,  
				@Postal_Street,  
				@Postal_ZipCode,  
				@ReadDate,  
				@ServiceZipCode,  
				@Service_City,  
				@Service_HouseNo,  
				@Service_Street,  
				@TariffName,  
				@Postal_LandMark,  
				@Service_LandMark,  
				@OldAccountNumber
			)  
			
			---------------------------------------------------Set Variables to NULL-  

			SET @TotalAmount = NULL  
			SET @EnergyCharges = NULL  
			SET @FixedCharges = NULL  
			SET @TaxValue = NULL  

			SET @PreviousReading  = NULL        
			SET @CurrentReading   = NULL       
			SET @Usage   = NULL  
			SET @ReadType =NULL  
			SET @BillType   = NULL       
			SET @AdjustmentAmount    = NULL  
			SET @RemainingBalanceUnits   = NULL   
			SET @TotalBillAmountWithArrears=NULL  
			SET @BookNo=NULL  
			SET @AverageUsageForNewBill=NULL   
			SET @IsHaveLatest=0  
			SET @ActualUsage=NULL  
			SET @RegenCustomerBillId =NULL  
			SET @PaidAmount  =NULL  
			SET @PrevBillTotalPaidAmount =NULL  
			SET @PreviousBalance =NULL  
			SET @OpeningBalance =NULL  
			SET @CustomerFullName  =NULL  
			SET @BusinessUnitName  =NULL  
			SET @TariffName  =NULL  
			SET @ReadDate  =NULL  
			SET @Multiplier  =NULL  
			SET @Service_HouseNo  =NULL  
			SET @Service_Street  =NULL  
			SET @Service_City  =NULL  
			SET @ServiceZipCode =NULL  
			SET @Postal_HouseNo  =NULL  
			SET @Postal_Street =NULL  
			SET @Postal_City  =NULL  
			SET @Postal_ZipCode  =NULL  
			SET @Postal_LandMark =NULL  
			SET @Service_LandMark =NULL  
			SET @OldAccountNumber=NULL   
			SET @DisableDate=NULL  
			SET @BillingComments=NULL
			SET @TotalBillAmount=NULL
			SET @PaidMeterDeductedAmount=NULL
			SET @IsEmbassyCustomer=NULL
			SET @TaxPercentage=NULL
			SET @PaidMeterDeductedAmount=NULL
			
			IF(@IsFirstRptTblData=0)
			BEGIN	
				--PRINT '3'
			------------------------------------------6-Aug-2015 (ID-077) starts--------------------------------------------
					--Assigning variable AFTER Bill gen
			-------------------------------------------------------------------------------------
				
			SELECT TOP 1
						 @BU_ID_RPT = CB.BU_ID
						,@BusinessUnitName_RPT=BU.BusinessUnitName
						,@SU_ID_RPT=CB.SU_ID
						,@ServiceUnitName_RPT=SU.ServiceUnitName
						,@ServiceCenterId_RPT=CB.ServiceCenterId
						,@ServiceCenterName_RPT=SC.ServiceCenterName
						,@TariffId_RPT=CB.TariffId
						,@ClassName_RPT=TC.ClassName
						,@CustomerTypeId_RPT=CPD.CustomerTypeId
						,@CustomerType_RPT=CT.CustomerType
						,@BillYear_RPT=CB.BillYear
						,@BillMonth_RPT=CB.BillMonth
						,@AccountNo_RPT=CB.AccountNo
						,@ReadCodeID_RPT=CPD.ReadCodeID
						,@Usage_RPT=CB.Usage
						,@NetFixedCharges_RPT=CB.NetFixedCharges
						,@NetEnergyCharges_RPT=CB.NetEnergyCharges
						,@VATPercentage_RPT=CB.VATPercentage
						,@NetArrears_RPT=CB.NetArrears
						,@VAT_RPT=CB.VAT
						,@TotalBillAmount_RPT=CB.TotalBillAmount
						,@TotalBillAmountWithTax_RPT=CB.TotalBillAmountWithTax
						,@TotalBillAmountWithArrears_RPT=CB.TotalBillAmountWithArrears
						,@ReadType_RPT=CB.ReadType
						,@PaidAmount_RPT=CB.PaidAmount
						,@OutStandingAmount_RPT=CAD.OutStandingAmount
						,@TotalBillAmountWithTax_RPT =CB.TotalBillAmountWithTax 
						,@BillNo_RPT=CB.BillNo
						,@NoOfStubs_RPT=(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) 
						FROM Tbl_CustomerBills CB 
						INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
						INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
						INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
						INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
						INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
						INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
						INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
						INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
						INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
						WHERE	BillMonth = @Month 
						AND		BillYear = @Year
						AND		CB.AccountNo=@GlobalAccountNumber
			--------------------------------------------------------------------------------------
					--DEBIT AMOUNTS
			--------------------------------------------------------------------------------------	

			UPDATE Tbl_ReportBillingStatisticsBySCId
				SET	FixedCharges =FixedCharges+@NetFixedCharges_RPT
					,TotalAmountBilled =TotalAmountBilled+@TotalBillAmount_RPT				
					,EnergyBilled=EnergyBilled+@NetEnergyCharges_RPT
					,RevenueBilled=RevenueBilled+@TotalBillAmountWithTax_RPT
					,OpeningBalance=OpeningBalance+@TotalBillAmountWithTax_RPT
					,ClosingBalance=ClosingBalance+@ClosingBalance_RPT
					
				WHERE SC_ID=@ServiceCenterId							

			UPDATE Tbl_ReportBillingStatisticsByTariffId
				SET	FixedCharges =FixedCharges+@NetFixedCharges_RPT
					,TotalAmountBilled =TotalAmountBilled+@TotalBillAmount_RPT					
					,EnergyBilled=EnergyBilled+@NetEnergyCharges_RPT
					,RevenueBilled=RevenueBilled+@TotalBillAmountWithTax_RPT
					,OpeningBalance=OpeningBalance+@TotalBillAmountWithTax_RPT
					,ClosingBalance=ClosingBalance+@ClosingBalance_RPT
				WHERE TariffId=@TariffId_RPT
					  AND MonthId=@Month AND YearId=@Year
					
			------------------------------------------6-Aug-2015 (ID-077) ends--------------------------------------------
			END
			ELSE 
			BEGIN
					--print '4'					
					exec dbo.USP_Insert_PDFReportData_BillGen @Month, @Year, @CreatedBy
			END
			SELECT * from  dbo.fn_GetCustomerBillsForPrint_Customerwise(@GlobalAccountNumber,@Month,@Year)   
		END  
	ELSE
		BEGIN
			SELECT 0 AS NoRows   
			SELECT @TotalPaidAmount AS TotalPaidAmount,ISNULL(ABS(@AdjustmentAmountEffected),0) AS AdjustmentAmountEffected
		END
END  
				 










GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeGlobalStatusLock]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 19-05-2014
-- Description:	The purpose of this procedure is to insert Billing Open Status
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeGlobalStatusLock]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @OpenStatusId INT
			,@ModifiedBy VARCHAR(50)
			,@BillMonthId INt
	SELECT   @OpenStatusId=C.value('(OpenStatusId)[1]','INT')
			,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
			,@BillMonthId=C.value('(BillMonthId)[1]','INT')
	FROM @XmlDoc.nodes('BillingMonthOpenBE') AS T(C)
	
	--Calculating Un Billed Book Groups
	DECLARE @MonthId INT=0,@YearId INT=0,@Count INT=0
	
	SELECT @MonthId=[Month],@YearId=[Year]
	FROM Tbl_BillingMonths
	WHERE BillMonthID=@BillMonthId
	
	SELECT AccountNo AS GlobalAccountNo
	INTO #Customers
	FROM Tbl_CustomerBills
	WHERE BillMonth=@MonthId AND BillYear=@YearId


	SELECT CycleId AS BilledCyclesId
	INTO #BillCycles
	FROM UDV_CustomerDescription CD
	JOIN #Customers C ON C.GlobalAccountNo=CD.GlobalAccountNumber
	GROUP BY CD.CycleId
	
	;WITH PagedResults AS      
	(      
	SELECT CycleId
	FROM UDV_CustomerDescription CD WHERE CycleId NOT IN( SELECT BilledCyclesId FROM #BillCycles)
	GROUP BY CycleId
	)

	SELECT @Count=COUNT(0) FROM PagedResults
	--Calculating Un Billed Book Groups
	
	IF(@Count > 0)
		BEGIN
			SELECT 1 AS IsSomeBGNotBilled FOR XML PATH('BillingMonthOpenBE')
		END
	ELSE 
		BEGIN
			UPDATE Tbl_BillingMonths SET OpenStatusId=@OpenStatusId
										,ModifiedBy=@ModifiedBy
										,ModifiedDate=GETDATE()
							WHERE BillMonthId=@BillMonthId
			SELECT 1 AS IsSuccess FOR XML PATH('BillingMonthOpenBE')
		END
		
		DROP TABLE #Customers
		DROP TABLE #BillCycles
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek.P
-- Create date: 31-07-2015
-- Description:	This procedure is used to get the bill details to generate PDF
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetCustomerBillDetails_ToGeneratePDF]
(
	@XmlDoc XML = NULL
)
AS
BEGIN

	DECLARE @GlobalAccountNo VARCHAR(50)
		,@Month INT
		,@year INT
		
	SELECT     
		 @GlobalAccountNo = C.value('(GlobalAccountNo)[1]','VARCHAR(50)') 
		,@Month = C.value('(BillMonth)[1]','INT') 
		,@year = C.value('(BillYear)[1]','INT') 
	FROM @XmlDoc.nodes('CustomerBillPDFBE') as T(C)  
		
	DECLARE @AccounNo VARCHAR(50)
		,@OldAccountNo VARCHAR(50)
		,@Name VARCHAR(200)
		,@Phase VARCHAR(50)
		,@BillingAddress VARCHAR(500)
		,@ServiceAddress VARCHAR(500)
		,@PhoneNo VARCHAR(20)
		,@EmailId VARCHAR(50)
		,@MeterNo VARCHAR(50)
		,@BusinessUnit VARCHAR(50)
		,@ServiceUnit VARCHAR(50)
		,@ServiceCenter VARCHAR(50)
		,@BookGroup VARCHAR(50)
		,@BookNo VARCHAR(50)
		,@PoleNo VARCHAR(50)
		,@RouteSequense VARCHAR(50)
		,@ConnectionType VARCHAR(50)
		,@TariffCategory VARCHAR(50)
		,@TariffType VARCHAR(50)
		,@BillDate VARCHAR(20)
		,@BillType VARCHAR(10)
		,@BillNo VARCHAR(50)
		,@BillRemarks VARCHAR(200)
		,@NetArrears VARCHAR(50) 
		,@OutStanding VARCHAR(50) 
		,@Payments VARCHAR(50)
		,@TotalBillAmountWithTax VARCHAR(50)
		,@NetAmountPayble VARCHAR(50)
		,@DueDate VARCHAR(20)
		,@LastPaidAmount VARCHAR(20)
		,@LastPaidDate VARCHAR(20)
		,@PaidMeterAmount VARCHAR(20)
		,@PaidMeterDeduction VARCHAR(20)
		,@PaidMeterBalance VARCHAR(20)
		,@Consumption VARCHAR(20)
		,@TariffRate VARCHAR(20)
		,@EnergyCharges VARCHAR(20)
		,@FixedCharges VARCHAR(20)
		,@TaxPercentage DECIMAL(18,2)
		,@Tax VARCHAR(20)
		,@TotalBillAmount VARCHAR(50)
		,@BillPeriod VARCHAR(100)
		,@IsCAPMIMeter BIT
		,@LastBilledDate VARCHAR(50)
		,@LastBilledNo VARCHAR(50)
		,@LastBillPayments DECIMAL(18,2)
		,@LastBillAdjustments DECIMAL(18,2)
		,@CustomerBillId INT
		,@PaidMeterPayments DECIMAL(18,2)
		,@PaidMeterAdjustments DECIMAL(18,2)
		,@InitialBillingKWh VARCHAR(20)
		,@BulkEmails VARCHAR(MAX)
		,@TariffId INT
		,@BillGeneratedDate DATETIME
		
	SELECT
		 @AccounNo = U.GlobalAccountNumber
		,@OldAccountNo = U.OldAccountNo
		,@Name = U.FullName
		,@Phase = U.Phase
		,@ServiceAddress = U.ServiceAddress
		,@BillingAddress = U.BillingAddress
		,@PhoneNo = U.ContactNo
		,@EmailId = U.EmailId
		,@BulkEmails=dbo.fn_GetCustomerEmailId(U.GlobalAccountNumber)
		,@MeterNo = U.MeterNumber
		,@BusinessUnit = U.BusinessUnitName
		,@ServiceUnit = U.ServiceUnitName
		,@ServiceCenter = U.ServiceCenterName
		,@BookGroup = U.CycleName
		,@BookNo = U.BookName
		,@PoleNo = U.PoleNo
		,@RouteSequense = U.RouteName
		,@ConnectionType = U.CustomerType
		,@TariffCategory = U.TariffCategory
		,@TariffType = U.TariffName
		,@BillDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate,106)
		,@BillType = RC.ReadCode
		,@BillNo = CB.BillNo
		,@BillRemarks = CB.Remarks
		--,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(CB.NetArrears,0) AS MONEY)), 1)
		,@TotalBillAmount = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmount,0) AS MONEY)), 1)
		,@TotalBillAmountWithTax = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,@NetAmountPayble = CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithArrears,0) AS MONEY)), 1)
		,@DueDate = CONVERT(VARCHAR(20),CB.BillGeneratedDate + 7,106)
		,@IsCAPMIMeter = U.IsCAPMIMeter
		,@PaidMeterAmount = CONVERT(VARCHAR,(CAST(ISNULL(U.CAPMIAmount,0) AS MONEY)), 1)
		,@Consumption = REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		--,@TariffRate = CONVERT(VARCHAR, (CAST(ISNULL(CB.TariffRates,0) AS MONEY)), 1)
		,@EnergyCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetEnergyCharges,0) AS MONEY)), 1)
		,@FixedCharges = CONVERT(VARCHAR, (CAST(ISNULL(CB.NetFixedCharges,0) AS MONEY)), 1)
		,@TaxPercentage = ISNULL(CB.VATPercentage,0)
		,@Tax = CONVERT(VARCHAR, (CAST(ISNULL(CB.VAT,0)AS MONEY)), 1)
		,@CustomerBillId = CB.CustomerBillId
		,@InitialBillingKWh = U.InitialBillingKWh
		,@OutStanding = CONVERT(VARCHAR,(CAST(ISNULL(U.OpeningBalance,0) AS MONEY)), 1)
	FROM UDV_CustomerDetailsForBillPDF U
	INNER JOIN Tbl_CustomerBills CB ON CB.AccountNo = U.GlobalAccountNumber 
			AND CB.BillMonth = @Month AND CB.BillYear = @year AND U.GlobalAccountNumber = @GlobalAccountNo
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
	
	SELECT @TariffRate = CONVERT(VARCHAR, (CAST(ISNULL(Amount,0) AS MONEY)), 1)
	FROM Tbl_LEnergyClassCharges 
	WHERE ClassID = @TariffId AND IsActive = 1 
		AND CONVERT(DATE,@BillGeneratedDate) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
	
	IF(SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo) > 1
		BEGIN
			SELECT TOP 1
				 @LastBilledDate = CONVERT(VARCHAR(20),BillGeneratedDate,106)
				,@LastBilledNo = BillNo
				,@NetArrears = CONVERT(VARCHAR,(CAST(ISNULL(TotalBillAmountWithArrears,0) AS MONEY)), 1)
			FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNo
				AND BillNo != @BillNo
			ORDER BY BillGeneratedDate DESC
			
			SELECT
				 @LastBillPayments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerPayments
			WHERE CONVERT(DATE,RecievedDate) >= CONVERT(DATE,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo	
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE CONVERT(DATE,CreatedDate) >= CONVERT(DATE,@LastBilledDate) 
			AND AccountNo = @GlobalAccountNo	
			
			--SELECT
			--	@LastBillPayments = SUM(ISNULL(PaidAmount,0))
			--FROM Tbl_CustomerBillPayments
			--WHERE CustomerBillId = @CustomerBillId
			
			--SELECT
			--	@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			--FROM Tbl_BillAdjustments
			--WHERE CustomerBillId = @CustomerBillId
	
		END
	ELSE
		BEGIN			
			SELECT
				 @LastBillPayments = SUM(ISNULL(PaidAmount,0))
			FROM Tbl_CustomerPayments
			WHERE AccountNo = @GlobalAccountNo	
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(TotalAmountEffected,0))
			FROM Tbl_BillAdjustments
			WHERE AccountNo = @GlobalAccountNo	
		END
	
	--SET @Payments = ISNULL(@LastBillPayments,0) + ISNULL(@LastBillAdjustments,0)
	
	SET @BillPeriod = (CASE WHEN ISNULL(@LastBilledDate,'') = '' THEN @BillDate ELSE @LastBilledDate + ' - ' + @BillDate END)
	
	SELECT 
		 TOP 1
		 @LastPaidAmount = CONVERT(VARCHAR,(CAST(ISNULL(PaidAmount,0) AS MONEY)), 1)
		,@LastPaidDate = CONVERT(VARCHAR(20),RecievedDate,106)
	FROM Tbl_CustomerPayments 
	WHERE AccountNo = @GlobalAccountNo
	ORDER BY RecievedDate DESC
	
	IF(@IsCAPMIMeter = 1)
		BEGIN
			SELECT TOP 1 
				@PaidMeterBalance = CONVERT(VARCHAR,(CAST(ISNULL(OutStandingAmount,0) AS MONEY)), 1)
			FROM Tbl_PaidMeterDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo AND ActiveStatusId = 1
			ORDER BY MeterId DESC
			
			SELECT
				@PaidMeterPayments = SUM(ISNULL(Amount,0))
			FROM Tbl_PaidMeterPaymentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SELECT
				@LastBillAdjustments = SUM(ISNULL(AdjustedAmount,0))
			FROM Tbl_PaidMeterAdjustmentDetails
			WHERE AccountNo = @GlobalAccountNo
			AND MeterNo = @MeterNo
			
			SET @PaidMeterDeduction = ISNULL(@PaidMeterPayments,0) + ISNULL(@LastBillAdjustments,0)
			
		END
		
	DECLARE @TableLastConsumption TABLE(BillNo VARCHAR(50), BillMonth VARCHAR(20), Consumption VARCHAR(20), ADC VARCHAR(20), CurrentDemand VARCHAR(20), TotalBillPayable VARCHAR(24), BillingType VARCHAR(10))
	
	INSERT INTO @TableLastConsumption
	(
		 BillNo
		,BillMonth
		,Consumption
		,ADC
		,CurrentDemand
		,TotalBillPayable
		,BillingType
	)
	SELECT TOP 5
		 CB.BillNo
		,CONVERT(VARCHAR(50),[dbo].[fn_GetMonthName](CB.BillMonth)) + ' ' + CONVERT(VARCHAR(10),BillYear)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CB.Usage,0) AS MONEY)), 1), '.00', '')
		,''
		,''
		,CONVERT(VARCHAR,(CAST(ISNULL(CB.TotalBillAmountWithTax,0) AS MONEY)), 1)
		,RC.ReadCode
	FROM Tbl_CustomerBills CB
	INNER JOIN Tbl_MReadCodes RC ON RC.ReadCodeId = CB.ReadType
		AND CB.BillNo != @BillNo AND CB.AccountNo = @GlobalAccountNo
	ORDER BY CB.BillGeneratedDate DESC
	
	DECLARE @TableLastReadings TABLE(MeterNo VARCHAR(50), CurrentReadingDate VARCHAR(20)
					, CurrentReading VARCHAR(20), PreviousReadingDate VARCHAR(20), PreviousReading VARCHAR(20)
					, Multiplier INT, Consumption VARCHAR(20))
	
		
	INSERT INTO @TableLastReadings
	(
		 MeterNo
		,CurrentReadingDate
		,CurrentReading
		,PreviousReadingDate
		,PreviousReading
		,Multiplier
		,Consumption
	)
	SELECT
		 CR.MeterNumber
		,CONVERT(VARCHAR(20),CR.ReadDate,106)
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PresentReading,0) AS MONEY)), 1), '.00', '')
		,(SELECT TOP 1 
				CONVERT(VARCHAR(20),CR2.ReadDate,106)
			FROM Tbl_CustomerReadings CR2
			WHERE CR2.GlobalAccountNumber = @GlobalAccountNo
				AND CR2.CustomerReadingId < CR.CustomerReadingId
			ORDER BY CR2.CustomerReadingId DESC)
		--,ISNULL((SELECT TOP 1 
		--		REPLACE(CONVERT(VARCHAR, (CAST(CR3.PresentReading AS MONEY)), 1), '.00', '')
		--	FROM Tbl_CustomerReadings CR3
		--	WHERE CR3.GlobalAccountNumber = @GlobalAccountNo
		--		AND CR3.CustomerReadingId < CR.CustomerReadingId
		--	ORDER BY CR3.CustomerReadingId DESC),REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', ''))
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.PreviousReading,0) AS MONEY)), 1), '.00', '')
		,CR.Multiplier
		,REPLACE(CONVERT(VARCHAR, (CAST(ISNULL(CR.Usage,0) AS MONEY)), 1), '.00', '')
	FROM Tbl_CustomerReadings CR
	WHERE CR.GlobalAccountNumber = @GlobalAccountNo
		AND CR.BillNo = @CustomerBillId
	ORDER BY CR.CustomerReadingId ASC
	
	SELECT
	(
		SELECT @AccounNo AS AccountNo
			,@GlobalAccountNo AS GlobalAccountNo
			,@OldAccountNo AS OldAccountNo
			,@Name AS Name
			,@Phase AS Phase
			,@BillingAddress AS BillingAddress
			,@ServiceAddress AS ServiceAddress
			,@PhoneNo AS PhoneNo
			,@EmailId AS EmailId
			,@BulkEmails AS BulkEmails 
			,@MeterNo AS MeterNo
			,@BusinessUnit AS BusinessUnit
			,@ServiceUnit AS ServiceUnit
			,@ServiceCenter AS ServiceCenter
			,@BookGroup AS BookGroup
			,@BookNo AS BookNo
			,@PoleNo AS PoleNo
			,@RouteSequense AS WalkingSequence
			,@ConnectionType AS ConnectionType
			,@TariffCategory AS TariffCategory
			,@TariffType AS TariffType
			,@BillDate AS BillDate
			,@BillType AS BillType
			,@BillNo AS BillNo
			,@BillRemarks AS BillRemarks
			,ISNULL(@NetArrears,@OutStanding) AS NetArrears
			--,@Payments AS Payments
			,ISNULL(@LastBillPayments,'0.00') AS Payments
			,ISNULL(@LastBillAdjustments,'0.00') AS Adjustments
			,@TotalBillAmountWithTax AS BillAmount
			,@NetAmountPayble AS NetAmountPayable
			,@DueDate AS DueDate
			,ISNULL(@LastPaidAmount,'--') AS LastPaidAmount
			,ISNULL(@LastPaidDate,'--') AS LastPaidDate
			,@PaidMeterAmount AS PaidMeterAmount
			,@PaidMeterDeduction AS PaidMeterDeduction
			,@PaidMeterBalance PaidMeterBalance
			,@Consumption AS Consumption
			,@TariffRate AS TariffRate
			,@EnergyCharges AS EnergyCharges
			,@BillPeriod AS BillPeriod
			,@FixedCharges AS FixedCharges
			,@TaxPercentage AS TaxPercentage
			,@Tax AS TaxAmount
			,@TotalBillAmount AS TotalBillAmount
			,@IsCAPMIMeter AS IsCAPMIMeter
			,(SELECT [dbo].[fn_GetMonthName](@Month)) AS [MonthName]
			,@year AS [Year]
			,@CustomerBillId AS CustomerBillId
		FOR XML PATH('CustomerBillDetails'),TYPE      
	)
	,
	(
		SELECT
			 BillNo
			,BillMonth
			,Consumption
			,ADC
			,CurrentDemand
			,TotalBillPayable
			,BillingType
		FROM @TableLastConsumption
		FOR XML PATH('CustomerLastConsumptionDetails'),TYPE      
	)      
	,
	(
		SELECT
			 MeterNo
			,CurrentReadingDate
			,CurrentReading
			,PreviousReadingDate
			,PreviousReading
			,Multiplier
			,Consumption
		FROM @TableLastReadings
		FOR XML PATH('CustomerLastReadingsDetails'),TYPE      
	)     
	,
	(
		SELECT
			 CF.FixedChargeId AS FixedChargeId
			,C.ChargeName AS FixedChargeName
			,CONVERT(VARCHAR,(CAST(ISNULL(CF.Amount,0) AS MONEY)), 1) AS FixedChargeAmount
		FROM Tbl_CustomerBillFixedCharges CF
		INNER JOIN Tbl_MChargeIds C ON CF.FixedChargeId = C.ChargeId AND IsAcitve = 1
			AND CF.CustomerBillId = @CustomerBillId
		FOR XML PATH('CustomerBillAdditionalChargeDetails'),TYPE   
	)
	FOR XML PATH(''),ROOT('CustomerBillPDFDetails')   

	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetPagePermisions]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author:  <Faiz-ID103>        
-- Create date: <09-JAN-2015>        
-- Description: <Retriving Master and Child page list for Role Permisions.>
-- Modified By :kalyan        
-- Modified Date :05-08-2015
--Description : added Page_Order in order by
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetPagePermisions]   
(  
@XmlDoc XML=null  
)  
AS        
 BEGIN       
 DECLARE @AccessLevelID INT  
   ,@RoleId INT   
  
 SELECT       
 @AccessLevelID = C.value('(AccessLevelID)[1]','INT')   
 ,@RoleId = C.value('(RoleId)[1]','INT')   
 FROM @XmlDoc.nodes('AdminBE') AS T(C)      
   
 Select (      
 --List for Master Menu Item      
       
       
   SELECT [MenuId]      
      ,[Name]      
      --,[Path]      
      ,[Menu_Order]      
      ,[ReferenceMenuId]      
      --,[Icon1]      
      --,[Icon2]      
      ,[Page_Order]      
      ,[IsActive]      
      --,[ActiveHeader]      
      --,[Menu_Image]      
   FROM Tbl_Menus WHERE Menu_Order IS NOT NULL AND IsActive= 1 ORDER BY Menu_Order      
   FOR XML PATH('AdminListBE'),TYPE      
    ),      
    (      
 --List for Child Menu Item      
    SELECT [MenuId]      
      ,[Name]      
      --,[Path]      
      ,[Menu_Order]      
      ,(CASE [ReferenceMenuId] WHEN 122 THEN 7 ELSE [ReferenceMenuId] END) AS [ReferenceMenuId]    
      --,[Icon1]      
      --,[Icon2]      
      ,[Page_Order]      
      ,[IsActive]      
      --,[ActiveHeader]      
      --,[Menu_Image]      
      ,ISNULL(Tbl_Menus.AccessLevelID,0) AS AccessId    
   FROM Tbl_Menus     
  LEFT JOIN Tbl_MMenuAccessLevels ON Tbl_Menus.AccessLevelID = Tbl_MMenuAccessLevels.AccessLevelID    
   WHERE Menu_Order IS NULL    
   AND IsActive= 1 AND Tbl_Menus.AccessLevelID >= @AccessLevelID  
 ORDER BY ReferenceMenuId,Page_Order
   FOR XML PATH('AdminListBE_1'),TYPE      
   )      
   FOR XML PATH(''),ROOT('AdminBEInfoByXml')            
 END    
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertValidMeterReadingsUploads]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Karteek
-- Create date: 29 Apr 2015
-- Description:	To validate the Meter readings data and Inserting the readings into readings realted tables 
-- =============================================
ALTER PROCEDURE [dbo].[USP_InsertValidMeterReadingsUploads]
	
AS
BEGIN
	
	DECLARE @FileUploadId INT
	
	-- To get the Payments data to validate
	SELECT 
		 CMI.GlobalAccountNumber
		,CMI.MeterNumber
		,CMI.MeterDials
		,CMI.Decimals
		,CMI.ActiveStatusId
		,T.CurrentReading
		,T.ReadingDate
		,CMI.ReadCodeID
		,CMI.BU_ID
		,CMI.InitialReading
		,CMI.MarketerId
		,T.RUploadFileId
		,T.ReadingTransactionId
		,T.CreatedBy
		,T.CreatedDate
		,T.SNO
		,T.AccountNo AS TempTableAccountNo
		,PUB.BU_ID AS BatchBU_ID
		,CMI.BookNo
		,ISNUMERIC(T.CurrentReading) AS IsValidReading
		,CMI.MeterMultiplier
		,CMI.CustomerTypeId
	INTO #CustomersListBook
	FROM Tbl_ReadingTransactions(NOLOCK) T
	INNER JOIN Tbl_ReadingUploadFiles(NOLOCK) PUF ON PUF.RUploadFileId = T.RUploadFileId  
	INNER JOIN Tbl_ReadingUploadBatches(NOLOCK) PUB ON PUB.RUploadBatchId = PUF.RUploadBatchId  
	LEFT JOIN UDV_CustomerMeterInformation(NOLOCK) CMI ON (CMI.OldAccountNo = T.AccountNo OR CMI.GlobalAccountNumber = T.AccountNo)
	WHERE T.RUploadFileId IN (SELECT MAX(RUploadFileId) FROM Tbl_ReadingTransactions) 
	
	Select MAX(CustomerReadingInner.CustomerReadingId) as CustomerReadingId  
	INTO #CusotmersWithMaxReadID
	From Tbl_CustomerReadings(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsBilled
	,CustomerReadingInner.IsRollOver
	INTO #CustomerLatestReadings
	From Tbl_CustomerReadings(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadID	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingId=CustomerReadingwithMaxID.CustomerReadingId
	
	Select MAX(CustomerReadingInner.CustomerReadingLogId) as CustomerReadingLogId  
	INTO #CusotmersWithMaxReadIDLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK)   CustomerReadingInner
	inner Join #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerReadingInner.GlobalAccountNumber
	Group By CustomerReadingInner.GlobalAccountNumber

	select CustomerReadingInner.GlobalAccountNumber 
	,CustomerReadingInner.AverageReading,CustomerReadingInner.CustomerReadingLogId
	,CustomerReadingInner.MeterNumber as ReadingMeterNumber
	,CustomerReadingInner.Multiplier as ReadingMultiplier,CustomerReadingInner.PresentReading
	,CustomerReadingInner.PreviousReading,CustomerReadingInner.ReadDate
	,CustomerReadingInner.Usage 
	,CustomerReadingInner.TotalReadings
	,CustomerReadingInner.IsRollOver
	,CustomerReadingInner.ApproveStatusId
	,CustomerReadingInner.IsLocked
	INTO #CustomerLatestReadingsLogs
	From Tbl_CustomerReadingApprovalLogs(NOLOCK) CustomerReadingInner
	INNER JOIN #CusotmersWithMaxReadIDLogs	CustomerReadingwithMaxID
	ON CustomerReadingInner.CustomerReadingLogId=CustomerReadingwithMaxID.CustomerReadingLogId
	
	Select   MAX(CustomerMeterChange.MeterInfoChangeLogId) as MeterInfoChangeLogId 
	INTO #CusotmersWithMeterChangeLogs
	From Tbl_CustomerMeterInfoChangeLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.AccountNo
	Group By CustomerMeterChange.AccountNo
 
	select CustomerMeterChange.AccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApproveStatusId
	,CustomerMeterChange.MeterChangedDate
	INTO #CustomerMeterApprovaStatus
	From Tbl_CustomerMeterInfoChangeLogs CustomerMeterChange
	INNER JOIN #CusotmersWithMeterChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.MeterInfoChangeLogId=CustomerMeterLogs.MeterInfoChangeLogId 

	Select   MAX(CustomerMeterChange.ReadToDirectId) as ReadToDirectId 
	INTO #CusotmersReadToDirectChangeLogs
	From Tbl_ReadToDirectCustomerActivityLogs   CustomerMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = CustomerMeterChange.GlobalAccountNo
	Group By CustomerMeterChange.GlobalAccountNo
 
	select CustomerMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,CustomerMeterChange.ApprovalStatusId
	INTO #CustomerReadToDirectApprovaStatus
	From Tbl_ReadToDirectCustomerActivityLogs CustomerMeterChange
	INNER JOIN #CusotmersReadToDirectChangeLogs	CustomerMeterLogs
	ON CustomerMeterChange.ReadToDirectId=CustomerMeterLogs.ReadToDirectId 

	Select   MAX(AssignedMeterChange.AssignedMeterId) as AssignedMeterId 
	INTO #CusotmersAssignMeterLogs
	From Tbl_Audit_AssignedMeterLogs   AssignedMeterChange
	inner Join 	 #CustomersListBook CustomerListInner 
	On CustomerListInner.GlobalAccountNumber = AssignedMeterChange.GlobalAccountNo
	Group By AssignedMeterChange.GlobalAccountNo
 
	select AssignedMeterChange.GlobalAccountNo AS  GlobalAccountNumber
	,AssignedMeterChange.AssignedMeterDate
	INTO #CustomerAssignMeterApprovaStatus
	From Tbl_Audit_AssignedMeterLogs AssignedMeterChange
	INNER JOIN #CusotmersAssignMeterLogs	CustomerMeterLogs
	ON AssignedMeterChange.AssignedMeterId=CustomerMeterLogs.AssignedMeterId 
	
	
	Select   Max(CustomerBillInnner.CustomerBillId) as CustomerBillId 
	INTO #CusotmersWithMaxBillID    
	from Tbl_CustomerBills CustomerBillInnner 
	INNER JOIN 	#CustomersListBook CustomerListInner ON CustomerBillInnner.AccountNo=CustomerListInner.GlobalAccountNumber
	Group By CustomerBillInnner.AccountNo

	 select CustomerBillMain.AccountNo as GlobalAccountNumber
	 ,CustomerBillMain.BillGeneratedDate
	 INTO #CustomerLatestBills  
	 from  Tbl_CustomerBills As CustomerBillMain
	 INNER JOIN 
	 #CusotmersWithMaxBillID  AS CustomersBillWithMaxID
	 on CustomersBillWithMaxID.CustomerBillId=CustomerBillMain.CustomerBillId
			

	SELECT DISTINCT
		 ISNULL(CB.GlobalAccountNumber,'') AS AccNum
		,CB.MeterNumber AS MeterNo
		,CB.MeterDials
		,CustomerReadings.ReadingMultiplier
		,CB.ActiveStatusId
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN CB.ReadingDate ELSE NULL END) AS ReadDate
		,CB.ReadCodeID AS ReadCodeId
		,CB.BU_ID
		,CB.MarketerId
		,(CASE WHEN IsValidReading = 1 THEN (CONVERT(DECIMAL(18,2),ISNULL(CB.CurrentReading,0)) - 
			(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END))
			ELSE 0 END) AS Usage
		,(CASE WHEN IsValidReading = 1 THEN CB.CurrentReading ELSE 0 END) AS PresentReading
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) 
				then (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN ISNULL(CustomerReadings.PreviousReading,ISNULL(CB.InitialReading,0)) ELSE ISNULL(CB.InitialReading,0) END)
				else (case when CB.MeterNumber=CustomerReadings.ReadingMeterNumber
						THEN CustomerReadings.PresentReading ELSE ISNULL(CB.InitialReading,0) END) end)
			else 0 END) AS PreviousReading
		,ISNULL(CustomerReadings.TotalReadings,0) AS TotalReadings
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN
			(case when CONVERT(DATE,CustomerReadings.ReadDate) = CONVERT(DATE,CB.ReadingDate) then 1  
				else 0 end)
			else 0 END) as PrvExist
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 THEN 
			(Case when CustomerReadings.ReadDate IS NULL then 0
				when CONVERT(DATE,CustomerReadings.ReadDate) > CONVERT(DATE,CB.ReadingDate) then 1 
				else 0 end)
			else 0 END) as IsFutureReadingsExist
		,(CASE WHEN ((SELECT COUNT(0) FROM #CustomersListBook WHERE GlobalAccountNumber=CB.GlobalAccountNumber GROUP BY GlobalAccountNumber) > 1) 
			THEN 1 ELSE 0 END) AS IsDuplicate
		--,(case when Exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy
		--									AND ActiveStatusId = 1 AND BU_ID = CB.BU_ID) then 1
		--		when not exists(select 0 from Tbl_UserBusinessUnits WHERE UserId = CB.CreatedBy) then 1
		--		else 0 end) as IsBUValidate 
		,ISDATE(CB.ReadingDate) AS IsValidDate
		,(CASE WHEN CB.BatchBU_ID = CB.BU_ID THEN 1 ELSE 0 END) AS IsBUValidate
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (CASE WHEN CONVERT(DATE,CB.ReadingDate) > CONVERT(DATE,CB.CreatedDate) then 1 ELSE 0 END)
			ELSE 0 END) AS IsGreaterDate
		,CB.CreatedBy
		,CB.CreatedDate
		,CB.RUploadFileId
		,CB.ReadingTransactionId
		,CB.SNO
		,CB.TempTableAccountNo
		,CB.IsValidReading
		,1 AS IsValid
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
				THEN (CASE WHEN (CustomerReadingsLogs.ApproveStatusId = 1 OR CustomerReadingsLogs.ApproveStatusId = 4) AND ISDATE(CB.ReadingDate) = 1
							THEN (CASE WHEN CONVERT(DATE,CustomerReadingsLogs.ReadDate) >= CONVERT(DATE,CB.ReadingDate) 
										THEN ISNULL(CustomerReadingsLogs.IsRollOver,0) 
										ELSE 0 END)
							ELSE (CASE WHEN CONVERT(DATE,CustomerReadings.ReadDate) >= CONVERT(DATE,CB.ReadingDate) 
										THEN ISNULL(CustomerReadings.IsRollOver,0) 
										ELSE 0 END)
					  END)
		  ELSE 0 END) AS IsRollOver
		,CustomerReadingsLogs.ApproveStatusId
		,CustomerReadingsLogs.IsLocked
		,(CASE WHEN (CustomerMeterApprovals.ApproveStatusId = 1 OR CustomerMeterApprovals.ApproveStatusId = 4) THEN 1 ELSE 0 END) AS IsMeterChangeApproval
		,(CASE WHEN (ReadToDirectApprovals.ApprovalStatusId = 1 OR ReadToDirectApprovals.ApprovalStatusId = 4) THEN 1 ELSE 0 END) AS IsReadToDirectApproval
		,CONVERT(VARCHAR(MAX),'') AS Comments
		--,(CASE WHEN BD.IsActive = 1 
		--		THEN (CASE WHEN ISNULL(IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
		--		ELSE 0 END) AS IsDisabledBook
		,(SELECT TOP 1 (CASE WHEN BD.IsActive = 1 
				THEN (CASE WHEN ISNULL(BD.IsPartialBill,0) = 0 THEN 1 ELSE 0 END)
				ELSE 0 END) 
			FROM Tbl_BillingDisabledBooks BD 
			WHERE BD.BookNo = CB.BookNo
			ORDER BY DisabledBookId DESC) AS IsDisabledBook
		,(CASE WHEN CustomerMeterApprovals.ApproveStatusId = 2 AND ISDATE(CB.ReadingDate) = 1
				THEN (CASE WHEN CONVERT(DATE,CustomerMeterApprovals.MeterChangedDate) >= CONVERT(DATE,CB.ReadingDate) 
					THEN 1 ELSE 0 END) 
				ELSE 0 END) AS IsMeterChangedDateExists
		,(CASE WHEN ISNULL(AssignMeterApprovals.AssignedMeterDate,'') != '' AND ISDATE(CB.ReadingDate) = 1 
					THEN (CASE WHEN CONVERT(DATE,AssignMeterApprovals.AssignedMeterDate) >= CONVERT(DATE,CB.ReadingDate) THEN 1 ELSE 0 END)
					ELSE 0 
					END) AS IsMeterAssignedDateExists
		,(CASE WHEN CustomerReadings.IsBilled = 1 THEN 1 ELSE 0 END) AS IsBilled
		,(CASE WHEN ISDATE(CB.ReadingDate) = 1 
			THEN (Case when CONVERT(DATE,CB.ReadingDate) < CONVERT(DATE,LatestBills.BillGeneratedDate) then 1 else 0 end)
			ELSE 0 END) as IsLatestBill
		,CB.MeterMultiplier
		,CB.CustomerTypeId
	INTO #ReadingsValidation
	FROM #CustomersListBook CB
	LEFT JOIN #CustomerLatestReadings As CustomerReadings ON CB.GlobalAccountNumber=CustomerReadings.GlobalAccountNumber 
	LEFT JOIN #CustomerLatestReadingsLogs As CustomerReadingsLogs ON CB.GlobalAccountNumber=CustomerReadingsLogs.GlobalAccountNumber 
	LEFT JOIN #CustomerMeterApprovaStatus As CustomerMeterApprovals ON CB.GlobalAccountNumber=CustomerMeterApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerReadToDirectApprovaStatus As ReadToDirectApprovals ON CB.GlobalAccountNumber=ReadToDirectApprovals.GlobalAccountNumber	
	LEFT JOIN #CustomerAssignMeterApprovaStatus As AssignMeterApprovals	ON CB.GlobalAccountNumber=AssignMeterApprovals.GlobalAccountNumber
	LEFT JOIN #CustomerLatestBills As LatestBills ON CB.GlobalAccountNumber=LatestBills.GlobalAccountNumber
	--LEFT JOIN Tbl_BillingDisabledBooks BD ON BD.BookNo = CB.BookNo
	
	SELECT TOP(1) @FileUploadId = RUploadFileId FROM #ReadingsValidation
	
	BEGIN TRY
		BEGIN TRAN
			
			-- TO check whether the Account No exists or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE AccNum = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Account Number'
					WHERE AccNum = ''
					
				END			
			-- TO check whether the customer related that user BU or not
			ELSE IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBUValidate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Customer in Another BU'
					WHERE IsBUValidate = 0
					
				END
				
			-- TO check whether the customer account duplicated or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDuplicate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Duplicate Account Number'
					WHERE IsDuplicate = 1
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ReadCodeId = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Direct Customer'
					WHERE ReadCodeId = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsDisabledBook = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Disabled Book'
					WHERE IsDisabledBook = 1
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 2)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', InActive Customer'
					WHERE ActiveStatusId = 2
					
				END
			
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Hold Customer'
					WHERE ActiveStatusId = 3
					
				END
				
			-- TO check whether the customer is in active or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ActiveStatusId = 4)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Closed Customer'
					WHERE ActiveStatusId = 4
					
				END
				
			-- TO check whether the customer is prepaid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE CustomerTypeId = 3)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Prepaid Customer'
					WHERE CustomerTypeId = 3
					
				END
			
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidDate = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid date'
					WHERE IsValidDate = 0
					
				END
				
			-- TO check whether the given date is valid or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsValidReading = 0 OR ISNULL(PresentReading,'') = '')
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Reading'
					WHERE IsValidReading = 0 OR ISNULL(PresentReading,'') = ''
					
				END
				
			-- TO check whether the given date is greater than today
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsGreaterDate = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Future Date Not Applicable'
					WHERE IsGreaterDate = 1
					
				END
			
			-- TO check whether the readings exist fro future day
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsFutureReadingsExist = 1)-- OR PrvExist = 1
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Already Readings Available'
					WHERE IsFutureReadingsExist = 1 --OR PrvExist = 1
					
				END
			
			-- TO check whether the readings billed ot not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsBilled = 1 AND IsLatestBill = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Bill Generated for Future Date'
					WHERE IsBilled = 1 AND IsLatestBill = 1
					
				END
				
			---- TO check whether the reading is valid or not
			--IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ISNULL(PresentReading,'') = '')
			--	BEGIN
					
			--		UPDATE #ReadingsValidation
			--		SET IsValid = 0,
			--			Comments = Comments + ', Invalid Readings'
			--		WHERE ISNULL(PresentReading,'') = ''
					
			--	END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE Usage < 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Invalid Usage'
					WHERE Usage < 0
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE ApproveStatusId IN (1,4))
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Readings Approval Process is pending for this Customer'
					WHERE ApproveStatusId IN (1,4)
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangeApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Change Approval Process is pending for this Customer'
					WHERE IsMeterChangeApproval = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsReadToDirectApproval = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Disconnect Change Approval Process is pending for this Customer'
					WHERE IsReadToDirectApproval = 1
					
				END
			
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterChangedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Changed Date'
					WHERE IsMeterChangedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsMeterAssignedDateExists = 1)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reading Date should be greater than the Meter Assigned Date'
					WHERE IsMeterAssignedDateExists = 1
					
				END
				
			-- TO check whether the valid usage or not
			IF EXISTS(SELECT 0 FROM #ReadingsValidation WHERE IsRollOver = 0)
				BEGIN
					
					UPDATE #ReadingsValidation
					SET IsValid = 0,
						Comments = Comments + ', Meter Reset'
					WHERE IsRollOver = 1
					
				END
				
			INSERT INTO Tbl_ReadingFailureTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,STUFF(Comments,1,2,'')
			FROM #ReadingsValidation
			WHERE IsValid = 0
					
			
			DECLARE @FailureTransactions INT = 0, @SuccessTransactions INT = 0
			SELECT @FailureTransactions = COUNT(0) FROM #ReadingsValidation WHERE IsValid = 0
			
			-- TO delete the Failure Transactions from Reading Transaction table	
			DELETE FROM Tbl_ReadingTransactions	
			WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation
										WHERE IsValid = 0)
		 
			-- TO delete the Failure Transactions from temp table
			DELETE FROM #ReadingsValidation WHERE IsValid = 0
									
			DECLARE 
				 @ReadDate varchar(50)
				,@ReadBy varchar(50)
				,@Multiple int
				,@TotalReadings VARCHAR(50)
				,@AverageReading VARCHAR(50)
				,@PresentReading VARCHAR(50)
				,@Usage VARCHAR(50)
				,@AccountNo VARCHAR(50)
				,@PreviousReading VARCHAR(50)
				,@IsExists BIT
				,@MeterNumber VARCHAR(50)
				,@Dials INT
				,@SNo INT
				,@TotalCount INT
				,@CreatedBy VARCHAR(50)
				,@MeterMultiplier INT
					
			SELECT
				 IDENTITY(INT, 1,1) AS RowNumber
				,AccNum
				,ReadDate
				,MarketerId
				,ReadingMultiplier
				,TotalReadings
				,PresentReading
				,Usage
				,PreviousReading
				,PrvExist
				,MeterNo
				,MeterDials
				,CreatedBy
				,MeterMultiplier
			INTO #ValidReadings
			FROM #ReadingsValidation
				
			SET @SNo = 1
			SELECT @TotalCount = COUNT(0) FROM #ValidReadings    
			SET @SuccessTransactions = @TotalCount

			WHILE(@SNo <= @TotalCount)
				BEGIN
					
					SELECT @AccountNo = AccNum,
						@ReadDate = ReadDate,
						@ReadBy = MarketerId,
						@Multiple = ReadingMultiplier,
						@TotalReadings = TotalReadings,
						@PresentReading = PresentReading,
						@PreviousReading = PreviousReading,
						@Usage = ISNULL(Usage,0),
						@IsExists = PrvExist,
						@MeterNumber = MeterNo,
						@Dials = MeterDials,
						@CreatedBy = CreatedBy,
						@MeterMultiplier = MeterMultiplier
					FROM #ValidReadings 
					WHERE RowNumber = @SNo
					
					IF(@IsExists = 1)
						BEGIN					
							DELETE FROM	Tbl_CustomerReadings 
							WHERE CustomerReadingId = (SELECT TOP(1) CustomerReadingId
													FROM Tbl_CustomerReadings
													WHERE GlobalAccountNumber = @AccountNo
													ORDER BY CustomerReadingId DESC)						
						END	
						
					SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
						
					SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
					
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@MeterMultiplier
						,2
						,@CreatedBy
						,GETDATE()
						,0
						,@MeterNumber
						,4 --Bulk upload
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
					
					SET @SNo = @SNo + 1 
					SET @TotalReadings = NULL
					SET @AverageReading = NULL
					SET @PresentReading = NULL
					SET @Usage = NULL
					SET @AccountNo = NULL
					SET @PreviousReading = NULL
					SET @IsExists = NULL
					SET @MeterNumber = NULL
					SET @Dials = NULL
					SET @ReadBy = NULL
					SET @ReadDate = NULL
					SET @Multiple = NULL
					SET @CreatedBy = NULL
						
				END
				
			INSERT INTO Tbl_ReadingSucessTransactions
			(
				 RUploadFileId
				,SNO
				,AccountNo
				,CurrentReading
				,ReadingDate
				,CreatedDate
				,CreatedBy
				,Comments
			)
			SELECT 
				 RUploadFileId
				,SNO
				,TempTableAccountNo
				,PresentReading
				,ReadDate
				,CreatedDate
				,CreatedBy
				,'Valid'
			FROM #ReadingsValidation
			WHERE IsValid = 1
			
			DELETE FROM Tbl_ReadingTransactions 
				WHERE ReadingTransactionId IN(SELECT ReadingTransactionId FROM #ReadingsValidation)
			
			UPDATE Tbl_ReadingUploadFiles
			SET TotalSucessTransactions = @SuccessTransactions
				,TotalFailureTransactions = @FailureTransactions
			WHERE RUploadFileId = @FileUploadId
			
		COMMIT TRAN	
	END TRY	   
	BEGIN CATCH
		ROLLBACK TRAN				
	END CATCH		
			
END


GO

/****** Object:  StoredProcedure [dbo].[USP_CustomerMeterChangeApproval]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================  
-- Modified By: Karteek
-- Modified Date: 04-04-2015
-- Description: Update mater number change Details of a customer after approval  
-- =============================================  
ALTER PROCEDURE [dbo].[USP_CustomerMeterChangeApproval]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
	
	DECLARE  
		 @ApprovalStatusId INT   
		,@FunctionId INT
		,@ModifiedBy VARCHAR(50)
		,@MeterNoChangeLogId INT
		,@IsFinalApproval BIT = 0
		,@Details VARCHAR(200) 
		,@BU_ID VARCHAR(50) 

	SELECT  
		 @MeterNoChangeLogId = C.value('(MeterNumberChangeLogId)[1]','INT')  
		,@ApprovalStatusId = C.value('(ApprovalStatusId)[1]','INT')   
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ModifiedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')
		,@Details = C.value('(Details)[1]','VARCHAR(200)') 
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
	
	DECLARE @AccountNo VARCHAR(50)
		,@NewMeterNo VARCHAR(100)
		,@NewMeterTypeId INT
		,@OldMeterTypeId INT
		,@NewMeterDials INT
		,@OldMeterDials INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading BIGINT
		,@NewMeterReading BIGINT
		,@PreviousReading BIGINT
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@MeterChangeDate DATETIME
		,@NewMeterInitialReading BIGINT
		,@CurrentLevel INT
		,@Remarks VARCHAR(500)
		,@IsCAPMIMeter BIT = 0
		,@CAPMIAmount DECIMAL(18,2) = 0
	
	SELECT 
		 @AccountNo = AccountNo
		,@OldMeterNo = OldMeterNo
		,@NewMeterNo = NewMeterNo
		,@OldMeterTypeId = OldMeterTypeId
		,@NewMeterTypeId = NewMeterTypeId
		,@OldMeterDials = OldDials
		,@NewMeterDials = NewDials
		,@Remarks = Remarks
		,@OldMeterReading = CAST(OldMeterReading AS NUMERIC)
		,@NewMeterReading = CAST(NewMeterReading AS NUMERIC)
		,@MeterChangeDate = MeterChangedDate
		,@InitialBillingkWh = InitialBillingkWh
		,@NewMeterInitialReading = CAST(NewMeterInitialReading AS NUMERIC)
		,@NewMeterReadingDate = NewMeterReadingDate
		,@IsCAPMIMeter = ISNULL(IsCAPMIMeter,0)
		,@CAPMIAmount = ISNULL(CAPMIAmount,0)
	FROM Tbl_CustomerMeterInfoChangeLogs
	WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SELECT TOP 1 
				 @PreviousReading = ISNULL(PresentReading,0)
				,@OldMeterReadingDate = ReadDate
			FROM Tbl_CustomerReadings 
			WHERE GlobalAccountNumber = @AccountNo ORDER BY CustomerReadingId DESC
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
			SET @OldMeterReadingDate = NULL
		END 
	
	IF(@ApprovalStatusId = 2)  -- Request Approved
		BEGIN  
			DECLARE @CurrentRoleId INT
			SET @CurrentRoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId

			DECLARE @Usage VARCHAR(50)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage = ISNULL(@OldMeterReading,0) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @OldMeterNo)

			DECLARE @NewMeterUsage DECIMAL(18,2) = 0
			SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

			DECLARE @OldAverage VARCHAR(25)			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_New(@AccountNo,CONVERT(DECIMAL(18,2),@Usage)))
	
			IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId ANd IsActive = 1) -- If Approval Levels Exists
				BEGIN
					--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
					--				RoleId = (SELECT PresentApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
					--						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))
						SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerMeterInfoChangeLogs 
											WHERE MeterInfoChangeLogId = @MeterNoChangeLogId)
					SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId   --Assigning Next level Roles of Approval
					
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,1,@BU_ID)

				DECLARE @FinalApproval BIT

					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID) =1)
						BEGIN
								SET @FinalApproval = (SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @CurrentRoleId AND BU_ID=@BU_ID)
						END
					ELSE
						BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
							SET @FinalApproval=(SELECT IsFinalApproval FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
						END



					IF(@FinalApproval= 1)
						BEGIN -- If Approval levels are there and If it is final approval then update tariff
						
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles & Approval status 
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = @ApprovalStatusId
								,CurrentApprovalLevel=0
								,IsLocked = 1
							WHERE AccountNo = @AccountNo 
							AND MeterInfoChangeLogId = @MeterNoChangeLogId  

							INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
								 MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate
								,IsCAPMIMeter
								,CAPMIAmount)
							SELECT  MeterInfoChangeLogId
								,AccountNo
								,OldMeterNo
								,NewMeterNo
								,OldMeterTypeId
								,NewMeterTypeId
								,OldDials
								,NewDials
								,CreatedBy
								,CreatedDate
								,ApproveStatusId
								,Remarks
								,OldMeterReading
								,NewMeterReading
								,MeterChangedDate
								,InitialBillingkWh
								,NewMeterInitialReading
								,NewMeterReadingDate
								,IsCAPMIMeter
								,CAPMIAmount
							FROM Tbl_CustomerMeterInfoChangeLogs
							WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

							UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
							SET
								 MeterNumber = @NewMeterNo
								,ModifedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
							WHERE GlobalAccountNumber = @AccountNo
							
							--START Old MeterREading Taken and insert in to Customer Bills Table

							--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							--SET
							--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
							--WHERE GlobalAccountNumber = @AccountNo

							IF (@Usage >= 0)
								BEGIN
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@OldMeterReadingDate
										,@ReadBy
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
										,@OldAverage
										,CONVERT(DECIMAL(18,2),@Usage)
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
												WHERE GlobalAccountNumber = @AccountNo) + @Usage
										,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@OldMeterNo)
								END
							-- END Old MeterREading Taken and insert in to Customer Bills Table

							-- START NEW MeterREading Taken and insert in to Customer Bills Table

							--If New MeterNo assighned to that customer
							UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
							SET PresentReading = @NewMeterReading,
								InitialReading = @NewMeterInitialReading,
								IsCAPMI = @IsCAPMIMeter,
								MeterAmount = @CAPMIAmount
							WHERE GlobalAccountNumber = @AccountNo

							IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
								BEGIN		
									
									SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
									
									INSERT INTO Tbl_CustomerReadings
										(GlobalAccountNumber
										,ReadDate
										,[ReadBy]
										,PreviousReading
										,PresentReading
										,[AverageReading]
										,Usage
										,[TotalReadingEnergies]
										,[TotalReadings]
										,Multiplier
										,ReadType
										,[CreatedBy]
										,CreatedDate
										,[IsTamper]
										,MeterNumber)
									VALUES (@AccountNo
										,@NewMeterReadingDate
										,@ReadBy									
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
										,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
										,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
										,@NewMeterUsage
										,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
										,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
										,1
										,2
										,@ModifiedBy
										,dbo.fn_GetCurrentDateTime()
										,0
										,@NewMeterNo)
								END 
								
							IF(@IsCAPMIMeter = 1)  
							BEGIN  
								INSERT INTO Tbl_PaidMeterDetails  
								(  
								 AccountNo  
								,MeterNo  
								,MeterTypeId  
								,MeterCost  
								,OutStandingAmount
								,ActiveStatusId  
								,MeterAssignedDate  
								,CreatedDate  
								,CreatedBy  
								)  
								values  
								(  
								 @AccountNo  
								,@NewMeterNo  
								,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@NewMeterNo)  
								,@CAPMIAmount  
								,@CAPMIAmount  
								,1  
								,@OldMeterReadingDate
								,dbo.fn_GetCurrentDateTime()  
								,@ModifiedBy  
								)       
							END  
								
						END
					ELSE -- Not a final approval
						BEGIN
							UPDATE Tbl_CustomerMeterInfoChangeLogs 
							SET   -- Updating Request with Level Roles
								 ModifiedBy = @ModifiedBy
								,ModifiedDate = dbo.fn_GetCurrentDateTime()
								,PresentApprovalRole = @PresentRoleId
								,NextApprovalRole = @NextRoleId 
								,ApproveStatusId = (CASE ApproveStatusId WHEN 4 THEN 1 ELSE ApproveStatusId END) 
								,CurrentApprovalLevel = CurrentApprovalLevel+1
								,IsLocked = 1
							WHERE AccountNo = @AccountNo  
							AND MeterInfoChangeLogId = @MeterNoChangeLogId							
						END
				END
			ELSE 
				BEGIN -- No Approval Levels are there
					UPDATE Tbl_CustomerMeterInfoChangeLogs 
					SET   -- Updating Request with Level Roles & Approval status 
						 ModifiedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
						,PresentApprovalRole = @PresentRoleId
						,NextApprovalRole = @NextRoleId 
						,ApproveStatusId = @ApprovalStatusId
						,CurrentApprovalLevel= 0
						,IsLocked = 1
					WHERE AccountNo = @AccountNo 
					AND MeterInfoChangeLogId = @MeterNoChangeLogId  

					INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
						 MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate
						,IsCAPMIMeter
						,CAPMIAmount)
					SELECT  MeterInfoChangeLogId
						,AccountNo
						,OldMeterNo
						,NewMeterNo
						,OldMeterTypeId
						,NewMeterTypeId
						,OldDials
						,NewDials
						,CreatedBy
						,CreatedDate
						,ApproveStatusId
						,Remarks
						,OldMeterReading
						,NewMeterReading
						,MeterChangedDate
						,InitialBillingkWh
						,NewMeterInitialReading
						,NewMeterReadingDate
						,IsCAPMIMeter
						,CAPMIAmount
					FROM Tbl_CustomerMeterInfoChangeLogs
					WHERE MeterInfoChangeLogId = @MeterNoChangeLogId

					UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
					SET
						 MeterNumber = @NewMeterNo
						,ModifedBy = @ModifiedBy
						,ModifiedDate = dbo.fn_GetCurrentDateTime()
					WHERE GlobalAccountNumber = @AccountNo
					
					--START Old MeterREading Taken and insert in to Customer Bills Table

					--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					--SET
					--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
					--WHERE GlobalAccountNumber = @AccountNo

					IF (@Usage >= 0)
						BEGIN
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@OldMeterReadingDate
								,@ReadBy
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
								,@OldAverage
								,CONVERT(DECIMAL(18,2),@Usage)
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings 
										WHERE GlobalAccountNumber = @AccountNo) + @Usage
								,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@OldMeterNo)
						END
					-- END Old MeterREading Taken and insert in to Customer Bills Table

					-- START NEW MeterREading Taken and insert in to Customer Bills Table

					--If New MeterNo assighned to that customer
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET PresentReading = @NewMeterReading,
						InitialReading = @NewMeterInitialReading,
						IsCAPMI = @IsCAPMIMeter,
						MeterAmount = @CAPMIAmount
					WHERE GlobalAccountNumber = @AccountNo

					IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
						BEGIN		
							
							SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @NewMeterNo)
							
							INSERT INTO Tbl_CustomerReadings
								(GlobalAccountNumber
								,ReadDate
								,[ReadBy]
								,PreviousReading
								,PresentReading
								,[AverageReading]
								,Usage
								,[TotalReadingEnergies]
								,[TotalReadings]
								,Multiplier
								,ReadType
								,[CreatedBy]
								,CreatedDate
								,[IsTamper]
								,MeterNumber)
							VALUES (@AccountNo
								,@NewMeterReadingDate
								,@ReadBy									
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
								,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + CONVERT(DECIMAL(18,2),@NewMeterUsage))/2) -- New Average
								,@NewMeterUsage
								,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @NewMeterUsage
								,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
								,1
								,2
								,@ModifiedBy
								,dbo.fn_GetCurrentDateTime()
								,0
								,@NewMeterNo)
						END 
						
					IF(@IsCAPMIMeter = 1)  
						BEGIN  
							INSERT INTO Tbl_PaidMeterDetails  
							(  
							 AccountNo  
							,MeterNo  
							,MeterTypeId  
							,MeterCost  
							,OutStandingAmount
							,ActiveStatusId  
							,MeterAssignedDate  
							,CreatedDate  
							,CreatedBy  
							)  
							values  
							(  
							 @AccountNo  
							,@NewMeterNo  
							,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@NewMeterNo)  
							,@CAPMIAmount  
							,@CAPMIAmount  
							,1  
							,@OldMeterReadingDate
							,dbo.fn_GetCurrentDateTime()  
							,@ModifiedBy  
							)       
						END  
					
			END

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE
		END  
	ELSE  
		BEGIN  -- Request Not Approved
			IF(@ApprovalStatusId = 3) -- Request is Rejected
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId AND @ApprovalStatusId = 3)
						BEGIN -- Approval levels are there and status is rejected
							SET @CurrentLevel =(SELECT CurrentApprovalLevel FROM Tbl_CustomerMeterInfoChangeLogs 
																WHERE MeterInfoChangeLogId = @MeterNoChangeLogId)
							--SET @CurrentLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND
							--						RoleId = (SELECT NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
							--									WHERE MeterInfoChangeLogId = @MeterNoChangeLogId))

							SELECT @PresentRoleId = PresentRoleId, @NextRoleId = NextRoleId 
							FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,0,@BU_ID)
						END
				END
			ELSE IF(@ApprovalStatusId = 4) -- Request is Hold
				BEGIN
					IF EXISTS(SELECT 0 FROM TBL_FunctionalAccessPermission WHERE FunctionId = @FunctionId)
						SELECT @PresentRoleId = PresentApprovalRole,@NextRoleId = NextApprovalRole FROM Tbl_CustomerMeterInfoChangeLogs 
						WHERE MeterInfoChangeLogId = @MeterNoChangeLogId
				END

			-- If there are no Approval levels then Present and Next Approval roles will be updated as NULL			
			UPDATE Tbl_CustomerMeterInfoChangeLogs 
			SET   
				 ModifiedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
				,PresentApprovalRole = @PresentRoleId
				,NextApprovalRole = @NextRoleId 
				,ApproveStatusId = @ApprovalStatusId
				,Remarks = @Details
				,IsLocked=1
			WHERE AccountNo = @AccountNo  
			AND MeterInfoChangeLogId = @MeterNoChangeLogId

			SELECT 1 As IsSuccess  
			FOR XML PATH('ChangeBookNoBe'),TYPE  
		END  

END

GO

/****** Object:  StoredProcedure [dbo].[USP_ChangeCustomerMeterInformation]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:  V.Bhimaraju    
-- Create date: 07-10-2014    
-- Description: The purpose of this procedure is to Update Customer MeterInformation 
-- =============================================
ALTER PROCEDURE [dbo].[USP_ChangeCustomerMeterInformation]
(
@XmlDoc xml
)
AS
BEGIN
	DECLARE @AccountNo VARCHAR(50)
		,@ModifiedBy VARCHAR(50)
		,@MeterNo VARCHAR(100)
		,@MeterTypeId INT
		,@MeterDials INT
		,@Flag INT  
		,@Details VARCHAR(MAX)
		,@FunctionId INT
		,@PresentRoleId INT
		,@NextRoleId INT
		,@OldMeterReading VARCHAR(20)
		,@NewMeterReading VARCHAR(20)
		,@BU_ID VARCHAR(50)
		,@OldMeterNo VARCHAR(50)
		,@InitialBillingkWh INT
		,@OldMeterReadingDate DATETIME
		,@NewMeterReadingDate DATETIME
		,@NewMeterInitialReading VARCHAR(20)
		,@CurrentApprovalLevel INT
		,@IsFinalApproval BIT = 0
	SELECT
		 @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)')
		,@MeterNo=C.value('(MeterNo)[1]','VARCHAR(100)')
		,@MeterTypeId=C.value('(MeterTypeId)[1]','INT')
		,@MeterDials=C.value('(MeterDials)[1]','INT')
		,@Flag=C.value('(Flag)[1]','INT')
		,@ModifiedBy=C.value('(ModifiedBy)[1]','VARCHAR(50)')
		,@Details=C.value('(Details)[1]','VARCHAR(MAX)')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@OldMeterReading = C.value('(OldMeterReading)[1]','VARCHAR(20)')
		,@NewMeterReading = C.value('(InitialReading)[1]','VARCHAR(20)')
		,@NewMeterInitialReading = C.value('(NewMeterInitialReading)[1]','VARCHAR(20)')
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
		,@OldMeterNo = C.value('(OldMeterNo)[1]','VARCHAR(50)')
		,@OldMeterReadingDate = C.value('(OldMeterReadingDate)[1]','DATETIME')
		,@NewMeterReadingDate = C.value('(NewMeterReadingDate)[1]','DATETIME')
		,@InitialBillingkWh = C.value('(InitialBillingkWh)[1]','INT')
		,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	FROM @XmlDoc.nodes('ChangeBookNoBe') as T(C)
		
	DECLARE @PrvMeterNo VARCHAR(50)
	SET @PrvMeterNo = (SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails]
					WHERE GlobalAccountNumber = @AccountNo)
	
	SET @MeterDials=(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo AND ActiveStatusId = 1)
	
	DECLARE @PreviousReading DECIMAL(18,2)
	
	IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
		BEGIN
			SET @PreviousReading =(SELECT TOP 1 ISNULL(PresentReading,0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo ORDER BY CustomerReadingId DESC)
		END
	ELSE
		BEGIN
			SET @PreviousReading = (SELECT ISNULL(InitialReading,0) FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber = @AccountNo)
		END 
		
	
	DECLARE @IsCAPMIMeter BIT, @CAPMIAmount DECIMAL(18,2)
			
	SELECT @IsCAPMIMeter = ISNULL(IsCAPMIMeter,0), 
		@CAPMIAmount = ISNULL(CAPMIAmount,0),
		@MeterTypeId = MeterType
	FROM Tbl_MeterInformation 
	WHERE MeterNo = @MeterNo AND ActiveStatusId = 1	
		
	IF (NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo AND BU_ID=@BU_ID AND ActiveStatusId = 1)AND @MeterNo != '')  --AND BU_ID=@BU_ID
		BEGIN  
			SELECT 1 AS IsMeterNumberNotExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF(@MeterTypeId != 2)
		BEGIN  
			SELECT 1 AS IsNotCreditMeter FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE MeterNumber = @MeterNo AND GlobalAccountNumber != @AccountNo)
		BEGIN  
			SELECT 1 AS IsMeterNoExists FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE AccountNo = @AccountNo AND ApproveStatusId NOT IN (2,3))
		BEGIN  
			SELECT 1 AS IsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerMeterInfoChangeLogs WHERE NewMeterNo = @MeterNo AND ApproveStatusId NOT IN(2,3))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_AssignedMeterLogs WHERE MeterNo = @MeterNo AND ApprovalStatusId IN(1,4))
		BEGIN  
			SELECT 1 AS IsMeterNoAlreadyRaised FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_ReadToDirectCustomerActivityLogs WHERE GlobalAccountNo = @AccountNo AND ApprovalStatusId IN(1,4))
		BEGIN  
			SELECT 1 AS IsReadToDirectApproval FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF EXISTS(SELECT 0 FROM Tbl_CustomerReadingApprovalLogs WHERE GlobalAccountNumber = @AccountNo AND ApproveStatusId IN (1,4))
		BEGIN  
			SELECT 1 AS IsReadingsExist FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF((CONVERT(DECIMAL(18,2),@OldMeterReading) < @PreviousReading))
		BEGIN  
			SELECT 1 AS IsOldMeterReadingIsWrong FOR XML PATH('ChangeBookNoBe')  
		END
	ELSE IF ((SELECT ISNULL(OutStandingAmount,0) FROM Tbl_PaidMeterDetails WHERE AccountNo=@AccountNo AND MeterNo=@OldMeterNo AND ActiveStatusId = 1) > 0)
		BEGIN
			SELECT 1 AS IsHavingCapmiAmt FOR XML PATH('ChangeBookNoBe')  
		END	
	ELSE IF(@Flag = 1)
		BEGIN
			DECLARE @RoleId INT, @CurrentLevel INT

			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @ModifiedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			

			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@ModifiedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
						
						
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)

				END
				ELSE 
					BEGIN
						SET @IsFinalApproval=1
						SET @PresentRoleId=0
						SET @NextRoleId=0
						SET @CurrentLevel=0	
						SET @CurrentApprovalLevel =0
					END
			
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs( AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,PresentApprovalRole
				,NextApprovalRole
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,IsCAPMIMeter
				,CAPMIAmount
				)
			SELECT   GlobalAccountNumber
				,CASE @PrvMeterNo WHEN '' THEN NULL ELSE @PrvMeterNo END
				,@MeterNo
				,MI.MeterType--,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,MI.MeterType
				,MI.MeterDials--,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) 
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,1 --For Processing
				,@Details 
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,@IsCAPMIMeter
				,@CAPMIAmount
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo

			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END 
	ELSE
		BEGIN
			DECLARE @MeterInfoChangeLogId INT
		
			INSERT INTO Tbl_CustomerMeterInfoChangeLogs(AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,CurrentApprovalLevel
				,IsLocked
				,PresentApprovalRole
				,NextApprovalRole
				,IsCAPMIMeter
				,CAPMIAmount
				)
			SELECT   GlobalAccountNumber
				,(SELECT MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber=@AccountNo)
				,@MeterNo
				,(SELECT MeterType FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber)
				,@MeterTypeId
				,(SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = CD.MeterNumber) AS MeterDials
				,@MeterDials
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,2 -- For Approval
				,@Details
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
				,CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading)) END
				,@OldMeterReadingDate
				,@InitialBillingkWh
				,CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading)) END
				,@NewMeterReadingDate
				,@ModifiedBy
				,dbo.fn_GetCurrentDateTime()
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,@IsCAPMIMeter
				,@CAPMIAmount
			FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD
			--INNER JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
			WHERE GlobalAccountNumber = @AccountNo
			
			SET @MeterInfoChangeLogId = SCOPE_IDENTITY()
			
			INSERT INTO Tbl_Audit_CustomerMeterInfoChangeLogs(
				 MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,IsCAPMIMeter
				,CAPMIAmount)
			SELECT  MeterInfoChangeLogId
				,AccountNo
				,OldMeterNo
				,NewMeterNo
				,OldMeterTypeId
				,NewMeterTypeId
				,OldDials
				,NewDials
				,CreatedBy
				,CreatedDate
				,ApproveStatusId
				,Remarks
				,OldMeterReading
				,NewMeterReading
				,MeterChangedDate
				,InitialBillingkWh
				,NewMeterInitialReading
				,NewMeterReadingDate
				,ModifiedBy
				,ModifiedDate
				,IsCAPMIMeter
				,CAPMIAmount
			FROM Tbl_CustomerMeterInfoChangeLogs
			WHERE MeterInfoChangeLogId = @MeterInfoChangeLogId

			UPDATE [CUSTOMERS].[Tbl_CustomerProceduralDetails] 
			SET
				MeterNumber = @MeterNo
				,ModifedBy = @ModifiedBy
				,ModifiedDate = dbo.fn_GetCurrentDateTime()
			WHERE GlobalAccountNumber = @AccountNo
			--START Old MeterREading Taken and insert in to Customer Bills Table

			--UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			--SET
			--	InitialReading = @InitialBillingkWh, PresentReading = @NewMeterReading
			--WHERE GlobalAccountNumber = @AccountNo

			DECLARE  @Usage DECIMAL(18,2)
				,@PrvMeterDials INT
				,@ReadBy VARCHAR(50)
			SET @Usage =	CONVERT(DECIMAL(18,2),ISNULL(@OldMeterReading,0)) - ISNULL(@PreviousReading,0)
			SET @ReadBy = (SELECT TOP 1 ISNULL(ReadBy,'0') FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)
			SET @PrvMeterDials = (SELECT ISNULL(MeterDials,5) FROM Tbl_MeterInformation WHERE MeterNo = @PrvMeterNo)

			DECLARE @OldAverage VARCHAR(25)
			
			SELECT @OldAverage = (dbo.fn_GetAverageReading_Save(@AccountNo,@Usage))
	
			IF (@Usage >= 0)
				BEGIN
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)

					VALUES (@AccountNo
						,@OldMeterReadingDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@OldMeterReading))
						,@OldAverage
						,CONVERT(DECIMAL(18,2),@Usage)
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+@Usage
						,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@OldMeterNo)
				END
			-- END Old MeterREading Taken and insert in to Customer Bills Table

			-- START NEW MeterREading Taken and insert in to Customer Bills Table

			--If New MeterNo assighned to that customer
			UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
			SET PresentReading = CASE WHEN ISNULL(@NewMeterReading,'') = '' THEN 0 ELSE @NewMeterReading END,
				InitialReading = CASE WHEN ISNULL(@NewMeterInitialReading,'') = '' THEN 0 ELSE @NewMeterInitialReading END,
				IsCAPMI = @IsCAPMIMeter,
				MeterAmount = @CAPMIAmount
			WHERE GlobalAccountNumber = @AccountNo

			IF(@NewMeterReadingDate != '' AND @NewMeterReading != '')
				BEGIN		
					DECLARE @NewMeterUsage DECIMAL(18,2) = 0
					SET @NewMeterUsage = CONVERT(DECIMAL(18,2),@NewMeterReading) - CONVERT(DECIMAL(18,2),@NewMeterInitialReading)

					DECLARE @NewMeterDials INT		
					SET @NewMeterDials = (SELECT MeterDials FROM Tbl_MeterInformation WHERE MeterNo = @MeterNo)
					
					INSERT INTO Tbl_CustomerReadings
						(GlobalAccountNumber
						,ReadDate
						,[ReadBy]
						,PreviousReading
						,PresentReading
						,[AverageReading]
						,Usage
						,[TotalReadingEnergies]
						,[TotalReadings]
						,Multiplier
						,ReadType
						,[CreatedBy]
						,CreatedDate
						,[IsTamper]
						,MeterNumber)
					VALUES (@AccountNo
						,@NewMeterReadingDate
						,@ReadBy									
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterInitialReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@NewMeterReading))
						,CONVERT(DECIMAL(18,2),(CONVERT(DECIMAL(18,2),@OldAverage) + @NewMeterUsage)/2) -- New Average
						,@NewMeterUsage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM( Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)+@NewMeterUsage
						,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo)+1)
						,1
						,2
						,@ModifiedBy
						,dbo.fn_GetCurrentDateTime()
						,0
						,@MeterNo)
				END
			-- END NEW MeterREading Taken and insert in to Customer Bills Table
			
			IF(@IsCAPMIMeter = 1)  
				BEGIN  
					INSERT INTO Tbl_PaidMeterDetails  
					(  
					 AccountNo  
					,MeterNo  
					,MeterTypeId  
					,MeterCost  
					,OutStandingAmount
					,ActiveStatusId  
					,MeterAssignedDate  
					,CreatedDate  
					,CreatedBy  
					)  
					values  
					(  
					 @AccountNo  
					,@MeterNo  
					,(SELECT MeterType FROM Tbl_MeterInformation where MeterNo=@MeterNo)  
					,@CAPMIAmount  
					,@CAPMIAmount  
					,1  
					,@OldMeterReadingDate  
					,dbo.fn_GetCurrentDateTime()  
					,@ModifiedBy  
					)       
				END  
			
			SELECT 1 AS IsSuccess FOR XML PATH('ChangeBookNoBe')
		END
END



GO

/****** Object:  StoredProcedure [dbo].[USP_InsertBookwiseMeterReadings_WithDT]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 04-05-2015
-- This is to insert the meter readings bookwise bulk into logs 
-- =============================================
ALTER PROCEDURE  [dbo].[USP_InsertBookwiseMeterReadings_WithDT]
(
	 @MeterReadingFrom INT
	,@CreateBy varchar(50)
	,@BU_ID VARCHAR(50)
	,@IsFinalApproval BIT
	,@FunctionId INT
	,@ActiveStatusId INT
	,@TempReadings TblMeterReadingsUpload READONLY
)	
AS
BEGIN
	DECLARE 
		 @ReadDate varchar(50)
		,@ReadBy varchar(50)
		,@Multiple int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@IsSuccess BIT = 0

	DECLARE @TotalReadings VARCHAR(50), @AverageReading VARCHAR(50),
			@PresentReading VARCHAR(50), @Usage VARCHAR(50), @AccountNo VARCHAR(50),
			@IsTamper BIT, @PreviousReading VARCHAR(50), @IsExists BIT,
			@MeterNumber VARCHAR(50), @Dials INT 
		,@CurrentApprovalLevel INT
				
	DECLARE @SNo INT, @TotalCount INT
	SET @SNo = 1
	
	SELECT @TotalCount = COUNT(0) FROM @TempReadings
	
	WHILE(@SNo <= @TotalCount)
		BEGIN
		
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreateBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level			
			
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
					DECLARE @UserDetailedId INT
					SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreateBy)
				
					IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
							BEGIN
									SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																				CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																						ELSE NULL END
																			END
																			 FROM TBL_FunctionApprovalRole 
																			WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
							END
						ELSE
							BEGIN
								
								SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
							END
							
					DECLARE @Forward INT
					SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
						END 
												
					SELECT @PresentRoleId = PresentRoleId 
						,@NextRoleId = NextRoleId 
					FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
		
				END
			ELSE
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END	
				
			 SELECT 
				 @TotalReadings = TotalReadings
				,@AverageReading = AverageReading
				,@PresentReading = PresentReading
				,@Usage = ISNULL(Usage,0)
				,@AccountNo = AccNum
				,@IsTamper = IsTamper
				,@PreviousReading = PreviousReading
				,@IsExists = IsExists
				,@ReadBy = ReadBy
				,@ReadDate = ReadDate
				,@Multiple = Multiplier
			 FROM @TempReadings WHERE SNo = @SNo
			 
			IF(@IsExists = 1)
				BEGIN
					DECLARE @ReadId INT
					Declare @IsExistingRollOver bit
					
					SELECT TOP(1) @ReadId = CustomerReadingLogId,@IsExistingRollOver=IsRollOver
					FROM Tbl_CustomerReadingApprovalLogs
					WHERE GlobalAccountNumber = @AccountNo
					ORDER BY CustomerReadingLogId DESC
						 
					IF @IsExistingRollOver = 1
					BEGIN
						DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId=   @ReadId or CustomerReadingLogId=@ReadId -1
					END
					ELSE
					BEGIN
						DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId =   @ReadId    
					END	
				END
			 
			SELECT @MeterNumber = MeterNumber FROM CUSTOMERS.Tbl_CustomerProceduralDetails WHERE GlobalAccountNumber = @AccountNo 
			
			SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccountNo,@Usage)
			
			SET @Usage = CONVERT(NUMERIC(20,4), @PresentReading) - CONVERT(NUMERIC(20,4), @PreviousReading)
				
			INSERT INTO Tbl_CustomerReadingApprovalLogs(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver
				,ApproveStatusId
				,PresentApprovalRole
				,NextApprovalRole
				,CurrentApprovalLevel
				,IsLocked)		
			VALUES(
				 @AccountNo
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
				,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,0
				,(CASE WHEN @IsFinalApproval = 1 THEN 2 ELSE @ActiveStatusId END)
				,(CASE WHEN @IsFinalApproval = 1 THEN 0 ELSE @PresentRoleId END)
				,(CASE WHEN @IsFinalApproval = 1 THEN 0 ELSE @NextRoleId END)
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
				
			IF(@IsFinalApproval = 1)
				BEGIN
				
					INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver
						,ApproveStatusId
						,PresentApprovalRole
						,NextApprovalRole)
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreateBy
						,GETDATE()
						,@IsTamper
						,@MeterNumber
						,@MeterReadingFrom
						,0
						,@ActiveStatusId
						,(CASE WHEN @PresentRoleId = 0 THEN @RoleId ELSE @PresentRoleId END)
						,(CASE WHEN @NextRoleId = 0 THEN @RoleId ELSE @NextRoleId END))
				
					INSERT INTO Tbl_CustomerReadings(
						 GlobalAccountNumber
						,[ReadDate]
						,[ReadBy]
						,[PreviousReading]
						,[PresentReading]
						,[AverageReading]
						,[Usage]
						,[TotalReadingEnergies]
						,[TotalReadings]
						,[Multiplier]
						,[ReadType]
						,[CreatedBy]
						,[CreatedDate]
						,[IsTamper]
						,MeterNumber
						,[MeterReadingFrom]
						,IsRollOver)	
					VALUES(
						 @AccountNo
						,@ReadDate
						,@ReadBy
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,@AverageReading
						,@Usage
						,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + @Usage
						,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo) + 1)
						,@Multiple
						,2
						,@CreateBy
						,GETDATE()
						,@IsTamper
						,@MeterNumber
						,@MeterReadingFrom
						,0
						)	
						
					UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
					SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PreviousReading))
						,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@PresentReading))
						,AvgReading = CONVERT(NUMERIC,@AverageReading) 
					WHERE GlobalAccountNumber = @AccountNo
				END
				
			SET @IsSuccess = 1
			SET @SNo = @SNo + 1 
			SET @TotalReadings = NULL
			SET @AverageReading = NULL
			SET @PresentReading = NULL
			SET @Usage = NULL
			SET @AccountNo = NULL
			SET @IsTamper = NULL
			SET @PreviousReading = NULL
			SET @IsExists = NULL
			SET @MeterNumber = NULL
			SET @Dials = NULL
			SET @ReadBy = NULL
			SET @ReadDate = NULL
			SET @Multiple = NULL
			
		END
	
	SELECT @IsSuccess AS IsSuccess 
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingAdjustment]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 13 July 2015
-- Description: TO insert the customer meter readings Adjustment into the Readings  logs table
-- =============================================   
ALTER PROCEDURE [dbo].[USP_InsertCustomerMeterReadingAdjustment]
(
	@XmlDoc Xml
)	
AS
BEGIN	
	
	DECLARE  @GlobalAccountNumber VARCHAR(50)
			,@CreatedBy varchar(50)
			,@PreviousReading VARCHAR(50)
			,@PresentReading VARCHAR(50)
			,@ModifiedReading VARCHAR(50)
			,@AverageReading VARCHAR(50)
			,@Usage numeric(20,4)
			,@MeterNumber VARCHAR(50)
			,@Multipiler INT
			,@ReadBy INT
			,@Count INT
			,@TotalReadingEnergies DECIMAL(18,2)
			,@ApprovalStatusId INT  
			,@IsFinalApproval BIT = 0
			,@FunctionId INT
			,@BU_ID VARCHAR(50)
			,@CurrentApprovalLevel INT
	
	SELECT 
	 @GlobalAccountNumber = C.value('(GlobalAccountNumber)[1]','VARCHAR(50)')
	,@CreatedBy = C.value('(CreatedBy)[1]','varchar(50)')
	,@PreviousReading = C.value('(PreviousReading)[1]','VARCHAR(50)')
	,@PresentReading = C.value('(PresentReading)[1]','VARCHAR(50)')
	,@ModifiedReading = C.value('(CurrentReading)[1]','VARCHAR(50)')
	,@MeterNumber = C.value('(MeterNo)[1]','VARCHAR(50)')
	,@ApprovalStatusId=C.value('(ApproveStatusId)[1]','INT')
	,@IsFinalApproval = C.value('(IsFinalApproval)[1]','BIT')  
	,@FunctionId = C.value('(FunctionId)[1]','INT')   
	,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	-- This is for Adjusting Initilal Billingkwh 
	IF((SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)=0)
	BEGIN
		
		UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
		SET InitialReading=@ModifiedReading
			,PresentReading=@ModifiedReading
		WHERE GlobalAccountNumber=@GlobalAccountNumber
		
		SELECT 1 AS IsInitialBillingkWh
		FOR XML PATH('BillingBE'),TYPE
	END	
	
	-- This is for Adjusting Present Meter Reading
	
	ELSE IF((SELECT COUNT(0)FROM Tbl_PresentReadingAdjustmentApprovalLog 
				WHERE GlobalAccountNumber = @GlobalAccountNumber AND ApprovalStatusId IN(1,4)) > 0)
		BEGIN
			SELECT 1 AS IsApprovalInProcess FOR XML PATH('BillingBE')
		END   
	ELSE
		BEGIN
	
			DECLARE @RoleId INT, @CurrentLevel INT
					,@PresentRoleId INT, @NextRoleId INT
			
			SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreatedBy) -- Approving User RoleId
			SET @CurrentLevel = 0 -- For Stating Level	
	
			IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND IsActive = 1 AND BU_ID=@BU_ID)
				BEGIN
							DECLARE @UserDetailedId INT
							SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreatedBy)
						
							IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
									BEGIN
											SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																						CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																								ELSE NULL END
																					END
																					 FROM TBL_FunctionApprovalRole 
																					WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
									END
								ELSE
									BEGIN
										SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
									END
				
				DECLARE @Forward INT
				SET @Forward=1
					IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
						BEGIN
								SET @CurrentLevel=1	
								SET @CurrentApprovalLevel =1
								SET @Forward=0
						END
						ELSE
						BEGIN
							SET @CurrentApprovalLevel = @CurrentLevel+1
					END 
					
					
				SELECT @PresentRoleId = PresentRoleId 
					,@NextRoleId = NextRoleId 
				FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
				
				END
			ELSE 
				BEGIN
					SET @IsFinalApproval=1
					SET @PresentRoleId=0
					SET @NextRoleId=0
					SET @CurrentLevel=0	
					SET @CurrentApprovalLevel =0
				END				
					
				IF(CONVERT(NUMERIC(20,4), @ModifiedReading) > CONVERT(NUMERIC(20,4), @PresentReading))
					BEGIN
						SELECT 1 AS IsMaxReading FOR XML PATH('BillingBE'),TYPE 
					END
				ELSE IF(CONVERT(NUMERIC(20,4), @ModifiedReading) < CONVERT(NUMERIC(20,4), @PreviousReading))
					BEGIN
						SELECT 1 AS IsMinReading FOR XML PATH('BillingBE'),TYPE 
					END
				ELSE IF ((SELECT TOP 1 IsBilled FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber ORDER BY CustomerReadingId DESC)=0)
					BEGIN
						SELECT 1 AS IsBilled FOR XML PATH('BillingBE'),TYPE 
					END
				ELSE
					BEGIN
					
					DECLARE @PreviousReadDate DATETIME
					SELECT TOP 1 @PreviousReadDate=ReadDate FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber
					ORDER BY CustomerReadingId DESC
					
					INSERT INTO Tbl_PresentReadingAdjustmentApprovalLog(
							 GlobalAccountNumber 
							,PreviousReading 
							,PresentReading 
							,NewPresentReading 
							,PreviousReadDate 
							,PresentReadDate 
							,MeterNumber 
							,Remarks 
							,ApprovalStatusId 
							,PresentApprovalRole 
							,NextApprovalRole 
							,CurrentAppovalLevel 
							,IsLocked
							,CreatedBy 
							,CreatedDate 
							,ModifiedBy 
							,ModifiedDate)
						SELECT 
							 @GlobalAccountNumber
							,@PreviousReading 
							,@PresentReading 
							,@ModifiedReading 
							,@PreviousReadDate 
							,dbo.fn_GetCurrentDateTime() 
							,@MeterNumber 
							,NULL 
							,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ApprovalStatusId END 
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END 
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END 
							,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END 
							,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END
							,@CreatedBy 
							,dbo.fn_GetCurrentDateTime()
							,@CreatedBy 
							,dbo.fn_GetCurrentDateTime()
					
					IF(@IsFinalApproval = 1)
						BEGIN
					
						SET @Usage = CONVERT(NUMERIC(20,4), @ModifiedReading) - CONVERT(NUMERIC(20,4), @PreviousReading)			
						
						SET @Count=(SELECT COUNT(0) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber)
						
						IF (@Count=1)
							BEGIN
								SET @TotalReadingEnergies=@Usage
							END
						ELSE
							BEGIN
								
								DECLARE @Last2ndTotalReadingEnergies DECIMAL(18,2)
								
								SELECT @Last2ndTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
								WHERE TotalReadings =(SELECT MAX(TotalReadings)-1
																FROM Tbl_CustomerReadings
																WHERE GlobalAccountNumber=@GlobalAccountNumber)
								AND GlobalAccountNumber=@GlobalAccountNumber
								
								--SELECT @Last1stTotalReadingEnergies=TotalReadingEnergies FROM Tbl_CustomerReadings 
								--WHERE TotalReadings =(SELECT MAX(TotalReadings)
								--								FROM Tbl_CustomerReadings
								--								WHERE GlobalAccountNumber=@GlobalAccountNumber)
								--AND GlobalAccountNumber=@GlobalAccountNumber
								
								UPDATE Tbl_CustomerReadings
								SET IsReadingWrong = 1
								WHERE TotalReadings =(SELECT MAX(TotalReadings)
																FROM Tbl_CustomerReadings
																WHERE GlobalAccountNumber=@GlobalAccountNumber)
								AND GlobalAccountNumber = @GlobalAccountNumber

								SET @TotalReadingEnergies= @Last2ndTotalReadingEnergies+@Usage
							END
						
						SELECT @Multipiler=ISNULL(MeterMultiplier,1) FROM Tbl_MeterInformation WHERE MeterNo=@MeterNumber
						SELECT @ReadBy= MarketerId FROM Tbl_BookNumbers WHERE BookNo=(SELECT BookNo FROM UDV_CustomerDescription 
																						WHERE GlobalAccountNumber=@GlobalAccountNumber)
						
						--DECLARE @PreviousReadDate DATETIME
						--SELECT TOP 1 @PreviousReadDate=ReadDate FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber
						--ORDER BY CustomerReadingId DESC
						
						
						INSERT INTO Tbl_CustomerReadings(
								 GlobalAccountNumber
								,[ReadDate]
								,[ReadBy]
								,[PreviousReading]
								,[PresentReading]
								--,[AverageReading]
								,[Usage]
								,[TotalReadingEnergies]
								,[TotalReadings]
								,[Multiplier]
								,[ReadType]
								,[CreatedBy]
								,[CreatedDate]
								--,[IsTamper]
								,MeterNumber
								,[MeterReadingFrom]
								--,IsReadingWrong
								--,IsRollOver
								)
							VALUES(
								 @GlobalAccountNumber
								,GETDATE()
								,@ReadBy
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
								,CONVERT(VARCHAR(50),CONVERT(BIGINT,@ModifiedReading))
								--,@AverageReading
								,0
								--,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + @Usage
								,@TotalReadingEnergies
								,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @GlobalAccountNumber) + 1)
								,@Multipiler
								,2
								,@CreatedBy
								,GETDATE()
								--,@IsTamper
								,@MeterNumber
								,(SELECT MeterReadingFromId FROM MASTERS.Tbl_MMeterReadingsFrom WHERE MeterReadingFrom='Current Reading Adjustment')
								--,@IsRollover
								--,1
								)
							
						SELECT @AverageReading = dbo.fn_GetAverageReading_Adjustment(@GlobalAccountNumber)
						
						UPDATE Tbl_CustomerReadings
						SET AverageReading = @AverageReading
						WHERE TotalReadings =(SELECT MAX(TotalReadings)
														FROM Tbl_CustomerReadings
														WHERE GlobalAccountNumber=@GlobalAccountNumber)
						AND GlobalAccountNumber = @GlobalAccountNumber
									
						INSERT INTO Tbl_PresentReadingAdjustmentLog
								(
									 GlobalAccountNumber
									,PreviousReading 
									,PresentReading 
									,NewPresentReading 
									,PreviousReadDate 
									,PresentReadDate 
									,MeterNumber 
									,CreatedBy 
									,CreatedDate
								)
						VALUES	(
									@GlobalAccountNumber
									,@PreviousReading
									,@PresentReading
									,@ModifiedReading
									,@PreviousReadDate
									,dbo.fn_GetCurrentDateTime()
									,@MeterNumber
									,@CreatedBy
									,dbo.fn_GetCurrentDateTime()
								)
						
						UPDATE CUSTOMERS.Tbl_CustomerActiveDetails
						SET InitialReading=@ModifiedReading
							,PresentReading=@ModifiedReading
						WHERE GlobalAccountNumber=@GlobalAccountNumber
						
					END
					
					SELECT 1 AS IsSuccess	
					FOR XML PATH('BillingBE'),TYPE 	
					END
	END
END

GO

/****** Object:  StoredProcedure [dbo].[USP_InsertCustomerMeterReadingLogs]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  Karteek
-- Create date: 04 may 2015
-- Description: TO insert the customer meter readings into the logs table
-- =============================================   
ALTER PROCEDURE [dbo].[USP_InsertCustomerMeterReadingLogs]
(
	@XmlDoc Xml
)	
AS
BEGIN
	
	DECLARE @ReadDate datetime
		,@ReadBy varchar(50)
		,@Previous VARCHAR(50)
		,@Current  VARCHAR(50)
		,@Usage numeric(20,4)
		,@CreateBy varchar(50)
		,@AccNum varchar(50)
		,@IsTamper BIT
		,@CustUn varchar(50)
		--
		,@MeterReadingFrom INT
		,@Multiple numeric(20,4)
		,@MeterNumber VARCHAR(50)
		,@AverageReading VARCHAR(50)
		,@IsRollover bit=0
		,@Dials Int
		,@MaxDialsValue bigint='999999999999'
		,@MinDialsValue varchar(15)='0000000000'
		,@FirstPrevReading varchar(50)
		,@FirstPresentValue varchar(50)
		,@SecoundReadingPrevReading varchar(20)
		
		,@IsFinalApproval BIT
		,@FunctionId INT
		,@ActiveStatusId INT
		,@IsExists BIT
		,@BU_ID VARCHAR(50) 
		,@CurrentApprovalLevel INT
	
	SELECT @ReadDate = C.value('(ReadDate)[1]','datetime')
		,@ReadBy = C.value('(ReadBy)[1]','VARCHAR(50)')
		,@Previous = C.value('(PreviousReading)[1]','VARCHAR(50)')
		,@Current = C.value('(PresentReading)[1]','VARCHAR(50)')
		,@Usage = CONVERT(NUMERIC(20,4),C.value('(Usage)[1]','VARCHAR(50)'))
		,@CreateBy = C.value('(CreatedBy)[1]','varchar(50)')
		,@AccNum = C.value('(AccNum)[1]','varchar(50)')
		,@IsTamper = C.value('(IsTamper)[1]','BIT')
		,@MeterReadingFrom = C.value('(MeterReadingFrom)[1]','INT')
		,@IsRollover=C.value('(Rollover)[1]','bit')
		,@IsFinalApproval=C.value('(IsFinalApproval)[1]','bit')
		,@FunctionId = C.value('(FunctionId)[1]','INT')
		,@ActiveStatusId = C.value('(ActiveStatusId)[1]','INT')
		,@IsExists = C.value('(IsExists)[1]','BIT')  
		,@BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingBE') AS T(C)
	
	DECLARE @RoleId INT, @CurrentLevel INT
			,@PresentRoleId INT, @NextRoleId INT
			
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @CreateBy) -- Approving User RoleId
	SET @CurrentLevel = 0 -- For Stating Level			
	
	IF EXISTS(SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND IsActive = 1)
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@CreateBy)
		
			IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
					BEGIN
							SET @CurrentLevel = (SELECT CASE WHEN UserIds IS NULL THEN [Level] ELSE
																		CASE WHEN EXISTS (SELECT 0 FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,','))) THEN [Level]
																				ELSE NULL END
																	END
																	 FROM TBL_FunctionApprovalRole 
																	WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
					END
				ELSE
					BEGIN
						
						SET @CurrentLevel=(SELECT [Level] FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
					END
					
			DECLARE @Forward INT
			SET @Forward=1
			IF(@CurrentLevel=0 OR @CurrentLevel IS NULL)
				BEGIN
						SET @CurrentLevel=1	
						SET @CurrentApprovalLevel =1
						SET @Forward=0
				END
				ELSE
				BEGIN
					SET @CurrentApprovalLevel = @CurrentLevel+1
				END 
										
			SELECT @PresentRoleId = PresentRoleId 
				,@NextRoleId = NextRoleId 
			FROM dbo.fn_GetCurrentApprovalRoles(@FunctionId,@CurrentLevel,@Forward,@BU_ID)
	
		END
	ELSE
		BEGIN
			SET @IsFinalApproval=1
			SET @PresentRoleId=0
			SET @NextRoleId=0
			SET @CurrentLevel=0	
			SET @CurrentApprovalLevel =0
		END
			
	SELECT @Multiple = MI.MeterMultiplier
		,@MeterNumber = CPD.MeterNumber
		,@Dials=MI.MeterDials 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_MeterInformation MI ON MI.MeterNo = CPD.MeterNumber AND CPD.GlobalAccountNumber = @AccNum
	
	IF(@IsExists = 1)
		BEGIN
			DECLARE @ReadId INT
			Declare @IsExistingRollOver bit
			
			SELECT TOP(1) @ReadId = CustomerReadingLogId,@IsExistingRollOver=IsRollOver
			FROM Tbl_CustomerReadingApprovalLogs
			WHERE GlobalAccountNumber = @AccNum
			ORDER BY CustomerReadingLogId DESC
				 
			IF @IsExistingRollOver = 1
			BEGIN
				DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId=   @ReadId or CustomerReadingLogId=@ReadId -1
			END
			ELSE
			BEGIN
				DELETE FROM	   Tbl_CustomerReadingApprovalLogs where   CustomerReadingLogId =   @ReadId    
			END	
		END
	
	SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@Usage)
	
	SET @Usage = CONVERT(NUMERIC(20,4), @Current) - CONVERT(NUMERIC(20,4), @Previous)
	
	 Declare @FirstReadingUsage bigint =@Usage
	 Declare @SecountReadingUsage Bigint =0
	 SET @FirstPrevReading   = @Previous
	 SET @FirstPresentValue    =	 @Current
	 
	IF @IsRollover=1 
		BEGIN
		  SET @MaxDialsValue=Left(@MaxDialsValue,@dials);
		  SET @FirstReadingUsage=convert(bigint,@MaxDialsValue) - convert(bigint,@Previous)
		  --SET @SecountReadingUsage=case when @FirstReadingUsage=0 then 0 else  convert(bigint,@Current+1) end
		  SET @SecountReadingUsage=convert(bigint,@Current+1) 
		  SET @Usage=@FirstReadingUsage
		  --SET @SecountReadingUsage = @Current
		  SET @FirstPresentValue=  @MaxDialsValue
		  SET @SecoundReadingPrevReading =	Left(@MinDialsValue,@dials);		  
		END
		
		--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@FirstReadingUsage)
		--IF	@Usage > 0
	
			INSERT INTO Tbl_CustomerReadingApprovalLogs(
				 GlobalAccountNumber
				,[ReadDate]
				,[ReadBy]
				,[PreviousReading]
				,[PresentReading]
				,[AverageReading]
				,[Usage]
				,[TotalReadingEnergies]
				,[TotalReadings]
				,[Multiplier]
				,[ReadType]
				,[CreatedBy]
				,[CreatedDate]
				,[IsTamper]
				,MeterNumber
				,[MeterReadingFrom]
				,IsRollOver
				,ApproveStatusId
				,PresentApprovalRole
				,NextApprovalRole
				,CurrentApprovalLevel
				,IsLocked)	
			VALUES(
				 @AccNum
				,@ReadDate
				,@ReadBy
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
				,CONVERT(VARCHAR(50),CONVERT(BIGINT,@FirstPresentValue))
				,@AverageReading
				,@Usage
				,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
				,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
				,@Multiple
				,2
				,@CreateBy
				,GETDATE()
				,@IsTamper
				,@MeterNumber
				,@MeterReadingFrom
				,@IsRollover
				,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
				,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
				,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
				

			IF(@IsRollover=1)
				BEGIN
		 
					--IF @SecountReadingUsage != 0
					--	BEGIN
						--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
						 --IF	 @SecountReadingUsage > 0
										 INSERT INTO Tbl_CustomerReadingApprovalLogs(
											 GlobalAccountNumber
											,[ReadDate]
											,[ReadBy]
											,[PreviousReading]
											,[PresentReading]
											,[AverageReading]
											,[Usage]
											,[TotalReadingEnergies]
											,[TotalReadings]
											,[Multiplier]
											,[ReadType]
											,[CreatedBy]
											,[CreatedDate]
											,[IsTamper]
											,MeterNumber
											,[MeterReadingFrom]
											,IsRollOver
											,ApproveStatusId
											,PresentApprovalRole
											,NextApprovalRole
											,CurrentApprovalLevel
											,IsLocked)	
										VALUES(
											 @AccNum
											,@ReadDate
											,@ReadBy
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@SecoundReadingPrevReading))
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
											,@AverageReading
											,@SecountReadingUsage
											,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
											,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum))
											,@Multiple
											,2
											,@CreateBy
											,GETDATE()
											,@IsTamper
											,@MeterNumber
											,@MeterReadingFrom
											,@IsRollover
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @CurrentApprovalLevel END
											,CASE WHEN @IsFinalApproval=1 THEN 1 ELSE 0 END)
											
						--END
				END
				
		IF(@IsFinalApproval = 1)
			BEGIN		
			
				INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					,IsRollOver
					,ApproveStatusId
					,PresentApprovalRole
					,NextApprovalRole)	
				VALUES(
					@AccNum
					,@ReadDate
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@FirstPresentValue))
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
					,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
					,@Multiple
					,2
					,@CreateBy
					,GETDATE()
					,@IsTamper
					,@MeterNumber
					,@MeterReadingFrom
					,@IsRollover
					,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
					,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END)	
				
				INSERT INTO Tbl_CustomerReadings(
					 GlobalAccountNumber
					,[ReadDate]
					,[ReadBy]
					,[PreviousReading]
					,[PresentReading]
					,[AverageReading]
					,[Usage]
					,[TotalReadingEnergies]
					,[TotalReadings]
					,[Multiplier]
					,[ReadType]
					,[CreatedBy]
					,[CreatedDate]
					,[IsTamper]
					,MeterNumber
					,[MeterReadingFrom]
					,IsRollOver)
				VALUES(
					 @AccNum
					,@ReadDate
					,@ReadBy
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
					,CONVERT(VARCHAR(50),CONVERT(BIGINT,@FirstPresentValue))
					,@AverageReading
					,@Usage
					,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @Usage
					,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + 1)
					,@Multiple
					,2
					,@CreateBy
					,GETDATE()
					,@IsTamper
					,@MeterNumber
					,@MeterReadingFrom
					,@IsRollover)
					

				IF(@IsRollover=1)
					BEGIN
			 
						--IF @SecountReadingUsage != 0
						--	BEGIN
							--SELECT @AverageReading = dbo.fn_GetAverageReading_New(@AccNum,@SecountReadingUsage+1)
							 --IF	 @SecountReadingUsage > 0
								-- BEGIN
									INSERT INTO Tbl_CustomerReadings(
												 GlobalAccountNumber
												,[ReadDate]
												,[ReadBy]
												,[PreviousReading]
												,[PresentReading]
												,[AverageReading]
												,[Usage]
												,[TotalReadingEnergies]
												,[TotalReadings]
												,[Multiplier]
												,[ReadType]
												,[CreatedBy]
												,[CreatedDate]
												,[IsTamper]
												,MeterNumber
												,[MeterReadingFrom]
												,IsRollOver)
											VALUES(
												 @AccNum
												,@ReadDate
												,@ReadBy
												,CONVERT(VARCHAR(50),CONVERT(BIGINT,@SecoundReadingPrevReading))
												,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
												,@AverageReading
												,@SecountReadingUsage
												,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
												,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum))
												,@Multiple
												,2
												,@CreateBy
												,GETDATE()
												,@IsTamper
												,@MeterNumber
												,@MeterReadingFrom
												,@IsRollover)
												
										INSERT INTO Tbl_Audit_CustomerReadingApprovalLogs(
											 GlobalAccountNumber
											,[ReadDate]
											,[ReadBy]
											,[PreviousReading]
											,[PresentReading]
											,[AverageReading]
											,[Usage]
											,[TotalReadingEnergies]
											,[TotalReadings]
											,[Multiplier]
											,[ReadType]
											,[CreatedBy]
											,[CreatedDate]
											,[IsTamper]
											,MeterNumber
											,[MeterReadingFrom]
											,IsRollOver
											,ApproveStatusId
											,PresentApprovalRole
											,NextApprovalRole)	
										VALUES(
											 @AccNum
											,@ReadDate
											,@ReadBy
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@SecoundReadingPrevReading))
											,CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
											,@AverageReading
											,@SecountReadingUsage
											,(SELECT case when SUM(Usage) IS NULL then 0 else SUM(Usage) end FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum) + @SecountReadingUsage
											,((SELECT MAX(TotalReadings) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccNum))
											,@Multiple
											,2
											,@CreateBy
											,GETDATE()
											,@IsTamper
											,@MeterNumber
											,@MeterReadingFrom
											,@IsRollover
											,CASE WHEN @IsFinalApproval=1 THEN 2 ELSE @ActiveStatusId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @PresentRoleId END
											,CASE WHEN @IsFinalApproval=1 THEN 0 ELSE @NextRoleId END)
								END												
							--END
					--END
					
			 
				UPDATE CUSTOMERS.Tbl_CustomerActiveDetails 
				SET InitialReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@Previous))
					,PresentReading = CONVERT(VARCHAR(50),CONVERT(BIGINT,@Current))
					,AvgReading = CONVERT(NUMERIC,@AverageReading) 
				WHERE GlobalAccountNumber = @AccNum
			END
		
	SELECT 1 AS IsSuccess FOR XML PATH('BillingBE')
		
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetEstimationSettings_CycleWiseData_BySC]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Karteek
-- Create date: 14-07-2015
-- Description:	The purpose of this procedure is to get Cycles list by SC
-- =============================================
ALTER PROCEDURE [dbo].[USP_GetEstimationSettings_CycleWiseData_BySC]
(
	@XmlDoc xml
)
AS
BEGIN

	DECLARE @Year INT      
        ,@Month int                
        ,@ServiceCenterId VARCHAR(50)
        ,@BillingRule INT
        
    DECLARE @Capacity DECIMAL(18,2),
		@SUPPMConsumption DECIMAL(18,2),
		@SUCreditConsumption DECIMAL(18,2)    
	 
	Declare @CustomerStatus varchar(max) = '1,2'	
	Declare @CycleIds varchar(max) 
	
	SELECT                      
		   @Year = C.value('(Year)[1]','INT'),              
		   @Month = C.value('(Month)[1]','INT'),      
		   @ServiceCenterId = C.value('(ServiceCenterId)[1]','VARCHAR(50)'),	
		   @BillingRule = C.value('(BillingRule)[1]','INT')
	 FROM @XmlDoc.nodes('EstimationBE') as T(C) 
	 
	SELECT @CycleIds = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
			 FROM Tbl_Cycles
			 WHERE ActiveStatusId = 1
			 AND ServiceCenterId = @ServiceCenterId
			 FOR XML PATH(''), TYPE)
			.value('.','NVARCHAR(MAX)'),1,1,'')
	
	SELECT CR.GlobalAccountNumber,SUM(isnull(Usage,0))	as Usage
		,ClusterCategoryId
	INTO #ReadCustomersList
	FROM Tbl_CustomerReadings CR
	INNER JOIN Customers.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber and IsBilled=0
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo	
	AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))  	 
	GROUP BY CR.GlobalAccountNumber,ClusterCategoryId
	
	SELECT Top 1 @Capacity = Capacity,
		@SUPPMConsumption = SUPPMConsumption,
		@SUCreditConsumption = SUCreditConsumption 
	FROM Tbl_ConsumptionDelivered(NOLOCK)
	WHERE SU_ID In(SELECT DISTINCT TOP 1 SU_ID FROM Tbl_ServiceCenter(NOLOCK) WHERE ServiceCenterId=@ServiceCenterID)
	
	SELECT DISTINCT
		 ClassID
		,TC.ClassName
		,CC.CategoryName
		,CC.ClusterCategoryId 
		,(CASE WHEN Cycles.CycleCode IS NULL THEN Cycles.CycleName ELSE Cycles.CycleName+' ('+Cycles.CycleCode+')' END) AS Cycle
		,Cycles.CycleId 
		,Cycles.CycleName
		,CPD.CustomerTypeId
		,CSD.ActiveStatusId
		,BDB.IsActive DisabledActiveStatus
		,BDB.DisableTypeId
		,BDB.IsPartialBill
		,ReadCodeID	  
		,CPD.GlobalAccountNumber CustomerGlobalAccNo
		,CR.GlobalAccountNumber ReadingGlobalAccNo
		,DCAVG.GlobalAccountNumber AvgGlobalAccNo
		,CR.Usage
		,CAD.AvgReading CustomerAvgReading
		,CAD.InitialBillingKWh  
		,DCAVG.AverageReading UploadedAvgReading
		,ES.EnergytoCalculate
	INTO #ReaultData
	FROM Customers.Tbl_CustomerProceduralDetails(NOLOCK) CPD 	  
	INNER JOIN Tbl_MTariffClasses(NOLOCK) TC ON CPD.TariffClassID=TC.ClassID 
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber
	INNER JOIN CUSTOMERS.Tbl_CustomersDetail (NOLOCK) CSD ON CSD.GlobalAccountNumber=CPD.GlobalAccountNumber and CSD.ActiveStatusId In (Select com From dbo.fn_Split(@CustomerStatus,',')) 	 --Need To Check once all completed
	INNER JOIN Tbl_BookNumbers(NOLOCK) BN ON BN.BookNo=CPD.BookNo AND BN.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
	INNER JOIN Tbl_Cycles(NOLOCK) Cycles On	Cycles.ServiceCenterId = @ServiceCenterId and Cycles.CycleId = BN.CycleId  
	INNER JOIN MASTERS.Tbl_MClusterCategories(NOLOCK) CC ON CC.ClusterCategoryId=CPD.ClusterCategoryId
	LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON ISNULL(BDB.BookNo,0) = BN.BookNo and ISNULL(BDB.IsActive,0)=1 
	LEFT JOIN #ReadCustomersList(NOLOCK) CR	ON CPD.GlobalAccountNumber=CR.GlobalAccountNumber 
	LEFT JOIN Tbl_DirectCustomersAvgReadings(NOLOCK) DCAVG ON CPD.GlobalAccountNumber=DCAVG.GlobalAccountNumber and DCAVG.ActiveStatusId=1
	LEFT JOIN Tbl_EstimationSettings(NOLOCK) ES	ON ES.CycleId=BN.CycleId and ES.CycleId in (select com from dbo.fn_Split(@CycleIds,','))
			AND ES.ClusterCategoryId = CC.ClusterCategoryId 
			AND ES.TariffId=CPD.TariffClassID AND ES.ActiveStatusId=1 
			And ES.BillingMonth= @Month	   
			AND ES.BillingYear=	@year
	
	
	
			SELECT
			(			
				SELECT 
					 ClassID
					,ClassName
					,CategoryName
					,ClusterCategoryId 
					,Cycle
					,CycleId 
					,CycleName
					,MAX(isnull(EnergytoCalculate,0)) As EnergyToCalculate
					,SUM(case 
						when isnull(CustomerTypeId,0)<>3
						then
							case
							when ((ISNULL(ActiveStatusId,1)=2) OR (ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(DisableTypeId,0) = 2 AND ISNULL(IsPartialBill,0) = 0))
							then 1 
							else 0
							end
						else 0
						end
					) as TotalInActiveCustomersCount
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersCount	  
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					) as TotalDirectCustomers
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else case when isnull(ReadingGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalReadCustomersHavingReadings
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0
							then 0 
							else case when isnull(AvgGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalUplodedCustomersCount
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(Usage,(CASE WHEN ISNULL(CustomerAvgReading,0) = 0 THEN InitialBillingKWh ELSE CustomerAvgReading END))  
														Else 0 end) end )
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomers
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0
											else (case when  isnull(ReadCodeID,0)=2 
														then  ISNULL(Usage,0)  
														Else 0 end)  end)
								else 0 end) AS NUMERIC) as TotalUsageForReadCustomersHavingReadings
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
								then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
											then 0 
											else (case when  isnull(ReadCodeID,0)=1 
														then  ISNULL(UploadedAvgReading,(CASE WHEN ISNULL(CustomerAvgReading,0) = 0 THEN InitialBillingKWh ELSE CustomerAvgReading END))  
														Else 0 end) end ) 
								else 0 end) AS NUMERIC) as TotalDirectCustomersUsage
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  ISNULL(UploadedAvgReading,0)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalUsageForUploadedCustomers
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					)
					-
					SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=1 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(AvgGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					) as TotalNonUploadedCustomersCount
					,SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0 
							else 1
							end
						else 0
						end)
						else 0 end
					)  
					-
					SUM(case when ISNULL(ActiveStatusId,1)=1 then
						(case 
						when isnull(ReadCodeID,0)=2 and isnull(CustomerTypeId,0)<>3
						then
							case
							when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --ISNULL(BDB.IsPartialBill,1)  =1 
							then 0
							else case when isnull(ReadingGlobalAccNo,0)=0 then 0 else 1 end
							end
						else 0
						end)
						else 0 end
					)  As TotalNonReadCustomersCount
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=2 
															then (case when  Usage IS NULL  
																		then (CASE WHEN ISNULL(CustomerAvgReading,0) = 0 THEN InitialBillingKWh ELSE CustomerAvgReading END)
																		else 0 end)
															Else 0
															 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonReadCustomersUsage
					,CAST(SUM(case when ISNULL(ActiveStatusId,1)=1  
									then (case when ISNULL(DisabledActiveStatus,0) = 1 AND ISNULL(IsPartialBill,0) = 0 --isnull(BDB.IsPartialBill,1)=1 
												then 0
												else (case when  isnull(ReadCodeID,0)=1 
															then  (case when UploadedAvgReading IS NULL  
																		then (CASE WHEN ISNULL(CustomerAvgReading,0) = 0 THEN InitialBillingKWh ELSE CustomerAvgReading END)
																		else 0  end)  
															Else 0 end) end ) 
									else 0 end) AS NUMERIC) as TotalNonUploadedCustomersUsage
				FROM #ReaultData				 
				GROUP BY ClassID,ClassName,CategoryName
					,ClusterCategoryId,CycleName,Cycle
					,CycleId 
				ORDER BY CycleId,ClusterCategoryId
				FOR XML PATH('EstimationDetails'),TYPE
			)
			,
			(
				SELECT	
					 @Capacity as CapacityInKVA
					,@SUPPMConsumption as SUPPMConsumption
					,@SUCreditConsumption as SUCreditConsumption
				FOR XML PATH('EstimatedUsageDetails'),TYPE 
			)
			FOR XML PATH(''),ROOT('EstimationInfoByXml')
		
	DROP TABLE #ReadCustomersList
	DROP TABLE #ReaultData
	
END

GO

/****** Object:  StoredProcedure [dbo].[USP_GetAssignMetersLog]    Script Date: 08/07/2015 19:03:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get AssignMeters request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetAssignMetersLog]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''
		,@PageNo INT
		,@PageSize INT
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')
	   ,@PageSize=C.value('(PageSize)[1]','INT')
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY AM.CreatedDate DESC ) AS RowNumber				
				,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,AM.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),AM.AssignedMeterDate,107) AS AssignedMeterDate
			   ,(CAST(AM.InitialReading AS INT )) AS InitialBillingkWh
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,AM.Remarks
			   ,CONVERT(VARCHAR(50),ISNULL(AM.ModifiedDate,AM.CreatedDate),107) AS LastTransactionDate  
			   ,CONVERT(VARCHAR(50),ISNULL(AM.ModifiedDate,AM.CreatedDate),107) AS ModifiedDate  
			   ,AM.CreatedBy
			   ,ISNULL(AM.ModifiedBy,AM.CreatedBy) AS ModifiedBy
			   ,CustomerView.ClassName
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo
			   ,AM.CreatedDate
			   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode+' ) ') AS BookNumber
			   ,CustomerView.BookSortOrder  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name
			   ,COUNT(0) OVER() AS TotalChanges
		 FROM Tbl_AssignedMeterLogs  AM
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON AM.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,AM.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = AM.ApprovalStatusId 
		 
		 )
		 
		 SELECT * FROM PageResult
		WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogs]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Description: The purpose of this procedure is to get Tariff change logs based on search criteria  
-- Modified By : Padmini  
-- Modified Date : 26-12-2014    
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015 
-- Modified By: Bhimaraju V
-- Modified Date: 18-04-2015 
-- Description : ClusterTypes are added.
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
   DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''
		,@PageNo INT
		,@PageSize INT  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END      
     
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
     
		 ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY TCR.CreatedDate DESC) AS RowNumber
			,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
		   ,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		   ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
		   ,TCR.Remarks  
		   ,TCR.CreatedBy
		   ,ISNULL(TCR.ModifiedBy,TCR.CreatedBy) AS ModifiedBy
			,CONVERT(VARCHAR(50),ISNULL(TCR.ModifiedDate,TCR.CreatedDate),107) AS LastTransactionDate
			,CONVERT(VARCHAR(50),ISNULL(TCR.ModifiedDate,TCR.CreatedDate),107) AS ModifiedDate
		   ,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode +' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder    
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo 
		   ,COUNT(0) OVER() AS TotalChanges 
		 FROM Tbl_LCustomerTariffChangeRequest  TCR  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId    
		 --ORDER BY TCR.CreatedDate DESC
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
END  
-----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectLogs]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get ReadToDirect request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetReadToDirectLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''
		,@PageNo INT
		,@PageSize INT  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
		 ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY RD.CreatedDate DESC) AS RowNumber
			   ,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,RD.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),ISNULL(RD.ModifiedDate,RD.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),ISNULL(RD.ModifiedDate,RD.CreatedDate),107) AS ModifiedDate   
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy
			   ,ISNULL(RD.ModifiedBy,RD.CreatedBy) AS ModifiedBy
			   ,CustomerView.ClassName
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder  
			   ,CustomerView.CycleName
			   ,CustomerView.OldAccountNo 
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder
			   ,COUNT(0) OVER() AS TotalChanges 
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 --ORDER BY RD.CreatedDate DESC
		 -- changed by karteek end 
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerTypeChangeLogs]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  M.Padmini  
-- Create date: 20-03-2015  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- Description: The purpose of this procedure is to get limited CustomerType request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerTypeChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)='' 
		,@PageNo INT
		,@PageSize INT   
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	--SELECT(  
		 ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY CC.CreatedDate DESC) AS RowNumber
			,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,CC.ApprovalStatusId  
		   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType  
		   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType  
		   ,CONVERT(VARCHAR(30),ISNULL(CC.ModifiedDate,CC.CreatedDate),107) AS LastTransactionDate   
		   ,CONVERT(VARCHAR(30),ISNULL(CC.ModifiedDate,CC.CreatedDate),107) AS ModifiedDate   
		   ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus  
		   ,CC.Remarks  
		   ,CC.CreatedBy
		   ,ISNULL(CC.ModifiedBy,CC.CreatedBy) AS ModifiedBy
		   ,CustomerView.ClassName
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder  
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder      
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo
		   ,COUNT(0) OVER() AS TotalChanges 
		 FROM Tbl_CustomerTypeChangeLogs  CC  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId    
		 --ORDER BY CC.CreatedBy DESC
		 -- changed by karteek end 
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
END
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerPresentMeterReadingChangeLogs]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================   
-- Modified By: Bhimaraju V
-- Modified Date: 30-07-2015
-- Description: PresentMeterREading Changes Log Display for AuditTray
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerPresentMeterReadingChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)='' 
		,@PageNo INT
		,@PageSize INT   
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	
     
	
	;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY CC.CreatedDate DESC) AS RowNumber
			,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,CC.ApprovalStatusId
		   ,CC.PreviousReading
		   ,CC.PresentReading
		   ,CC.NewPresentReading
		   ,CONVERT(VARCHAR(30),CC.PreviousReadDate,107) AS PreviousReadDate   
		   ,CONVERT(VARCHAR(30),CC.PresentReadDate,107) AS PresentReadDate
		   ,CC.MeterNumber
		   ,CONVERT(VARCHAR(30),ISNULL(CC.ModifiedDate,CC.CreatedDate),107) AS LastTransactionDate   
		   ,CONVERT(VARCHAR(30),ISNULL(CC.ModifiedDate,CC.CreatedDate),107) AS ModifiedDate   
		   ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus  
		   ,CC.Remarks  
		   ,CC.CreatedBy
		   ,ISNULL(CC.ModifiedBy,CC.CreatedBy) AS ModifiedBy
		   ,CustomerView.ClassName
		   ,CustomerView.BusinessUnitName
		   ,CustomerView.ServiceUnitName
		   ,CustomerView.ServiceCenterName
		   ,CustomerView.SortOrder AS CustomerSortOrder  
		   ,CustomerView.CycleName
		   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		   ,CustomerView.BookSortOrder      
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo
		   ,COUNT(0) OVER() AS TotalChanges 
		 FROM Tbl_PresentReadingAdjustmentApprovalLog  CC  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId    
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize   
END
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogs]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogs]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''
		,@PageNo INT
		,@PageSize INT
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY CNL.CreatedDate DESC) AS RowNumber
			   ,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
			   ,CNL.Remarks
			   ,dbo.fn_GetCustomerFullName_New(CNL.OldTitle,CNL.OldFirstName,CNL.OldMiddleName,CNL.OldLastName) AS [OldName]
			   ,dbo.fn_GetCustomerFullName_New(CNL.NewTitle,CNL.NewFirstName,CNL.NewMiddleName,CNL.NewLastName) AS [NewName]
			   --,CNL.OldTitle  
			   --,CNL.NewTitle  
			   --,CNL.OldFirstName  
			   --,CNL.NewFirstName  
			   --,CNL.OldMiddleName  
			   --,CNL.NewMiddleName  
			   --,CNL.OldLastName  
			   --,CNL.NewLastName  
			   ,CNL.OldKnownAs  AS OldSurName 
			   ,CNL.NewKnownAs  AS NewSurName 
			   ,CONVERT(VARCHAR(30),ISNULL(CNL.ModifiedDate,CNL.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),ISNULL(CNL.ModifiedDate,CNL.CreatedDate),107) AS ModifiedDate   
			   ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CNL.CreatedBy  
			   ,ISNULL(CNL.ModifedBy,CNL.CreatedBy) AS ModifiedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder 
			   ,CustomerView.CycleName
			   ,(CustomerView.BookId + ' - ' + CustomerView.BookCode) AS BookNumber
			   ,CustomerView.BookSortOrder     
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
			   ,CustomerView.OldAccountNo 
			   ,CustomerView.ClassName
			   ,CNL.CreatedDate
			   ,COUNT(0) OVER() AS TotalChanges 
		 FROM Tbl_CustomerNameChangeLogs CNL  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId   
		 --ORDER BY CNL.CreatedDate DESC
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
END 
------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMeterChangeLogs]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015     
-- Description: The purpose of this procedure is to get limited Meter change request logs  
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)='' 
		,@PageNo INT
		,@PageSize INT  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
    
	--SELECT(  
		 ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY CMI.CreatedDate DESC) AS RowNumber
			,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			   ,(CMI.OldMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.OldDials) + ' )') AS OldMeterNo
			   ,(CMI.NewMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.NewDials) + ' )') AS NewMeterNo
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType  
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType
			   ,('Old Reading: '+CMI.OldMeterReading +' <br/> New Reading: '+ CONVERT(VARCHAR(10),CAST(ISNULL(NewMeterInitialReading,0) AS NUMERIC))) AS Readings
			   ,CMI.Remarks
			   ,CustomerView.ClassName  
			   ,CONVERT(VARCHAR(30),ISNULL(CMI.ModifiedDate,CMI.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),ISNULL(CMI.ModifiedDate,CMI.CreatedDate),107) AS ModifiedDate   
			   ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate   
			   ,CONVERT(VARCHAR(30),CMI.MeterChangedDate,107) AS AssignedMeterDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CMI.CreatedBy  
			   ,ISNULL(CMI.ModifiedBy,CMI.CreatedBy)  AS ModifiedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder 
			   ,CustomerView.CycleName
			   ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode +' ) ') AS BookNumber
		       ,CustomerView.BookSortOrder     
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name 
			   ,CustomerView.OldAccountNo
			   ,COUNT(0) OVER() AS TotalChanges    
		 FROM Tbl_CustomerMeterInfoChangeLogs  CMI  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId
		 --ORDER BY CMI.CreatedDate DESC
		 -- changed by karteek end    
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
END  
-----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetAssignMetersWithLimit]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get AssignMeters request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetAssignMetersWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,AM.MeterNo AS NewMeterNo
			   ,CONVERT(VARCHAR(30),AM.AssignedMeterDate,107) AS AssignedMeterDate
			   ,CAST(AM.InitialReading AS INT) AS InitialBillingkWh
			   ,CONVERT(VARCHAR(30),AM.CreatedDate,107) AS TransactionDate
			   ,CONVERT(VARCHAR(50),ISNULL(AM.ModifiedDate,AM.CreatedDate),107) AS LastTransactionDate  
			   ,ApproveSttus.ApprovalStatus
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName			     
			   ,AM.Remarks  
			   ,AM.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges
			   ,ISNULL(AM.ModifiedBy,AM.CreatedBy)AS ModifiedBy
			   ,CONVERT(VARCHAR(30),ISNULL(AM.ModifiedDate,AM.CreatedDate),107) AS ModifiedDate
		 FROM Tbl_AssignedMeterLogs  AM  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON AM.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,AM.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = AM.ApprovalStatusId 
		 ORDER BY AM.CreatedDate DESC
		 -- changed by karteek end       
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogs]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''
		,@PageNo INT
		,@PageSize INT  
    
  SELECT  
		 @BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
		,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
		
		;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY BookChange.CreatedDate DESC) AS RowNumber
			 ,(CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldPostal_HouseNo,BookChange.OldPostal_StreetName,
				BookChange.OldPostal_Landmark,BookChange.OldPostal_City,BookChange.OldPostal_AreaCode,BookChange.OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewPostal_HouseNo,BookChange.NewPostal_StreetName,
				BookChange.NewPostal_Landmark,BookChange.NewPostal_City,BookChange.NewPostal_AreaCode,BookChange.NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldService_HouseNo,BookChange.OldService_StreetName,
				BookChange.OldService_Landmark,BookChange.OldService_City,BookChange.OldService_AreaCode,BookChange.OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewService_HouseNo,BookChange.NewService_StreetName,
				BookChange.NewService_Landmark,BookChange.NewService_City,BookChange.NewService_AreaCode,BookChange.NewService_ZipCode) AS NewServiceAddress

			 ,('BU : '+BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) <br/> SU : '+
			  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) <br/> SC : '+
			  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) <br/> BG : '+
			  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) <br/> Book : '+
			  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
			,('BU : '+BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br/> SU : '+
			  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br/> SC : '+
			  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br/> BG : '+
			  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br/> Book : '+
			  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
			  
			,BookChange.Remarks
			,BookChange.CreatedBy
			,MTC.ClassName
			,CONVERT(VARCHAR(50),ISNULL(BookChange.ModifiedDate,BookChange.CreatedDate),107) AS LastTransactionDate
			,CONVERT(VARCHAR(50),ISNULL(BookChange.ModifiedDate,BookChange.CreatedDate),107) AS ModifiedDate
			,ISNULL(BookChange.ModifiedBy,BookChange.CreatedBy) AS ModifiedBy
			,CD.OldAccountNo
			,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name  
			,BD.BusinessUnitName
			,BD.ServiceUnitName
			,BD.ServiceCenterName
			,BD.CycleName
			,(BD.ID +' ( '+BD.BookCode+ ' ) ') AS BookNumber
			,BD.BookSortOrder
			,'' AS CustomerSortOrder
			,COUNT(0) OVER() AS TotalChanges 
		FROM Tbl_BookNoChangeLogs  BookChange  
		INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.GlobalAccountNo
		AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApprovalStatusId 
		--ORDER BY  BookChange.CreatedDate DESC--,BD.BookSortOrder ASC--,--BD.CustomerSortOrder ASC
		
		)
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
		--SELECT 
		--	 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 
		--	--,(BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) <br/>'+
		--	--  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) <br/>'+
		--	--  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) <br/>'+
		--	--  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) <br/>'+
		--	--  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
		--	--,(BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br/>'+
		--	--  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br/>'+
		--	--  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br/>'+
		--	--  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br/>'+
		--	--  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
		--	,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.OldBookNo) AS OldBookNo
		--   ,(SELECT Id +' ( '+BookCode+' ) ' FROM Tbl_BookNumbers WHERE BookNo=BookChange.NewBookNo) AS NewBookNo  

		--	,BookChange.Remarks
		--	,BookChange.CreatedBy
		--	,MTC.ClassName
		--	,CD.OldAccountNo
		--	,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
		--	,ApproveSttus.ApprovalStatus  
		--	,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name
		--	,BD.BusinessUnitName
		--	,BD.ServiceUnitName
		--	,BD.ServiceCenterName
		--	,BD.CycleName
		--	,(BD.ID +' ( '+BD.BookCode+ ' ) ') AS BookNumber
		--	,BD.BookSortOrder
		--	,BD.CustomerSortOrder
		--FROM Tbl_BookNoChangeLogs  BookChange  
		--INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.AccountNo
		--AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		--INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		--INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		--INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		--INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		--INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		--INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		--INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		--INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		--INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		--INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		--INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApproveStatusId 
		--ORDER BY  BookChange.CreatedBy DESC,BD.BookSortOrder ASC,BD.CustomerSortOrder ASC

END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogs]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015    
-- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By : Bhimaraju V
-- Modified Date: 22-Apr-2015
--Desc : Getting Single field instead of all
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
		,@PageNo INT
		,@PageSize INT
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
	   ,@PageNo=C.value('(PageNo)[1]','VARCHAR(20)')  
	   ,@PageSize=C.value('(PageSize)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END    
  
	-- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
	;WITH PageResult AS(
		SELECT ROW_NUMBER() OVER(ORDER BY CA.CreatedDate DESC) AS RowNumber    
			 ,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As GlobalAccountNumber
			 ,dbo.fn_GetCustomerServiceAddress_New(OldPostal_HouseNo,OldPostal_StreetName,
			 OldPostal_Landmark,OldPostal_City,OldPostal_AreaCode,OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(NewPostal_HouseNo,NewPostal_StreetName,
			 NewPostal_Landmark,NewPostal_City,NewPostal_AreaCode,NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(OldService_HouseNo,OldService_StreetName,
			 OldService_Landmark,OldService_City,OldService_AreaCode,OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(NewService_HouseNo,NewService_StreetName,
			 NewService_Landmark,NewService_City,NewService_AreaCode,NewService_ZipCode) AS NewServiceAddress			  
			 ,CA.Remarks  
			 ,CONVERT(VARCHAR(30),CA.CreatedDate,107) AS TransactionDate   
			 ,CONVERT(VARCHAR(30),ISNULL(CA.ModifiedDate,CA.CreatedDate),107) AS LastTransactionDate   
			 ,CONVERT(VARCHAR(30),ISNULL(CA.ModifiedDate,CA.CreatedDate),107) AS ModifiedDate   
			 ,ApproveSttus.ApprovalStatus  
			 ,CA.CreatedBy
			 ,ISNULL(CA.ModifiedBy,CA.CreatedBy) AS ModifiedBy
			 ,CustomerView.ClassName
			 ,CustomerView.BusinessUnitName
			 ,CustomerView.ServiceUnitName
			 ,CustomerView.ServiceCenterName
			 ,CustomerView.SortOrder AS CustomerSortOrder  
			 ,CustomerView.CycleName
		     ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode+' ) ') AS BookNumber
		     ,CustomerView.BookSortOrder     
			 ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		     ,CustomerView.OldAccountNo
		     ,COUNT(0) OVER() AS TotalChanges 
		 FROM Tbl_CustomerAddressChangeLog_New CA   
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CA.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CA.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CA.ApprovalStatusId     
		 
	)  
    
    SELECT(  
    SELECT   *
			--,(SELECT COUNT(0) FROM PageResult) AS TotalChanges
    FROM PageResult
	WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize
	FOR XML PATH('AuditTrayAddressList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')
END 
---------------------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogs]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015   
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogs]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)='' 
		,@PageNo INT
		,@PageSize INT   
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek  
     
		 ;WITH PageResult AS(
		 SELECT ROW_NUMBER() OVER(ORDER BY CCL.CreatedDate DESC) AS RowNumber
			   ,(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
			   ,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
			   ,CCL.Remarks
			   ,CustomerView.ClassName
			   ,CONVERT(VARCHAR(30),ISNULL(CCL.ModifiedDate,CCL.CreatedDate),107) AS LastTransactionDate
			   ,CONVERT(VARCHAR(30),ISNULL(CCL.ModifiedDate,CCL.CreatedDate),107) AS ModifiedDate
			   ,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate
			   ,CONVERT(VARCHAR(30),CCL.ChangeDate,107) AS StatusChangedDate
			   ,ApproveSttus.ApprovalStatus  
			   ,CCL.CreatedBy
			   ,ISNULL(CCL.ModifiedBy,CCL.CreatedBy) AS ModifiedBy
			   ,CustomerView.BusinessUnitName
			   ,CustomerView.ServiceUnitName
			   ,CustomerView.ServiceCenterName
			   ,CustomerView.SortOrder AS CustomerSortOrder
			   ,CustomerView.CycleName
		       ,(CustomerView.BookId + ' ( ' + CustomerView.BookCode + ' ) ') AS BookNumber
		       ,CustomerView.BookSortOrder      
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
			   ,CustomerView.OldAccountNo 
			   ,COUNT(0) OVER() AS TotalChanges
		 FROM Tbl_CustomerActiveStatusChangeLogs CCL   
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
		 --ORDER BY CCL.CreatedDate DESC
		 -- changed by karteek end   
		 )
		 
		 SELECT * FROM PageResult
		 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
   
END  
-------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [CUSTOMERS].[USP_GetApprovalRegistrationsAuditRay]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 24-JULY-2014
-- Description:	
-- =============================================
ALTER PROCEDURE [CUSTOMERS].[USP_GetApprovalRegistrationsAuditRay]
(
	@XmlDoc xml
)
AS
BEGIN 

--------------------------------------------
--              Declaration
--------------------------------------------

	DECLARE 
	@BU_ID VARCHAR(MAX) --='BEDC_BU_0012' 
   ,@RoleId INT  
   ,@UserIds VARCHAR(500)  
   ,@UserId VARCHAR(50)  
   ,@FunctionId INT  = 15
   ,@ApprovalRoleId INT 
   ,@ARID INT 
   ,@SU_ID VARCHAR(MAX)--='BEDC_SU_0036'  
   ,@SC_ID VARCHAR(MAX)--='BEDC_SC_0043,BEDC_SC_0044'  
   ,@CycleId VARCHAR(MAX)--='BEDC_C_0176,BEDC_C_0179,BEDC_C_0180,BEDC_C_0177,BEDC_C_0178,BEDC_C_0192'  
   ,@TariffId VARCHAR(MAX)--='2,3,4,5,7,8,9,11,12,13,15,16,19,21,22,39,44,45,47,49,50,51,54,55'  
   ,@BookNo VARCHAR(MAX)--='BEDC_B_1009,BEDC_B_1010,BEDC_B_1011,BEDC_B_1012,BEDC_B_1013,BEDC_B_1014,BEDC_B_1015,BEDC_B_1016,BEDC_B_1017,BEDC_B_1018'  
   ,@FromDate VARCHAR(20)--='07/04/2015'  
   ,@ToDate VARCHAR(20)--='08/01/2015' 
   ,@PageNo INT--=1
   ,@PageSize INT--=5 
   ,@Count INT
        
--------------------------------------------
--              Deserialization
--------------------------------------------
        
	 SELECT       
	 
	   @BU_ID = C.value('(BU_ID)[1]','VARCHAR(50)')  
	  ,@UserId = C.value('(UserId)[1]','VARCHAR(50)')  
	  ,@FunctionId = C.value('(FunctionId)[1]','INT')  
	  ,@ARID = C.value('(ARID)[1]','INT')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')
	   ,@PageNo=C.value('(PageNo)[1]','INT')  
	   ,@PageSize=C.value('(PageSize)[1]','INT')   
	    
	  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
	 
	 
--------------------------------------------
--              Initialization on variables
-------------------------------------------- 
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 

--------------------------------------------
--              Requirement
-------------------------------------------- 	
	SET @RoleId = (SELECT RoleId FROM Tbl_UserDetails WHERE UserId = @UserId)
	IF((SELECT COUNT(0) FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID) =1)
		BEGIN
			SET @UserIds = (SELECT UserIds FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND RoleId = @RoleId AND BU_ID=@BU_ID)
		END
	ELSE
		BEGIN
			DECLARE @UserDetailedId INT
			SET @UserDetailedId =(SELECT UserDetailsId FROM Tbl_UserDetails WHERE UserId=@UserId)
			SET @UserIds=(SELECT @UserDetailedId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
			SET @ApprovalRoleId =(SELECT ApprovalRoleId FROM TBL_FunctionApprovalRole WHERE FunctionId = @FunctionId AND BU_ID=@BU_ID AND  @UserDetailedId IN (SELECT COM FROM dbo.fn_Split(UserIds,',')))
		END
	
	
	SET @UserId = (SELECT CONVERT(VARCHAR(50),UserDetailsId) FROM Tbl_UserDetails WHERE UserId = @UserId)  
	SELECT @UserIds = (CASE WHEN @UserIds IS NULL THEN @UserId ELSE @UserIds END)
   
   ;WITH PageResult AS
   (		
		SELECT    
		ARID AS ARID
	   ,ROW_NUMBER() OVER (ORDER BY CD.ARID DESC) AS RowNumber 
	   ,CD.Title + ' '+ CD.FirstName + ' '+CD.MiddleName + ' '+CD.LastName AS FullName 
	   ,CASE ISNULL(CD.GlobalAccountNumber,'') WHEN '' THEN 'Not Assigned' WHEN NULL THEN 'Not Assigned' ELSE CD.GlobalAccountNumber END AS GlobalAccountNumber  
	  ,CASE CD.Title WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title END AS TitleLandlord  
	  ,CASE CD.FirstName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName END AS FirstNameLandlord  
	  ,CASE CD.MiddleName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName END AS MiddleNameLandlord  
	  ,CASE CD.LastName WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName END AS LastNameLandlord  
	  ,CASE CD.KnownAs WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.KnownAs END AS KnownAs  
	  ,CASE CD.EmailId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailId END AS EmailIdLandlord  
	  ,CASE CD.HomeContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HomeContactNumber END AS HomeContactNumberLandlord  
	  ,CASE CD.BusinessContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.BusinessContactNumber END AS BusinessPhoneNumberLandlord  
	  ,CASE CD.OtherContactNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.OtherContactNumber END AS OtherPhoneNumberLandlord  
	  ,CD.DocumentNo   
	  ,CASE CD.ConnectionDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ConnectionDate,106) END AS ConnectionDate   
	  ,CASE CD.ApplicationDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.ApplicationDate,106) END AS ApplicationDate   
	  ,CASE CD.SetupDate WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CONVERT(VARCHAR(20),CD.SetupDate,106) END AS SetupDate   
	  ,CD.OldAccountNo   
	  ,CD.CustomerTypeId_Value AS CustomerType  
	  ,CD.PoleID    
	  ,CD.TariffClassID_Value AS Tariff   
	  ,CD.IsEmbassyCustomer   
	  ,CD.EmbassyCode   
	  ,CD.PhaseId_Value AS Phase   
	  ,CD.ReadCodeID_Value as ReadType  
	  ,CD.IsVIPCustomer  
	  ,CD.IsBEDCEmployee  
	  ,CASE CD.HouseNo_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoService   
	  ,CASE CD.StreetName_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetService   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS CityService  
	  ,CD.AreaCode_Service AS AreaService   
	  ,CASE CD.ZipCode_Service WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Service END AS SZipCode     
	  ,CD.IsCommunication_Service AS IsCommunicationService     
	  ,CASE CD.HouseNo_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.HouseNo_Postal END AS HouseNoPostal   
	  ,CASE CD.StreetName_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.StreetName_Postal END AS StreetPostal   
	  ,CASE CD.City_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.City_Postal END AS  CityPostaL   
	  ,ISNULL(CONVERT(VARCHAR(10),CD.AreaCode_Postal),'--') AS  AreaPostal    --Faiz-ID103
	  ,CASE CD.ZipCode_Postal WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ZipCode_Postal END AS  PZipCode  
	  ,CD.IsActive_Postal AS IsCommunicationPostal        
	  ,CD.IsCAPMI  
	  ,CD.InitialBillingKWh   
	  ,CD.InitialReading   
	  ,CD.PresentReading 
	  ,CD.AvgReading as AverageReading   
	  ,CD.Highestconsumption   
	  ,CD.OutStandingAmount_ActiveDetails AS OutStandingAmount   
	  ,OpeningBalance  
	  ,CASE CD.Remarks WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Remarks END AS Remarks  
	  ,CASE CD.Seal1 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal1 END AS Seal1  
	  ,CASE CD.Seal2 WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Seal2 END AS Seal2  
	  ,CASE CD.AccountTypeId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AccountTypeId_Value END AS AccountType  
	  ,CASE CD.TariffClassID_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.TariffClassID_Value END AS ClassName  
	  ,CASE CD.ClusterCategoryId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ClusterCategoryId_Value END AS ClusterCategoryName  
	  ,CASE CD.PhaseId WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhaseId END AS PhaseId  
	  ,CASE CD.RouteSequenceNumber_value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.RouteSequenceNumber_value END AS RouteName  
	  ,CASE CD.Title_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.Title_Tenant END AS TitleTanent  
	  ,CASE CD.FirstName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.FirstName_Tenant END AS FirstNameTanent  
	  ,CASE CD.MiddleName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MiddleName_Tenant END AS MiddleNameTanent  
	  ,CASE CD.LastName_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.LastName_Tenant END AS LastNameTanent  
	  ,CASE CD.PhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.PhoneNumber END AS PhoneNumberTanent  
	  ,CASE CD.AlternatePhoneNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AlternatePhoneNumber END AS AlternatePhoneNumberTanent  
	  ,CASE CD.EmailID_Tenant WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmailID_Tenant END AS EmailIdTanent  
	 ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeName  
	  ,CASE CD.ApplicationProcessedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.ApplicationProcessedBy_Value END AS ApplicationProcessedBy  
	  ,CASE CD.EmployeeCode_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.EmployeeCode_Value END AS EmployeeNameProcessBy  
	  ,CASE CD.AgencyId_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.AgencyId_Value END AS AgencyName  
	  ,CASE CD.MeterNumber WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.MeterNumber END AS MeterNumber  
	  ,CASE CD.MeterAmount WHEN 0 THEN 0 WHEN NULL THEN 0 ELSE CD.MeterAmount END AS MeterAmount  
	  ,CASE CD.CertifiedBy_Value WHEN ''THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy1  
	  ,CASE CD.CertifiedBy_Value WHEN '' THEN '--' WHEN NULL THEN '--' ELSE CD.CertifiedBy_Value END AS CertifiedBy   
	  ,CD.IsSameAsService  
	  ,CD.OutStandingAmount_ActiveDetails
		,NR.RoleName AS NextRole
		,CR.RoleName AS CurrentRole
		,CD.PresentApprovalRole
		,CD.NextApprovalRole
		,CD.ApproveStatusId
		,(BN.ID+' ( '+BN.BookCode+' )') AS BookCode
		, BU.BusinessUnitName AS BusinessUnit
		, SU.ServiceUnitName AS ServiceUnitName
		, SC.ServiceCenterName AS ServiceCenterName
		, C.CycleName AS CycleName
		,CD.IsSameAsTenent  AS IsSameAsTenent
		,BN.BookNo AS BookNumber
	   ,ISNULL((CASE WHEN CD.NextApprovalRole IS NULL 
						THEN (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending' ELSE APS.ApprovalStatus END)
						ELSE (CASE APS.ApprovalStatusId WHEN 1 THEN 'Action Pending From ' + CR.RoleName ELSE APS.ApprovalStatus + ' By ' + CR.RoleName END)
						END),'Approved') AS [Status]
			,(CASE WHEN (CD.PresentApprovalRole IS NULL OR (CD.PresentApprovalRole = @RoleId 
			AND CD.CurrentApprovalLevel = (SELECT [Level] FROM TBL_FunctionApprovalRole WHERE ApprovalRoleId = @ApprovalRoleId)
			AND (@UserId IN (SELECT com FROM dbo.fn_Split(@UserIds,',')) OR @UserIds = '')))
					THEN 1
					ELSE 0
					END) AS IsPerformAction 
	  ,CONVERT(VARCHAR(19),CD.CreatedDate) AS CreatedDate
	  ,BN.SortOrder AS BookSortOrder
	  ,ISNULL(CD.ModifedBy,CD.CreatedBy) AS ModifiedBy
	  ,CONVERT(VARCHAR(30),ISNULL(CD.ModifiedDate,CD.CreatedDate),107) AS ModifiedDate
	 FROM CUSTOMERS.Tbl_ApprovalRegistration CD 	
	 INNER JOIN Tbl_MApprovalStatus AS APS ON APS.ApprovalStatusId =CD.ApproveStatusId 
	 LEFT JOIN Tbl_MRoles AS NR ON CD.NextApprovalRole = NR.RoleId  
	 LEFT JOIN Tbl_MRoles AS CR ON CD.PresentApprovalRole = CR.RoleId  
	  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
	INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
	INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
	INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
	INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
	 --WHERE @RoleId IN (SELECT  RoleId FROM TBL_FunctionApprovalRole 
		--				WHERE FunctionId = @FunctionId  AND BU_ID=@BU_ID) 
		AND CONVERT (DATE,CD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate) 
			AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
			AND SU.SU_Id IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
			AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
			AND C.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
			AND BN.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,','))
			AND CD.TariffClassID IN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,','))
	 )
 	 SELECT * FROM PageResult
	 WHERE RowNumber between (@PageNo - 1) * @PageSize + 1 and @PageNo * @PageSize    
		ORDER BY ARID DESC 
		
	SET @Count =(select COUNT(0) from CUSTOMERS.Tbl_ApprovalRegistration as cd
				INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = CD.BookNo 
				INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
				INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
				INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
				INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
				AND CONVERT (DATE,CD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate) 
				AND BU.BU_ID IN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,','))
				AND SU.SU_Id IN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,','))
				AND SC.ServiceCenterId IN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,','))
				AND C.CycleId IN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,','))
				AND BN.BookNo IN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,','))
				AND CD.TariffClassID IN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')))
	 SELECT @Count AS [Count]
END
GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentLastTransactions]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================        
-- Author		:  NEERAJ KANOJIYA        
-- Create date	:  11/JUNE/2015        
-- Description	:  GET ADJUSTMENT LAST TRANSACTIONS
-- Modified By : Bhimaraju Vanka
-- Desc: Here in Adjustment (No Bill) These details are not having in master table we need do left join 
-- =============================================        
ALTER PROCEDURE [dbo].[USP_GetAdjustmentLastTransactions]        
(        
 @XmlDoc xml          
)        
AS        
  BEGIN 
BEGIN TRY  
 BEGIN TRAN 
	DECLARE @AccountNo VARCHAR(50)
			,@IsSuccess VARCHAR(50)
	SELECT  
		  @AccountNo=C.value('(AccountNo)[1]','VARCHAR(50)') 
	FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C) 
	SET @IsSuccess= 'Select adjustment list.';
	
	;WITH PageResult AS(
	SELECT TOP 5 ISNULL(BT.Name,'Adjustment(No Bill)') AS AdjustmentName
		--SELECT TOP 5 BT.Name AS AdjustmentName
			,CONVERT(DECIMAL(18,2),BA.TotalAmountEffected) AS TotalAmountEffected
			,CONVERT(DECIMAL(18,2),BA.AmountEffected) AS AmountEffected
			,ISNULL(BA.AdjustedUnits,0) AS AdjustedUnits
			,BAD.BatchNo AS BatchNo
			--,(CASE WHEN TotalAmountEffected < 0 THEN ' Dr' ELSE ' Cr' END) AS Format
			,CONVERT(VARCHAR(12), BA.CreatedDate,106) AS CreatedDate
			,BA.Remarks
		FROM Tbl_BillAdjustments AS BA
		--JOIN Tbl_BillAdjustmentDetails AS BD ON BA.BillAdjustmentId=BD.BillAdjustmentId
		JOIN Tbl_BATCH_ADJUSTMENT AS BAD ON BAD.BatchID=BA.BatchNo
		LEFT JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		--JOIN Tbl_BillAdjustmentType AS BT ON BA.BillAdjustmentType=BT.BATID
		WHERE BA.AccountNo=@AccountNo
		ORDER BY BA.CreatedDate DESC
		)
		
		
	SELECT
	(	
		SELECT AdjustmentName
				--,(REPLACE(CONVERT(VARCHAR(25), CAST(AmountEffected AS MONEY), 1),'-','')+Format) AS NewAmountEffected
				,AmountEffected
				,TotalAmountEffected
				,AdjustedUnits
				,BatchNo
				,CreatedDate
				,Remarks
		FROM PageResult
		FOR XML PATH('BillAdjustments'),TYPE
	)
	FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')	
 COMMIT TRAN   
END TRY 
BEGIN CATCH  
 ROLLBACK TRAN 
 SELECT  @IsSuccess AS IsSuccess			
		FOR XML PATH('BillAdjustments')
END CATCH  
END 
GO

/****** Object:  StoredProcedure [dbo].[USP_GetAdjustmentBatchDetails]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--=============================================================    
--AUTHOR : NEERAJ KANOJIYA    
--DESC  : INSERT BATCH DETAILS FOR ADJUSTMENTS    
--CREATED ON: 4-OCT-14    
--=============================================================    
ALTER PROCEDURE [dbo].[USP_GetAdjustmentBatchDetails]  
(    
@XmlDoc XML    
)    
AS    
 BEGIN    
 DECLARE @BatchID INT, @BU_ID VARCHAR(50)  
     
 SELECT @BatchID  = C.value('(BatchID)[1]','INT')    
 , @BU_ID  = C.value('(BU_ID)[1]','VARCHAR(50)')    
 FROM @XmlDoc.nodes('BillAdjustmentsBe') AS T(C)    
     
     
SELECT (  
  SELECT BA.BatchID,  
    BA.BatchNo,  
    BA.BatchDate,  
    BA.BatchTotal,  
    (SELECT COUNT(0) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS TotalCustomers,  
    RC.ReasonCode, --+ ' '+RC.[DESCRIPTION] AS ReasonCode  
    (BA.BatchTotal-  
    (SELECT SUM(TotalAmountEffected) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID)) AS PendingAmount,  
    convert(int, (BA.BatchTotal-  
    (SELECT SUM(AdjustedUnits) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID))) AS PendingUnit,  
    (SELECT TOP 1 (BillAdjustmentType) FROM Tbl_BillAdjustments AS BAD WHERE BAD.BatchNo=@BatchID) AS BillAdjustmentType  
   FROM  Tbl_BATCH_ADJUSTMENT AS BA  
   JOIN TBL_ReasonCode AS RC ON BA.Reason=RC.RCID AND BA.BatchStatusID = 1  
   AND (BA.BU_ID = @BU_ID OR @BU_ID = '') AND BA.BatchID=@BatchID  
  
      
   FOR XML PATH('BillAdjustments'),TYPE            
   ),  
   (  
    SELECT BA.BatchNO,  
      BAT.Name AS AdjustmentName,  
      CD.GlobalAccountNumber AS AccountNo,  
      BLA.TotalAmountEffected,  
      BLA.AmountEffected,  
      ISNULL(BLA.AdjustedUnits,0) AS AdjustedUnits,  
      BLA.CreatedDate  
      ,BLA.Remarks  
    from Tbl_BATCH_ADJUSTMENT  AS BA  
    JOIN Tbl_BillAdjustments  AS BLA ON BA.BatchID = BLA.BatchNo AND BA.BatchStatusID = 1  
      AND (BA.BU_ID = @BU_ID OR @BU_ID = '') AND BA.BatchID=@BatchID  
    --JOIN Tbl_BillAdjustmentDetails AS BAD ON BLA.BillAdjustmentId=BAD.BillAdjustmentId  
    JOIN Tbl_BillAdjustmentType  AS BAT ON BLA.BillAdjustmentType=BAT.BATID  
    JOIN [CUSTOMERS].[Tbl_CustomerSDetail] AS CD ON BLA.AccountNo=CD.GlobalAccountNumber  
      
    FOR XML PATH('BillAdjustmentLisBeDetails'),TYPE        
   )  
   FOR XML PATH(''),ROOT('BillAdjustmentsInfoByXml')        
END  
GO

/****** Object:  StoredProcedure [dbo].[USP_GetTariffChangeLogsWithLimit]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014   
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
---- Description: The purpose of this procedure is to get limited Tariff change request logs
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetTariffChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END    
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
	SELECT
	(  
		SELECT TOP 10 
			(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			,CustomerView.OldAccountNo
			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = PreviousTariffId) AS OldTariff  
			,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID = ChangeRequestedTariffId) AS NewTariff  
			,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.OldClusterCategoryId) AS OldCluster  
		    ,(SELECT CategoryName FROM MASTERS.Tbl_MClusterCategories WHERE ClusterCategoryId = TCR.NewClusterCategoryId) AS NewCluster  
			,TCR.Remarks
			,CONVERT(VARCHAR(50),ISNULL(TCR.ModifiedDate,TCR.CreatedDate),107) AS LastTransactionDate
			,CONVERT(VARCHAR(50),ISNULL(TCR.ModifiedDate,TCR.CreatedDate),107) AS ModifiedDate
			,CONVERT(VARCHAR(30),TCR.CreatedDate,107) AS TransactionDate
			,ISNULL(TCR.ModifiedBy,TCR.CreatedBy) AS ModifiedBy
			,TCR.CreatedBy
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_LCustomerTariffChangeRequest  TCR  
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON TCR.AccountNo = CustomerView.GlobalAccountNumber  
		AND CONVERT (DATE,TCR.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = TCR.ApprovalStatusId   
		ORDER BY TCR.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
--------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetReadToDirectWithLimit]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  V.Bhimaraju
-- Create date: 16-04-2015  
-- Description: The purpose of this procedure is to get limited ReadToDirect request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetReadToDirectWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As AccountNo
			   ,RD.MeterNo AS NewMeterNo
			   ,CustomerView.ClassName
			   ,CustomerView.OldAccountNo
			   ,CONVERT(VARCHAR(30),ISNULL(RD.ModifiedDate,RD.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),ISNULL(RD.ModifiedDate,RD.CreatedDate),107) AS ModifiedDate   
			   ,CONVERT(VARCHAR(30),RD.CreatedDate,107) AS TransactionDate
			   ,ISNULL(RD.ModifiedBy,RD.CreatedBy) AS ModifiedBy
			   ,ApproveSttus.ApprovalStatus  
			   ,RD.Remarks  
			   ,RD.CreatedBy  
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_ReadToDirectCustomerActivityLogs  RD  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON RD.GlobalAccountNo=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,RD.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = RD.ApprovalStatusId 
		 ORDER BY RD.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerTypeChangeWithLimit]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  M.Padmini  
-- Create date: 20-03-2015  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015   
-- Description: The purpose of this procedure is to get limited CustomerType request logs  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid

-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetCustomerTypeChangeWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
     
   SELECT(  
		 SELECT TOP 10 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.OldCustomerTypeId) AS OldCustomerType  
			   ,(SELECT CustomerType FROM Tbl_MCustomerTypes WHERE CustomerTypeId=CC.NewCustomerTypeId) AS NewCustomerType  
		       ,CONVERT(VARCHAR(30),ISNULL(CC.ModifiedDate,CC.CreatedDate),107) AS LastTransactionDate
		       ,CONVERT(VARCHAR(30),ISNULL(CC.ModifiedDate,CC.CreatedDate),107) AS ModifiedDate
		       ,ISNULL(CC.ModifiedBy,CC.CreatedBy) AS ModifiedBy
			   ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
			   ,ApproveSttus.ApprovalStatus  
			   ,CC.Remarks  
			   ,CC.CreatedBy
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			   ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_CustomerTypeChangeLogs  CC  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId 
		 -- changed by karteek end       
			ORDER BY CC.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerPresentMeterReadingChangeWithLimit]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Modified By: Bhimaraju V
-- Modified Date: 30-07-2015
-- Description: PresentMeterREading Changes Log Display for AuditTray
-- =============================================      
ALTER PROCEDURE [dbo].[USP_GetCustomerPresentMeterReadingChangeWithLimit]  
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END     
     
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
     
   SELECT(    
		SELECT TOP 10
			(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo 
		   ,CC.ApprovalStatusId
		   ,CC.PreviousReading
		   ,CC.PresentReading
		   ,CC.NewPresentReading
		   ,CONVERT(VARCHAR(30),CC.PreviousReadDate,107) AS PreviousReadDate   
		   ,CONVERT(VARCHAR(30),CC.PresentReadDate,107) AS PresentReadDate
		   ,CC.MeterNumber AS NewMeterNo
		   ,CONVERT(VARCHAR(30),ISNULL(CC.ModifiedDate,CC.CreatedDate),107) AS LastTransactionDate   
		   ,CONVERT(VARCHAR(30),ISNULL(CC.ModifiedDate,CC.CreatedDate),107) AS ModifiedDate   
		   ,CONVERT(VARCHAR(30),CC.CreatedDate,107) AS TransactionDate   
		   ,ApproveSttus.ApprovalStatus  
		   ,ISNULL(CC.Remarks,'--')AS Remarks
		   ,CC.CreatedBy
		   ,CustomerView.ClassName
		   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name   
		   ,CustomerView.OldAccountNo
		   ,ISNULL(CC.ModifiedBy,CC.CreatedBy) AS ModifiedBy	
		   ,COUNT(0) OVER() AS TotalChanges 
		 FROM Tbl_PresentReadingAdjustmentApprovalLog  CC  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CC.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CC.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CC.ApprovalStatusId    
		 ORDER BY CC.CreatedDate DESC
		      
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
    FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
----------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerNameChangeLogsWithLimit]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014  
---- Description: The purpose of this procedure is to get limited Customer Name change logs  
---- ModifiedBy : Padmini  
---- ModifiedDate : 22-01-2015
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid   
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerNameChangeLogsWithLimit]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
	    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END   
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
   SELECT(  
		 SELECT TOP 10 (CustomerView.AccountNo+' - '+CustomerView.GlobalAccountNumber) AS AccountNo  
			   ,CNL.CreatedBy  
			   ,CNL.Remarks
			   ,dbo.fn_GetCustomerFullName_New(CNL.OldTitle,CNL.OldFirstName,CNL.OldMiddleName,CNL.OldLastName) AS [OldName]
			   ,dbo.fn_GetCustomerFullName_New(CNL.NewTitle,CNL.NewFirstName,CNL.NewMiddleName,CNL.NewLastName) AS [NewName]
				--,CNL.OldTitle  
			   --,CNL.NewTitle  
			   --,CNL.OldFirstName AS OldName  
			   --,CNL.NewFirstName AS [NewName]  
			   --,CNL.OldMiddleName  
			   --,CNL.NewMiddleName  
			   --,CNL.OldLastName  
			   --,CNL.NewLastName  
			   ,CNL.OldKnownAs AS OldSurName  
			   ,CNL.NewKnownAs AS NewSurName  
			   ,CONVERT(VARCHAR(30),ISNULL(CNL.ModifiedDate,CNL.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),CNL.CreatedDate,107) AS TransactionDate   
			   ,CONVERT(VARCHAR(30),ISNULL(CNL.ModifiedDate,CNL.CreatedDate),107) AS ModifiedDate   
			   ,ApproveSttus.ApprovalStatus
			   ,COUNT(0) OVER() AS TotalChanges  
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName
			   ,ISNULL(CNL.ModifedBy,CNL.CreatedBy) AS ModifiedBy
		 FROM Tbl_CustomerNameChangeLogs CNL  
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CNL.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CNL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CNL.ApproveStatusId 
		 -- changed by karteek end      
		 ORDER BY CNL.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')  
END  
--------------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerMeterChangeLogsWithLimit]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================    
-- Author:  T.Karthik    
-- Create date: 06-10-2014   
-- Modified By: Karteek.P  
-- Modified Date: 23-03-2015      
-- Description: The purpose of this procedure is to get limited Meter change request logs    
-- =============================================       
ALTER PROCEDURE [dbo].[USP_GetCustomerMeterChangeLogsWithLimit]    
(    
@XmlDoc xml    
)    
AS    
BEGIN    
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''    
      
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END 
    
	-- start By Karteek  
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END
	-- end By Karteek   
      
	SELECT(    
		 SELECT TOP 10 			
				(CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo
			   ,(CMI.OldMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.OldDials) + ' )') AS OldMeterNo
			   ,(CMI.NewMeterNo + ' ( ' + CONVERT(VARCHAR(10),CMI.NewDials) + ' )') AS NewMeterNo
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.OldMeterTypeId) AS OldMeterType    
			   ,(SELECT MeterType FROM Tbl_MMeterTypes WHERE MeterTypeId=CMI.NewMeterTypeId) AS NewMeterType
			   ,('Old Reading: '+CMI.OldMeterReading +' <br/> New Reading: '+ CONVERT(VARCHAR(10),CAST(ISNULL(NewMeterInitialReading,0) AS NUMERIC))) AS Readings
			   ,CMI.Remarks
			   ,CMI.CreatedBy
			   ,CustomerView.OldAccountNo
			   ,CustomerView.ClassName
			   ,CONVERT(VARCHAR(30),CMI.MeterChangedDate,107) AS AssignedMeterDate     
			   ,CONVERT(VARCHAR(30),ISNULL(CMI.ModifiedDate,CMI.CreatedDate),107) AS LastTransactionDate   
			   ,CONVERT(VARCHAR(30),ISNULL(CMI.ModifiedDate,CMI.CreatedDate),107) AS ModifiedDate   
			   ,CONVERT(VARCHAR(30),CMI.CreatedDate,107) AS TransactionDate     
			   ,ApproveSttus.ApprovalStatus
			   ,ISNULL(CMI.ModifiedBy,CMI.CreatedBy) AS ModifiedBy
			   ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name    
			   ,COUNT(0) OVER() AS TotalChanges    
		 FROM Tbl_CustomerMeterInfoChangeLogs  CMI    
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CMI.AccountNo=CustomerView.GlobalAccountNumber    
				AND CONVERT (DATE,CMI.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)    
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID     
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID  
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId  
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId  
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo  
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId  
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CMI.ApproveStatusId
		 ORDER BY CMI.CreatedDate DESC
		 -- changed by karteek end     
		FOR XML PATH('AuditTrayList'),TYPE    
	)    
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
	
END   
---------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerChangeLogsWithLimit]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================  
---- Author:  T.Karthik  
---- Create date: 06-10-2014 
---- Modified By: Karteek.P
---- Modified Date: 23-03-2015 
---- Description: The purpose of this procedure is to get BookNo change logs based on search criteria  
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
---- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		@BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END  
  
  -- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek     
     
	SELECT
	(  
		SELECT  TOP 10 
			 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) AS AccountNo  
			,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.OldStatus) AS OldStatus  
			,(SELECT [StatusName] FROM Tbl_MCustomerStatus WHERE StatusId=CCL.NewStatus) AS NewStatus  
			,CCL.Remarks
			,CCL.CreatedBy
			,CustomerView.OldAccountNo
			,CustomerView.ClassName
			,CONVERT(VARCHAR(30),ISNULL(CCL.ModifiedDate,CCL.CreatedDate),107) AS LastTransactionDate
			,CONVERT(VARCHAR(30),CCL.ChangeDate,107) AS StatusChangedDate
			,CONVERT(VARCHAR(30),CCL.CreatedDate,107) AS TransactionDate   
			,CONVERT(VARCHAR(30),ISNULL(CCL.ModifiedDate,CCL.CreatedDate),107) AS ModifiedDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges
			,ISNULL(CCL.ModifiedBy,CCL.CreatedBy) AS ModifiedBy
		FROM Tbl_CustomerActiveStatusChangeLogs CCL   
		INNER JOIN [UDV_CustomerDescription]  CustomerView ON CCL.AccountNo = CustomerView.GlobalAccountNumber  
			AND CONVERT (DATE,CCL.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CCL.ApproveStatusId
		ORDER BY CCL.CreatedDate DESC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')   
   
END  
----------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_GetCustomerAddressChangeLogsWithLimit]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014 
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015    
-- Description: The purpose of this procedure is to get limited Customer Address change logs  
-- Modified By : Bhimaraju V
-- Modified Date: 22-Apr-2015
--Desc : Getting Single field instead of all
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for showing in Grid
-- =============================================  
ALTER PROCEDURE [dbo].[USP_GetCustomerAddressChangeLogsWithLimit]   
(  
@XmlDoc xml  
)  
AS  
BEGIN  
  DECLARE @BU_ID VARCHAR(MAX)=''  
		,@SU_ID VARCHAR(MAX)=''  
		,@SC_ID VARCHAR(MAX)=''  
		,@CycleId VARCHAR(MAX)=''  
		,@TariffId VARCHAR(MAX)=''  
		,@BookNo VARCHAR(MAX)=''  
		,@FromDate VARCHAR(20)=''  
		,@ToDate VARCHAR(20)=''  
    
  SELECT  
		@BU_ID=C.value('(BU_ID)[1]','VARCHAR(MAX)')  
	   ,@SU_ID=C.value('(SU_ID)[1]','VARCHAR(MAX)')  
	   ,@SC_ID=C.value('(SC_ID)[1]','VARCHAR(MAX)')  
	   ,@CycleId=C.value('(CycleId)[1]','VARCHAR(MAX)')  
	   ,@TariffId=C.value('(ClassId)[1]','VARCHAR(MAX)')  
	   ,@BookNo=C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
	   ,@FromDate=C.value('(FromDate)[1]','VARCHAR(20)')  
	   ,@ToDate=C.value('(ToDate)[1]','VARCHAR(20)')  
  FROM @XmlDoc.nodes('RptAuditAddressBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate=dbo.fn_GetCurrentDateTime()-60  
			SET @Todate=dbo.fn_GetCurrentDateTime()  
		END  
  
	-- start By Karteek
     IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
   
  
   SELECT(  
		 SELECT TOP 10  
			 (CustomerView.AccountNo +' - '+ CustomerView.GlobalAccountNumber) As GlobalAccountNumber
			 ,dbo.fn_GetCustomerServiceAddress_New(OldPostal_HouseNo,OldPostal_StreetName,
			 OldPostal_Landmark,OldPostal_City,OldPostal_AreaCode,OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(NewPostal_HouseNo,NewPostal_StreetName,
			 NewPostal_Landmark,NewPostal_City,NewPostal_AreaCode,NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(OldService_HouseNo,OldService_StreetName,
			 OldService_Landmark,OldService_City,OldService_AreaCode,OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(NewService_HouseNo,NewService_StreetName,
			 NewService_Landmark,NewService_City,NewService_AreaCode,NewService_ZipCode) AS NewServiceAddress			  
			 ,CA.Remarks
			 ,CustomerView.ClassName
			 ,CustomerView.OldAccountNo
			 ,CONVERT(VARCHAR(30),CA.CreatedDate,107) AS TransactionDate   
			 ,CONVERT(VARCHAR(30),ISNULL(CA.ModifiedDate,CA.CreatedDate),107) AS LastTransactionDate   
			 ,CONVERT(VARCHAR(30),ISNULL(CA.ModifiedDate,CA.CreatedDate),107) AS ModifiedDate   
			 ,ApproveSttus.ApprovalStatus  
			 ,CA.CreatedBy  
			 ,ISNULL(CA.ModifiedBy,CA.CreatedBy) AS ModifiedBy  
			 ,dbo.fn_GetCustomerFullName_New(CustomerView.Title,CustomerView.FirstName,CustomerView.MiddleName,CustomerView.LastName) As Name  
			 ,COUNT(0) OVER() AS TotalChanges  
		 FROM Tbl_CustomerAddressChangeLog_New CA        
		 INNER JOIN [UDV_CustomerDescription]  CustomerView ON CA.GlobalAccountNumber=CustomerView.GlobalAccountNumber  
				AND CONVERT (DATE,CA.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)  
		 INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = CustomerView.BU_ID
		 INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = CustomerView.SU_ID
		 INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = CustomerView.ServiceCenterId
		 INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = CustomerView.CycleId
		 INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = CustomerView.BookNo
		 INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CustomerView.TariffId
		 INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = CA.ApprovalStatusId   
		 -- changed by karteek end    
		ORDER BY CA.CreatedDate DESC
		FOR XML PATH('AuditTrayAddressList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayAddressInfoByXml')  
END  
-------------------------------------------------------------------------------------

GO

/****** Object:  StoredProcedure [dbo].[USP_GetBookNoChangeLogsWithLimit]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  T.Karthik  
-- Create date: 06-10-2014  
-- Modified By: Karteek.P
-- Modified Date: 23-03-2015
-- Description: The purpose of this procedure is to get limited Book change request logs
-- Modified By: Bhimaraju V
-- Modified Date: 22-04-2015
-- Description: Some fields are added for shown in Grid
-- =============================================     
ALTER PROCEDURE [dbo].[USP_GetBookNoChangeLogsWithLimit]  
(  
	@XmlDoc xml  
)  
AS  
BEGIN  
	DECLARE @BU_ID VARCHAR(MAX) = ''  
		,@SU_ID VARCHAR(MAX) = ''  
		,@SC_ID VARCHAR(MAX) = ''  
		,@CycleId VARCHAR(MAX) = ''  
		,@TariffId VARCHAR(MAX) = ''  
		,@BookNo VARCHAR(MAX) = ''  
		,@FromDate VARCHAR(20) = ''  
		,@ToDate VARCHAR(20) = ''  
    
	SELECT  
		 @BU_ID = C.value('(BU_ID)[1]','VARCHAR(MAX)')  
		,@SU_ID = C.value('(SU_ID)[1]','VARCHAR(MAX)')  
		,@SC_ID = C.value('(SC_ID)[1]','VARCHAR(MAX)')  
		,@CycleId = C.value('(CycleId)[1]','VARCHAR(MAX)')  
		,@TariffId = C.value('(ClassId)[1]','VARCHAR(MAX)')  
		,@BookNo = C.value('(NewBookNo)[1]','VARCHAR(MAX)')  
		,@FromDate = C.value('(FromDate)[1]','VARCHAR(20)')  
		,@ToDate = C.value('(ToDate)[1]','VARCHAR(20)')  
	FROM @XmlDoc.nodes('RptAuditTrayBe') as T(C)  
    
    
	IF (@FromDate ='' OR @FromDate IS NULL)  
		BEGIN  
			SET @FromDate = dbo.fn_GetCurrentDateTime() - 60  
		END   
	IF (@ToDate ='' OR @ToDate IS NULL)  
		BEGIN  
			SET @Todate = dbo.fn_GetCurrentDateTime()  
		END   
   
   -- start By Karteek 
   IF(@BU_ID = '')
		BEGIN
			SELECT @BU_ID = STUFF((SELECT ',' + CAST(BU_ID AS VARCHAR(50)) 
					 FROM Tbl_BussinessUnits 
					 WHERE ActiveStatusId = 1
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SU_ID = '')
		BEGIN
			SELECT @SU_ID = STUFF((SELECT ',' + CAST(SU_ID AS VARCHAR(50)) 
					 FROM Tbl_ServiceUnits 
					 WHERE ActiveStatusId = 1
					 AND BU_ID IN(SELECT com FROM dbo.fn_Split(@BU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@SC_ID = '')
		BEGIN
			SELECT @SC_ID = STUFF((SELECT ',' + CAST(ServiceCenterId AS VARCHAR(50)) 
					 FROM Tbl_ServiceCenter
					 WHERE ActiveStatusId = 1
					 AND SU_ID IN(SELECT com FROM dbo.fn_Split(@SU_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@CycleId = '')
		BEGIN
			SELECT @CycleId = STUFF((SELECT ',' + CAST(CycleId AS VARCHAR(50)) 
					 FROM Tbl_Cycles
					 WHERE ActiveStatusId = 1
					 AND ServiceCenterId IN(SELECT com FROM dbo.fn_Split(@SC_ID,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@BookNo = '')
		BEGIN
			SELECT @BookNo = STUFF((SELECT ',' + CAST(BookNo AS VARCHAR(50)) 
					 FROM Tbl_BookNumbers
					 WHERE ActiveStatusId = 1
					 AND CycleId IN(SELECT com FROM dbo.fn_Split(@CycleId,','))
					 FOR XML PATH(''), TYPE)
					.value('.','NVARCHAR(MAX)'),1,1,'')
		END
	IF(@TariffId = '')
		BEGIN
			SELECT @TariffId = STUFF((SELECT ',' + CAST(SC.ClassID AS VARCHAR(50)) 
					 FROM Tbl_MTariffClasses C
					 JOIN Tbl_MTariffClasses SC ON C.ClassID=SC.RefClassID    
					  AND SC.IsActiveClass =1    
					  AND C.IsActiveClass=1
					  FOR XML PATH(''), TYPE)
					  .value('.','NVARCHAR(MAX)'),1,1,'')
		END 
	-- end By Karteek 
  
  
  SELECT
	(  
		SELECT TOP 10
			 (CD.AccountNo +' - '+ CD.GlobalAccountNumber) AS AccountNo
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldPostal_HouseNo,BookChange.OldPostal_StreetName,
				BookChange.OldPostal_Landmark,BookChange.OldPostal_City,BookChange.OldPostal_AreaCode,BookChange.OldPostal_ZipCode) AS OldPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewPostal_HouseNo,BookChange.NewPostal_StreetName,
				BookChange.NewPostal_Landmark,BookChange.NewPostal_City,BookChange.NewPostal_AreaCode,BookChange.NewPostal_ZipCode) AS NewPostalAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.OldService_HouseNo,BookChange.OldService_StreetName,
				BookChange.OldService_Landmark,BookChange.OldService_City,BookChange.OldService_AreaCode,BookChange.OldService_ZipCode) AS OldServiceAddress
			 ,dbo.fn_GetCustomerServiceAddress_New(BookChange.NewService_HouseNo,BookChange.NewService_StreetName,
				BookChange.NewService_Landmark,BookChange.NewService_City,BookChange.NewService_AreaCode,BookChange.NewService_ZipCode) AS NewServiceAddress

			 ,('BU : '+BDOld.BusinessUnitName +' ( '+BDOld.BUCode+ ' ) <br/> SU : '+
			  BDOld.ServiceUnitName +' ( '+BDOld.SUCode+ ' ) <br/> SC : '+
			  BDOld.ServiceCenterName +' ( '+BDOld.SCCode+ ' ) <br/> BG : '+
			  BDOld.CycleName +' ( '+BDOld.CycleCode+ ' ) <br/> Book : '+
			  BDOld.ID +' ( '+BDOld.BookCode+ ' )' ) AS OldBookNo
			  
			,('BU : '+BD.BusinessUnitName +' ( '+BD.BUCode+ ' ) <br/> SU : '+
			  BD.ServiceUnitName +' ( '+BD.SUCode+ ' ) <br/> SC : '+
			  BD.ServiceCenterName +' ( '+BD.SCCode+ ' ) <br/> BG : '+
			  BD.CycleName +' ( '+BD.CycleCode+ ' ) <br/> Book : '+
			  BD.ID +' ( '+BD.BookCode+ ' )' ) AS NewBookNo
			  
			,BookChange.Remarks
			,BookChange.CreatedBy
			,MTC.ClassName
			,ISNULL(BookChange.ModifiedBy,BookChange.CreatedBy) AS ModifiedBy
			,CONVERT(VARCHAR(50),ISNULL(BookChange.ModifiedDate,BookChange.CreatedDate),107) AS LastTransactionDate
			,CD.OldAccountNo
			,CONVERT(VARCHAR(30),BookChange.CreatedDate,107) AS TransactionDate   
			,CONVERT(VARCHAR(30),ISNULL(BookChange.ModifiedDate,BookChange.CreatedDate),107) AS ModifiedDate   
			,ApproveSttus.ApprovalStatus  
			,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) As Name  
			,COUNT(0) OVER() AS TotalChanges  
		FROM Tbl_BookNoChangeLogs  BookChange  
		INNER JOIN CUSTOMERS.Tbl_CustomersDetail CD ON CD.GlobalAccountNumber= BookChange.GlobalAccountNo
		AND CONVERT (DATE,BookChange.CreatedDate) BETWEEN CONVERT (DATE,@FromDate) AND CONVERT (DATE,@Todate)
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CD.GlobalAccountNumber= CPD.GlobalAccountNumber
		INNER JOIN UDV_BookNumberDetails BD ON BookChange.NewBookNo= BD.BookNo
		INNER JOIN UDV_BookNumberDetails BDOld ON BookChange.OldBookNo= BDOld.BookNo
		INNER JOIN (SELECT [com] AS BU_ID FROM dbo.fn_Split(@BU_ID,',')) BU ON BU.BU_ID = BD.BU_ID
		INNER JOIN (SELECT [com] AS SU_Id FROM dbo.fn_Split(@SU_ID,',')) SU ON SU.SU_Id = BD.SU_ID
		INNER JOIN (SELECT [com] AS SC_ID FROM dbo.fn_Split(@SC_ID,',')) SC ON SC.SC_ID = BD.ServiceCenterId
		INNER JOIN (SELECT [com] AS CycleId FROM dbo.fn_Split(@CycleId,',')) BG ON BG.CycleId = BD.CycleId
		INNER JOIN (SELECT [com] AS BookNo FROM dbo.fn_Split(@BookNo,',')) BN ON BN.BookNo = BD.BookNo
		INNER JOIN (SELECT [com] AS TariffId FROM dbo.fn_Split(@TariffId,',')) TC ON TC.TariffId = CPD.TariffClassID
		INNER JOIN Tbl_MTariffClasses MTC ON CPD.TariffClassID = MTC.ClassID
		INNER JOIN Tbl_MApprovalStatus ApproveSttus ON ApproveSttus.ApprovalStatusId = BookChange.ApprovalStatusId 
		ORDER BY  BookChange.CreatedDate DESC--,BD.BookSortOrder ASC--,--BD.CustomerSortOrder ASC
		FOR XML PATH('AuditTrayList'),TYPE  
	)  
	FOR XML PATH(''),ROOT('RptAuditTrayInfoByXml')    
END  
----------------------------------------------------------------------------------------------


GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_PDFReportDataBlk_Bill_Gen]    Script Date: 08/07/2015 19:03:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Karteek.P
-- Created Date: 23-07-2015
-- Description: Inserting Report Data 
-- Modified By: Neeraj Kanojiya
-- Modified Date: 3-Aug-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_Insert_PDFReportDataBlk_Bill_Gen]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
DECLARE @BillMonthList VARCHAR(MAX)
		,@BillYearList VARCHAR(MAX)
		,@Month INT 
		,@Year INT 
		,@LastMonth INT
		,@LastYear INT
		,@CreatedBy VARCHAR(50)
		
	--SELECT * FROM DBO.fn_splitTwo('6|2|2|','2|2|2|','|')
	
		SELECT  
		 @BillMonthList = C.value('(BillMonthList)[1]','varchar(max)')  
		,@BillYearList = C.value('(BillYearList)[1]','varchar(max)')   
		,@CreatedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillGenerationBe') as T(C)
	
	CREATE TABLE #BillMonthList(RowNum INT identity(1,1), BillMonth varchar(10), BillYear varchar(10))
	
	INSERT INTO #BillMonthList(BillMonth,BillYear)
	
	SELECT DISTINCT * FROM DBO.fn_splitTwo(@BillMonthList,@BillYearList,'|')
	
	DECLARE @index INT=0  
	DECLARE @rwCount INT  
	SET @rwCount=(SELECT MAX(RowNum) FROM #BillMonthList)  
	WHILE(@index<@rwCount) --loop
	BEGIN		
		SET @index=@index+1
		SET @Month=(SELECT BillMonth FROM #BillMonthList WHERE RowNum=@index)
		SET @Year=(SELECT BillYear FROM #BillMonthList WHERE RowNum=@index)
		
		IF(@Month = 1)
			BEGIN
				SET @LastMonth = 12
				SET @LastYear = @Year - 1
			END
		ELSE
			BEGIN
				SET @LastMonth = @Month - 1
				SET @LastYear = @Year
			END
			
		DELETE FROM Tbl_ReportBillingStatisticsBySCId WHERE MonthId = @Month AND YearId = @Year
	
		DELETE FROM Tbl_ReportBillingStatisticsByTariffId WHERE MonthId = @Month AND YearId = @Year
		
		--------------------------------------------------------------------------------------
		--CustomersList table for those customer who belongs to parameterised blll open month
		--------------------------------------------------------------------------------------
		
		SELECT 
			 CB.BU_ID
			,BU.BusinessUnitName
			,CB.SU_ID
			,SU.ServiceUnitName
			,CB.ServiceCenterId
			,SC.ServiceCenterName
			,CB.TariffId
			,TC.ClassName
			,CPD.CustomerTypeId
			,CT.CustomerType
			,CB.BillYear
			,CB.BillMonth
			,CB.AccountNo
			,CPD.ReadCodeID
			,CB.Usage
			,CB.NetFixedCharges
			,CB.NetEnergyCharges
			,CB.VATPercentage
			,CB.NetArrears
			,CB.VAT
			,CB.TotalBillAmount
			,CB.TotalBillAmountWithTax
			,CB.TotalBillAmountWithArrears
			,CB.ReadType
			,CB.PaidAmount
			,CAD.OutStandingAmount
			,CB.TotalBillAmountWithTax AS ClosingBalance
			,CB.BillNo
			,(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) AS NoOfStubs
		INTO #CustomersList
		FROM Tbl_CustomerBills CB 
		INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
		INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
		INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
		INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
		INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
		INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
		INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
		INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
		WHERE BillMonth = @Month AND BillYear = @Year

		--------------------------------------------------------------------------------------
		-- Total bill amount with tax after bill gen of last month.
		--------------------------------------------------------------------------------------
		
		SELECT 
			  AccountNo
			 ,TotalBillAmountWithTax
		INTO #CustomerOpeningBalance
		FROM Tbl_CustomerBills
		WHERE BillMonth = @LastMonth AND BillYear = @LastYear

		--------------------------------------------------------------------------------------
		-- Total no of customer in those tariff and cust type.
		--------------------------------------------------------------------------------------
		
		SELECT 
			  CPD.BookNo
			 ,BN.CycleId
			 ,BG.ServiceCenterId
			 ,SC.SU_ID
			 ,SU.BU_ID
			 ,CPD.TariffClassID
			 ,CPD.CustomerTypeId
			 ,GlobalAccountNumber
		INTO #CustomerPopulation
		FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
		INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CPD.BookNo  
		INNER JOIN Tbl_Cycles BG ON BN.CycleId = BG.CycleId
		INNER JOIN Tbl_ServiceCenter SC ON BG.ServiceCenterId = SC.ServiceCenterId
		INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID

		INSERT INTO Tbl_ReportBillingStatisticsBySCId
		(
			 BU_ID
			,BusinessUnitName
			,SU_ID
			,ServiceUnitName
			,SC_ID
			,ServiceCenterName
			,CustomerTypeId
			,CustomerType
			,YearId
			,MonthId
			,ActiveCustomersCount
			,InActiveCustomersCount
			,HoldCustomersCount
			,TotalPopulationCount
			,EnergyDelivered
			,FixedCharges
			,NoOfBilledCustomers
			,TotalAmountBilled
			,MINFCCustomersCount
			,ReadCustomersCount
			,ESTCustomersCount
			,DirectCustomersCount
			,EnergyBilled
			,RevenueBilled
			--,Payments
			,OpeningBalance
			,ClosingBalance
			--,RevenueCollected
			,KVASold
			--,TotalAmountCollected
			--,NoOfStubs
			,WeightedAvg
			,CreatedBy
			,CreatedDate 
		)
		SELECT 
			 CL.BU_ID
			,CL.BusinessUnitName
			,CL.SU_ID
			,CL.ServiceUnitName
			,CL.ServiceCenterId
			,CL.ServiceCenterName
			,CL.CustomerTypeId
			,CL.CustomerType
			,CL.BillYear
			,CL.BillMonth
			,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
			,0 AS InActiveCustomersCount
			,0 AS HoldCustomersCount
			,(SELECT COUNT(0) FROM #CustomerPopulation CP 
					WHERE CP.BU_ID = CL.BU_ID AND CP.SU_ID = CL.SU_ID
						AND CP.ServiceCenterId = CL.ServiceCenterId 
						AND CP.CustomerTypeId = CL.CustomerTypeId) AS TotalPopulationCount
			,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
			,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
			,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
			,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS TotalAmountBilled
			,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
						THEN 1
						ELSE 0 END) AS MINFCCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
						THEN 1
						ELSE 0 END) AS ReadCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
						THEN 1
						ELSE 0 END) AS ESTCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
						THEN 1
						ELSE 0 END) AS DirectCustomersCount
			,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
			,SUM(ISNULL(CL.NetEnergyCharges,0)) AS RevenueBilled
			--,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
			,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
			,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
			--,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
			,0 AS KVASold
			--,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
			--,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
			,0 AS WeightedAvg
			,@CreatedBy AS CreatedBy
			,dbo.fn_GetCurrentDateTime() AS CreatedDate
		FROM #CustomersList CL 
		LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
		GROUP BY CL.BU_ID
			,CL.BusinessUnitName
			,CL.SU_ID
			,CL.ServiceUnitName
			,CL.ServiceCenterId
			,CL.ServiceCenterName
			,CL.CustomerTypeId
			,CL.CustomerType
			,CL.BillYear
			,CL.BillMonth
			
		INSERT INTO Tbl_ReportBillingStatisticsByTariffId
		(
			 BU_ID
			,BusinessUnitName
			,TariffId
			,TariffName
			,YearId
			,MonthId
			,ActiveCustomersCount
			,InActiveCustomersCount
			,HoldCustomersCount
			,TotalPopulationCount
			,NoOfBilledCustomers
			,EnergyDelivered
			,FixedCharges
			,TotalAmountBilled
			,MINFCCustomersCount
			,ReadCustomersCount
			,ESTCustomersCount
			,DirectCustomersCount
			,EnergyBilled
			,RevenueBilled
			--,Payments
			,OpeningBalance
			,ClosingBalance
			--,RevenueCollected
			,KVASold
			--,TotalAmountCollected
			--,NoOfStubs
			,WeightedAvg
			,CreatedBy
			,CreatedDate 
		)
		SELECT 
			 CL.BU_ID
			,CL.BusinessUnitName
			,CL.TariffId
			,CL.ClassName
			,CL.BillYear
			,CL.BillMonth
			,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
			,0 AS InActiveCustomersCount
			,0 AS HoldCustomersCount
			,(SELECT COUNT(0) FROM #CustomerPopulation CP 
					WHERE CP.BU_ID = CL.BU_ID  
						AND CP.TariffClassID = CL.TariffId) AS TotalPopulationCount
			,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
			,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
			,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
			,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS TotalAmountBilled
			,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
						THEN 1
						ELSE 0 END) AS MINFCCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
						THEN 1
						ELSE 0 END) AS ReadCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
						THEN 1
						ELSE 0 END) AS ESTCustomersCount
			,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
						THEN 1
						ELSE 0 END) AS DirectCustomersCount
			,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
			,SUM(ISNULL(CL.NetEnergyCharges,0)) AS RevenueBilled
			--,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
			,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
			,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
			--,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
			,0 AS KVASold
			--,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
			--,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
			,0 AS WeightedAvg
			,@CreatedBy AS CreatedBy
			,dbo.fn_GetCurrentDateTime() AS CreatedDate
		FROM #CustomersList CL 
		LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
		GROUP BY CL.BU_ID
			,CL.BusinessUnitName
			,CL.TariffId
			,CL.ClassName
			,CL.BillYear
			,CL.BillMonth		
	END			
		BEGIN TRY			
			DROP TABLE #CustomersList
			DROP TABLE #CustomerOpeningBalance
			DROP TABLE #CustomerPopulation	
			DROP TABLE #BillMonthList
		END try
		BEGIN CATCH
		END CATCH

	SELECT 1 AS IsSuccess
	FOR XML PATH('BillGenerationBe'),TYPE  
	
END



GO

/****** Object:  StoredProcedure [dbo].[USP_Update_PDFReportData]    Script Date: 08/07/2015 19:03:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Bhimaraju Vanka
-- Created Date: 05-08-2015
-- Description: Updating Report Data 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_Update_PDFReportData]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
			DECLARE @Month INT 
					,@Year INT 
					,@LastMonth INT
					,@LastYear INT
					,@CreatedBy VARCHAR(50)
					
			SELECT  
				 @Month = C.value('(Month)[1]','INT')  
				,@Year = C.value('(Year)[1]','INT')   
				,@CreatedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
			FROM @XmlDoc.nodes('BillingMonthOpenBE') as T(C)
	
	----------Customer Details-----------
		SELECT	 CB.BillYear
				,CB.BillMonth
				,CB.BillNo
				,CB.TariffId
				,CB.ServiceCenterId
				,CB.SU_ID
				,CB.BU_ID
				,CB.AccountNo
				,CPD.CustomerTypeId
				,CB.CustomerBillId
				,CB.BillGeneratedDate
		INTO #CustomerBills
		FROM Tbl_CustomerBills CB 
		INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo
		WHERE CB.BillYear=@Year AND CB.BillMonth=@Month

	----------Payments Details-----------
		SELECT	 SUM(BP.PaidAmount) AS BillPaidAmount
				,CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,COUNT(0) AS NoOfStubs
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId
		INTO #Payments
		FROM Tbl_CustomerPayments BP
		JOIN #CustomerBills CB ON CB.AccountNo=BP.AccountNo
			AND CONVERT(DATE,BP.RecievedDate) >= CONVERT(DATE,CB.BillGeneratedDate)
		GROUP BY CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId
				
		----------Adjustments Details-----------		
		SELECT	 SUM(BP.TotalAmountEffected) AS BillPaidAmount
				,CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,COUNT(0) AS NoOfStubs
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId
		INTO #Adjustments
		FROM Tbl_BillAdjustments BP
		JOIN #CustomerBills CB ON BP.AccountNo =CB.AccountNo
			AND CONVERT(DATE,BP.CreatedDate) >= CONVERT(DATE,CB.BillGeneratedDate)
		GROUP BY CB.BillYear
				,CB.BillMonth
				,CB.TariffId
				,CB.BU_ID
				,CB.SU_ID
				,CB.ServiceCenterId
				,CB.CustomerTypeId

		----------Tbl_ReportBillingStatisticsBySCId Details-----------		
		UPDATE RBS SET   Payments = ISNULL(Payments,0)+ISNULL(P.BillPaidAmount,0)
						,RevenueCollected= ISNULL(RevenueCollected,0)+ISNULL(P.BillPaidAmount,0)
						,TotalAmountCollected= ISNULL(TotalAmountCollected,0)+ISNULL(P.BillPaidAmount,0)
						,NoOfStubs= ISNULL(RBS.NoOfStubs,0)+ISNULL(P.NoOfStubs,0)
		FROM Tbl_ReportBillingStatisticsBySCId RBS
		JOIN #Payments P ON P.BU_ID=RBS.BU_ID
		AND P.SU_ID=RBS.SU_ID
		AND P.ServiceCenterId =RBS.SC_ID
		AND RBS.MonthId=P.BillMonth
		AND RBS.YearId=P.BillYear
		AND RBS.CustomerTypeId=P.CustomerTypeId
		
		UPDATE RBS SET   AdjustmentAmount = ISNULL(AdjustmentAmount,0)+ISNULL(A.BillPaidAmount,0)
						,RevenueCollected= ISNULL(RevenueCollected,0)+ISNULL(A.BillPaidAmount,0)
						,TotalAmountCollected= ISNULL(TotalAmountCollected,0)+ISNULL(A.BillPaidAmount,0)
						,NoOfStubs= ISNULL(RBS.NoOfStubs,0)+ISNULL(A.NoOfStubs,0)
		FROM Tbl_ReportBillingStatisticsBySCId RBS
		JOIN #Adjustments A ON A.BU_ID=RBS.BU_ID
		AND A.SU_ID=RBS.SU_ID
		AND A.ServiceCenterId =RBS.SC_ID
		AND RBS.MonthId=A.BillMonth
		AND RBS.YearId=A.BillYear
		AND RBS.CustomerTypeId=A.CustomerTypeId
		
		----------Tbl_ReportBillingStatisticsByTariffId Updation-----------		
		UPDATE RBST SET   Payments = ISNULL(Payments,0)+ISNULL(P.BillPaidAmount,0)
						,RevenueCollected= ISNULL(RevenueCollected,0)+ISNULL(P.BillPaidAmount,0)
						,TotalAmountCollected= ISNULL(TotalAmountCollected,0)+ISNULL(P.BillPaidAmount,0)
						,NoOfStubs= ISNULL(RBST.NoOfStubs,0)+ISNULL(P.NoOfStubs,0)
		FROM Tbl_ReportBillingStatisticsByTariffId RBST
		JOIN #Payments P ON P.BU_ID=RBST.BU_ID
		AND RBST.MonthId=P.BillMonth
		AND RBST.YearId=P.BillYear
		AND RBST.TariffId=P.TariffId
		
		UPDATE RBST SET   AdjustmentAmount = ISNULL(AdjustmentAmount,0)+ISNULL(A.BillPaidAmount,0)
						,RevenueCollected= ISNULL(RevenueCollected,0)+ISNULL(A.BillPaidAmount,0)
						,TotalAmountCollected= ISNULL(TotalAmountCollected,0)+ISNULL(A.BillPaidAmount,0)
						,NoOfStubs= ISNULL(RBST.NoOfStubs,0)+ISNULL(A.NoOfStubs,0)
		FROM Tbl_ReportBillingStatisticsByTariffId RBST
		JOIN #Adjustments A ON A.BU_ID=RBST.BU_ID
		AND RBST.MonthId=A.BillMonth
		AND RBST.YearId=A.BillYear
		AND RBST.TariffId=A.TariffId		
		
		UPDATE Tbl_ReportBillingStatisticsBySCId
		SET ClosingBalance = ISNULL(OpeningBalance,0) - ISNULL(RevenueCollected,0) + ISNULL(TotalAmountBilled,0)
		WHERE MonthId = @Month
			AND YearId = @Year
			
		UPDATE Tbl_ReportBillingStatisticsByTariffId
		SET ClosingBalance = ISNULL(OpeningBalance,0) - ISNULL(RevenueCollected,0) + ISNULL(TotalAmountBilled,0)
		WHERE MonthId = @Month
			AND YearId = @Year

		DROP TABLE #CustomerBills
		DROP TABLE #Payments
		DROP TABLE #Adjustments
	
	SELECT 1 AS IsSuccess
	FOR XML PATH('BillingMonthOpenBE'),TYPE  

END
GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_PDFReportData_BillGen]    Script Date: 08/07/2015 19:03:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Karteek.P
-- Created Date: 23-07-2015
-- Description: Inserting Report Data 
-- Modified By: Neeraj kanojya
-- Date		  : 6-Aug-2015
-- =============================================  
ALTER PROCEDURE [dbo].[USP_Insert_PDFReportData_BillGen]  
(  
	@Month INT 
	,@Year INT 
	,@CreatedBy VARCHAR(50)
)  
AS  
BEGIN   
SET NOCOUNT ON;
DECLARE @LastMonth INT
		,@LastYear INT		
		
	
	DELETE FROM Tbl_ReportBillingStatisticsBySCId WHERE MonthId = @Month AND YearId = @Year
	
	DELETE FROM Tbl_ReportBillingStatisticsByTariffId WHERE MonthId = @Month AND YearId = @Year
		
	IF(@Month = 1)
		BEGIN
			SET @LastMonth = 12
			SET @LastYear = @Year - 1
		END
	ELSE
		BEGIN
			SET @LastMonth = @Month - 1
			SET @LastYear = @Year
		END
		
	SELECT 
		 CB.BU_ID
		,BU.BusinessUnitName
		,CB.SU_ID
		,SU.ServiceUnitName
		,CB.ServiceCenterId
		,SC.ServiceCenterName
		,CB.TariffId
		,TC.ClassName
		,CPD.CustomerTypeId
		,CT.CustomerType
		,CB.BillYear
		,CB.BillMonth
		,CB.AccountNo
		,CPD.ReadCodeID
		,CB.Usage
		,CB.NetFixedCharges
		,CB.NetEnergyCharges
		,CB.VATPercentage
		,CB.NetArrears
		,CB.VAT
		,CB.TotalBillAmount
		,CB.TotalBillAmountWithTax
		,CB.TotalBillAmountWithArrears
		,CB.ReadType
		,CB.PaidAmount
		,CAD.OutStandingAmount
		,CB.TotalBillAmountWithTax AS ClosingBalance
		,CB.BillNo
		,(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) AS NoOfStubs
	INTO #CustomersList
	FROM Tbl_CustomerBills CB 
	INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
	INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
	INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
	INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
	INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
	WHERE BillMonth = @Month AND BillYear = @Year

	SELECT 
		  AccountNo
		 ,TotalBillAmountWithTax
	INTO #CustomerOpeningBalance
	FROM Tbl_CustomerBills
	WHERE BillMonth = @LastMonth AND BillYear = @LastYear

	SELECT 
		  CPD.BookNo
		 ,BN.CycleId
		 ,BG.ServiceCenterId
		 ,SC.SU_ID
		 ,SU.BU_ID
		 ,CPD.TariffClassID
		 ,CPD.CustomerTypeId
		 ,GlobalAccountNumber
	INTO #CustomerPopulation
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CPD.BookNo  
	INNER JOIN Tbl_Cycles BG ON BN.CycleId = BG.CycleId
	INNER JOIN Tbl_ServiceCenter SC ON BG.ServiceCenterId = SC.ServiceCenterId
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID

	INSERT INTO Tbl_ReportBillingStatisticsBySCId
	(
		 BU_ID
		,BusinessUnitName
		,SU_ID
		,ServiceUnitName
		,SC_ID
		,ServiceCenterName
		,CustomerTypeId
		,CustomerType
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,EnergyDelivered
		,FixedCharges
		,NoOfBilledCustomers
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		--,Payments
		,OpeningBalance
		,ClosingBalance
		--,RevenueCollected
		,KVASold
		--,TotalAmountCollected
		--,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID AND CP.SU_ID = CL.SU_ID
					AND CP.ServiceCenterId = CL.ServiceCenterId 
					AND CP.CustomerTypeId = CL.CustomerTypeId) AS TotalPopulationCount
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS RevenueBilled
		--,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		--,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		--,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		--,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		
	INSERT INTO Tbl_ReportBillingStatisticsByTariffId
	(
		 BU_ID
		,BusinessUnitName
		,TariffId
		,TariffName
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,NoOfBilledCustomers
		,EnergyDelivered
		,FixedCharges
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		--,Payments
		,OpeningBalance
		,ClosingBalance
		--,RevenueCollected
		,KVASold
		--,TotalAmountCollected
		--,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID  
					AND CP.TariffClassID = CL.TariffId) AS TotalPopulationCount
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS RevenueBilled
		--,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		--,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		--,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		--,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth

	DROP TABLE #CustomersList
	DROP TABLE #CustomerOpeningBalance
	DROP TABLE #CustomerPopulation	
	
END
GO

/****** Object:  StoredProcedure [dbo].[USP_Insert_PDFReportData]    Script Date: 08/07/2015 19:03:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created By: Karteek.P
-- Created Date: 23-07-2015
-- Description: Inserting Report Data 
-- =============================================  
ALTER PROCEDURE [dbo].[USP_Insert_PDFReportData]  
(  
	@XmlDoc XML=null  
)  
AS  
BEGIN   
SET NOCOUNT ON;
DECLARE @Month INT 
		,@Year INT 
		,@LastMonth INT
		,@LastYear INT
		,@CreatedBy VARCHAR(50)
		
	SELECT  
		 @Month = C.value('(Month)[1]','INT')  
		,@Year = C.value('(Year)[1]','INT')   
		,@CreatedBy = C.value('(ModifiedBy)[1]','VARCHAR(50)')
	FROM @XmlDoc.nodes('BillingMonthOpenBE') as T(C)
	
	DELETE FROM Tbl_ReportBillingStatisticsBySCId WHERE MonthId = @Month AND YearId = @Year
	
	DELETE FROM Tbl_ReportBillingStatisticsByTariffId WHERE MonthId = @Month AND YearId = @Year
		
	IF(@Month = 1)
		BEGIN
			SET @LastMonth = 12
			SET @LastYear = @Year - 1
		END
	ELSE
		BEGIN
			SET @LastMonth = @Month - 1
			SET @LastYear = @Year
		END
		
	SELECT 
		 CB.BU_ID
		,BU.BusinessUnitName
		,CB.SU_ID
		,SU.ServiceUnitName
		,CB.ServiceCenterId
		,SC.ServiceCenterName
		,CB.TariffId
		,TC.ClassName
		,CPD.CustomerTypeId
		,CT.CustomerType
		,CB.BillYear
		,CB.BillMonth
		,CB.AccountNo
		,CPD.ReadCodeID
		,CB.Usage
		,CB.NetFixedCharges
		,CB.NetEnergyCharges
		,CB.VATPercentage
		,CB.NetArrears
		,CB.VAT
		,CB.TotalBillAmount
		,CB.TotalBillAmountWithTax
		,CB.TotalBillAmountWithArrears
		,CB.ReadType
		,CB.PaidAmount
		,CAD.OutStandingAmount
		,CB.TotalBillAmountWithTax AS ClosingBalance
		,CB.BillNo
		,(SELECT COUNT(0) FROM Tbl_CustomerBillPayments CBP WHERE CBP.BillNo = CB.BillNo) AS NoOfStubs
	INTO #CustomersList
	FROM Tbl_CustomerBills CB 
	INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = CB.BU_ID --AND BU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = CB.SU_ID --AND SU.ActiveStatusId = 1
	INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId = CB.ServiceCenterId --AND SC.ActiveStatusId = 1
	INNER JOIN Tbl_Cycles BG ON BG.ServiceCenterId = SC.ServiceCenterId --AND BG.ActiveStatusId = 1
	INNER JOIN Tbl_BookNumbers BN ON BN.CycleId = BG.CycleId --AND BN.ActiveStatusId = 1
	INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CB.AccountNo AND CPD.BookNo = BN.BookNo
	INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber = CPD.GlobalAccountNumber
	INNER JOIN Tbl_MTariffClasses TC ON TC.ClassID = CB.TariffId AND CPD.TariffClassID = TC.ClassID --AND TC.IsActiveClass = 1
	INNER JOIN Tbl_MCustomerTypes CT ON CT.CustomerTypeId = CPD.CustomerTypeId --AND CT.ActiveStatusId = 1
	WHERE BillMonth = @Month AND BillYear = @Year

	SELECT 
		  AccountNo
		 ,TotalBillAmountWithTax
	INTO #CustomerOpeningBalance
	FROM Tbl_CustomerBills
	WHERE BillMonth = @LastMonth AND BillYear = @LastYear

	SELECT 
		  CPD.BookNo
		 ,BN.CycleId
		 ,BG.ServiceCenterId
		 ,SC.SU_ID
		 ,SU.BU_ID
		 ,CPD.TariffClassID
		 ,CPD.CustomerTypeId
		 ,GlobalAccountNumber
	INTO #CustomerPopulation
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CPD 
	INNER JOIN Tbl_BookNumbers BN ON BN.BookNo = CPD.BookNo  
	INNER JOIN Tbl_Cycles BG ON BN.CycleId = BG.CycleId
	INNER JOIN Tbl_ServiceCenter SC ON BG.ServiceCenterId = SC.ServiceCenterId
	INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID = SC.SU_ID

	INSERT INTO Tbl_ReportBillingStatisticsBySCId
	(
		 BU_ID
		,BusinessUnitName
		,SU_ID
		,ServiceUnitName
		,SC_ID
		,ServiceCenterName
		,CustomerTypeId
		,CustomerType
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,EnergyDelivered
		,FixedCharges
		,NoOfBilledCustomers
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		,Payments
		,OpeningBalance
		,ClosingBalance
		,RevenueCollected
		,KVASold
		,TotalAmountCollected
		,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID AND CP.SU_ID = CL.SU_ID
					AND CP.ServiceCenterId = CL.ServiceCenterId 
					AND CP.CustomerTypeId = CL.CustomerTypeId) AS TotalPopulationCount
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS RevenueBilled
		,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.SU_ID
		,CL.ServiceUnitName
		,CL.ServiceCenterId
		,CL.ServiceCenterName
		,CL.CustomerTypeId
		,CL.CustomerType
		,CL.BillYear
		,CL.BillMonth
		
	INSERT INTO Tbl_ReportBillingStatisticsByTariffId
	(
		 BU_ID
		,BusinessUnitName
		,TariffId
		,TariffName
		,YearId
		,MonthId
		,ActiveCustomersCount
		,InActiveCustomersCount
		,HoldCustomersCount
		,TotalPopulationCount
		,NoOfBilledCustomers
		,EnergyDelivered
		,FixedCharges
		,TotalAmountBilled
		,MINFCCustomersCount
		,ReadCustomersCount
		,ESTCustomersCount
		,DirectCustomersCount
		,EnergyBilled
		,RevenueBilled
		,Payments
		,OpeningBalance
		,ClosingBalance
		,RevenueCollected
		,KVASold
		,TotalAmountCollected
		,NoOfStubs
		,WeightedAvg
		,CreatedBy
		,CreatedDate 
	)
	SELECT 
		 CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth
		,(COUNT(CL.AccountNo)) AS ActiveCustomersCount
		,0 AS InActiveCustomersCount
		,0 AS HoldCustomersCount
		,(SELECT COUNT(0) FROM #CustomerPopulation CP 
				WHERE CP.BU_ID = CL.BU_ID  
					AND CP.TariffClassID = CL.TariffId) AS TotalPopulationCount
		,(COUNT(CL.AccountNo)) AS NoOfBilledCustomers
		,SUM(ISNULL(CL.Usage,0)) AS EnergyDelivered
		,SUM(ISNULL(CL.NetFixedCharges,0)) AS FixedCharges
		,SUM(ISNULL(CL.TotalBillAmountWithTax,0)) AS TotalAmountBilled
		,SUM(CASE WHEN ISNULL(CL.NetEnergyCharges,0) = 0 AND ISNULL(CL.NetFixedCharges,0) != 0
					THEN 1
					ELSE 0 END) AS MINFCCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 2
					THEN 1
					ELSE 0 END) AS ReadCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 3
					THEN 1
					ELSE 0 END) AS ESTCustomersCount
		,SUM(CASE WHEN ISNULL(CL.ReadType,0) = 1
					THEN 1
					ELSE 0 END) AS DirectCustomersCount
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS EnergyBilled
		,SUM(ISNULL(CL.NetEnergyCharges,0)) AS RevenueBilled
		,SUM(ISNULL(CL.PaidAmount,0)) AS Payments
		,SUM(ISNULL(COB.TotalBillAmountWithTax,0)) AS OpeningBalance
		,SUM(ISNULL(CL.ClosingBalance,0)) AS ClosingBalance
		,SUM(ISNULL(CL.PaidAmount,0)) AS RevenueCollected
		,0 AS KVASold
		,SUM(ISNULL(CL.PaidAmount,0)) AS TotalAmountCollected
		,SUM(ISNULL(CL.NoOfStubs,0)) AS NoOfStubs
		,0 AS WeightedAvg
		,@CreatedBy AS CreatedBy
		,dbo.fn_GetCurrentDateTime() AS CreatedDate
	FROM #CustomersList CL 
	LEFT JOIN #CustomerOpeningBalance COB ON CL.AccountNo = COB.AccountNo
	GROUP BY CL.BU_ID
		,CL.BusinessUnitName
		,CL.TariffId
		,CL.ClassName
		,CL.BillYear
		,CL.BillMonth

	DROP TABLE #CustomersList
	DROP TABLE #CustomerOpeningBalance
	DROP TABLE #CustomerPopulation
	
	SELECT 1 AS IsSuccess
	FOR XML PATH('BillingMonthOpenBE'),TYPE  

END
GO


