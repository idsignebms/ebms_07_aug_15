
GO
-- =============================================
-- Author:		Faiz-ID103
-- Create date: 17-Aug-2015
-- Description:	This purpose of this procedure is to check IsSuperAdmin based on RoleId
-- =============================================
CREATE PROCEDURE USP_IsSuperAdmin
(
@XmlDoc XML
)
AS
BEGIN
	DECLARE @UserId VARCHAR(50)    
			,@SuperAdminRoleId INT
		 ,@IsSuccess BIT=0   
 SELECT     
	@UserId=C.value('(UserId)[1]','VARCHAR(50)')    
	FROM @XmlDoc.nodes('LoginPageBE') as T(C)  
	
	Select @SuperAdminRoleId=RoleId From Tbl_UserDetails where UserId=@UserId
	
	IF(@SuperAdminRoleId=1)--Checking for SuperAdmin
	BEGIN
		SET @IsSuccess = 1
	END
	
	Select @IsSuccess AS IsSuccess
	FOR XML PATH('LoginPageBE'),TYPE    
END
GO
