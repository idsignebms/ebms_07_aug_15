GO
/****** Object:  Table [dbo].[Tbl_MApprovalStatus]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MApprovalStatus](
	[ApprovalStatusId] [int] IDENTITY(0,1) NOT NULL,
	[ApprovalStatus] [varchar](50) NULL,
	[StatusCode] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[ApprovalStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MApprovalStatus] ON
INSERT [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId], [ApprovalStatus], [StatusCode]) VALUES (0, N'New', N'N')
INSERT [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId], [ApprovalStatus], [StatusCode]) VALUES (1, N'Process', N'P')
INSERT [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId], [ApprovalStatus], [StatusCode]) VALUES (2, N'Approved', N'A')
INSERT [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId], [ApprovalStatus], [StatusCode]) VALUES (3, N'Rejected', N'R')
INSERT [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId], [ApprovalStatus], [StatusCode]) VALUES (4, N'Hold', N'H')
SET IDENTITY_INSERT [dbo].[Tbl_MApprovalStatus] OFF
/****** Object:  Table [dbo].[Tbl_MApplicationProccessedBy]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MApplicationProccessedBy](
	[ProccessedById] [int] IDENTITY(1,1) NOT NULL,
	[ProcessedBy] [varchar](200) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProccessedById] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MApplicationProccessedBy] ON
INSERT [dbo].[Tbl_MApplicationProccessedBy] ([ProccessedById], [ProcessedBy], [Details], [ActiveStatusId]) VALUES (1, N'BEDC', NULL, 1)
INSERT [dbo].[Tbl_MApplicationProccessedBy] ([ProccessedById], [ProcessedBy], [Details], [ActiveStatusId]) VALUES (2, N'Others', NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MApplicationProccessedBy] OFF
/****** Object:  Table [dbo].[Tbl_MActiveStatusDetails]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MActiveStatusDetails](
	[ActiveStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[ActiveStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MActiveStatusDetails] ON
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (1, N'Active')
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (2, N'InActive')
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (3, N'Delete')
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (4, N'ReDeactivate')
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (5, N'Close')
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (6, N'Block')
SET IDENTITY_INSERT [dbo].[Tbl_MActiveStatusDetails] OFF
/****** Object:  Table [dbo].[Tbl_MAccountTypes]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MAccountTypes](
	[AccountTypeId] [int] IDENTITY(1,1) NOT NULL,
	[AccountType] [varchar](100) NULL,
	[Details] [varchar](max) NULL,
	[AccountCode] [varchar](10) NULL,
	[IsMaster] [bit] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AccountTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MAccountTypes] ON
INSERT [dbo].[Tbl_MAccountTypes] ([AccountTypeId], [AccountType], [Details], [AccountCode], [IsMaster], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Commercial', N'---', N'C', 1, 1, NULL, NULL, N'Admin', CAST(0x0000A46B016149AC AS DateTime))
INSERT [dbo].[Tbl_MAccountTypes] ([AccountTypeId], [AccountType], [Details], [AccountCode], [IsMaster], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Govt', NULL, N'G', 1, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MAccountTypes] ([AccountTypeId], [AccountType], [Details], [AccountCode], [IsMaster], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Street', NULL, N'S', 1, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MAccountTypes] ([AccountTypeId], [AccountType], [Details], [AccountCode], [IsMaster], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'Personal', NULL, N'P', 1, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MAccountTypes] ([AccountTypeId], [AccountType], [Details], [AccountCode], [IsMaster], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'Industrial', N'---', N'D', 1, 1, NULL, NULL, N'admin', CAST(0x0000A468013BE18E AS DateTime))
INSERT [dbo].[Tbl_MAccountTypes] ([AccountTypeId], [AccountType], [Details], [AccountCode], [IsMaster], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'Army / Military', N'Sample', N'A', 0, 1, N'Admin', CAST(0x0000A468013DEFF8 AS DateTime), N'Admin', CAST(0x0000A46B01614310 AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_MAccountTypes] OFF
/****** Object:  Table [dbo].[TBL_FunctionApprovalRole]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FunctionApprovalRole](
	[ApprovalRoleId] [int] IDENTITY(1,1) NOT NULL,
	[FunctionId] [int] NULL,
	[RoleId] [int] NULL,
	[Level] [int] NULL,
	[IsFinalApproval] [bit] NULL,
	[UserIds] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ApprovalRoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_FunctionApprovalRole] ON
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 1, 6, 1, 0, NULL, N'Admin', CAST(0x0000A468013ED9A2 AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 1, 2, 2, 0, NULL, N'Admin', CAST(0x0000A468013ED9A2 AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 1, 1, 3, 1, N'2', N'Admin', CAST(0x0000A468013ED9A2 AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 3, 2, 1, 0, NULL, N'admin', CAST(0x0000A468013EFFAA AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 3, 1, 2, 1, N'2', N'admin', CAST(0x0000A468013EFFAA AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, 8, 2, 1, 0, NULL, N'admin', CAST(0x0000A46D010883F1 AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, 8, 1, 2, 1, NULL, N'admin', CAST(0x0000A46D010883F1 AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, 10, 2, 1, 0, N'14', N'admin', CAST(0x0000A46F0150A8AC AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, 10, 1, 2, 1, N'2', N'admin', CAST(0x0000A46F0150A8AC AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, 9, 2, 1, 0, N'14', N'stu', CAST(0x0000A47000BB0925 AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, 9, 1, 2, 1, N'2', N'stu', CAST(0x0000A47000BB0925 AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, 7, 6, 1, 0, NULL, N'admin', CAST(0x0000A470014576BE AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, 7, 2, 2, 0, N'14', N'admin', CAST(0x0000A470014576BE AS DateTime), NULL, NULL)
INSERT [dbo].[TBL_FunctionApprovalRole] ([ApprovalRoleId], [FunctionId], [RoleId], [Level], [IsFinalApproval], [UserIds], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, 7, 1, 3, 1, N'2', N'admin', CAST(0x0000A470014576BE AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[TBL_FunctionApprovalRole] OFF
/****** Object:  Table [dbo].[TBL_FunctionalAccessPermission]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FunctionalAccessPermission](
	[FunctionId] [int] IDENTITY(1,1) NOT NULL,
	[Function] [varchar](200) NULL,
	[AccessLevels] [int] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[FunctionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_FunctionalAccessPermission] ON
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Bill Adjustment', 3, 0, N'Admin', CAST(0x0000A3E300000000 AS DateTime), N'Admin', CAST(0x0000A468013EE0D1 AS DateTime))
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Payment Entry', 2, 0, N'Admin', CAST(0x0000A3E300000000 AS DateTime), N'Admin', CAST(0x0000A3F9010247CF AS DateTime))
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Change Customer Tariff', 2, 1, N'Admin', CAST(0x0000A3E300000000 AS DateTime), N'admin', CAST(0x0000A468013EFFAA AS DateTime))
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'Change Customer Book No', 1, 0, N'Admin', CAST(0x0000A3E300000000 AS DateTime), N'Admin', CAST(0x0000A3F000C4A9BC AS DateTime))
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'BookNo Disabled', 1, 0, N'Admin', CAST(0x0000A3E300000000 AS DateTime), N'Admin', CAST(0x0000A3F10138A4B1 AS DateTime))
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'Change Customer Address', 1, 0, N'Admin', CAST(0x0000A3E300000000 AS DateTime), N'Admin', CAST(0x0000A3F000C4A9BC AS DateTime))
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'Change Customer MeterNo', 3, 1, N'Admin', CAST(0x0000A3E300000000 AS DateTime), N'admin', CAST(0x0000A470014576BE AS DateTime))
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, N'Change Customer Name', 2, 1, N'Admin', CAST(0x0000A3E300000000 AS DateTime), N'admin', CAST(0x0000A46F01134EA2 AS DateTime))
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, N'Change Customer Status', 2, 1, N'Admin', CAST(0x0000A3E300000000 AS DateTime), N'stu', CAST(0x0000A47000BB0925 AS DateTime))
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, N'Customer Type Change', 2, 1, N'Admin', CAST(0x0000A45800C52F82 AS DateTime), N'admin', CAST(0x0000A46F0150A8AC AS DateTime))
SET IDENTITY_INSERT [dbo].[TBL_FunctionalAccessPermission] OFF
/****** Object:  Table [dbo].[Tbl_CustomerBillPaymentsApproval]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerBillPaymentsApproval](
	[CustomerBillPaymentId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerPaymentId] [int] NULL,
	[PaidAmount] [decimal](20, 4) NULL,
	[BillNo] [varchar](50) NULL,
	[Approval] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MASTERS].[Tbl_ControlRefMaster]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MASTERS].[Tbl_ControlRefMaster](
	[ControlTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ControlTypeName] [varchar](50) NULL,
	[IsHavingRef] [bit] NULL,
	[ActiveStatusId] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ControlTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [MASTERS].[Tbl_ControlRefMaster] ON
INSERT [MASTERS].[Tbl_ControlRefMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef], [ActiveStatusId]) VALUES (1, N'TextBox', 0, 1)
INSERT [MASTERS].[Tbl_ControlRefMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef], [ActiveStatusId]) VALUES (2, N'DropDownList', 1, 1)
SET IDENTITY_INSERT [MASTERS].[Tbl_ControlRefMaster] OFF
/****** Object:  Table [dbo].[Tbl_CompanyDetails]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CompanyDetails](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](300) NULL,
	[Details] [varchar](max) NULL,
	[Address] [varchar](max) NULL,
	[Logo] [varchar](max) NULL,
	[KeyCode] [varchar](50) NULL,
	[BillPaymentModeId] [int] NULL,
	[IsEmbassyHaveTax] [bit] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_CompanyDetails] ON
INSERT [dbo].[Tbl_CompanyDetails] ([CompanyId], [CompanyName], [Details], [Address], [Logo], [KeyCode], [BillPaymentModeId], [IsEmbassyHaveTax], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifedBy], [ModifiedDate]) VALUES (1, N'BEDC', NULL, NULL, NULL, N'BEDC', 2, 0, 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_CompanyDetails] OFF
/****** Object:  Table [dbo].[Tbl_BillPaymentModes]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillPaymentModes](
	[BillPaymentModeId] [int] IDENTITY(1,1) NOT NULL,
	[BillPaymentMode] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillPaymentModeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_BillPaymentModes] ON
INSERT [dbo].[Tbl_BillPaymentModes] ([BillPaymentModeId], [BillPaymentMode]) VALUES (1, N'Bill-Bill')
INSERT [dbo].[Tbl_BillPaymentModes] ([BillPaymentModeId], [BillPaymentMode]) VALUES (2, N'FIFO')
SET IDENTITY_INSERT [dbo].[Tbl_BillPaymentModes] OFF
/****** Object:  Table [dbo].[Tbl_BillAdjustmentType]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillAdjustmentType](
	[BATID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_BillAdjustmentType] PRIMARY KEY CLUSTERED 
(
	[BATID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_BillAdjustmentType] ON
INSERT [dbo].[Tbl_BillAdjustmentType] ([BATID], [Name], [Status]) VALUES (1, N'Meter Reading Adjustment', 1)
INSERT [dbo].[Tbl_BillAdjustmentType] ([BATID], [Name], [Status]) VALUES (2, N'Additional Charges Adjustment', 1)
INSERT [dbo].[Tbl_BillAdjustmentType] ([BATID], [Name], [Status]) VALUES (3, N'Consumption Adjustment', 1)
INSERT [dbo].[Tbl_BillAdjustmentType] ([BATID], [Name], [Status]) VALUES (4, N'Bill Adjustment', 1)
SET IDENTITY_INSERT [dbo].[Tbl_BillAdjustmentType] OFF
/****** Object:  Table [dbo].[Tbl_BillAdjustmentDetails]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillAdjustmentDetails](
	[BADID] [int] IDENTITY(1,1) NOT NULL,
	[BillAdjustmentId] [int] NULL,
	[PreviousReading] [varchar](50) NULL,
	[CurrentReadingAfterAdjustment] [varchar](50) NULL,
	[EnergryCharges] [decimal](20, 4) NULL,
	[AdditionalCharges] [decimal](20, 4) NULL,
	[AdditionalChargesID] [int] NULL,
	[CurrentReadingBeforeAdjustment] [varchar](50) NULL,
 CONSTRAINT [PK_Tbl_BillAdjustmentDetails] PRIMARY KEY CLUSTERED 
(
	[BADID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_BillAdjustmentDetails] ON
INSERT [dbo].[Tbl_BillAdjustmentDetails] ([BADID], [BillAdjustmentId], [PreviousReading], [CurrentReadingAfterAdjustment], [EnergryCharges], [AdditionalCharges], [AdditionalChargesID], [CurrentReadingBeforeAdjustment]) VALUES (18, 18, NULL, NULL, CAST(-55.0000 AS Decimal(20, 4)), NULL, 0, NULL)
INSERT [dbo].[Tbl_BillAdjustmentDetails] ([BADID], [BillAdjustmentId], [PreviousReading], [CurrentReadingAfterAdjustment], [EnergryCharges], [AdditionalCharges], [AdditionalChargesID], [CurrentReadingBeforeAdjustment]) VALUES (24, 24, NULL, NULL, CAST(1000.0000 AS Decimal(20, 4)), NULL, 0, NULL)
INSERT [dbo].[Tbl_BillAdjustmentDetails] ([BADID], [BillAdjustmentId], [PreviousReading], [CurrentReadingAfterAdjustment], [EnergryCharges], [AdditionalCharges], [AdditionalChargesID], [CurrentReadingBeforeAdjustment]) VALUES (25, 25, NULL, NULL, CAST(-1000.0000 AS Decimal(20, 4)), NULL, 0, NULL)
INSERT [dbo].[Tbl_BillAdjustmentDetails] ([BADID], [BillAdjustmentId], [PreviousReading], [CurrentReadingAfterAdjustment], [EnergryCharges], [AdditionalCharges], [AdditionalChargesID], [CurrentReadingBeforeAdjustment]) VALUES (41, 41, NULL, NULL, CAST(-454.0000 AS Decimal(20, 4)), NULL, 0, NULL)
INSERT [dbo].[Tbl_BillAdjustmentDetails] ([BADID], [BillAdjustmentId], [PreviousReading], [CurrentReadingAfterAdjustment], [EnergryCharges], [AdditionalCharges], [AdditionalChargesID], [CurrentReadingBeforeAdjustment]) VALUES (54, 54, NULL, NULL, CAST(45754.0000 AS Decimal(20, 4)), NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_BillAdjustmentDetails] OFF
/****** Object:  Table [dbo].[Tbl_WebServiceWorkingKey]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_WebServiceWorkingKey](
	[WSWorkingKey] [int] IDENTITY(1,1) NOT NULL,
	[DeviceName] [varchar](50) NULL,
	[Key] [char](32) NULL,
 CONSTRAINT [PK_Tbl_WebServiceWorkingKey] PRIMARY KEY CLUSTERED 
(
	[WSWorkingKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_WebServiceWorkingKey] ON
INSERT [dbo].[Tbl_WebServiceWorkingKey] ([WSWorkingKey], [DeviceName], [Key]) VALUES (1, N'WC', N'KJFDKFD5455544                  ')
SET IDENTITY_INSERT [dbo].[Tbl_WebServiceWorkingKey] OFF
/****** Object:  Table [dbo].[Tbl_UserLoginDetails]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserLoginDetails](
	[IdentityUserId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](50) NOT NULL,
	[Password] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_UserLoginDetails] ON
INSERT [dbo].[Tbl_UserLoginDetails] ([IdentityUserId], [UserId], [Password], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, N'Admin', N'cTtGyvnFUpHfxPNt+CFvh9UsfbkFe1HQSpcigAT8IKA=', 1, N'Admin', CAST(0x0000A46F015064DB AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_UserLoginDetails] OFF
/****** Object:  Table [dbo].[Tbl_UserDetails]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserDetails](
	[UserDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](50) NULL,
	[Name] [varchar](300) NULL,
	[SurName] [varchar](300) NULL,
	[PrimaryContact] [varchar](20) NULL,
	[SecondaryContact] [varchar](20) NULL,
	[PrimaryEmailId] [varchar](500) NULL,
	[SecondaryEmailId] [varchar](500) NULL,
	[Address] [varchar](max) NULL,
	[Photo] [varchar](max) NULL,
	[ScannedDocument] [varchar](max) NULL,
	[GenderId] [int] NULL,
	[Details] [varchar](max) NULL,
	[RoleId] [int] NULL,
	[DesignationId] [int] NULL,
	[FilePath] [varchar](max) NULL,
	[IsMobileAccess] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserDetailsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_UserDetails] ON
INSERT [dbo].[Tbl_UserDetails] ([UserDetailsId], [UserId], [Name], [SurName], [PrimaryContact], [SecondaryContact], [PrimaryEmailId], [SecondaryEmailId], [Address], [Photo], [ScannedDocument], [GenderId], [Details], [RoleId], [DesignationId], [FilePath], [IsMobileAccess], [CreatedBy], [CreatedDate], [ModifedBy], [ModifiedDate]) VALUES (2, N'Admin', N'Admin', N'Adfgdf', N'123-45612345', N'123-456123556', N'bedc@gmail.com', N'bedc@gmail.com', N'Hyderabad', N'lord_rama_by_molee_19_02_2015_11_39_47.png', NULL, 1, N'Best Employee', 1, 1, NULL, NULL, N'Admin', CAST(0x0000A2E30146A4BE AS DateTime), N'admin', CAST(0x0000A46B00D51422 AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_UserDetails] OFF
/****** Object:  Table [MASTERS].[Tbl_USerDefinedControlDetails]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MASTERS].[Tbl_USerDefinedControlDetails](
	[UDCId] [int] IDENTITY(1,1) NOT NULL,
	[FieldName] [varchar](50) NULL,
	[ControlTypeId] [int] NULL,
	[IsMandatory] [bit] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UDCId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [MASTERS].[Tbl_USerDefinedControlDetails] ON
INSERT [MASTERS].[Tbl_USerDefinedControlDetails] ([UDCId], [FieldName], [ControlTypeId], [IsMandatory], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Ref Name', 1, 0, 0, NULL, NULL, N'Admin', CAST(0x0000A473017B8606 AS DateTime))
INSERT [MASTERS].[Tbl_USerDefinedControlDetails] ([UDCId], [FieldName], [ControlTypeId], [IsMandatory], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Ref id', 2, 0, 1, N'admin', CAST(0x0000A468013E0DEC AS DateTime), N'Admin', CAST(0x0000A473014AB105 AS DateTime))
INSERT [MASTERS].[Tbl_USerDefinedControlDetails] ([UDCId], [FieldName], [ControlTypeId], [IsMandatory], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'Images2', 2, 0, 1, N'Admin', CAST(0x0000A468013E6A3E AS DateTime), N'Admin', CAST(0x0000A46C0101A36D AS DateTime))
SET IDENTITY_INSERT [MASTERS].[Tbl_USerDefinedControlDetails] OFF
/****** Object:  Table [dbo].[TBL_ReasonCode]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ReasonCode](
	[RCID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonCode] [varchar](50) NULL,
	[DESCRIPTION] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[RCID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_ReasonCode] ON
INSERT [dbo].[TBL_ReasonCode] ([RCID], [ReasonCode], [DESCRIPTION]) VALUES (1, N'Over Estimation', N'Wrong Estimation')
INSERT [dbo].[TBL_ReasonCode] ([RCID], [ReasonCode], [DESCRIPTION]) VALUES (2, N'Under Estimation', N'Wrong Estimation')
SET IDENTITY_INSERT [dbo].[TBL_ReasonCode] OFF
/****** Object:  Table [dbo].[Tbl_NewPagePermissions]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_NewPagePermissions](
	[Page_PermissionId] [int] IDENTITY(1,1) NOT NULL,
	[Role_Id] [int] NULL,
	[MenuId] [int] NULL,
	[View] [bit] NULL,
	[Create] [bit] NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Page_PermissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_NewPagePermissions] ON
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1, 1, 1, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (2, 1, 2, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (3, 1, 3, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (4, 1, 4, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (5, 1, 5, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (6, 1, 6, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (7, 1, 7, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (8, 1, 8, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (9, 1, 9, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (10, 1, 10, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (11, 1, 11, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (12, 1, 12, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (13, 1, 13, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (14, 1, 14, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (15, 1, 15, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (16, 1, 16, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (17, 1, 17, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (18, 1, 18, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (19, 1, 19, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (20, 1, 20, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (21, 1, 21, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (22, 1, 22, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (23, 1, 23, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (24, 1, 24, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (25, 1, 25, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (26, 1, 26, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (27, 1, 27, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (28, 1, 28, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (29, 1, 29, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (30, 1, 30, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (31, 1, 31, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (32, 1, 32, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (33, 1, 33, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (34, 1, 34, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (35, 1, 35, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (36, 1, 36, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (37, 1, 37, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (38, 1, 38, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (39, 1, 79, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (40, 1, 82, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (41, 1, 83, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (42, 1, 88, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (43, 1, 89, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (44, 1, 90, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (45, 1, 91, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (46, 1, 92, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (47, 1, 93, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (48, 1, 94, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (49, 1, 95, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (50, 1, 96, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (51, 1, 97, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (52, 1, 98, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (53, 1, 99, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (54, 1, 100, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (55, 1, 101, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (56, 1, 102, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (57, 1, 103, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (58, 1, 104, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (59, 1, 105, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (60, 1, 106, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (61, 1, 107, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (62, 1, 108, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (63, 1, 109, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (64, 1, 110, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (65, 1, 111, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (66, 1, 112, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (67, 1, 113, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (68, 1, 114, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (69, 1, 115, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (70, 1, 117, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (71, 1, 118, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (72, 1, 119, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (73, 1, 120, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (74, 1, 121, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (75, 1, 122, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (76, 1, 123, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (77, 1, 124, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (78, 1, 39, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (79, 1, 40, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (80, 1, 41, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (81, 1, 42, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (82, 1, 43, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (83, 1, 44, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (84, 1, 45, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (85, 1, 46, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (86, 1, 47, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (87, 1, 48, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (88, 1, 49, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (89, 1, 50, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (90, 1, 51, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (91, 1, 52, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (92, 1, 53, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (93, 1, 54, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (94, 1, 55, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (95, 1, 56, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (96, 1, 57, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (97, 1, 58, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (98, 1, 59, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (99, 1, 60, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (100, 1, 61, 1, 1, N'Admin', NULL, NULL, NULL, 1)
GO
print 'Processed 100 total records'
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (101, 1, 62, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (102, 1, 63, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (103, 1, 64, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (104, 1, 65, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (105, 1, 66, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (106, 1, 67, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (107, 1, 68, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (108, 1, 69, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (109, 1, 70, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (110, 1, 71, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (111, 1, 72, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (112, 1, 73, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (113, 1, 74, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (114, 1, 75, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (115, 1, 76, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (116, 1, 77, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (117, 1, 78, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (118, 1, 125, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (119, 1, 126, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (120, 1, 127, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (121, 1, 128, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (122, 1, 129, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (123, 1, 130, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (124, 1, 131, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (125, 1, 132, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (126, 1, 133, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (127, 1, 134, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (128, 1, 135, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (129, 1, 136, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (130, 1, 137, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (131, 1, 138, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (132, 1, 139, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (133, 1, 140, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (134, 1, 141, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (135, 1, 142, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (136, 1, 143, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (137, 1, 144, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (138, 1, 145, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (139, 1, 146, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (140, 1, 147, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (141, 1, 148, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (142, 1, 149, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (143, 1, 150, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (144, 1, 151, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (145, 1, 152, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (146, 1, 153, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (147, 1, 154, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (148, 1, 155, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (149, 1, 156, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (150, 1, 157, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (151, 1, 160, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (152, 1, 161, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (153, 1, 162, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (154, 1, 163, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (155, 1, 164, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (156, 1, 165, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (157, 1, 166, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (158, 1, 167, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (159, 1, 168, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (160, 1, 169, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (161, 1, 170, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (162, 1, 171, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (163, 1, 172, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (164, 1, 173, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (165, 1, 174, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (166, 1, 175, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (167, 12, 1, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (168, 12, 2, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (169, 12, 3, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (170, 12, 4, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (171, 12, 5, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (172, 12, 6, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (173, 12, 7, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (174, 12, 8, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (175, 12, 9, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (176, 12, 10, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (177, 12, 11, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (178, 12, 12, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (179, 12, 13, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (180, 12, 14, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (181, 12, 15, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (182, 12, 16, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (183, 12, 17, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (184, 12, 18, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (185, 12, 19, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (186, 12, 20, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (187, 12, 21, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (188, 12, 22, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (189, 12, 23, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (190, 12, 24, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (191, 12, 25, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (192, 12, 26, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (193, 12, 27, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (194, 12, 28, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (195, 12, 29, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (196, 12, 30, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (197, 12, 31, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (198, 12, 32, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (199, 12, 33, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (200, 12, 34, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (201, 12, 35, 1, 1, N'Admin', NULL, NULL, NULL, 1)
GO
print 'Processed 200 total records'
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (202, 12, 36, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (203, 12, 37, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (204, 12, 38, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (205, 12, 79, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (206, 12, 82, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (207, 12, 83, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (208, 12, 88, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (209, 12, 89, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (210, 12, 90, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (211, 12, 91, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (212, 12, 92, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (213, 12, 93, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (214, 12, 94, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (215, 12, 95, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (216, 12, 96, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (217, 12, 97, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (218, 12, 98, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (219, 12, 99, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (220, 12, 100, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (221, 12, 101, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (222, 12, 102, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (223, 12, 103, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (224, 12, 104, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (225, 12, 105, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (226, 12, 106, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (227, 12, 107, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (228, 12, 108, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (229, 12, 109, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (230, 12, 110, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (231, 12, 111, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (232, 12, 112, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (233, 12, 113, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (234, 12, 114, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (235, 12, 115, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (236, 12, 117, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (237, 12, 118, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (238, 12, 119, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (239, 12, 120, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (240, 12, 121, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (241, 12, 122, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (242, 12, 123, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (243, 12, 124, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (244, 12, 39, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (245, 12, 40, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (246, 12, 41, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (247, 12, 42, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (248, 12, 43, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (249, 12, 44, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (250, 12, 45, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (251, 12, 46, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (252, 12, 47, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (253, 12, 48, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (254, 12, 49, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (255, 12, 50, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (256, 12, 51, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (257, 12, 52, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (258, 12, 53, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (259, 12, 54, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (260, 12, 55, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (261, 12, 56, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (262, 12, 57, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (263, 12, 58, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (264, 12, 59, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (265, 12, 60, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (266, 12, 61, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (267, 12, 62, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (268, 12, 63, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (269, 12, 64, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (270, 12, 65, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (271, 12, 66, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (272, 12, 67, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (273, 12, 68, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (274, 12, 69, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (275, 12, 70, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (276, 12, 71, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (277, 12, 72, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (278, 12, 73, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (279, 12, 74, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (280, 12, 75, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (281, 12, 76, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (282, 12, 77, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (283, 12, 78, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (284, 12, 125, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (285, 12, 126, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (286, 12, 127, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (287, 12, 128, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (288, 12, 129, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (289, 12, 130, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (290, 12, 131, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (291, 12, 132, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (292, 12, 133, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (293, 12, 134, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (294, 12, 135, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (295, 12, 136, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (296, 12, 137, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (297, 12, 138, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (298, 12, 139, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (299, 12, 140, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (300, 12, 141, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (301, 12, 142, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (302, 12, 143, 1, 1, N'Admin', NULL, NULL, NULL, 1)
GO
print 'Processed 300 total records'
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (303, 12, 144, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (304, 12, 145, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (305, 12, 146, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (306, 12, 147, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (307, 12, 148, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (308, 12, 149, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (309, 12, 150, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (310, 12, 151, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (311, 12, 152, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (312, 12, 153, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (313, 12, 154, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (314, 12, 155, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (315, 12, 156, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (316, 12, 157, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (317, 12, 160, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (318, 12, 161, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (319, 12, 162, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (320, 12, 163, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (321, 12, 164, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (322, 12, 165, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (323, 12, 166, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (324, 12, 167, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (325, 12, 168, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (326, 12, 169, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (327, 12, 170, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (328, 12, 171, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (329, 12, 172, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (330, 12, 173, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (331, 12, 174, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (332, 12, 175, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (333, 1, 176, 1, 1, N'Admin', CAST(0x0000A47701539963 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (334, 12, 176, 1, 1, N'Admin', CAST(0x0000A47701539964 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (335, 1, 177, 1, 1, N'Admin', CAST(0x0000A479010F09FC AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (336, 12, 177, 1, 1, N'Admin', CAST(0x0000A479010F4BA9 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (337, 1, 178, 1, 1, N'Admin', CAST(0x0000A47A014FE6B6 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (338, 12, 178, 1, 1, N'Admin', CAST(0x0000A47A014FE6B8 AS DateTime), NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_NewPagePermissions] OFF
/****** Object:  Table [dbo].[Tbl_MUserControlMaster]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MUserControlMaster](
	[ControlTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ControlTypeName] [varchar](50) NULL,
	[IsHavingRef] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ControlTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MUserControlMaster] ON
INSERT [dbo].[Tbl_MUserControlMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef]) VALUES (1, N'TextBox', 1)
INSERT [dbo].[Tbl_MUserControlMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef]) VALUES (2, N'DropDownList', 1)
INSERT [dbo].[Tbl_MUserControlMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef]) VALUES (3, N'RadioButtonList', 1)
INSERT [dbo].[Tbl_MUserControlMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef]) VALUES (4, N'CheckBoxList', 1)
INSERT [dbo].[Tbl_MUserControlMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef]) VALUES (5, N'CheckBox', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MUserControlMaster] OFF
/****** Object:  Table [dbo].[Tbl_MTaxTypeDetails]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MTaxTypeDetails](
	[TaxTypeId] [int] IDENTITY(1,1) NOT NULL,
	[TaxType] [varchar](50) NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[TaxTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MTaxTypeDetails] ON
INSERT [dbo].[Tbl_MTaxTypeDetails] ([TaxTypeId], [TaxType], [IsActive]) VALUES (1, N'Pecentage', 1)
INSERT [dbo].[Tbl_MTaxTypeDetails] ([TaxTypeId], [TaxType], [IsActive]) VALUES (2, N'Amount', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MTaxTypeDetails] OFF
/****** Object:  Table [dbo].[Tbl_MTaxDetails]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MTaxDetails](
	[TaxId] [int] IDENTITY(1,1) NOT NULL,
	[TaxName] [varchar](100) NULL,
	[TaxValue] [decimal](18, 0) NULL,
	[IsAcitive] [bit] NULL,
	[TaxTypeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[TaxId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MTaxDetails] ON
INSERT [dbo].[Tbl_MTaxDetails] ([TaxId], [TaxName], [TaxValue], [IsAcitive], [TaxTypeId]) VALUES (1, N'VAT', CAST(5 AS Decimal(18, 0)), 1, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MTaxDetails] OFF
/****** Object:  Table [dbo].[Tbl_MTariffClasses]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MTariffClasses](
	[ClassID] [int] IDENTITY(1,1) NOT NULL,
	[ClassName] [varchar](200) NULL,
	[Description] [varchar](max) NULL,
	[Remarks] [varchar](max) NULL,
	[IsActiveClass] [bit] NULL,
	[RefClassID] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK__Tbl_MTar__CB1927A00DAF0CB0] PRIMARY KEY CLUSTERED 
(
	[ClassID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MTariffClasses] ON
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Residential', N'Residential', NULL, 1, NULL, NULL, NULL, N'Admin', CAST(0x0000A46B015E9E96 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'R1', N'--', NULL, 1, 1, NULL, NULL, N'Admin', CAST(0x0000A46B015EAB4B AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'R2', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'R3', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'R4', NULL, NULL, 1, 1, NULL, NULL, N'Admin', CAST(0x0000A42D00C6DDB5 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'Commercial', N'Commercial', NULL, 1, NULL, NULL, NULL, N'Admin', CAST(0x0000A43100F68979 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'C1', NULL, NULL, 1, 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, N'C2', NULL, NULL, 1, 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, N'C3', NULL, NULL, 1, 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, N'Industrial', N'Industrial', NULL, 1, NULL, NULL, NULL, N'Admin', CAST(0x0000A43100F68B32 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, N'D1', NULL, NULL, 1, 10, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, N'D2', NULL, NULL, 1, 10, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, N'D3', NULL, NULL, 1, 10, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, N'Special', N'Special', NULL, 1, NULL, NULL, NULL, N'Admin', CAST(0x0000A43100F68CD3 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, N'A1', NULL, NULL, 1, 14, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, N'A2', NULL, NULL, 1, 14, NULL, NULL, N'admin', NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, N'A3', N'--', NULL, 1, 14, NULL, NULL, N'superadmin', CAST(0x0000A44F01278FC5 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, N'Street Light', N'Street Light', NULL, 1, NULL, NULL, NULL, N'superadmin', CAST(0x0000A44F00DD7466 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (19, N'S1', N's1', NULL, 1, 18, NULL, NULL, N'Admin', CAST(0x0000A42C011865DA AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (20, N'BEDC', N'For BEDC employees only', NULL, 1, NULL, NULL, CAST(0x0000A468012EC99D AS DateTime), N'admin', CAST(0x0000A46801322614 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (21, N'B1', N'For BEDC employees', NULL, 1, 20, NULL, CAST(0x0000A468012FB86C AS DateTime), N'admin', CAST(0x0000A46801327DD8 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (22, N'B2', N'Test', NULL, 1, 20, N'admin', CAST(0x0000A46900CAC51A AS DateTime), NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (23, N'New Test', N'New Test', NULL, 0, NULL, N'Admin', CAST(0x0000A46C00C150DC AS DateTime), N'Admin', CAST(0x0000A46C00D57C0D AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (24, N'sdf', NULL, NULL, 0, NULL, N'Admin', CAST(0x0000A46F00C58FCC AS DateTime), N'Admin', CAST(0x0000A46F00C596C2 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (25, N'New Tariff', N'new', NULL, 1, NULL, N'Admin', CAST(0x0000A46F00FF8051 AS DateTime), NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (26, N'New SubTariff', N'new sub', NULL, 0, 25, N'Admin', CAST(0x0000A46F00FF96F3 AS DateTime), N'Admin', CAST(0x0000A46F00FFFDE7 AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_MTariffClasses] OFF
/****** Object:  Table [dbo].[Tbl_MRoles]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MRoles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](100) NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MRoles] ON
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (1, N'Admin', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (2, N'BU Manager', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (3, N'Commercial Manager', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (4, N'Cashier', 0)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (5, N'Data Entry Operator', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (6, N'SU Manager', 0)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (7, N'Meter Reader', 0)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (8, N'Head Office', 0)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (9, N'Accountant', 0)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (10, N'Customer Service', 0)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (11, N'Supervisor', 0)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (12, N'SuperAdmin', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (13, N'Marketer', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (15, N'Role 1', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (16, N'Role 2', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (17, N'Role 3', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (18, N'Tert_Role_PK', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MRoles] OFF
/****** Object:  Table [dbo].[Tbl_MReadCodes]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MReadCodes](
	[ReadCodeId] [int] IDENTITY(1,1) NOT NULL,
	[ReadCode] [varchar](100) NULL,
	[Details] [varchar](max) NULL,
	[DisplayCode] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ReadCodeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MReadCodes] ON
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [DisplayCode], [ActiveStatusId]) VALUES (1, N'Direct', NULL, N'D', 1)
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [DisplayCode], [ActiveStatusId]) VALUES (2, N'Read', NULL, N'R', 1)
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [DisplayCode], [ActiveStatusId]) VALUES (3, N'Estimate', NULL, N'E', 1)
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [DisplayCode], [ActiveStatusId]) VALUES (4, N'Minimum', NULL, N'M', 1)
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [DisplayCode], [ActiveStatusId]) VALUES (5, N'Average', NULL, N'A', 1)
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [DisplayCode], [ActiveStatusId]) VALUES (6, N'High Consumption', NULL, N'HC', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MReadCodes] OFF
/****** Object:  Table [dbo].[Tbl_MPoleMasterDetails]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MPoleMasterDetails](
	[PoleMasterId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Description] [varchar](150) NULL,
	[PoleMasterOrderID] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PoleMasterCodeLength] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PoleMasterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MPoleMasterDetails] ON
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1007, N'TCN', N'TCN - 02', 1, 1, N'Admin', CAST(0x0000A40A00F08BEC AS DateTime), NULL, NULL, 2)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1008, N'TCN Power TR', N'TCN Power TR - 01', 2, 1, N'Admin', CAST(0x0000A40A00F153E4 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1009, N'33 KV Feeder', N'33 KV Feeder - 01', 3, 1, N'Admin', CAST(0x0000A40A00F17518 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1010, N'33/11 Inj. S/S', N'33/11 Inj. S/S - 02', 4, 1, N'Admin', CAST(0x0000A40A00F197CC AS DateTime), NULL, NULL, 2)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1011, N'33/11 Power TR', N'33/11 Power TR - 01', 5, 1, N'Admin', CAST(0x0000A40A00F1B849 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1012, N'11 KV Feeder', N'11 KV Feeder - 01', 6, 1, N'Admin', CAST(0x0000A40A00F1CE59 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1013, N'11/0.45KV TR', N'11/0.45KV TR - 02', 7, 1, N'Admin', CAST(0x0000A40A00F1EA1D AS DateTime), NULL, NULL, 3)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1014, N'LV Upraiser', N'LV Upraiser - 01', 8, 1, N'Admin', CAST(0x0000A40A00F20661 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1015, N'Pole No', N'Pole No – 03', 9, 1, N'Admin', CAST(0x0000A40A00F21B27 AS DateTime), N'Admin', CAST(0x0000A434011016FA AS DateTime), 3)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1017, N'sample', N'unit testing', 10, 1, N'Admin', CAST(0x0000A44100CBAA48 AS DateTime), N'Admin', CAST(0x0000A46901401B43 AS DateTime), 2)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1018, N'New Pole', N'dsdfdsf', 11, 1, N'Admin', CAST(0x0000A468013C7764 AS DateTime), N'Admin', CAST(0x0000A468013C806E AS DateTime), 3)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1019, N'New sample', N'21', 12, 1, N'Admin', CAST(0x0000A46901405D52 AS DateTime), NULL, NULL, 2)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1020, N'New sample Pole', N'asdf', 13, 1, N'Admin', CAST(0x0000A46B0109A5EA AS DateTime), NULL, NULL, 2)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1021, N'New sample Pole 2', N'14', 14, 1, N'Admin', CAST(0x0000A46B010A5479 AS DateTime), NULL, NULL, 2)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1022, N'pole for test', N'tesing on 30Mar15', 15, 1, N'Admin', CAST(0x0000A46B015FE3DF AS DateTime), N'Admin', CAST(0x0000A46B015FE9A5 AS DateTime), 2)
SET IDENTITY_INSERT [dbo].[Tbl_MPoleMasterDetails] OFF
/****** Object:  Table [dbo].[Tbl_MPhases]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MPhases](
	[PhaseId] [int] IDENTITY(1,1) NOT NULL,
	[Phase] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PhaseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MPhases] ON
INSERT [dbo].[Tbl_MPhases] ([PhaseId], [Phase], [Details], [ActiveStatus]) VALUES (1, N'Single Phase', N'S', 1)
INSERT [dbo].[Tbl_MPhases] ([PhaseId], [Phase], [Details], [ActiveStatus]) VALUES (2, N'Three Phase', N'T', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MPhases] OFF
/****** Object:  Table [dbo].[Tbl_MPaymentType]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MPaymentType](
	[PaymentTypeID] [int] IDENTITY(1,1) NOT NULL,
	[PaymentType] [varchar](50) NULL,
	[ActiveStatusID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MPaymentType] ON
INSERT [dbo].[Tbl_MPaymentType] ([PaymentTypeID], [PaymentType], [ActiveStatusID]) VALUES (1, N'Batch Payment', 1)
INSERT [dbo].[Tbl_MPaymentType] ([PaymentTypeID], [PaymentType], [ActiveStatusID]) VALUES (2, N'Bulk Upload', 1)
INSERT [dbo].[Tbl_MPaymentType] ([PaymentTypeID], [PaymentType], [ActiveStatusID]) VALUES (3, N'Customer Payment', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MPaymentType] OFF
/****** Object:  Table [dbo].[Tbl_MPaymentMode]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MPaymentMode](
	[PaymentModeId] [int] IDENTITY(1,1) NOT NULL,
	[PaymentMode] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[IsDefault] [bit] NULL,
 CONSTRAINT [PK_Table_MPaymentMode] PRIMARY KEY CLUSTERED 
(
	[PaymentModeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MPaymentMode] ON
INSERT [dbo].[Tbl_MPaymentMode] ([PaymentModeId], [PaymentMode], [ActiveStatusId], [IsDefault]) VALUES (1, N'Cash', 1, 1)
INSERT [dbo].[Tbl_MPaymentMode] ([PaymentModeId], [PaymentMode], [ActiveStatusId], [IsDefault]) VALUES (2, N'Cheque', 1, NULL)
INSERT [dbo].[Tbl_MPaymentMode] ([PaymentModeId], [PaymentMode], [ActiveStatusId], [IsDefault]) VALUES (3, N'DD', 1, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_MPaymentMode] OFF
/****** Object:  Table [dbo].[Tbl_MOrganizationTypes]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MOrganizationTypes](
	[OrganizationTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](300) NULL,
	[Details] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[OrganizationTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_MOrganizations]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MOrganizations](
	[OrganizationId] [int] IDENTITY(1,1) NOT NULL,
	[OrganizationCode] [varchar](50) NOT NULL,
	[NameOfOrganization] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[Contact1] [varchar](20) NULL,
	[Contact2] [varchar](20) NULL,
	[EmailId] [varchar](500) NULL,
	[TypeOfOrganizationId] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[OrganizationCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_MMeterTypes]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MMeterTypes](
	[MeterTypeId] [int] IDENTITY(1,1) NOT NULL,
	[MeterType] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[IsMaster] [bit] NULL,
	[ActiveStatus] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MMeterTypes] ON
INSERT [dbo].[Tbl_MMeterTypes] ([MeterTypeId], [MeterType], [Details], [IsMaster], [ActiveStatus], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Prepaid Meter', NULL, 1, 1, N'Admin', NULL, NULL, CAST(0x0000A41100CAEAED AS DateTime))
INSERT [dbo].[Tbl_MMeterTypes] ([MeterTypeId], [MeterType], [Details], [IsMaster], [ActiveStatus], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Credit Meter', N'Credit Meter', 1, 1, N'Admin', NULL, N'Admin', CAST(0x0000A42F00DB3046 AS DateTime))
INSERT [dbo].[Tbl_MMeterTypes] ([MeterTypeId], [MeterType], [Details], [IsMaster], [ActiveStatus], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Bulk Meter', N'Bulk Meter', 1, 1, N'Admin', NULL, N'Admin', CAST(0x0000A42F00DB3A4F AS DateTime))
INSERT [dbo].[Tbl_MMeterTypes] ([MeterTypeId], [MeterType], [Details], [IsMaster], [ActiveStatus], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'New Meter', NULL, 0, 1, N'Admin', CAST(0x0000A4680137BB00 AS DateTime), NULL, NULL)
INSERT [dbo].[Tbl_MMeterTypes] ([MeterTypeId], [MeterType], [Details], [IsMaster], [ActiveStatus], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'new mtr', N'sadjklf', 0, 1, N'Admin', CAST(0x0000A46B015F439B AS DateTime), N'Admin', CAST(0x0000A46B015F4BF1 AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_MMeterTypes] OFF
/****** Object:  Table [dbo].[Tbl_MMeterStatus]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MMeterStatus](
	[MeterStatusId] [int] IDENTITY(1,1) NOT NULL,
	[MeterStatus] [varchar](100) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MMeterStatus] ON
INSERT [dbo].[Tbl_MMeterStatus] ([MeterStatusId], [MeterStatus], [Details], [ActiveStatusId]) VALUES (1, N'New Meter', NULL, 1)
INSERT [dbo].[Tbl_MMeterStatus] ([MeterStatusId], [MeterStatus], [Details], [ActiveStatusId]) VALUES (2, N'Old Meter', NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MMeterStatus] OFF
/****** Object:  Table [MASTERS].[Tbl_MMeterReadingsFrom]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MASTERS].[Tbl_MMeterReadingsFrom](
	[MeterReadingFromId] [int] IDENTITY(1,1) NOT NULL,
	[MeterReadingFrom] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterReadingFromId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [MASTERS].[Tbl_MMeterReadingsFrom] ON
INSERT [MASTERS].[Tbl_MMeterReadingsFrom] ([MeterReadingFromId], [MeterReadingFrom]) VALUES (1, N'AccountWise')
INSERT [MASTERS].[Tbl_MMeterReadingsFrom] ([MeterReadingFromId], [MeterReadingFrom]) VALUES (2, N'BookWise')
INSERT [MASTERS].[Tbl_MMeterReadingsFrom] ([MeterReadingFromId], [MeterReadingFrom]) VALUES (4, N'BulkUpload')
SET IDENTITY_INSERT [MASTERS].[Tbl_MMeterReadingsFrom] OFF
/****** Object:  Table [dbo].[Tbl_MLGA]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MLGA](
	[LGAId] [int] IDENTITY(1,1) NOT NULL,
	[LGAName] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MLGA] ON
INSERT [dbo].[Tbl_MLGA] ([LGAId], [LGAName], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'EDO LGA', 1, N'Admin', CAST(0x0000A41500DDA3D8 AS DateTime), N'superadmin', CAST(0x0000A44C00B051E1 AS DateTime))
INSERT [dbo].[Tbl_MLGA] ([LGAId], [LGAName], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Indian LGA ', 1, N'Admin', CAST(0x0000A468013035AE AS DateTime), N'Admin', CAST(0x0000A46801305E43 AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_MLGA] OFF
/****** Object:  Table [dbo].[Tbl_MIdentityTypes]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MIdentityTypes](
	[IdentityId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdentityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MIdentityTypes] ON
INSERT [dbo].[Tbl_MIdentityTypes] ([IdentityId], [Type], [Details], [ActiveStatusId]) VALUES (1, N'License', NULL, 1)
INSERT [dbo].[Tbl_MIdentityTypes] ([IdentityId], [Type], [Details], [ActiveStatusId]) VALUES (2, N'Passport', NULL, 1)
INSERT [dbo].[Tbl_MIdentityTypes] ([IdentityId], [Type], [Details], [ActiveStatusId]) VALUES (3, N'PanCard', NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MIdentityTypes] OFF
/****** Object:  Table [dbo].[Tbl_MGovtAccountTypes]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MGovtAccountTypes](
	[GActTypeID] [int] IDENTITY(1,1) NOT NULL,
	[AccountType] [varchar](100) NULL,
	[Details] [varchar](max) NULL,
	[RefActTypeID] [int] NULL,
	[AccountCode] [varchar](10) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[GActTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MGovtAccountTypes] ON
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [Details], [RefActTypeID], [AccountCode], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Govt', N'', NULL, N'', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [Details], [RefActTypeID], [AccountCode], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Federal', N'Under Govt', 1, NULL, 1, NULL, NULL, N'Admin', CAST(0x0000A4680137A840 AS DateTime))
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [Details], [RefActTypeID], [AccountCode], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Local', N'Under Govt', 1, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [Details], [RefActTypeID], [AccountCode], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'State', NULL, 1, NULL, 1, N'Mar  3 2015 12:46PM', NULL, NULL, NULL)
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [Details], [RefActTypeID], [AccountCode], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'Police', NULL, 2, NULL, 1, N'Mar  3 2015 12:48PM', NULL, NULL, NULL)
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [Details], [RefActTypeID], [AccountCode], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'Army / Military', NULL, 2, NULL, 1, N'Mar  3 2015 12:48PM', NULL, N'Admin', CAST(0x0000A46801378034 AS DateTime))
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [Details], [RefActTypeID], [AccountCode], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'Air Force', NULL, 2, NULL, 1, N'Mar  3 2015 12:49PM', NULL, NULL, NULL)
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [Details], [RefActTypeID], [AccountCode], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, N'Ministries', NULL, 4, NULL, 1, N'Mar  3 2015 12:49PM', NULL, NULL, NULL)
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [Details], [RefActTypeID], [AccountCode], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, N'Railways', NULL, 2, NULL, 1, N'Mar 27 2015  6:53PM', NULL, N'Admin', CAST(0x0000A46801377502 AS DateTime))
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [Details], [RefActTypeID], [AccountCode], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, N'static', NULL, 2, NULL, 1, N'Mar 31 2015 12:56PM', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_MGovtAccountTypes] OFF
/****** Object:  Table [dbo].[Tbl_MGender]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MGender](
	[GenderId] [int] IDENTITY(1,1) NOT NULL,
	[GenderName] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[GenderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MGender] ON
INSERT [dbo].[Tbl_MGender] ([GenderId], [GenderName]) VALUES (1, N'Male')
INSERT [dbo].[Tbl_MGender] ([GenderId], [GenderName]) VALUES (2, N'Female')
SET IDENTITY_INSERT [dbo].[Tbl_MGender] OFF
/****** Object:  Table [dbo].[Tbl_Menus]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Menus](
	[Name] [varchar](1000) NULL,
	[Path] [varchar](max) NULL,
	[Menu_Order] [int] NULL,
	[ReferenceMenuId] [int] NULL,
	[Icon1] [varchar](500) NULL,
	[Icon2] [varchar](500) NULL,
	[Page_Order] [int] NULL,
	[IsActive] [bit] NULL,
	[ActiveHeader] [bit] NOT NULL,
	[Menu_Image] [varchar](200) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[MenuId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Management', NULL, 1, NULL, N'nav-report', N'billing', NULL, 1, 1, N'Customer-management.png', NULL, CAST(0x0000A43D00F0FE7D AS DateTime), NULL, N'Admin', 1)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'User Management', NULL, 2, NULL, N'nav-collection', N'UserManagement', NULL, 0, 0, N'user-management.png', NULL, CAST(0x0000A43D00DE7D6A AS DateTime), NULL, N'Admin', 2)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Process Management', NULL, 3, NULL, N'nav-energy', N'meeter', NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 3)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Billing', NULL, 5, NULL, N'nav-disconnection', N'spotbilling', NULL, 1, 1, N'billingmistion.png', NULL, NULL, NULL, NULL, 4)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Spot Billing', NULL, 6, NULL, N'nav-work', N'colelction', NULL, 0, 1, N'spotbillingmenu.png', NULL, NULL, NULL, NULL, 5)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Payments', NULL, 7, NULL, N'nav-connection', N'connection', NULL, 1, 1, N'collection.png', NULL, NULL, NULL, NULL, 6)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Settings', NULL, 9, NULL, N'nav-settigns', N'setting', NULL, 1, 1, N'setting-icon.png', NULL, NULL, NULL, NULL, 7)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Reports', NULL, 8, NULL, N'nav-newreport', N'Report', NULL, 1, 1, N'reportstop.png', NULL, NULL, NULL, NULL, 8)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Unit Portal', NULL, 4, NULL, NULL, N'Unit_portal', NULL, 0, 1, N'customer_unit.png', NULL, NULL, NULL, NULL, 9)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'New Customer', N'../ConsumerManagement/NewCustomer.aspx', NULL, 1, NULL, NULL, 1, 1, 1, NULL, NULL, CAST(0x0000A43D00F10674 AS DateTime), NULL, N'Admin', 10)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'View Customer', N'../ConsumerManagement/SearchConsumer.aspx', NULL, 1, NULL, NULL, 2, 0, 1, NULL, NULL, NULL, NULL, NULL, 11)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Search Customer', N'../ConsumerManagement/AdvancedSearchCustomer.aspx', NULL, 1, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, 12)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Customer Tariff', N'../TariffManagement/CustomerChangeTariff.aspx', NULL, 1, NULL, NULL, 12, 1, 1, NULL, NULL, NULL, NULL, NULL, 13)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Tariff Approval', N'../TariffManagement/CustomerTariffApproval.aspx', NULL, 1, NULL, NULL, 5, 0, 1, NULL, NULL, NULL, NULL, NULL, 14)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'View Customers Locator', N'../ConsumerManagement/ViewNewCustomersLocator.aspx', NULL, 1, NULL, NULL, 6, 0, 1, NULL, NULL, NULL, NULL, NULL, 15)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Approve Customers Locator', N'../ConsumerManagement/ApprovedNewCustomersLocator.aspx', NULL, 1, NULL, NULL, 7, 0, 1, NULL, NULL, NULL, NULL, NULL, 16)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Ledger', N'../Reports/CustomerLedger.aspx', NULL, 1, NULL, NULL, 8, 0, 1, NULL, NULL, CAST(0x0000A43D010D6A98 AS DateTime), NULL, N'Admin', 17)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Customer BookNo', N'../ConsumerManagement/CustomerBookNoChange.aspx', NULL, 1, NULL, NULL, 11, 1, 1, NULL, NULL, NULL, NULL, NULL, 18)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Customer Name', N'../ConsumerManagement/ChangeCustomerName.aspx', NULL, 1, NULL, NULL, 9, 1, 1, NULL, NULL, NULL, NULL, NULL, 19)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Customer Address', N'../ConsumerManagement/ChangeCustomerAddress.aspx', NULL, 1, NULL, NULL, 10, 1, 1, NULL, NULL, NULL, NULL, NULL, 20)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Customer Status', N'../ConsumerManagement/ChangeCustomerStatus.aspx', NULL, 1, NULL, NULL, 17, 1, 1, NULL, NULL, NULL, NULL, NULL, 21)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Customer MeterNo', N'../ConsumerManagement/ChangeCustomerMeterNo.aspx', NULL, 1, NULL, NULL, 13, 1, 1, NULL, NULL, NULL, NULL, NULL, 22)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Payments', N'../Billing/PaymentsToAccountNo.aspx', NULL, 6, NULL, NULL, 14, 1, 1, NULL, NULL, NULL, NULL, NULL, 23)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Import', N'../ConsumerManagement/CustomerBulkUpload.aspx', NULL, 1, NULL, NULL, 21, 1, 1, NULL, NULL, CAST(0x0000A468013E9401 AS DateTime), NULL, N'admin', 24)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Book Wise Customer Export', N'../ConsumerManagement/CustomerExportBookWise.aspx', NULL, 4, NULL, NULL, 5, 1, 1, NULL, NULL, NULL, NULL, NULL, 25)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'New User', N'../UserManagement/AddUser.aspx', NULL, 2, NULL, NULL, 1, 1, 1, NULL, NULL, CAST(0x0000A43D00DE5FBD AS DateTime), NULL, N'Admin', 26)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'New Agent', N'../UnderConstruction.aspx', NULL, 2, NULL, NULL, 2, 0, 1, NULL, NULL, NULL, NULL, NULL, 27)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'New Connection Process', N'../UnderConstruction.aspx', NULL, 3, NULL, NULL, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, 28)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Disconnection Process', N'../UnderConstruction.aspx', NULL, 3, NULL, NULL, 2, 0, 1, NULL, NULL, NULL, NULL, NULL, 29)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Re-Connection Process', N'../UnderConstruction.aspx', NULL, 3, NULL, NULL, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, 30)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Bill Calculator', N'../TariffManagement/TariffDetails.aspx', NULL, 4, NULL, NULL, 7, 0, 1, NULL, NULL, NULL, NULL, NULL, 31)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Bulk Bill Generation', N'../UnderConstruction.aspx', NULL, 4, NULL, NULL, 10, 0, 1, NULL, NULL, NULL, NULL, NULL, 32)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Complaint Management -Entry', N'../UnderConstruction.aspx', NULL, 4, NULL, NULL, 14, 0, 1, NULL, NULL, NULL, NULL, NULL, 33)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'HT Bill Generation', N'../UnderConstruction.aspx', NULL, 4, NULL, NULL, 14, 0, 1, NULL, NULL, NULL, NULL, NULL, 34)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'LT Bill Back Office', N'../UnderConstruction.aspx', NULL, 4, NULL, NULL, 15, 0, 1, NULL, NULL, NULL, NULL, NULL, 35)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Bill Month Close', N'../Billing/BillingMonthClose.aspx', NULL, 4, NULL, NULL, 16, 0, 1, NULL, NULL, NULL, NULL, NULL, 36)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Meter Reading', N'../Billing/MeterReading.aspx', NULL, 4, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, 37)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Batch Entry', N'../Billing/Batchentry.aspx', NULL, 4, NULL, NULL, 15, 0, 1, NULL, NULL, NULL, NULL, NULL, 38)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Customer Contact Info', N'../ConsumerManagement/ChangeCustomerContactInfo.aspx', NULL, 1, NULL, NULL, 22, 1, 1, NULL, NULL, NULL, NULL, NULL, 176)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Direct Customer Average Update', N'../ConsumerManagement/DirectCustomerAverageUpload.aspx', NULL, 4, NULL, NULL, 22, 1, 1, NULL, NULL, NULL, NULL, NULL, 178)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Password', N'../UserManagement/ChangePassword.aspx', NULL, 122, NULL, NULL, 27, 0, 1, NULL, NULL, NULL, NULL, NULL, 79)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add BusinessUnits', N'../Masters/AddBusinessUnits.aspx', NULL, 122, NULL, NULL, 9, 1, 1, NULL, NULL, NULL, NULL, NULL, 82)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add ServiceUnits', N'../Masters/AddServiceUnits.aspx', NULL, 122, NULL, NULL, 23, 1, 1, NULL, NULL, NULL, NULL, NULL, 83)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Book Group', N'../Masters/AddCycle.aspx', NULL, 122, NULL, NULL, 36, 1, 1, NULL, NULL, NULL, NULL, NULL, 88)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add BookNumbers', N'../Masters/AddBookNumbers.aspx', NULL, 122, NULL, NULL, 39, 1, 1, NULL, NULL, NULL, NULL, NULL, 89)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Global Message', N'../Masters/AddGlobalMessage.aspx', NULL, 122, NULL, NULL, 24, 1, 1, NULL, NULL, NULL, NULL, NULL, 90)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Business Unit Messages', N'../Masters/AddDistrictlevelMessage.aspx', NULL, 122, NULL, NULL, 5, 0, 1, NULL, NULL, NULL, NULL, NULL, 91)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Agencies', N'../Masters/AddAgencies.aspx', NULL, 122, NULL, NULL, 42, 1, 1, NULL, NULL, NULL, NULL, NULL, 92)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Meter Types', N'../Masters/AddMeterTypes.aspx', NULL, 122, NULL, NULL, 18, 1, 1, NULL, NULL, NULL, NULL, NULL, 93)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Book Book Group Mapping', N'../Mappings/BookCycleMapping.aspx', NULL, 122, NULL, NULL, 10, 0, 1, NULL, NULL, NULL, NULL, NULL, 94)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add ServiceCenters', N'../Masters/AddServiceCenters.aspx', NULL, 122, NULL, NULL, 26, 1, 1, NULL, NULL, NULL, NULL, NULL, 95)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Countries Master', N'../Masters/AddCountries.aspx', NULL, 122, NULL, NULL, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, 96)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'States Master', N'../Masters/AddStates.aspx', NULL, 122, NULL, NULL, 2, 0, 1, NULL, NULL, NULL, NULL, NULL, 97)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Energy Charges', N'../TariffManagement/TariffEntry.aspx', NULL, 122, NULL, NULL, 20, 1, 1, NULL, NULL, NULL, NULL, NULL, 98)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Additional Charges', N'../TariffManagement/TariffFixed.aspx', NULL, 122, NULL, NULL, 21, 1, 1, NULL, NULL, NULL, NULL, NULL, 99)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Route Management', N'../Masters/AddRouteManagement.aspx', NULL, 122, NULL, NULL, 15, 1, 1, NULL, NULL, NULL, NULL, NULL, 100)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Cash Offices', N'../Masters/AddCashOffices.aspx', NULL, 122, NULL, NULL, 43, 1, 1, NULL, NULL, NULL, NULL, NULL, 101)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Meter Information', N'../Masters/MeterInformation.aspx', NULL, 122, NULL, NULL, 19, 1, 1, NULL, NULL, NULL, NULL, NULL, 102)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Latest News Master', N'../Masters/LatestNewsMaster.aspx', NULL, 122, NULL, NULL, 25, 1, 1, NULL, NULL, NULL, NULL, NULL, 103)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Tariff Category', N'../Masters/AddTariffCategory.aspx', NULL, 122, NULL, NULL, 16, 1, 1, NULL, NULL, NULL, NULL, NULL, 104)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Tariff SubCategory', N'../Masters/AddTariffSubCategory.aspx', NULL, 122, NULL, NULL, 17, 1, 1, NULL, NULL, NULL, NULL, NULL, 105)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add New Fixed Charge', N'../TariffManagement/AddNewFixedCharge.aspx', NULL, 122, NULL, NULL, 22, 1, 1, NULL, NULL, NULL, NULL, NULL, 106)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Search Bussiness Unit', N'../Masters/SearchBussinessUnitMaster.aspx', NULL, 122, NULL, NULL, 27, 0, 1, NULL, NULL, NULL, NULL, NULL, 107)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Search Service Unit', N'../Masters/SearchServiceUnitMaster.aspx', NULL, 122, NULL, NULL, 28, 0, 1, NULL, NULL, NULL, NULL, NULL, 108)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Search Service Center', N'../Masters/SearchServiceCenterMaster.aspx', NULL, 122, NULL, NULL, 29, 0, 1, NULL, NULL, NULL, NULL, NULL, 109)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Search Book Group', N'../Masters/SearchCycleMaster.aspx', NULL, 122, NULL, NULL, 30, 0, 1, NULL, NULL, NULL, NULL, NULL, 110)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Search Book Number', N'../Masters/SearchBookNoMaster.aspx', NULL, 122, NULL, NULL, 31, 0, 1, NULL, NULL, NULL, NULL, NULL, 111)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Search Meter Information', N'../Masters/MeterInformation.aspx', NULL, 122, NULL, NULL, 32, 0, 1, NULL, NULL, NULL, NULL, NULL, 112)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Search Route', N'../Masters/SearchRouteMaster.aspx', NULL, 122, NULL, NULL, 33, 0, 1, NULL, NULL, NULL, NULL, NULL, 113)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Search User', N'../Masters/SearchUser.aspx', NULL, 122, NULL, NULL, 34, 0, 1, NULL, NULL, NULL, NULL, NULL, 114)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Approval Settings', N'../Admin/AssignApprovalLevels.aspx', NULL, 122, NULL, NULL, 35, 1, 1, NULL, NULL, NULL, NULL, NULL, 115)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Marketers', N'../Masters/AddMarketers.aspx', NULL, 122, NULL, NULL, 38, 1, 1, NULL, NULL, NULL, NULL, NULL, 117)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Meter BU', N'../Admin/ChangeMeterBusinessUnit.aspx', NULL, 122, NULL, NULL, 37, 1, 1, NULL, NULL, NULL, NULL, NULL, 118)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Regions', N'../Masters/AddRegions.aspx', NULL, 122, NULL, NULL, 8, 1, 1, NULL, NULL, NULL, NULL, NULL, 119)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add LGA', N'../Masters/AddLGA.aspx', NULL, 122, NULL, NULL, 6, 1, 1, NULL, NULL, NULL, NULL, NULL, 120)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Pole Information', N'../Masters/addPoleDescription.aspx', NULL, 122, NULL, NULL, 40, 1, 1, NULL, NULL, NULL, NULL, NULL, 121)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Configuration Settings', N'../Masters/AllMasters.aspx', NULL, 7, NULL, NULL, 41, 1, 1, NULL, NULL, NULL, NULL, NULL, 122)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Disconnection Reports', N'../UnderConstruction.aspx', NULL, 8, NULL, NULL, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, 123)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'New Connection Reports', N'../UnderConstruction.aspx', NULL, 8, NULL, NULL, 2, 0, 1, NULL, NULL, NULL, NULL, NULL, 124)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Bill Adjustment', N'../Billing/BillAdjustment.aspx', NULL, 4, NULL, NULL, 16, 0, 1, NULL, NULL, NULL, NULL, NULL, 39)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Bill Generation', N'../Billing/ScheduleBillGeneration.aspx', NULL, 4, NULL, NULL, 17, 1, 1, NULL, NULL, NULL, NULL, NULL, 40)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Direct Customers Billing', N'../Billing/DirectCustomersBilling.aspx', NULL, 4, NULL, NULL, 10, 0, 1, NULL, NULL, NULL, NULL, NULL, 41)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customerwise Billing', N'../Billing/EstimatedCustomerBill.aspx', NULL, 4, NULL, NULL, 11, 0, 1, NULL, NULL, NULL, NULL, NULL, 42)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Meter Reading Upload', N'../Billing/MeterReadingUpload.aspx', NULL, 4, NULL, NULL, 4, 1, 1, NULL, NULL, NULL, NULL, NULL, 43)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Bill Month Open', N'../Billing/BillingMonthOpen.aspx', NULL, 4, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, 44)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Bills', N'../Reports/CustomerBills.aspx', NULL, 8, NULL, NULL, 17, 1, 1, NULL, NULL, NULL, NULL, NULL, 45)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Execute Bill Generation', N'../Billing/ExecuteBillGeneration.aspx', NULL, 4, NULL, NULL, 9, 0, 1, NULL, NULL, NULL, NULL, NULL, 46)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Account Wise Bill Generation', N'../Billing/GenerateCustomerBill.aspx', NULL, 4, NULL, NULL, 19, 0, 1, NULL, NULL, NULL, NULL, NULL, 47)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Bill Schedule Lock', N'../Billing/BilledMonthLockStatus.aspx', NULL, 4, NULL, NULL, 6, 0, 1, NULL, NULL, NULL, NULL, NULL, 48)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Energy Tariff Master', N'../TariffManagement/TariffEntry.aspx', NULL, 4, NULL, NULL, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, 49)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Fixed Tariff Master', N'../TariffManagement/TariffFixed.aspx', NULL, 4, NULL, NULL, 2, 0, 1, NULL, NULL, NULL, NULL, NULL, 50)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Estimation Settings', N'../Billing/EstimateSettings.aspx', NULL, 4, NULL, NULL, 13, 1, 1, NULL, NULL, NULL, NULL, NULL, 51)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'HH Input File Generation', N'../Billing/HHInputFileGeneration.aspx', NULL, 5, NULL, NULL, 11, 1, 1, NULL, NULL, NULL, NULL, NULL, 52)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Download HH Input File', N'../Billing/DownloadHHInputFile.aspx', NULL, 5, NULL, NULL, 12, 1, 1, NULL, NULL, NULL, NULL, NULL, 53)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'HH Output File Upload', N'../Billing/HHOutputUploads.aspx', NULL, 5, NULL, NULL, 13, 1, 1, NULL, NULL, NULL, NULL, NULL, 54)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Bill Adjustments', N'../Billing/AdjustmentBatch.aspx', NULL, 6, NULL, NULL, 15, 1, 1, NULL, NULL, NULL, NULL, NULL, 55)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Billing Disabled Books', N'../Billing/BillingDisabledBooks.aspx', NULL, 4, NULL, NULL, 8, 1, 1, NULL, NULL, NULL, NULL, NULL, 56)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customers Average Consumption Upload', N'../Billing/DirectCustomersReadingsUpload.aspx', NULL, 4, NULL, NULL, 7, 1, 1, NULL, NULL, NULL, NULL, NULL, 57)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Paid Meters Adjustments', N'../Billing/PaidMetersAdjustments.aspx', NULL, 6, NULL, NULL, 17, 1, 1, NULL, NULL, NULL, NULL, NULL, 58)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Batch Payment Status ', N'../Billing/BatchPaymentStatus.aspx', NULL, 6, NULL, NULL, 18, 1, 1, NULL, NULL, NULL, NULL, NULL, 59)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Batch Adjustment Status ', N'../Billing/BatchAdjustmentStatus.aspx', NULL, 6, NULL, NULL, 19, 1, 1, NULL, NULL, NULL, NULL, NULL, 60)
GO
print 'Processed 100 total records'
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Data Download', N'../UnderConstruction.aspx', NULL, 5, NULL, NULL, 4, 0, 1, NULL, NULL, NULL, NULL, NULL, 61)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Data Preparation & File Creation', N'../UnderConstruction.aspx', NULL, 5, NULL, NULL, 5, 0, 1, NULL, NULL, NULL, NULL, NULL, 62)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Route Plan Reports', N'../UnderConstruction.aspx', NULL, 5, NULL, NULL, 7, 0, 1, NULL, NULL, NULL, NULL, NULL, 63)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Route Plan & Sequence', N'../Mappings/CustomerRouteSeqManagement.aspx', NULL, 5, NULL, NULL, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, 64)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Bill Upload', N'../UnderConstruction.aspx', NULL, 5, NULL, NULL, 6, 0, 1, NULL, NULL, NULL, NULL, NULL, 65)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'BookNo Sequence Management', N'../Mappings/BookNoReorder.aspx', NULL, 5, NULL, NULL, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, 66)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Sequence Management', N'../Mappings/CustomerReorderBookWise.aspx', NULL, 5, NULL, NULL, 2, 0, 1, NULL, NULL, NULL, NULL, NULL, 67)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Adjustments(Bill / energy)', N'../Billing/BillAdjustment.aspx', NULL, 6, NULL, NULL, 4, 0, 1, NULL, NULL, NULL, NULL, NULL, 68)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Cash Counters', N'../UnderConstruction.aspx', NULL, 6, NULL, NULL, 9, 0, 1, NULL, NULL, NULL, NULL, NULL, 69)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Disputed Amount Entries', N'../UnderConstruction.aspx', NULL, 6, NULL, NULL, 7, 0, 1, NULL, NULL, NULL, NULL, NULL, 70)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Receipt Adjustments', N'../UnderConstruction.aspx', NULL, 6, NULL, NULL, 8, 0, 1, NULL, NULL, NULL, NULL, NULL, 71)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Account Head changes', N'../UnderConstruction.aspx', NULL, 6, NULL, NULL, 10, 0, 1, NULL, NULL, NULL, NULL, NULL, 72)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Cash Reports', N'../UnderConstruction.aspx', NULL, 6, NULL, NULL, 6, 0, 1, NULL, NULL, NULL, NULL, NULL, 73)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Cancellation Of Reports', N'../UnderConstruction.aspx', NULL, 6, NULL, NULL, 11, 0, 1, NULL, NULL, NULL, NULL, NULL, 74)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Batch Wise Payments', N'../Billing/Batchentry.aspx', NULL, 6, NULL, NULL, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, 75)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Payment Uploads', N'../Billing/PaymentUpload.aspx', NULL, 6, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, 76)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'CashOffice Master', N'../Masters/AddCashOffices.aspx', NULL, 6, NULL, NULL, 5, 1, 1, NULL, NULL, NULL, NULL, NULL, 77)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Bill Payment', N'../Masters/CustomerBillPayment.aspx', NULL, 6, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, 78)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customers Without Book Group Report', N'../Reports/AccountWithoutCycle.aspx', NULL, 8, NULL, NULL, 3, 1, 1, NULL, NULL, NULL, NULL, NULL, 125)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Tariff-BU Report', N'../Reports/TariffBUReport.aspx', NULL, 8, NULL, NULL, 4, 1, 1, NULL, NULL, NULL, NULL, NULL, 126)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Direct Customer Reports', N'../Reports/CustomerWithNoMeter.aspx', NULL, 8, NULL, NULL, 5, 1, 1, NULL, NULL, NULL, NULL, NULL, 127)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customers With Out Feeder', N'../Reports/AccountWithOutFeeder.aspx', NULL, 8, NULL, NULL, 6, 0, 1, NULL, NULL, NULL, NULL, NULL, 128)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Accounts On Continuous Estimation', N'../Reports/AccountsOnContinuousEstimations.aspx', NULL, 8, NULL, NULL, 7, 1, 1, NULL, NULL, NULL, NULL, NULL, 129)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Account Status ', N'../Reports/AccountStatusReport.aspx', NULL, 8, NULL, NULL, 8, 1, 1, NULL, NULL, NULL, NULL, NULL, 130)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Account With Debit Balance', N'../Reports/AccountsWithDebitBalenceReport.aspx', NULL, 8, NULL, NULL, 9, 1, 1, NULL, NULL, NULL, NULL, NULL, 131)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Payments Report', N'../Reports/PaymentsReport.aspx', NULL, 8, NULL, NULL, 10, 1, 1, NULL, NULL, NULL, NULL, NULL, 132)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Accounts With Credit Balance', N'../Reports/AccountsWithCreditBalance.aspx', NULL, 8, NULL, NULL, 11, 1, 1, NULL, NULL, NULL, NULL, NULL, 133)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Accounts With No Name/Address', N'../Reports/AccountsWithNoNameOrAddress.aspx', NULL, 8, NULL, NULL, 12, 1, 1, NULL, NULL, NULL, NULL, NULL, 134)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Check Meter Customers List', N'../Reports/CheckMetersCustomersList.aspx', NULL, 8, NULL, NULL, 13, 0, 1, NULL, NULL, NULL, NULL, NULL, 135)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'BatchNo Wise Payments List', N'../Reports/BatchNoWisePaymentEntryList.aspx', NULL, 8, NULL, NULL, 14, 1, 1, NULL, NULL, NULL, NULL, NULL, 136)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Pre Billing Report', N'../Reports/PreBillingReport.aspx', NULL, 4, NULL, NULL, 14, 1, 1, NULL, NULL, NULL, NULL, NULL, 137)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Audit Tray Report', N'../Reports/AuditTrayReport.aspx', NULL, 1, NULL, NULL, 23, 1, 1, NULL, NULL, NULL, NULL, NULL, 138)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'BatchNo Wise Bill Adjustments List', N'../Reports/BatchNoWiseAdjustmentsList.aspx', NULL, 8, NULL, NULL, 17, 1, 1, NULL, NULL, NULL, NULL, NULL, 139)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'New Customer Report', N'../Reports/NewCustomersReport.aspx', NULL, 8, NULL, NULL, 18, 1, 1, NULL, NULL, NULL, NULL, NULL, 140)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Aged Customers Receivable Report', N'../Reports/AgedCustomersAccountReceivableReport.aspx', NULL, 8, NULL, NULL, 19, 1, 1, NULL, NULL, NULL, NULL, NULL, 141)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'NoUsage Customers Report', N'../Reports/NoUsageCustomersReport.aspx', NULL, 8, NULL, NULL, 20, 1, 1, NULL, NULL, NULL, NULL, NULL, 142)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer With No Fixed Charges', N'../Reports/CustomerWithNoFixedChargesReport.aspx', NULL, 8, NULL, NULL, 21, 1, 1, NULL, NULL, NULL, NULL, NULL, 143)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'UnBilled Customers Report', N'../Reports/UnBilledCustomersReport.aspx', NULL, 8, NULL, NULL, 22, 1, 1, NULL, NULL, NULL, NULL, NULL, 144)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Ledger', N'../CustomerUnitPortal/CustomerLedger.aspx', NULL, 9, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, 145)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Assign Meter', N'../ConsumerManagement/AssignMeters.aspx', NULL, 1, NULL, NULL, 5, 1, 1, NULL, NULL, NULL, NULL, NULL, 146)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Read To Direct', N'../ConsumerManagement/ReadToDirect.aspx', NULL, 1, NULL, NULL, 15, 1, 1, NULL, NULL, CAST(0x0000A459008E7086 AS DateTime), NULL, N'admin', 147)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Customer Type', N'../ConsumerManagement/ChangeCustomerType.aspx', NULL, 1, NULL, NULL, 20, 1, 1, NULL, NULL, CAST(0x0000A46F010E2ABD AS DateTime), NULL, N'admin', 148)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Report Summary', N'../Reports/ReportsSummary.aspx', NULL, 8, NULL, NULL, 24, 1, 1, NULL, NULL, NULL, NULL, NULL, 149)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Download Billing File (Service Center)', N'../Billing/DownloadBillingFile.aspx', NULL, 4, NULL, NULL, 20, 1, 1, NULL, NULL, NULL, NULL, NULL, 150)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Edit List', N'../Reports/EditListReportSummay.aspx', NULL, 8, NULL, NULL, 23, 1, 1, NULL, NULL, NULL, NULL, NULL, 151)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer wise Bill Generation', N'../ConsumerManagement/GenerateBillByCustomer.aspx', NULL, 4, NULL, NULL, 18, 1, 1, NULL, NULL, NULL, NULL, NULL, 152)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Adjustment (No Bill)', N'../ConsumerManagement/Adjustment_NoBill.aspx', NULL, 6, NULL, NULL, 16, 1, 1, NULL, NULL, NULL, NULL, NULL, 153)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Get Customer Details', N'../ConsumerManagement/SearchCustomer.aspx', NULL, 1, NULL, NULL, 4, 1, 1, NULL, NULL, NULL, NULL, NULL, 154)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Change Customer Approvals', N'../ConsumerManagement/ChangesApproval.aspx', NULL, 1, NULL, NULL, 22, 1, 1, NULL, NULL, NULL, NULL, NULL, 155)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Countries', N'../Masters/AddCountries.aspx', NULL, 122, NULL, NULL, 4, 1, 1, NULL, NULL, NULL, NULL, NULL, 156)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add States', N'../Masters/AddStates.aspx', NULL, 122, NULL, NULL, 7, 1, 1, NULL, NULL, NULL, NULL, NULL, 157)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add GovtType', N'../Masters/AddGovtType.aspx', NULL, 122, NULL, NULL, 44, 1, 1, NULL, NULL, NULL, NULL, NULL, 160)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Edit GovtType', N'../Masters/EditGovtAccTypes.aspx', NULL, 122, NULL, NULL, 45, 1, 1, NULL, NULL, NULL, NULL, NULL, 161)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'New User', N'../UserManagement/AddUser.aspx', NULL, 122, NULL, NULL, 46, 1, 1, NULL, NULL, NULL, NULL, NULL, 162)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Search User', N'../UserManagement/SearchUser.aspx', NULL, 122, NULL, NULL, 47, 1, 1, NULL, NULL, NULL, NULL, NULL, 163)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Pole Management', N'../Masters/AddPoleManagement.aspx', NULL, 122, NULL, NULL, 48, 1, 1, NULL, NULL, NULL, NULL, NULL, 164)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'BookNo Sequence Management', N'../Mappings/BookNoReorder.aspx', NULL, 122, NULL, NULL, 49, 1, 1, NULL, NULL, NULL, NULL, NULL, 165)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Sequence Mangement', N'../Mappings/CustomerReorderBookWise.aspx', NULL, 122, NULL, NULL, 50, 1, 1, NULL, NULL, NULL, NULL, NULL, 166)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Roles', N'../Admin/AddRoles.aspx', NULL, 122, NULL, NULL, 51, 1, 1, NULL, NULL, NULL, NULL, NULL, 167)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Update Roles', N'../Admin/UpdateRoles.aspx', NULL, 122, NULL, NULL, 52, 1, 1, NULL, NULL, NULL, NULL, NULL, 168)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Customer Types', N'../Masters/AddCustomerTypes.aspx', NULL, 122, NULL, NULL, 53, 1, 1, NULL, NULL, NULL, NULL, NULL, 169)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Add Account Types', N'../Masters/AddAccountTypes.aspx', NULL, 122, NULL, NULL, 54, 1, 1, NULL, NULL, NULL, NULL, NULL, 170)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Edit Menu', N'../Admin/EditMenu.aspx', NULL, 122, NULL, NULL, 55, 1, 1, NULL, NULL, NULL, NULL, NULL, 171)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Customer Mandatory Fields', N'../Admin/CustomerMandatoryFields.aspx', NULL, 122, NULL, NULL, 56, 1, 1, NULL, NULL, NULL, NULL, NULL, 172)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'User Defined Fields', N'../Admin/RegistrationCustomFields.aspx', NULL, 122, NULL, NULL, 57, 1, 1, NULL, NULL, NULL, NULL, NULL, 173)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'BEDC Employee Registration', N'../ConsumerManagement/AddBEDCEmployee.aspx', NULL, 122, NULL, NULL, 58, 1, 1, NULL, NULL, NULL, NULL, NULL, 174)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Password Strength', N'../Admin/PasswordStrength.aspx', NULL, 122, NULL, NULL, 59, 1, 1, NULL, NULL, NULL, NULL, NULL, 175)
INSERT [dbo].[Tbl_Menus] ([Name], [Path], [Menu_Order], [ReferenceMenuId], [Icon1], [Icon2], [Page_Order], [IsActive], [ActiveHeader], [Menu_Image], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [MenuId]) VALUES (N'Download Billing File(Customer wise)', N'../Billing/DownloadBillingFileForCustomers.aspx', NULL, 4, NULL, NULL, 21, 1, 1, NULL, NULL, NULL, NULL, NULL, 177)
/****** Object:  Table [dbo].[TBl_MDisableType]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBl_MDisableType](
	[DisableTypeId] [int] IDENTITY(1,1) NOT NULL,
	[DisableType] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[DisableTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBl_MDisableType] ON
INSERT [dbo].[TBl_MDisableType] ([DisableTypeId], [DisableType]) VALUES (1, N'No Power')
INSERT [dbo].[TBl_MDisableType] ([DisableTypeId], [DisableType]) VALUES (2, N'Temporary Close')
SET IDENTITY_INSERT [dbo].[TBl_MDisableType] OFF
/****** Object:  Table [dbo].[Tbl_MDesignations]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MDesignations](
	[DesignationId] [int] IDENTITY(1,1) NOT NULL,
	[DesignationName] [varchar](200) NULL,
	[ActiveStatusId] [int] NULL,
	[Details] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[DesignationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MDesignations] ON
INSERT [dbo].[Tbl_MDesignations] ([DesignationId], [DesignationName], [ActiveStatusId], [Details]) VALUES (1, N'Manager', 1, NULL)
INSERT [dbo].[Tbl_MDesignations] ([DesignationId], [DesignationName], [ActiveStatusId], [Details]) VALUES (2, N'Employee', 1, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_MDesignations] OFF
/****** Object:  Table [dbo].[Tbl_MCustomerTypes]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MCustomerTypes](
	[CustomerTypeId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerType] [varchar](100) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[IsMaster] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerTypes] ON
INSERT [dbo].[Tbl_MCustomerTypes] ([CustomerTypeId], [CustomerType], [Details], [ActiveStatusId], [IsMaster], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'MD', N'---', 1, 1, NULL, NULL, N'admin', CAST(0x0000A468013B253A AS DateTime))
INSERT [dbo].[Tbl_MCustomerTypes] ([CustomerTypeId], [CustomerType], [Details], [ActiveStatusId], [IsMaster], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'NON MD', NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MCustomerTypes] ([CustomerTypeId], [CustomerType], [Details], [ActiveStatusId], [IsMaster], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Pre Paid', N'---', 1, 0, NULL, NULL, N'Admin', CAST(0x0000A469011396F5 AS DateTime))
INSERT [dbo].[Tbl_MCustomerTypes] ([CustomerTypeId], [CustomerType], [Details], [ActiveStatusId], [IsMaster], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'Prime', N'Prime Customers', 1, 0, N'admin', CAST(0x0000A45200EBD44A AS DateTime), N'admin', CAST(0x0000A468013B20C0 AS DateTime))
INSERT [dbo].[Tbl_MCustomerTypes] ([CustomerTypeId], [CustomerType], [Details], [ActiveStatusId], [IsMaster], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'Sample', N'details', 2, 0, N'Admin', CAST(0x0000A468013DCAD5 AS DateTime), N'Admin', CAST(0x0000A468013DD54D AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerTypes] OFF
/****** Object:  Table [dbo].[Tbl_MCustomerType]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MCustomerType](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerTypeName] [varchar](100) NULL,
	[IsAcitve] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerType] ON
INSERT [dbo].[Tbl_MCustomerType] ([CustomerId], [CustomerTypeName], [IsAcitve]) VALUES (1, N'Demand', 1)
INSERT [dbo].[Tbl_MCustomerType] ([CustomerId], [CustomerTypeName], [IsAcitve]) VALUES (2, N'Consumption', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerType] OFF
/****** Object:  Table [dbo].[Tbl_MCustomerStatus]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MCustomerStatus](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusName] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerStatus] ON
INSERT [dbo].[Tbl_MCustomerStatus] ([StatusId], [StatusName]) VALUES (1, N'Active')
INSERT [dbo].[Tbl_MCustomerStatus] ([StatusId], [StatusName]) VALUES (2, N'InActive')
INSERT [dbo].[Tbl_MCustomerStatus] ([StatusId], [StatusName]) VALUES (3, N'Hold')
INSERT [dbo].[Tbl_MCustomerStatus] ([StatusId], [StatusName]) VALUES (4, N'Closed')
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerStatus] OFF
/****** Object:  Table [MASTERS].[Tbl_MControlRefMaster]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MASTERS].[Tbl_MControlRefMaster](
	[MRefId] [int] IDENTITY(1,1) NOT NULL,
	[UDCId] [int] NULL,
	[VText] [varchar](150) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MRefId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [MASTERS].[Tbl_MControlRefMaster] ON
INSERT [MASTERS].[Tbl_MControlRefMaster] ([MRefId], [UDCId], [VText], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, 2, N'License', 1, NULL, NULL, N'Admin', CAST(0x0000A43A01231921 AS DateTime))
INSERT [MASTERS].[Tbl_MControlRefMaster] ([MRefId], [UDCId], [VText], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, 3, N'PAN id number', 1, NULL, NULL, NULL, NULL)
INSERT [MASTERS].[Tbl_MControlRefMaster] ([MRefId], [UDCId], [VText], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, 3, N'Voter id number', 1, NULL, NULL, NULL, NULL)
INSERT [MASTERS].[Tbl_MControlRefMaster] ([MRefId], [UDCId], [VText], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, 3, N'Licence number', 1, NULL, NULL, NULL, NULL)
INSERT [MASTERS].[Tbl_MControlRefMaster] ([MRefId], [UDCId], [VText], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, 3, N'Test id number', 1, N'admin', CAST(0x0000A468013E26E2 AS DateTime), N'admin', CAST(0x0000A468013E33CC AS DateTime))
INSERT [MASTERS].[Tbl_MControlRefMaster] ([MRefId], [UDCId], [VText], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, 3, N'', 1, N'admin', CAST(0x0000A468013E392F AS DateTime), NULL, NULL)
INSERT [MASTERS].[Tbl_MControlRefMaster] ([MRefId], [UDCId], [VText], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, 4, N'Sample', 1, NULL, NULL, NULL, NULL)
INSERT [MASTERS].[Tbl_MControlRefMaster] ([MRefId], [UDCId], [VText], [IsActive], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, 4, N'Test', 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [MASTERS].[Tbl_MControlRefMaster] OFF
/****** Object:  Table [dbo].[Tbl_MConnectionReasons]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MConnectionReasons](
	[ConnectionReasonId] [int] IDENTITY(1,1) NOT NULL,
	[Reason] [varchar](500) NULL,
	[Notes] [varchar](max) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ConnectionReasonId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MConnectionReasons] ON
INSERT [dbo].[Tbl_MConnectionReasons] ([ConnectionReasonId], [Reason], [Notes], [Details], [ActiveStatus]) VALUES (1, N'New', NULL, NULL, 1)
INSERT [dbo].[Tbl_MConnectionReasons] ([ConnectionReasonId], [Reason], [Notes], [Details], [ActiveStatus]) VALUES (2, N'Disconnected', NULL, NULL, 1)
INSERT [dbo].[Tbl_MConnectionReasons] ([ConnectionReasonId], [Reason], [Notes], [Details], [ActiveStatus]) VALUES (3, N'Replacement', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MConnectionReasons] OFF
/****** Object:  Table [MASTERS].[Tbl_MClusterCategories]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MASTERS].[Tbl_MClusterCategories](
	[ClusterCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NULL,
	[CategoryDescription] [varchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[ClusterCategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [MASTERS].[Tbl_MClusterCategories] ON
INSERT [MASTERS].[Tbl_MClusterCategories] ([ClusterCategoryId], [CategoryName], [CategoryDescription]) VALUES (1, N'A', N'High Usage')
INSERT [MASTERS].[Tbl_MClusterCategories] ([ClusterCategoryId], [CategoryName], [CategoryDescription]) VALUES (2, N'B', N'Medium Usage')
INSERT [MASTERS].[Tbl_MClusterCategories] ([ClusterCategoryId], [CategoryName], [CategoryDescription]) VALUES (3, N'C', N'Low Usage')
SET IDENTITY_INSERT [MASTERS].[Tbl_MClusterCategories] OFF
/****** Object:  Table [dbo].[Tbl_MChargeIds]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MChargeIds](
	[ChargeId] [int] IDENTITY(1,1) NOT NULL,
	[ChargeName] [varchar](100) NULL,
	[IsAcitve] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ChargeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MChargeIds] ON
INSERT [dbo].[Tbl_MChargeIds] ([ChargeId], [ChargeName], [IsAcitve], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Additional Charge', 1, NULL, NULL, N'admin', CAST(0x0000A468013300CC AS DateTime))
INSERT [dbo].[Tbl_MChargeIds] ([ChargeId], [ChargeName], [IsAcitve], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Meter Maintenance Charge', 1, NULL, NULL, N'admin', CAST(0x0000A468013303EA AS DateTime))
INSERT [dbo].[Tbl_MChargeIds] ([ChargeId], [ChargeName], [IsAcitve], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Fixed charges', 0, N'admin', CAST(0x0000A4680131AD34 AS DateTime), N'admin', CAST(0x0000A468013306FA AS DateTime))
INSERT [dbo].[Tbl_MChargeIds] ([ChargeId], [ChargeName], [IsAcitve], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'Service Charges', 0, N'admin', CAST(0x0000A4680132C66D AS DateTime), N'admin', CAST(0x0000A4680132D92F AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_MChargeIds] OFF
/****** Object:  Table [dbo].[Tbl_MBillStatus]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillStatus](
	[BillStatusId] [int] IDENTITY(1,1) NOT NULL,
	[BillStatus] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillStatus] ON
INSERT [dbo].[Tbl_MBillStatus] ([BillStatusId], [BillStatus]) VALUES (1, N'Paid')
INSERT [dbo].[Tbl_MBillStatus] ([BillStatusId], [BillStatus]) VALUES (2, N'Due')
SET IDENTITY_INSERT [dbo].[Tbl_MBillStatus] OFF
/****** Object:  Table [dbo].[Tbl_MBillProcessType]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillProcessType](
	[BillProcessTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillProcessTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillProcessType] ON
INSERT [dbo].[Tbl_MBillProcessType] ([BillProcessTypeId], [Type]) VALUES (1, N'CycleWise')
INSERT [dbo].[Tbl_MBillProcessType] ([BillProcessTypeId], [Type]) VALUES (2, N'FeederWise')
SET IDENTITY_INSERT [dbo].[Tbl_MBillProcessType] OFF
/****** Object:  Table [dbo].[Tbl_MBillMonthOpenStatus]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillMonthOpenStatus](
	[OpenStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[OpenStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillMonthOpenStatus] ON
INSERT [dbo].[Tbl_MBillMonthOpenStatus] ([OpenStatusId], [Status]) VALUES (1, N'Opened')
INSERT [dbo].[Tbl_MBillMonthOpenStatus] ([OpenStatusId], [Status]) VALUES (2, N'Closed')
INSERT [dbo].[Tbl_MBillMonthOpenStatus] ([OpenStatusId], [Status]) VALUES (3, N'Cancelled')
SET IDENTITY_INSERT [dbo].[Tbl_MBillMonthOpenStatus] OFF
/****** Object:  Table [dbo].[Tbl_MBillingTypes]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillingTypes](
	[BillingTypeId] [int] IDENTITY(1,1) NOT NULL,
	[BillingType] [varchar](500) NULL,
	[Notes] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[BillingTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillingTypes] ON
INSERT [dbo].[Tbl_MBillingTypes] ([BillingTypeId], [BillingType], [Notes], [ActiveStatusId]) VALUES (1, N'Mobile', NULL, 1)
INSERT [dbo].[Tbl_MBillingTypes] ([BillingTypeId], [BillingType], [Notes], [ActiveStatusId]) VALUES (2, N'Web', NULL, 1)
INSERT [dbo].[Tbl_MBillingTypes] ([BillingTypeId], [BillingType], [Notes], [ActiveStatusId]) VALUES (3, N'Spot', NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MBillingTypes] OFF
/****** Object:  Table [dbo].[Tbl_MBillingRules]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillingRules](
	[RuleID] [int] IDENTITY(1,1) NOT NULL,
	[RuleName] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[RuleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillingRules] ON
INSERT [dbo].[Tbl_MBillingRules] ([RuleID], [RuleName]) VALUES (1, N'As Per Settings Rule')
INSERT [dbo].[Tbl_MBillingRules] ([RuleID], [RuleName]) VALUES (2, N'Avg Reading Rule')
SET IDENTITY_INSERT [dbo].[Tbl_MBillingRules] OFF
/****** Object:  Table [dbo].[Tbl_MBillGenarationStatus]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillGenarationStatus](
	[BillGenarationStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillGenarationStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillGenarationStatus] ON
INSERT [dbo].[Tbl_MBillGenarationStatus] ([BillGenarationStatusId], [Status]) VALUES (1, N'BillingGenarationCompleted')
INSERT [dbo].[Tbl_MBillGenarationStatus] ([BillGenarationStatusId], [Status]) VALUES (2, N'FileCreated')
INSERT [dbo].[Tbl_MBillGenarationStatus] ([BillGenarationStatusId], [Status]) VALUES (3, N'EmailSent')
INSERT [dbo].[Tbl_MBillGenarationStatus] ([BillGenarationStatusId], [Status]) VALUES (4, N'BillingQueeClosed')
INSERT [dbo].[Tbl_MBillGenarationStatus] ([BillGenarationStatusId], [Status]) VALUES (5, N'BillGenarationPending')
SET IDENTITY_INSERT [dbo].[Tbl_MBillGenarationStatus] OFF
/****** Object:  Table [EMPLOYEE].[Tbl_MBEDCEmployeeDetails]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [EMPLOYEE].[Tbl_MBEDCEmployeeDetails](
	[BEDCEmpId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeName] [varchar](50) NULL,
	[EmployeeDesignation] [varchar](50) NULL,
	[EmployeeLocation] [varchar](50) NULL,
	[EmployeCode] [varchar](50) NULL,
	[ContactNo] [varchar](20) NULL,
	[AnotherContactNo] [varchar](20) NULL,
	[Address] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[BEDCEmpId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [EMPLOYEE].[Tbl_MBEDCEmployeeDetails] ON
INSERT [EMPLOYEE].[Tbl_MBEDCEmployeeDetails] ([BEDCEmpId], [EmployeeName], [EmployeeDesignation], [EmployeeLocation], [EmployeCode], [ContactNo], [AnotherContactNo], [Address], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifedBy], [ModifiedDate]) VALUES (1, N'Admin', N'Manger', N'Benin', N'BEDC_001', N'123-123123', N'123-123123', N'Manger', 1, NULL, NULL, N'Admin', CAST(0x0000A43A011F67CB AS DateTime))
SET IDENTITY_INSERT [EMPLOYEE].[Tbl_MBEDCEmployeeDetails] OFF
/****** Object:  Table [dbo].[Tbl_MBatchStatus]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBatchStatus](
	[BatchStatusId] [int] IDENTITY(1,1) NOT NULL,
	[BatchStatus] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[BatchStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBatchStatus] ON
INSERT [dbo].[Tbl_MBatchStatus] ([BatchStatusId], [BatchStatus]) VALUES (1, N'Pending')
INSERT [dbo].[Tbl_MBatchStatus] ([BatchStatusId], [BatchStatus]) VALUES (2, N'Finished')
INSERT [dbo].[Tbl_MBatchStatus] ([BatchStatusId], [BatchStatus]) VALUES (3, N'Bill Generated')
SET IDENTITY_INSERT [dbo].[Tbl_MBatchStatus] OFF
/****** Object:  Table [MASTERS].[Tbl_MAreaDetails]    Script Date: 04/15/2015 13:37:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MASTERS].[Tbl_MAreaDetails](
	[AreaCode] [int] IDENTITY(1,1) NOT NULL,
	[AreaDetails] [varchar](100) NULL,
	[ZipCode] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AreaCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [MASTERS].[Tbl_MAreaDetails] ON
INSERT [MASTERS].[Tbl_MAreaDetails] ([AreaCode], [AreaDetails], [ZipCode], [CreatedBy], [CreatedDate], [ModifedBy], [ModifiedDate]) VALUES (10, N'Hyderabad', N'482001', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [MASTERS].[Tbl_MAreaDetails] OFF
/****** Object:  Default [DF_Tbl_BillAdjustmentDetails_AdditionalChargesID]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_BillAdjustmentDetails] ADD  CONSTRAINT [DF_Tbl_BillAdjustmentDetails_AdditionalChargesID]  DEFAULT ((0)) FOR [AdditionalChargesID]
GO
/****** Object:  Default [DF_BillAdjustmentType_Status]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_BillAdjustmentType] ADD  CONSTRAINT [DF_BillAdjustmentType_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF__Tbl_Compa__Activ__41EDCAC5]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_CompanyDetails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__TBL_Funct__IsAct__42E1EEFE]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[TBL_FunctionalAccessPermission] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__TBL_Funct__IsFin__43D61337]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[TBL_FunctionApprovalRole] ADD  DEFAULT ((0)) FOR [IsFinalApproval]
GO
/****** Object:  Default [DF__Tbl_MAcco__IsMas__45BE5BA9]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MAccountTypes] ADD  DEFAULT ((0)) FOR [IsMaster]
GO
/****** Object:  Default [DF__Tbl_MAcco__Activ__44CA3770]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MAccountTypes] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MAppl__Activ__46B27FE2]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MApplicationProccessedBy] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MBill__Activ__489AC854]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MBillingTypes] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MChar__IsAci__498EEC8D]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MChargeIds] ADD  DEFAULT ((1)) FOR [IsAcitve]
GO
/****** Object:  Default [DF__Tbl_MConn__Activ__4A8310C6]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MConnectionReasons] ADD  DEFAULT ((1)) FOR [ActiveStatus]
GO
/****** Object:  Default [DF__Tbl_MCust__IsAci__4B7734FF]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MCustomerType] ADD  DEFAULT ((1)) FOR [IsAcitve]
GO
/****** Object:  Default [DF__Tbl_MCust__Activ__4C6B5938]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MCustomerTypes] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MCust__IsMas__4D5F7D71]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MCustomerTypes] ADD  DEFAULT ((0)) FOR [IsMaster]
GO
/****** Object:  Default [DF__Tbl_MDesi__Activ__4E53A1AA]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MDesignations] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF_Tbl_Menus_IsActive]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_Menus] ADD  CONSTRAINT [DF_Tbl_Menus_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Menus__Activ__47E69B3D]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_Menus] ADD  DEFAULT ((1)) FOR [ActiveHeader]
GO
/****** Object:  Default [DefaultActivestatus]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MGovtAccountTypes] ADD  CONSTRAINT [DefaultActivestatus]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MIden__Activ__503BEA1C]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MIdentityTypes] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MLGA__Active__51300E55]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MLGA] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MMete__Activ__5224328E]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MMeterStatus] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MMete__IsMas__540C7B00]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MMeterTypes] ADD  DEFAULT ((0)) FOR [IsMaster]
GO
/****** Object:  Default [DF__Tbl_MMete__Activ__531856C7]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MMeterTypes] ADD  DEFAULT ((1)) FOR [ActiveStatus]
GO
/****** Object:  Default [DF__Tbl_MOrga__Activ__55009F39]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MOrganizations] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF_Table_MPaymentMode_ActiveStatusId]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MPaymentMode] ADD  CONSTRAINT [DF_Table_MPaymentMode_ActiveStatusId]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MPaym__Activ__7C255952]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MPaymentType] ADD  DEFAULT ((1)) FOR [ActiveStatusID]
GO
/****** Object:  Default [DF__Tbl_MPhas__Activ__56E8E7AB]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MPhases] ADD  DEFAULT ((1)) FOR [ActiveStatus]
GO
/****** Object:  Default [MPoleMasterDetails_ActiveStatusId_Default_Value]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MPoleMasterDetails] ADD  CONSTRAINT [MPoleMasterDetails_ActiveStatusId_Default_Value]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MRead__Activ__58D1301D]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MReadCodes] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MRole__IsAct__5AB9788F]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MRoles] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_MTari__IsAci__0F975522]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MTariffClasses] ADD  CONSTRAINT [DF__Tbl_MTari__IsAci__0F975522]  DEFAULT ((1)) FOR [IsActiveClass]
GO
/****** Object:  Default [DF__Tbl_MTaxD__IsAci__5D95E53A]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MTaxDetails] ADD  DEFAULT ((1)) FOR [IsAcitive]
GO
/****** Object:  Default [DF__Tbl_MTaxT__IsAct__5E8A0973]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_MTaxTypeDetails] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_Tbl_NewPagePermissions_IsActive]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_NewPagePermissions] ADD  CONSTRAINT [DF_Tbl_NewPagePermissions_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_UserD__IsMob__4870AB22]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_UserDetails] ADD  DEFAULT ((0)) FOR [IsMobileAccess]
GO
/****** Object:  Default [DF__Tbl_UserL__Activ__6166761E]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [dbo].[Tbl_UserLoginDetails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MBEDC__Activ__625A9A57]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [EMPLOYEE].[Tbl_MBEDCEmployeeDetails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Contr__Activ__634EBE90]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [MASTERS].[Tbl_ControlRefMaster] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MCont__IsAct__6442E2C9]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [MASTERS].[Tbl_MControlRefMaster] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_USerD__IsAct__65370702]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [MASTERS].[Tbl_USerDefinedControlDetails] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId10]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [MASTERS].[Tbl_MAreaDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId10] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [MASTERS].[Tbl_MAreaDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId10]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId11]    Script Date: 04/15/2015 13:37:02 ******/
ALTER TABLE [MASTERS].[Tbl_MAreaDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId11] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [MASTERS].[Tbl_MAreaDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId11]
GO
