GO
/****** Object:  Table [dbo].[Tbl_Audit_UserSpecialPermissions]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserSpecialPermissions](
	[IdentityUserPermissionId] [int] IDENTITY(1,1) NOT NULL,
	[UserPermissionId] [int] NOT NULL,
	[UserId] [varchar](50) NULL,
	[BudgetSegmentWiseMargin] [bit] NULL,
	[CustomerDetailsModification] [bit] NULL,
	[BillAdjustment] [bit] NULL,
	[TariffAdjustments] [bit] NULL,
	[PaymentsAdjustments] [bit] NULL,
	[Disconnection] [bit] NULL,
	[ReConnections] [bit] NULL,
	[BillGeneration] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_UserServiceUnits]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserServiceUnits](
	[IdentityUserSU_ID] [int] IDENTITY(1,1) NOT NULL,
	[UserServiceUnitId] [int] NOT NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_UserServiceCenters]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserServiceCenters](
	[IdentityUserSC_ID] [int] IDENTITY(1,1) NOT NULL,
	[UserServiceCenterId] [int] NOT NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_UserLoginDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserLoginDetails](
	[IdentityUserId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](50) NOT NULL,
	[Password] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_UserDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserDetails](
	[IdentityUserId] [int] IDENTITY(1,1) NOT NULL,
	[UserDetailsId] [int] NOT NULL,
	[UserId] [varchar](50) NULL,
	[Name] [varchar](300) NULL,
	[SurName] [varchar](300) NULL,
	[PrimaryContact] [varchar](20) NULL,
	[SecondaryContact] [varchar](20) NULL,
	[PrimaryEmailId] [varchar](500) NULL,
	[SecondaryEmailId] [varchar](500) NULL,
	[Address] [varchar](max) NULL,
	[Photo] [varchar](max) NULL,
	[ScannedDocument] [varchar](max) NULL,
	[GenderId] [int] NULL,
	[Details] [varchar](max) NULL,
	[RoleId] [int] NULL,
	[DesignationId] [int] NULL,
	[FilePath] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_UserBusinessUnits]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserBusinessUnits](
	[IdentityUserBU_ID] [int] IDENTITY(1,1) NOT NULL,
	[UserBusinessUnitId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_States]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_States](
	[IdentityStateId] [int] IDENTITY(1,1) NOT NULL,
	[StateCode] [varchar](20) NOT NULL,
	[StateName] [varchar](50) NULL,
	[DisplayCode] [varchar](20) NULL,
	[CountryCode] [varchar](20) NULL,
	[StateDetails] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_ServiceUnits]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_ServiceUnits](
	[IdentitySU_ID] [int] IDENTITY(1,1) NOT NULL,
	[SU_ID] [varchar](20) NOT NULL,
	[ServiceUnitName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[BU_ID] [varchar](20) NULL,
	[SUCode] [varchar](10) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[ZIP] [varchar](100) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_ReadToDirectCustomerActivityLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_ReadToDirectCustomerActivityLogs](
	[ReadToDirectId] [int] NULL,
	[GlobalAccountNo] [varchar](50) NULL,
	[MeterNo] [varchar](50) NULL,
	[Remarks] [varchar](max) NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_PaymentsLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_PaymentsLogs](
	[PaymentLogId] [bigint] NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[AccountNo] [varchar](50) NULL,
	[ReceiptNumber] [varchar](20) NULL,
	[PaymentMode] [int] NULL,
	[PaidAmount] [decimal](18, 2) NULL,
	[PadiDate] [datetime] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PaymentFromId] [int] NULL,
	[DocumentPath] [varchar](max) NULL,
	[Cashier] [varchar](50) NULL,
	[CashOffice] [int] NULL,
	[DocumentName] [varchar](max) NULL,
	[BatchNo] [int] NULL,
	[PaymentType] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_MMeterTypes]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_MMeterTypes](
	[IdentityMeterId] [int] IDENTITY(1,1) NOT NULL,
	[MeterTypeId] [int] NOT NULL,
	[MeterType] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatus] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_LEnergyClassCharges]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_LEnergyClassCharges](
	[IdentityChargeId] [int] IDENTITY(1,1) NOT NULL,
	[EnergyClassChargeID] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[FromKW] [int] NULL,
	[ToKW] [int] NULL,
	[FromDate] [datetime] NULL,
	[Todate] [datetime] NULL,
	[ClassID] [int] NULL,
	[IsActive] [bit] NULL,
	[TaxId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_AUDIT_LAdditionalClassCharges]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_AUDIT_LAdditionalClassCharges](
	[IdentityChargeId] [int] IDENTITY(1,1) NOT NULL,
	[AdditionalChargeID] [int] NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[ClassID] [int] NULL,
	[IsActive] [bit] NULL,
	[ChargeId] [int] NULL,
	[TaxId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_FunctionApprovalRole]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_FunctionApprovalRole](
	[ApprovalRoleId] [int] NULL,
	[FunctionId] [int] NULL,
	[RoleId] [int] NULL,
	[Level] [int] NULL,
	[IsFinalApproval] [bit] NULL,
	[UserIds] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_FunctionalAccessPermission]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_FunctionalAccessPermission](
	[FunctionId] [int] NULL,
	[Function] [varchar](200) NULL,
	[AccessLevels] [int] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_Cycles]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_Cycles](
	[IdentityCycleId] [int] IDENTITY(1,1) NOT NULL,
	[CycleId] [varchar](20) NOT NULL,
	[CycleName] [varchar](300) NULL,
	[DetailsOfCycle] [varchar](max) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[ContactName] [varchar](200) NULL,
	[ContactNo] [varchar](20) NULL,
	[CycleCode] [varchar](10) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerTypeChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerTypeChangeLogs](
	[CustomerTypeChangeLogId] [int] NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[OldCustomerTypeId] [int] NULL,
	[NewCustomerTypeId] [int] NULL,
	[Remarks] [varchar](200) NULL,
	[ApprovalStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerTariffChangeRequest]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerTariffChangeRequest](
	[TariffChangeRequestId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[PreviousTariffId] [int] NULL,
	[ChangeRequestedTariffId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[OldClusterCategoryId] [int] NULL,
	[NewClusterCategoryId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerReadings]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerReadings](
	[AuditCustomerReadingId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerReadingId] [int] NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[AccountNumber] [varchar](50) NULL,
	[ReadDate] [datetime] NULL,
	[ReadBy] [varchar](50) NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Usage] [numeric](20, 4) NULL,
	[TotalReadingEnergies] [decimal](18, 2) NULL,
	[TotalReadings] [int] NULL,
	[Multiplier] [int] NULL,
	[ReadType] [int] NULL,
	[IsBilled] [bit] NULL,
	[BilledBy] [varchar](50) NULL,
	[BilledDate] [varchar](50) NULL,
	[AverageReading] [varchar](50) NULL,
	[StatusId] [varchar](50) NULL,
	[StatusDate] [datetime] NULL,
	[BillNo] [varchar](50) NULL,
	[UsageForBilling] [decimal](18, 2) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerReadingApprovalLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerReadingApprovalLogs](
	[AuditCustomerReadingLogId] [int] IDENTITY(1,1) NOT NULL,
	[ReadDate] [datetime] NULL,
	[ReadBy] [varchar](50) NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Usage] [numeric](18, 0) NULL,
	[TotalReadingEnergies] [decimal](18, 2) NULL,
	[TotalReadings] [int] NULL,
	[Multiplier] [int] NULL,
	[ReadType] [int] NULL,
	[AverageReading] [varchar](50) NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[IsTamper] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[MeterNumber] [varchar](50) NULL,
	[MeterReadingFrom] [int] NULL,
	[IsRollOver] [bit] NULL,
	[ApproveStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[Remarks] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[AuditCustomerReadingLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerNameChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerNameChangeLogs](
	[NameChangeLogId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[OldTitle] [varchar](10) NULL,
	[NewTitle] [varchar](10) NULL,
	[OldFirstName] [varchar](50) NULL,
	[NewFirstName] [varchar](50) NULL,
	[OldMiddleName] [varchar](50) NULL,
	[NewMiddleName] [varchar](50) NULL,
	[OldLastName] [varchar](50) NULL,
	[NewLastName] [varchar](50) NULL,
	[OldKnownAs] [varchar](150) NULL,
	[NewKnownAs] [varchar](150) NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerMeterInfoChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerMeterInfoChangeLogs](
	[MeterInfoChangeLogId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[OldMeterNo] [varchar](100) NULL,
	[NewMeterNo] [varchar](100) NULL,
	[OldMeterTypeId] [int] NULL,
	[NewMeterTypeId] [int] NULL,
	[OldDials] [int] NULL,
	[NewDials] [int] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[OldMeterReading] [varchar](20) NULL,
	[NewMeterReading] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[MeterChangedDate] [datetime] NULL,
	[InitialBillingkWh] [int] NULL,
	[NewMeterInitialReading] [varchar](20) NULL,
	[NewMeterReadingDate] [datetime] NULL,
	[IsCAPMIMeter] [bit] NULL,
	[CAPMIAmount] [decimal](18, 2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerDetails](
	[Audit_Identity] [int] IDENTITY(1,1) NOT NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[AccountNo] [varchar](50) NULL,
	[Title] [varchar](10) NULL,
	[Name] [varchar](100) NULL,
	[SurName] [varchar](100) NULL,
	[KnownAs] [varchar](100) NULL,
	[EmailId] [varchar](max) NULL,
	[DocumentNo] [varchar](100) NULL,
	[EmployeeCode] [varchar](100) NULL,
	[HomeContactNo] [varchar](20) NULL,
	[BusinessContactNo] [varchar](20) NULL,
	[OtherContactNo] [varchar](20) NULL,
	[PostalLandMark] [varchar](200) NULL,
	[PostalStreet] [varchar](200) NULL,
	[PostalCity] [varchar](200) NULL,
	[PostalHouseNo] [varchar](200) NULL,
	[PostalDetails] [varchar](max) NULL,
	[PostalZipCode] [varchar](50) NULL,
	[IdentityNo] [varchar](100) NULL,
	[IdentityId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[BookNo] [varchar](20) NULL,
	[InjectionSubStationId] [varchar](20) NULL,
	[FeederId] [varchar](20) NULL,
	[TransformerId] [varchar](20) NULL,
	[PoleId] [varchar](20) NULL,
	[ServiceLandMark] [varchar](200) NULL,
	[ServiceStreet] [varchar](200) NULL,
	[ServiceCity] [varchar](200) NULL,
	[ServiceHouseNo] [varchar](200) NULL,
	[ServiceDetails] [varchar](max) NULL,
	[ServiceZipCode] [varchar](50) NULL,
	[ServiceEmailId] [varchar](max) NULL,
	[MobileNo] [varchar](20) NULL,
	[RouteSequenceNo] [int] NULL,
	[NeighborAccountNo] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
	[Longitude] [varchar](50) NULL,
	[TariffId] [int] NULL,
	[MultiplicationFactor] [decimal](18, 4) NULL,
	[AccountTypeId] [int] NULL,
	[ApplicationDate] [datetime] NULL,
	[ReadCodeId] [int] NULL,
	[OrganizationCode] [varchar](50) NULL,
	[CustomerTypeId] [int] NULL,
	[OldAccountNo] [varchar](50) NULL,
	[MeterTypeId] [int] NULL,
	[PhaseId] [int] NULL,
	[IsEmbassyCustomer] [bit] NULL,
	[EmbassyCode] [varchar](50) NULL,
	[IsVIPCustomer] [bit] NULL,
	[MeterNo] [varchar](100) NULL,
	[MeterSerialNo] [varchar](100) NULL,
	[ConnectionDate] [datetime] NULL,
	[MeterStatusId] [int] NULL,
	[ConnectionReasonId] [int] NULL,
	[InitialReading] [varchar](max) NULL,
	[CurrentReading] [varchar](max) NULL,
	[AverageReading] [varchar](max) NULL,
	[SetupDate] [datetime] NULL,
	[CertifiedBy] [varchar](100) NULL,
	[ProccessedById] [int] NULL,
	[InstalledBy] [varchar](100) NULL,
	[AgencyId] [int] NULL,
	[ContactPersonName] [varchar](100) NULL,
	[AgencyDetails] [varchar](max) NULL,
	[RouteNo] [int] NULL,
	[CustomerSequenceNo] [bigint] NULL,
	[CustomerUniqueId] [int] NULL,
	[StateCode] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[MinimumReading] [varchar](max) NULL,
	[Highestconsumption] [varchar](15) NULL,
	[SortOrder] [int] NULL,
	[StatusId] [varchar](10) NULL,
	[IsBookNoChanged] [bit] NULL,
	[OutStandingAmount] [decimal](18, 2) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerContactInfoChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerContactInfoChangeLogs](
	[CustomerContactInfoChangeLogId] [int] NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[OldEmailId] [varchar](100) NULL,
	[NewEmailId] [varchar](100) NULL,
	[OldHomeContactNumber] [varchar](20) NULL,
	[NewHomeContactNumber] [varchar](20) NULL,
	[OldBusinessContactNumber] [varchar](20) NULL,
	[NewBusinessContactNumber] [varchar](20) NULL,
	[OldOtherContactNumber] [varchar](20) NULL,
	[NewOtherContactNumber] [varchar](20) NULL,
	[ApprovalStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[Remarks] [varchar](500) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerBills]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerBills](
	[AuditCustomerBillId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerBillId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerId] [varchar](50) NULL,
	[TotalBillAmount] [decimal](20, 4) NULL,
	[ServiceAddress] [varchar](max) NULL,
	[MeterNo] [varchar](50) NULL,
	[Dials] [int] NULL,
	[NetArrears] [decimal](20, 4) NULL,
	[NetEnergyCharges] [decimal](20, 4) NULL,
	[NetFixedCharges] [decimal](20, 4) NULL,
	[VAT] [decimal](20, 4) NULL,
	[VATPercentage] [decimal](20, 4) NULL,
	[Messages] [varchar](max) NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[SubStationId] [varchar](20) NULL,
	[FeederId] [varchar](20) NULL,
	[TransFormerId] [varchar](20) NULL,
	[PoleId] [varchar](20) NULL,
	[BillGeneratedBy] [varchar](50) NULL,
	[BillGeneratedDate] [datetime] NULL,
	[PaymentLastDate] [datetime] NULL,
	[TariffId] [int] NULL,
	[BillYear] [int] NULL,
	[BillMonth] [int] NULL,
	[CycleId] [varchar](20) NULL,
	[TotalBillAmountWithArrears] [decimal](20, 4) NULL,
	[ActiveStatusId] [int] NULL,
	[BillNo] [varchar](50) NULL,
	[PaymentStatusID] [int] NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Usage] [decimal](20, 4) NULL,
	[AverageReading] [decimal](20, 4) NULL,
	[TotalBillAmountWithTax] [decimal](20, 4) NULL,
	[EstimatedUsage] [int] NULL,
	[ReadCodeId] [int] NULL,
	[ReadType] [int] NULL,
	[StatusId] [varchar](50) NULL,
	[StatusDate] [datetime] NULL,
	[AdjustmentAmmount] [decimal](18, 2) NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerAddressChangeLog]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerAddressChangeLog](
	[AddressChangeLogId] [int] NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[OldPostal_HouseNo] [varchar](100) NULL,
	[OldPostal_StreetName] [varchar](255) NULL,
	[OldPostal_City] [varchar](100) NULL,
	[OldPostal_Landmark] [varchar](100) NULL,
	[OldPostal_AreaCode] [varchar](100) NULL,
	[OldPostal_ZipCode] [varchar](100) NULL,
	[NewPostal_HouseNo] [varchar](100) NULL,
	[NewPostal_StreetName] [varchar](255) NULL,
	[NewPostal_City] [varchar](100) NULL,
	[NewPostal_Landmark] [varchar](100) NULL,
	[NewPostal_AreaCode] [varchar](100) NULL,
	[NewPostal_ZipCode] [varchar](100) NULL,
	[OldService_HouseNo] [varchar](100) NULL,
	[OldService_StreetName] [varchar](255) NULL,
	[OldService_City] [varchar](100) NULL,
	[OldService_Landmark] [varchar](100) NULL,
	[OldService_AreaCode] [varchar](100) NULL,
	[OldService_ZipCode] [varchar](100) NULL,
	[NewService_HouseNo] [varchar](100) NULL,
	[NewService_StreetName] [varchar](255) NULL,
	[NewService_City] [varchar](100) NULL,
	[NewService_Landmark] [varchar](100) NULL,
	[NewService_AreaCode] [varchar](100) NULL,
	[NewService_ZipCode] [varchar](100) NULL,
	[ApprovalStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[OldServiceAddressID] [int] NULL,
	[NewServiceAddressID] [int] NULL,
	[OldPostalAddressID] [int] NULL,
	[NewPostalAddressID] [int] NULL,
	[OldIsSameAsService] [int] NULL,
	[NewIsSameAsService] [int] NULL,
	[IsPostalCommunication] [bit] NULL,
	[IsServiceComunication] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerActiveStatusChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerActiveStatusChangeLogs](
	[ActiveStatusChangeLogId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[OldStatus] [int] NULL,
	[NewStatus] [int] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](200) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[ChangeDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_Customer_Additionalcharges]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_Customer_Additionalcharges](
	[AuditCustomerAdditioalCharges] [int] IDENTITY(1,1) NOT NULL,
	[CustomerAdditioalCharges] [int] NULL,
	[CustomerID] [varchar](50) NULL,
	[AccountNO] [varchar](50) NULL,
	[CustomerBillId] [int] NULL,
	[TariffId] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[ChargeId] [int] NULL,
	[StatusId] [varchar](50) NULL,
	[StatusDate] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBY] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_Countries]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_Countries](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [varchar](20) NOT NULL,
	[CountryName] [varchar](50) NULL,
	[CountryDetails] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_BussinessUnits]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_BussinessUnits](
	[BU_Audit_Id] [int] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](20) NOT NULL,
	[BusinessUnitName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[IdentityBU_ID] [int] NULL,
	[StateCode] [varchar](20) NULL,
	[BUCode] [varchar](10) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_BookNumbers]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_BookNumbers](
	[Audit_Identity] [int] IDENTITY(1,1) NOT NULL,
	[BookNo] [varchar](20) NULL,
	[ID] [varchar](20) NULL,
	[CycleId] [varchar](20) NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[NoOfAccounts] [int] NULL,
	[Details] [varchar](max) NULL,
	[IdentityBookId] [int] NULL,
	[BookCode] [varchar](10) NULL,
	[SortOrder] [int] NULL,
	[StatusId] [varchar](10) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_BookNoChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_BookNoChangeLogs](
	[BookNoChangeLogId] [int] NULL,
	[GlobalAccountNo] [varchar](50) NULL,
	[OldBU_ID] [varchar](20) NULL,
	[NewBU_ID] [varchar](20) NULL,
	[OldSU_ID] [varchar](20) NULL,
	[NewSU_ID] [varchar](20) NULL,
	[OldSC_ID] [varchar](20) NULL,
	[NewSC_ID] [varchar](20) NULL,
	[OldCycleId] [varchar](20) NULL,
	[NewCycleId] [varchar](20) NULL,
	[OldBookNo] [varchar](20) NULL,
	[NewBookNo] [varchar](20) NULL,
	[OldPostal_HouseNo] [varchar](100) NULL,
	[OldPostal_StreetName] [varchar](255) NULL,
	[OldPostal_City] [varchar](100) NULL,
	[OldPostal_Landmark] [varchar](100) NULL,
	[OldPostal_AreaCode] [varchar](100) NULL,
	[OldPostal_ZipCode] [varchar](100) NULL,
	[NewPostal_HouseNo] [varchar](100) NULL,
	[NewPostal_StreetName] [varchar](255) NULL,
	[NewPostal_City] [varchar](100) NULL,
	[NewPostal_Landmark] [varchar](100) NULL,
	[NewPostal_AreaCode] [varchar](100) NULL,
	[NewPostal_ZipCode] [varchar](100) NULL,
	[OldService_HouseNo] [varchar](100) NULL,
	[OldService_StreetName] [varchar](255) NULL,
	[OldService_City] [varchar](100) NULL,
	[OldService_Landmark] [varchar](100) NULL,
	[OldService_AreaCode] [varchar](100) NULL,
	[OldService_ZipCode] [varchar](100) NULL,
	[NewService_HouseNo] [varchar](100) NULL,
	[NewService_StreetName] [varchar](255) NULL,
	[NewService_City] [varchar](100) NULL,
	[NewService_Landmark] [varchar](100) NULL,
	[NewService_AreaCode] [varchar](100) NULL,
	[NewService_ZipCode] [varchar](100) NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[OldServiceAddressID] [int] NULL,
	[NewServiceAddressID] [int] NULL,
	[OldPostalAddressID] [int] NULL,
	[NewPostalAddressID] [int] NULL,
	[OldIsSameAsService] [int] NULL,
	[NewIsSameAsService] [int] NULL,
	[IsPostalCommunication] [bit] NULL,
	[IsServiceComunication] [bit] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_BillingQueueSchedule]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_BillingQueueSchedule](
	[AuditBillingQueueScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[BillingQueueScheduleId] [int] NULL,
	[BillingMonth] [int] NULL,
	[BillingYear] [int] NULL,
	[BillSendingMode] [int] NULL,
	[BillProcessTypeId] [int] NULL,
	[TotalCustomersInQueue] [int] NULL,
	[OpenStatusId] [int] NULL,
	[ServiceStartDate] [datetime] NULL,
	[FeederId] [varchar](20) NULL,
	[CycleId] [varchar](20) NULL,
	[BillGenarationStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[StatusId] [varchar](50) NULL,
	[StatusDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_BillingQueeCustomers]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_BillingQueeCustomers](
	[AuditBillQueueCustomerId] [int] IDENTITY(1,1) NOT NULL,
	[BillQueueCustomerId] [int] NULL,
	[BillingQueuescheduleId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[Month] [int] NULL,
	[Year] [int] NULL,
	[BillGenarationStatusId] [int] NULL,
	[BillFile] [varchar](max) NULL,
	[StatusId] [varchar](50) NULL,
	[StatusDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_BillAdjustments]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_BillAdjustments](
	[AuditBillAdjustmentId] [int] IDENTITY(1,1) NOT NULL,
	[BillAdjustmentId] [int] NULL,
	[CustomerBillId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerId] [varchar](50) NULL,
	[Remarks] [varchar](max) NULL,
	[AmountEffected] [decimal](18, 4) NULL,
	[ApprovalStatusId] [int] NULL,
	[BillAdjustmentType] [int] NULL,
	[TaxEffected] [decimal](18, 2) NULL,
	[AdjustedUnits] [int] NULL,
	[TotalAmountEffected] [decimal](18, 2) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[ApprovedDate] [varchar](50) NULL,
	[EffectedBillId] [int] NULL,
	[StatusId] [varchar](10) NULL,
	[StatusDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_AssignedMeterLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_AssignedMeterLogs](
	[AssignedMeterId] [int] NULL,
	[GlobalAccountNo] [varchar](50) NULL,
	[MeterNo] [varchar](50) NULL,
	[AssignedMeterDate] [datetime] NULL,
	[InitialReading] [decimal](18, 2) NULL,
	[Remarks] [varchar](max) NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsCAPMIMeter] [bit] NULL,
	[CAPMIAmount] [decimal](18, 2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_Agencies]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_Agencies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AgencyId] [int] NULL,
	[AgencyName] [varchar](300) NULL,
	[Details] [varchar](max) NULL,
	[ContactNo1] [varchar](20) NULL,
	[ContactNo2] [varchar](20) NULL,
	[ContactPersonName] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_AdjustmentsLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_AdjustmentsLogs](
	[AdjustmentLogId] [bigint] NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[AccountNo] [varchar](50) NULL,
	[BillNumber] [varchar](50) NULL,
	[BillAdjustmentTypeId] [int] NULL,
	[MeterNumber] [varchar](20) NULL,
	[PreviousReading] [varchar](20) NULL,
	[CurrentReadingAfterAdjustment] [varchar](50) NULL,
	[CurrentReadingBeforeAdjustment] [varchar](50) NULL,
	[AdjustedUnits] [decimal](18, 2) NULL,
	[TaxEffected] [decimal](18, 2) NULL,
	[TotalAmountEffected] [decimal](18, 2) NULL,
	[EnergryCharges] [decimal](18, 2) NULL,
	[AdditionalCharges] [decimal](18, 2) NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit__ServiceCenter]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit__ServiceCenter](
	[IdentitySC_ID] [int] IDENTITY(1,1) NOT NULL,
	[ServiceCenterId] [varchar](20) NOT NULL,
	[ServiceCenterName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[SU_ID] [varchar](20) NULL,
	[SCCode] [varchar](10) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[ZIP] [varchar](100) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_AssignedMeterLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_AssignedMeterLogs](
	[AssignedMeterId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNo] [varchar](50) NULL,
	[MeterNo] [varchar](50) NULL,
	[AssignedMeterDate] [datetime] NULL,
	[InitialReading] [decimal](18, 2) NULL,
	[Remarks] [varchar](max) NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
	[IsCAPMIMeter] [bit] NULL,
	[CAPMIAmount] [decimal](18, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[AssignedMeterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_ApprovalRegistration]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_ApprovalRegistration](
	[ARID] [int] IDENTITY(1,1) NOT NULL,
	[CertifiedBy] [varchar](50) NULL,
	[CertifiedBy_Value] [varchar](50) NULL,
	[Seal1] [varchar](50) NULL,
	[Seal2] [varchar](50) NULL,
	[ApplicationProcessedBy] [varchar](50) NULL,
	[ApplicationProcessedBy_Value] [varchar](50) NULL,
	[ContactName] [varchar](50) NULL,
	[InstalledBy] [int] NULL,
	[InstalledBy_Value] [varchar](50) NULL,
	[AgencyId] [int] NULL,
	[AgencyId_Value] [varchar](50) NULL,
	[MeterAmount] [decimal](18, 2) NULL,
	[InitialBillingKWh] [bigint] NULL,
	[InitialReading] [bigint] NULL,
	[PresentReading] [bigint] NULL,
	[Highestconsumption] [decimal](18, 2) NULL,
	[OutStandingAmount_ActiveDetails] [decimal](18, 2) NULL,
	[AvgReading] [bigint] NULL,
	[IsCAPMI] [bit] NULL,
	[OpeningBalance] [decimal](18, 2) NULL,
	[IdentityNumber] [varchar](150) NULL,
	[IdentityTypeId] [int] NULL,
	[IdentityTypeId_Value] [varchar](50) NULL,
	[HouseNo_Postal] [varchar](200) NULL,
	[StreetName_Postal] [varchar](255) NULL,
	[City_Postal] [varchar](200) NULL,
	[Details_Postal] [varchar](200) NULL,
	[AreaCode_Postal] [int] NULL,
	[AreaCode_Postal_Value] [varchar](50) NULL,
	[Latitude_Postal] [varchar](50) NULL,
	[Longitude_Postal] [varchar](50) NULL,
	[IsServiceAddress_Postal] [bit] NULL,
	[ZipCode_Postal] [varchar](50) NULL,
	[IsCommunication_Postal] [bit] NULL,
	[IsActive_Postal] [bit] NULL,
	[HouseNo_Service] [varchar](200) NULL,
	[StreetName_Service] [varchar](255) NULL,
	[City_Service] [varchar](200) NULL,
	[Details_Service] [varchar](200) NULL,
	[AreaCode_Service] [int] NULL,
	[AreaCode_Service_Value] [varchar](50) NULL,
	[Latitude_Service] [varchar](50) NULL,
	[Longitude_Service] [varchar](50) NULL,
	[IsServiceAddress_Service] [bit] NULL,
	[ZipCode_Service] [varchar](50) NULL,
	[IsCommunication_Service] [bit] NULL,
	[IsActive_Service] [bit] NULL,
	[Title] [varchar](10) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[KnownAs] [varchar](150) NULL,
	[EmailId] [varchar](max) NULL,
	[HomeContactNumber] [varchar](20) NULL,
	[BusinessContactNumber] [varchar](20) NULL,
	[OtherContactNumber] [varchar](20) NULL,
	[IsSameAsService] [bit] NULL,
	[IsSameAsTenent] [bit] NULL,
	[DocumentNo] [varchar](50) NULL,
	[ApplicationDate] [datetime] NULL,
	[ConnectionDate] [datetime] NULL,
	[SetupDate] [datetime] NULL,
	[IsBEDCEmployee] [bit] NULL,
	[EmployeeCode] [int] NULL,
	[EmployeeCode_Value] [varchar](50) NULL,
	[IsVIPCustomer] [bit] NULL,
	[OldAccountNo] [varchar](20) NULL,
	[OrganizationCode] [varchar](20) NULL,
	[ConnectionReasonId] [varchar](20) NULL,
	[Service_HouseNo] [varchar](100) NULL,
	[Service_StreetName] [varchar](255) NULL,
	[Service_City] [varchar](100) NULL,
	[Service_Landmark] [varchar](100) NULL,
	[Service_AreaCode] [varchar](100) NULL,
	[Service_ZipCode] [varchar](100) NULL,
	[Postal_HouseNo] [varchar](100) NULL,
	[Postal_StreetName] [varchar](255) NULL,
	[Postal_City] [varchar](100) NULL,
	[Postal_Landmark] [varchar](100) NULL,
	[Postal_AreaCode] [varchar](100) NULL,
	[Postal_ZipCode] [varchar](100) NULL,
	[FirstName_Tenant] [varchar](100) NULL,
	[MiddleName_Tenant] [varchar](100) NULL,
	[LastName_Tenant] [varchar](100) NULL,
	[PhoneNumber] [varchar](20) NULL,
	[AlternatePhoneNumber] [varchar](20) NULL,
	[EmailID_Tenant] [varchar](max) NULL,
	[Title_Tenant] [varchar](50) NULL,
	[MeterNo_Tenant] [varchar](50) NULL,
	[MeterReaderId] [int] NULL,
	[ReadingDate] [datetime] NULL,
	[Reason] [varchar](max) NULL,
	[DocumentName] [varchar](max) NULL,
	[Path] [varchar](50) NULL,
	[UDCId] [int] NULL,
	[UDCId_Value] [varchar](50) NULL,
	[Value] [varchar](150) NULL,
	[MGActTypeID] [int] NULL,
	[MGActTypeID_Value] [varchar](50) NULL,
	[MeterNo] [varchar](100) NULL,
	[MeterTypeId] [int] NULL,
	[MeterTypeId_Value] [varchar](50) NULL,
	[MeterCost] [decimal](18, 2) NULL,
	[OutStandingAmount_PaidMeterDetails] [decimal](18, 2) NULL,
	[MeterAssignedDate] [datetime] NULL,
	[CustomerTypeId] [int] NULL,
	[CustomerTypeId_Value] [varchar](50) NULL,
	[TariffClassID] [int] NULL,
	[TariffClassID_Value] [varchar](50) NULL,
	[RouteSequenceNumber] [bigint] NULL,
	[RouteSequenceNumber_Value] [varchar](50) NULL,
	[IsEmbassyCustomer] [bit] NULL,
	[EmbassyCode] [varchar](50) NULL,
	[PhaseId] [int] NULL,
	[PhaseId_Value] [varchar](50) NULL,
	[ReadCodeID] [int] NULL,
	[ReadCodeID_Value] [varchar](50) NULL,
	[SortOrder] [int] NULL,
	[IsBookNoChanged] [bit] NULL,
	[ClusterCategoryId] [int] NULL,
	[ClusterCategoryId_Value] [varchar](50) NULL,
	[BookNo] [varchar](20) NULL,
	[PoleID] [varchar](50) NULL,
	[MeterNumber] [varchar](50) NULL,
	[AccountTypeId] [int] NULL,
	[AccountTypeId_Value] [varchar](50) NULL,
	[IdentityTypeIdList] [varchar](max) NULL,
	[IdentityNumberList] [varchar](max) NULL,
	[UDFTypeIdList] [varchar](max) NULL,
	[UDFValueList] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[ApproveStatusId] [int] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[Remarks] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ARID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Customer_Additionalcharges]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Customer_Additionalcharges](
	[CustomerAdditioalCharges] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [varchar](50) NULL,
	[AccountNO] [varchar](50) NULL,
	[CustomerBillId] [int] NULL,
	[TariffId] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[ChargeId] [int] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBY] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerAdditioalCharges] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Countries]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Countries](
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [varchar](20) NOT NULL,
	[CountryName] [varchar](50) NULL,
	[CountryDetails] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[Currency] [varchar](50) NULL,
	[CurrencySymbol] [nvarchar](20) NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_CountryId] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ConsumptionDelivered]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ConsumptionDelivered](
	[ConsumptionDeliveredId] [int] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](50) NULL,
	[SU_ID] [varchar](50) NULL,
	[Capacity] [decimal](18, 0) NULL,
	[SUPPMConsumption] [decimal](18, 0) NULL,
	[SUCreditConsumption] [decimal](18, 0) NULL,
	[TotalConsumption] [decimal](18, 0) NULL,
	[ConsumptionMonth] [int] NULL,
	[ConsumptionYear] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ConsumptionDeliveredId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CheckMeterReadings]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CheckMeterReadings](
	[CheckMeterReadingId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[BillNo] [varchar](50) NULL,
	[MeterPreviousReading] [varchar](50) NULL,
	[MeterCurrentReading] [varchar](50) NULL,
	[CheckMeterPreviousReading] [varchar](50) NULL,
	[CheckMeterCurrentReading] [varchar](50) NULL,
	[BillDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CheckMeterReadingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CheckMeterDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CheckMeterDetails](
	[CheckMeterDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[CheckMeterNumber] [varchar](50) NULL,
	[CycleId] [varchar](20) NULL,
	[InitialReading] [varchar](50) NULL,
	[CheckMeterReadingId] [int] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CheckMeterDetailsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CashOffices]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CashOffices](
	[CashOfficeId] [int] IDENTITY(1,1) NOT NULL,
	[CashOffice] [varchar](300) NULL,
	[StateCode] [varchar](20) NULL,
	[DistrictCode] [varchar](20) NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[Details] [varchar](max) NULL,
	[ContactNo1] [varchar](20) NULL,
	[ContactNo2] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CashOfficeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Cashier]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Cashier](
	[CashierId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[ContactNumber] [varchar](20) NULL,
	[AlternateContactNumber] [varchar](20) NULL,
	[Address1] [varchar](max) NULL,
	[Address2] [varchar](max) NULL,
	[City] [varchar](50) NULL,
	[Zipcode] [varchar](50) NULL,
	[EmailId] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[CashOfficeId] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_BussinessUnitsEBllMails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_BussinessUnitsEBllMails](
	[BUEBMID] [int] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](20) NULL,
	[TO] [varchar](max) NULL,
	[CC] [varchar](max) NULL,
	[BCC] [varchar](max) NULL,
	[CreatedDate] [smalldatetime] NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[BUEBMID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BussinessUnits]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BussinessUnits](
	[IdentityBU_ID] [int] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](20) NOT NULL,
	[BusinessUnitName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[StateCode] [varchar](20) NULL,
	[BUCode] [varchar](10) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[ZIP] [varchar](100) NULL,
	[RegionId] [int] NULL,
	[PoleId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[BU_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BUNotice]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BUNotice](
	[BUNoticeID] [int] IDENTITY(1,1) NOT NULL,
	[Notice_ID] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[IsAllBuss] [bit] NULL,
	[Activestatus] [int] NULL,
	[CreatedBy] [varchar](20) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](20) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Tbl_BUNotice] PRIMARY KEY CLUSTERED 
(
	[BUNoticeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_BU_HeadOffice]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TBL_BU_HeadOffice](
	[HeadOfficeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NULL,
	[TO] [varchar](50) NULL,
	[CC] [varchar](50) NULL,
	[BCC] [varchar](50) NULL,
	[CreatedDate] [smalldatetime] NULL,
	[ActiveStatusID] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[HeadOfficeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BookNumbers]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BookNumbers](
	[IDENTITYBOOKID] [int] IDENTITY(1,1) NOT NULL,
	[BookNo] [varchar](20) NOT NULL,
	[ID] [varchar](20) NULL,
	[CycleId] [varchar](20) NULL,
	[NoOfAccounts] [int] NULL,
	[Details] [varchar](max) NULL,
	[BookCode] [varchar](10) NULL,
	[SortOrder] [int] NULL,
	[MarketerId] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[BookNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BookNoChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BookNoChangeLogs](
	[BookNoChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNo] [varchar](50) NULL,
	[OldBU_ID] [varchar](20) NULL,
	[NewBU_ID] [varchar](20) NULL,
	[OldSU_ID] [varchar](20) NULL,
	[NewSU_ID] [varchar](20) NULL,
	[OldSC_ID] [varchar](20) NULL,
	[NewSC_ID] [varchar](20) NULL,
	[OldCycleId] [varchar](20) NULL,
	[NewCycleId] [varchar](20) NULL,
	[OldBookNo] [varchar](20) NULL,
	[NewBookNo] [varchar](20) NULL,
	[OldPostal_HouseNo] [varchar](100) NULL,
	[OldPostal_StreetName] [varchar](255) NULL,
	[OldPostal_City] [varchar](100) NULL,
	[OldPostal_Landmark] [varchar](100) NULL,
	[OldPostal_AreaCode] [varchar](100) NULL,
	[OldPostal_ZipCode] [varchar](100) NULL,
	[NewPostal_HouseNo] [varchar](100) NULL,
	[NewPostal_StreetName] [varchar](255) NULL,
	[NewPostal_City] [varchar](100) NULL,
	[NewPostal_Landmark] [varchar](100) NULL,
	[NewPostal_AreaCode] [varchar](100) NULL,
	[NewPostal_ZipCode] [varchar](100) NULL,
	[OldService_HouseNo] [varchar](100) NULL,
	[OldService_StreetName] [varchar](255) NULL,
	[OldService_City] [varchar](100) NULL,
	[OldService_Landmark] [varchar](100) NULL,
	[OldService_AreaCode] [varchar](100) NULL,
	[OldService_ZipCode] [varchar](100) NULL,
	[NewService_HouseNo] [varchar](100) NULL,
	[NewService_StreetName] [varchar](255) NULL,
	[NewService_City] [varchar](100) NULL,
	[NewService_Landmark] [varchar](100) NULL,
	[NewService_AreaCode] [varchar](100) NULL,
	[NewService_ZipCode] [varchar](100) NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[OldServiceAddressID] [int] NULL,
	[NewServiceAddressID] [int] NULL,
	[OldPostalAddressID] [int] NULL,
	[NewPostalAddressID] [int] NULL,
	[OldIsSameAsService] [int] NULL,
	[NewIsSameAsService] [int] NULL,
	[IsPostalCommunication] [bit] NULL,
	[IsServiceComunication] [bit] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillUnlockRemarks]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillUnlockRemarks](
	[BillUnlockRemarkId] [int] IDENTITY(1,1) NOT NULL,
	[BillingQueueScheduleId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[BillUnlockRemarkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillingQueueSchedule]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillingQueueSchedule](
	[BillingQueueScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[BillingMonth] [int] NULL,
	[BillingYear] [int] NULL,
	[BillSendingMode] [int] NULL,
	[BillProcessTypeId] [int] NULL,
	[TotalCustomersInQueue] [int] NULL,
	[OpenStatusId] [int] NULL,
	[ServiceStartDate] [datetime] NULL,
	[CycleId] [varchar](20) NULL,
	[BillGenarationStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BillingFile] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillingQueueScheduleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillingQueeCustomers]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillingQueeCustomers](
	[BillQueueCustomerId] [int] IDENTITY(1,1) NOT NULL,
	[BillingQueuescheduleId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[Month] [int] NULL,
	[Year] [int] NULL,
	[BillGenarationStatusId] [int] NULL,
	[BillFile] [varchar](max) NULL,
 CONSTRAINT [PK_Tbl_BillingQueeCustomers] PRIMARY KEY CLUSTERED 
(
	[BillQueueCustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillingMonths]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillingMonths](
	[BillMonthID] [int] IDENTITY(1,1) NOT NULL,
	[Year] [int] NULL,
	[Month] [int] NULL,
	[OpenStatusId] [int] NULL,
	[Details] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[BillMonthID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillingDisabledBooks]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillingDisabledBooks](
	[DisabledBookId] [int] IDENTITY(1,1) NOT NULL,
	[BookNo] [varchar](20) NULL,
	[YearId] [int] NULL,
	[MonthId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[DisableTypeId] [int] NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[IsPartialBill] [bit] NULL,
	[ApproveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[DisableDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[DisabledBookId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillFixedChagesDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillFixedChagesDetails](
	[BillDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerBillId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[TariffId] [int] NULL,
	[Amount] [decimal](20, 4) NULL,
	[ActivestatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[BillDetailsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillEnergyChargesDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillEnergyChargesDetails](
	[BillDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerBillId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[EnergyFrom] [int] NULL,
	[EnergyTo] [int] NULL,
	[ChargePerKW] [decimal](20, 4) NULL,
	[TotalCharges] [decimal](20, 4) NULL,
	[TariffId] [int] NULL,
	[ActivestatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[BillDetailsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillDetails](
	[CustomerBillDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerBillID] [int] NULL,
	[CustomerFullName] [varchar](200) NULL,
	[BusinessUnitName] [varchar](200) NULL,
	[TariffName] [varchar](200) NULL,
	[ReadDate] [datetime] NULL,
	[Multiplier] [int] NULL,
	[Service_HouseNo] [varchar](max) NULL,
	[Service_Street] [varchar](max) NULL,
	[Service_City] [varchar](max) NULL,
	[ServiceZipCode] [varchar](50) NULL,
	[Postal_HouseNo] [varchar](max) NULL,
	[Postal_Street] [varchar](max) NULL,
	[Postal_City] [varchar](max) NULL,
	[Postal_ZipCode] [varchar](50) NULL,
	[Service_Landmark] [varchar](max) NULL,
	[Postal_LandMark] [varchar](max) NULL,
	[OldAccountNumber] [varchar](50) NULL,
	[BillFilePath] [varchar](max) NULL,
	[Notes] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillAdjustments]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillAdjustments](
	[BillAdjustmentId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerBillId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerId] [varchar](50) NULL,
	[Remarks] [varchar](max) NULL,
	[AmountEffected] [decimal](18, 4) NULL,
	[ApprovalStatusId] [int] NULL,
	[BillAdjustmentType] [int] NULL,
	[TaxEffected] [decimal](18, 2) NULL,
	[AdjustedUnits] [int] NULL,
	[TotalAmountEffected] [decimal](18, 2) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[ApprovedDate] [varchar](50) NULL,
	[EffectedBillId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[BatchNo] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Tbl_BillAdjustments] PRIMARY KEY CLUSTERED 
(
	[BillAdjustmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillAdjustmentDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillAdjustmentDetails](
	[BADID] [int] IDENTITY(1,1) NOT NULL,
	[BillAdjustmentId] [int] NULL,
	[PreviousReading] [varchar](50) NULL,
	[CurrentReadingAfterAdjustment] [varchar](50) NULL,
	[EnergryCharges] [decimal](20, 4) NULL,
	[AdditionalCharges] [decimal](20, 4) NULL,
	[AdditionalChargesID] [int] NULL,
	[CurrentReadingBeforeAdjustment] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Tbl_BillAdjustmentDetails] PRIMARY KEY CLUSTERED 
(
	[BADID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_BillAdjustmentApproval]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_BillAdjustmentApproval](
	[BAAID] [int] IDENTITY(1,1) NOT NULL,
	[BAID] [int] NULL,
	[Approval] [int] NULL,
	[Remark] [int] NULL,
	[ApprovalClosed] [bit] NULL,
 CONSTRAINT [PK_TBL_BillAdjustmentApproval] PRIMARY KEY CLUSTERED 
(
	[BAAID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_BatchVsAccountNo]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BatchVsAccountNo](
	[CustBatchId] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustBatchId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BatchPayment]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BatchPayment](
	[BatchPaymentId] [int] IDENTITY(1,1) NOT NULL,
	[BatchId] [int] NULL,
	[PaymentId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Tbl_BatchPayment] PRIMARY KEY CLUSTERED 
(
	[BatchPaymentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BatchDetailsApproval]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BatchDetailsApproval](
	[BatchID] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [int] NOT NULL,
	[BatchName] [varchar](50) NULL,
	[BatchDate] [datetime] NULL,
	[CashOffice] [int] NULL,
	[BatchReceivedDate] [datetime] NULL,
	[BatchTotal] [numeric](20, 4) NULL,
	[Description] [varchar](max) NULL,
	[BatchStatus] [int] NULL,
	[CashierID] [varchar](50) NULL,
	[Approval] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BatchDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BatchDetails](
	[BatchID] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [int] NOT NULL,
	[BatchName] [varchar](50) NULL,
	[BatchDate] [datetime] NULL,
	[CashOffice] [int] NULL,
	[BatchReceivedDate] [datetime] NULL,
	[BatchTotal] [numeric](20, 4) NULL,
	[Description] [varchar](max) NULL,
	[BatchStatus] [int] NULL,
	[CashierID] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Tbl_BatchDetails] ADD [BU_ID] [varchar](50) NULL
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Batch_CustBulkUpload]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Batch_CustBulkUpload](
	[UploadBatchId] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [int] NULL,
	[BatchDate] [datetime] NULL,
	[BatchName] [varchar](500) NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Tbl_Batch_CustBulkUpload] ADD [BU_ID] [varchar](50) NULL
ALTER TABLE [dbo].[Tbl_Batch_CustBulkUpload] ADD PRIMARY KEY CLUSTERED 
(
	[UploadBatchId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BATCH_ADJUSTMENT]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BATCH_ADJUSTMENT](
	[BatchID] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [int] NULL,
	[Reason] [varchar](100) NULL,
	[BatchTotal] [decimal](18, 2) NULL,
	[BatchDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Tbl_BATCH_ADJUSTMENT] ADD [BU_ID] [varchar](50) NULL
ALTER TABLE [dbo].[Tbl_BATCH_ADJUSTMENT] ADD [BillAdjustmentTypeId] [bigint] NULL
ALTER TABLE [dbo].[Tbl_BATCH_ADJUSTMENT] ADD [BatchStatusID] [int] NULL
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_EstimationSettings]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_EstimationSettings](
	[EstimationSettingId] [int] IDENTITY(1,1) NOT NULL,
	[BillingYear] [int] NULL,
	[BillingMonth] [int] NULL,
	[BillingRule] [int] NULL,
	[CycleId] [varchar](20) NULL,
	[TariffId] [int] NULL,
	[EnergytoCalculate] [int] NULL,
	[ClusterCategoryId] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EstimationSettingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_EstimatedEnergy]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_EstimatedEnergy](
	[EstimatedEnergyId] [int] IDENTITY(1,1) NOT NULL,
	[EnergyToEstimate] [int] NULL,
	[BillYear] [int] NULL,
	[BillMonth] [int] NULL,
	[TariffId] [int] NULL,
	[FeederId] [varchar](20) NULL,
	[IsActive] [bit] NULL,
	[CycleId] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EstimatedEnergyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_EstimatedCustomerRule]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_EstimatedCustomerRule](
	[EstCustomerRuleId] [int] IDENTITY(1,1) NOT NULL,
	[TariffID] [int] NULL,
	[BillingMonth] [int] NULL,
	[BillingYEar] [int] NULL,
	[RuleId] [int] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EstCustomerRuleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_EditListReportSummary]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_EditListReportSummary](
	[PageId] [int] IDENTITY(1,1) NOT NULL,
	[ReportCode] [varchar](50) NULL,
	[ReportName] [varchar](100) NULL,
	[PagePath] [varchar](500) NULL,
	[PageOrder] [int] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EBillMailLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EBillMailLogs](
	[EBillMailLogsId] [int] IDENTITY(1,1) NOT NULL,
	[BUEBMID] [int] NULL,
	[BU_TxtFile] [varchar](max) NULL,
	[BU_XlsFile] [varchar](max) NULL,
	[ConsolidatedFile] [varchar](max) NULL,
	[ConsolidatedDuplicateFile] [varchar](max) NULL,
	[CreatedDate] [smalldatetime] NULL,
	[IsMailSent] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[EBillMailLogsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_DistrictMessages]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_DistrictMessages](
	[DistrictMessageId] [int] IDENTITY(1,1) NOT NULL,
	[Message] [varchar](max) NULL,
	[DistrictCode] [varchar](20) NULL,
	[IsGlobalMessage] [bit] NULL,
	[BillingTypeId] [int] NULL,
	[GlobalMessageId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK__Tbl_Dist__4F012867538D5813] PRIMARY KEY CLUSTERED 
(
	[DistrictMessageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_DirectCustomersAvgReadings]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_DirectCustomersAvgReadings](
	[AvgReadingId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[AverageReading] [decimal](18, 2) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AvgReadingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaidMeterPaymentDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaidMeterPaymentDetails](
	[CapnyMeterPaymentsId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[MeterNo] [varchar](100) NULL,
	[Amount] [decimal](18, 2) NULL,
	[BillNo] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CapnyMeterPaymentsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaidMeterDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaidMeterDetails](
	[MeterId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[MeterNo] [varchar](100) NULL,
	[MeterTypeId] [int] NULL,
	[MeterCost] [decimal](18, 2) NULL,
	[OutStandingAmount] [decimal](18, 2) NULL,
	[ActiveStatusId] [int] NULL,
	[MeterAssignedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaidMeterAdjustmentDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaidMeterAdjustmentDetails](
	[MeterAdjustmentId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[MeterNo] [varchar](100) NULL,
	[AdjustedAmount] [decimal](18, 2) NULL,
	[ApprovalStatusId] [int] NULL,
	[Reason] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterAdjustmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Notifications]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Notifications](
	[NoticeID] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[FilePath] [varchar](50) NULL,
	[Activestatus] [int] NULL,
 CONSTRAINT [PK_Tbl_Notifications] PRIMARY KEY CLUSTERED 
(
	[NoticeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_NewCustomerLocator]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_NewCustomerLocator](
	[NewCustomerLocatorId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Primary_ContactNo] [varchar](20) NULL,
	[Secondary_ContactNo] [varchar](20) NULL,
	[CycleID] [varchar](20) NULL,
	[MarketerName] [varchar](20) NULL,
	[Note] [varchar](500) NULL,
	[Longitude] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
	[ApprovalStatus] [int] NULL,
	[CreatedDate] [smalldatetime] NULL,
	[CreatedBy] [varchar](20) NULL,
	[ModifiedDate] [smalldatetime] NULL,
	[ModifiedBy] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[NewCustomerLocatorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_MRoutes]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MRoutes](
	[RouteId] [int] IDENTITY(1,1) NOT NULL,
	[RouteName] [varchar](500) NULL,
	[AvgMaxLimit] [numeric](20, 4) NULL,
	[ActiveStatusId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[Details] [varchar](max) NULL,
	[RouteCode] [varchar](10) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK__Tbl_MRou__80979B4D7F6BDA51] PRIMARY KEY CLUSTERED 
(
	[RouteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_MeterInformation]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MeterInformation](
	[MeterId] [int] IDENTITY(1,1) NOT NULL,
	[MeterType] [int] NULL,
	[MeterSize] [varchar](50) NULL,
	[MeterDetails] [varchar](50) NULL,
	[MeterMultiplier] [varchar](50) NULL,
	[MeterBrand] [varchar](50) NULL,
	[MeterStatus] [int] NULL,
	[MeterSerialNo] [varchar](50) NULL,
	[MeterRating] [varchar](50) NULL,
	[NextCalibrationDate] [datetime] NULL,
	[ServiceHoursBeforeCalibration] [varchar](50) NULL,
	[MeterDials] [int] NULL,
	[Decimals] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[MeterNo] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsCAPMIMeter] [bit] NULL,
	[CAPMIAmount] [decimal](18, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Marketers]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Marketers](
	[MarketerId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[ContactNumber] [varchar](20) NULL,
	[AlternateContactNumber] [varchar](20) NULL,
	[Address1] [varchar](max) NULL,
	[Address2] [varchar](max) NULL,
	[City] [varchar](50) NULL,
	[Zipcode] [varchar](50) NULL,
	[EmailId] [varchar](max) NULL,
	[Photo] [varchar](max) NULL,
	[BU_ID] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_LEnergyClassCharges]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_LEnergyClassCharges](
	[EnergyClassChargeID] [int] IDENTITY(1,1) NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[FromKW] [int] NULL,
	[ToKW] [int] NULL,
	[FromDate] [datetime] NULL,
	[Todate] [datetime] NULL,
	[ClassID] [int] NULL,
	[IsActive] [bit] NULL,
	[TaxId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EnergyClassChargeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_LCustomerTariffChangeRequest]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_LCustomerTariffChangeRequest](
	[TariffChangeRequestId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[PreviousTariffId] [int] NULL,
	[ChangeRequestedTariffId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[OldClusterCategoryId] [int] NULL,
	[NewClusterCategoryId] [int] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[TariffChangeRequestId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_LastBillGeneratedDate]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_LastBillGeneratedDate](
	[LastBillGeneratedDate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_LAdditionalClassCharges]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_LAdditionalClassCharges](
	[AdditionalChargeID] [int] IDENTITY(1,1) NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[ClassID] [int] NULL,
	[IsActive] [bit] NULL,
	[ChargeId] [int] NULL,
	[TaxId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AdditionalChargeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_HHInputFiles]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_HHInputFiles](
	[HHInputFileID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](100) NULL,
	[CycleID] [varchar](50) NULL,
	[Month] [int] NULL,
	[Year] [int] NULL,
	[Notes] [varchar](250) NULL,
	[ActiveStatus] [int] NULL,
	[CreatedBy] [varchar](20) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](20) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Tbl_HHInputFiles] PRIMARY KEY CLUSTERED 
(
	[HHInputFileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_GovtCustomers]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_GovtCustomers](
	[GlobalAccountNumber] [varchar](50) NULL,
	[MGActTypeID] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_GlobalMessages]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_GlobalMessages](
	[GlobalMessageId] [int] IDENTITY(1,1) NOT NULL,
	[Message] [varchar](max) NULL,
	[StateCode] [varchar](20) NULL,
	[BillingTypeId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[GlobalMessageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerDocuments]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerDocuments](
	[CustomerDocumentId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNo] [varchar](10) NULL,
	[DocumentName] [varchar](max) NULL,
	[Path] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerDocumentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerDetails](
	[CustomerUniqueId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerUniqueNo] [varchar](50) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[Title] [varchar](10) NULL,
	[Name] [varchar](100) NULL,
	[SurName] [varchar](100) NULL,
	[KnownAs] [varchar](100) NULL,
	[EmailId] [varchar](max) NULL,
	[DocumentNo] [varchar](100) NULL,
	[EmployeeCode] [varchar](100) NULL,
	[HomeContactNo] [varchar](20) NULL,
	[BusinessContactNo] [varchar](20) NULL,
	[OtherContactNo] [varchar](20) NULL,
	[PostalLandMark] [varchar](200) NULL,
	[PostalStreet] [varchar](200) NULL,
	[PostalCity] [varchar](200) NULL,
	[PostalHouseNo] [varchar](200) NULL,
	[PostalDetails] [varchar](max) NULL,
	[PostalZipCode] [varchar](50) NULL,
	[IdentityNo] [varchar](100) NULL,
	[IdentityId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[BookNo] [varchar](20) NULL,
	[InjectionSubStationId] [varchar](20) NULL,
	[FeederId] [varchar](20) NULL,
	[TransformerId] [varchar](20) NULL,
	[PoleId] [varchar](20) NULL,
	[ServiceLandMark] [varchar](200) NULL,
	[ServiceStreet] [varchar](200) NULL,
	[ServiceCity] [varchar](200) NULL,
	[ServiceHouseNo] [varchar](200) NULL,
	[ServiceDetails] [varchar](max) NULL,
	[ServiceZipCode] [varchar](50) NULL,
	[ServiceEmailId] [varchar](max) NULL,
	[MobileNo] [varchar](20) NULL,
	[RouteSequenceNo] [int] NULL,
	[NeighborAccountNo] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
	[Longitude] [varchar](50) NULL,
	[TariffId] [int] NULL,
	[MultiplicationFactor] [decimal](18, 4) NULL,
	[AccountTypeId] [int] NULL,
	[ApplicationDate] [datetime] NULL,
	[ReadCodeId] [int] NULL,
	[OrganizationCode] [varchar](50) NULL,
	[CustomerTypeId] [int] NULL,
	[OldAccountNo] [varchar](50) NULL,
	[MeterTypeId] [int] NULL,
	[PhaseId] [int] NULL,
	[IsEmbassyCustomer] [bit] NULL,
	[EmbassyCode] [varchar](50) NULL,
	[IsVIPCustomer] [bit] NULL,
	[MeterNo] [varchar](100) NULL,
	[MeterSerialNo] [varchar](100) NULL,
	[ConnectionDate] [datetime] NULL,
	[MeterStatusId] [int] NULL,
	[ConnectionReasonId] [int] NULL,
	[InitialReading] [varchar](max) NULL,
	[CurrentReading] [varchar](max) NULL,
	[AverageReading] [varchar](max) NULL,
	[SetupDate] [datetime] NULL,
	[CertifiedBy] [varchar](100) NULL,
	[ProccessedById] [int] NULL,
	[InstalledBy] [varchar](100) NULL,
	[AgencyId] [int] NULL,
	[ContactPersonName] [varchar](100) NULL,
	[AgencyDetails] [varchar](max) NULL,
	[RouteNo] [int] NULL,
	[CustomerSequenceNo] [bigint] NULL,
	[StateCode] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[MinimumReading] [varchar](max) NULL,
	[Highestconsumption] [varchar](15) NULL,
	[SortOrder] [int] NULL,
	[OutStandingAmount] [decimal](18, 2) NULL,
	[IsBookNoChanged] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerUniqueNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UX_Constraint_AccountNo] UNIQUE NONCLUSTERED 
(
	[AccountNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerContactInfoChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerContactInfoChangeLogs](
	[CustomerContactInfoChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[OldEmailId] [varchar](100) NULL,
	[NewEmailId] [varchar](100) NULL,
	[OldHomeContactNumber] [varchar](20) NULL,
	[NewHomeContactNumber] [varchar](20) NULL,
	[OldBusinessContactNumber] [varchar](20) NULL,
	[NewBusinessContactNumber] [varchar](20) NULL,
	[OldOtherContactNumber] [varchar](20) NULL,
	[NewOtherContactNumber] [varchar](20) NULL,
	[ApprovalStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[Remarks] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerContactInfoChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerBills]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerBills](
	[CustomerBillId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerId] [varchar](50) NULL,
	[TotalBillAmount] [decimal](20, 4) NULL,
	[ServiceAddress] [varchar](max) NULL,
	[MeterNo] [varchar](50) NULL,
	[Dials] [int] NULL,
	[NetArrears] [decimal](20, 4) NULL,
	[NetEnergyCharges] [decimal](20, 4) NULL,
	[NetFixedCharges] [decimal](20, 4) NULL,
	[VAT] [decimal](20, 4) NULL,
	[VATPercentage] [decimal](20, 4) NULL,
	[Messages] [varchar](max) NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[PoleId] [varchar](20) NULL,
	[BillGeneratedBy] [varchar](50) NULL,
	[BillGeneratedDate] [datetime] NULL,
	[PaymentLastDate] [datetime] NULL,
	[TariffId] [int] NULL,
	[BillYear] [int] NULL,
	[BillMonth] [int] NULL,
	[CycleId] [varchar](20) NULL,
	[TotalBillAmountWithArrears] [decimal](20, 4) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BillNo] [varchar](50) NULL,
	[PaymentStatusID] [int] NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Usage] [decimal](20, 4) NULL,
	[AverageReading] [decimal](20, 4) NULL,
	[TotalBillAmountWithTax] [decimal](20, 4) NULL,
	[EstimatedUsage] [int] NULL,
	[ReadCodeId] [int] NULL,
	[ReadType] [int] NULL,
	[AdjustmentAmmount] [decimal](18, 2) NULL,
	[Remarks] [varchar](max) NULL,
	[BalanceUsage] [int] NULL,
	[BillingTypeId] [int] NULL,
	[ActualUsage] [decimal](18, 2) NULL,
	[LastPaymentTotal] [decimal](18, 2) NULL,
	[PreviousBalance] [decimal](18, 2) NULL,
	[PaidAmount] [decimal](18, 2) NULL,
	[TariffRates] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerBillId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerBillPDFFiles]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerBillPDFFiles](
	[CustomerBillPDFId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNo] [varchar](50) NULL,
	[CustomerBillId] [int] NULL,
	[FilePath] [varchar](max) NULL,
	[BillNo] [varchar](200) NULL,
	[BillMonthId] [int] NULL,
	[BillYearId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[EmailId] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerBillPDFId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerBillPaymentsApproval]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerBillPaymentsApproval](
	[CustomerBillPaymentId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerPaymentId] [int] NULL,
	[PaidAmount] [decimal](20, 4) NULL,
	[BillNo] [varchar](50) NULL,
	[Approval] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerBillPayments]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerBillPayments](
	[CustomerBillPaymentId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerPaymentId] [int] NULL,
	[PaidAmount] [decimal](20, 4) NULL,
	[BillNo] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[CustomerBillId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerBillPaymentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerBillFixedCharges]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerBillFixedCharges](
	[CustomerBillFixedChargeId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerBillId] [int] NULL,
	[FixedChargeId] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[BillNo] [varchar](50) NULL,
	[BillMonth] [int] NULL,
	[BillYear] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerBillFixedChargeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerBillEnergyCharges]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tbl_CustomerBillEnergyCharges](
	[CusotmerNewBillID] [bigint] IDENTITY(1,1) NOT NULL,
	[FromKW] [int] NULL,
	[ToKW] [int] NULL,
	[Amount] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_CustomerAddressChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerAddressChangeLogs](
	[AddressChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[OldHouseNo] [varchar](200) NULL,
	[NewHouseNo] [varchar](200) NULL,
	[OldStreetName] [varchar](255) NULL,
	[NewStreetName] [varchar](255) NULL,
	[OldCity] [varchar](50) NULL,
	[NewCity] [varchar](50) NULL,
	[OldLandmark] [varchar](50) NULL,
	[NewLandmark] [varchar](50) NULL,
	[OldAreaCode] [varchar](50) NULL,
	[NewAreaCode] [varchar](50) NULL,
	[OldZipCode] [varchar](50) NULL,
	[NewZipCode] [varchar](50) NULL,
	[OldServiceAddressID] [int] NULL,
	[NewServiceAddressID] [int] NULL,
	[OldPostalAddressID] [int] NULL,
	[NewPostalAddressID] [int] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[IsServiceAddress] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AddressChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerAddressChangeLog_New]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerAddressChangeLog_New](
	[AddressChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[OldPostal_HouseNo] [varchar](100) NULL,
	[OldPostal_StreetName] [varchar](255) NULL,
	[OldPostal_City] [varchar](100) NULL,
	[OldPostal_Landmark] [varchar](100) NULL,
	[OldPostal_AreaCode] [varchar](100) NULL,
	[OldPostal_ZipCode] [varchar](100) NULL,
	[NewPostal_HouseNo] [varchar](100) NULL,
	[NewPostal_StreetName] [varchar](255) NULL,
	[NewPostal_City] [varchar](100) NULL,
	[NewPostal_Landmark] [varchar](100) NULL,
	[NewPostal_AreaCode] [varchar](100) NULL,
	[NewPostal_ZipCode] [varchar](100) NULL,
	[OldService_HouseNo] [varchar](100) NULL,
	[OldService_StreetName] [varchar](255) NULL,
	[OldService_City] [varchar](100) NULL,
	[OldService_Landmark] [varchar](100) NULL,
	[OldService_AreaCode] [varchar](100) NULL,
	[OldService_ZipCode] [varchar](100) NULL,
	[NewService_HouseNo] [varchar](100) NULL,
	[NewService_StreetName] [varchar](255) NULL,
	[NewService_City] [varchar](100) NULL,
	[NewService_Landmark] [varchar](100) NULL,
	[NewService_AreaCode] [varchar](100) NULL,
	[NewService_ZipCode] [varchar](100) NULL,
	[ApprovalStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[OldServiceAddressID] [int] NULL,
	[NewServiceAddressID] [int] NULL,
	[OldPostalAddressID] [int] NULL,
	[NewPostalAddressID] [int] NULL,
	[OldIsSameAsService] [int] NULL,
	[NewIsSameAsService] [int] NULL,
	[IsPostalCommunication] [bit] NULL,
	[IsServiceComunication] [bit] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[AddressChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerActiveStatusChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerActiveStatusChangeLogs](
	[ActiveStatusChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[OldStatus] [int] NULL,
	[NewStatus] [int] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[ChangeDate] [datetime] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ActiveStatusChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerPaymentsApproval]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerPaymentsApproval](
	[CustomerPaymentID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [varchar](50) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[ReceiptNo] [varchar](20) NULL,
	[PaymentMode] [int] NULL,
	[DocumentPath] [varchar](max) NULL,
	[Cashier] [varchar](50) NULL,
	[CashOffice] [int] NULL,
	[DocumentName] [varchar](max) NULL,
	[RecievedDate] [datetime] NULL,
	[PaymentType] [int] NULL,
	[PaidAmount] [decimal](18, 2) NULL,
	[BatchNo] [int] NULL,
	[ActivestatusId] [int] NULL,
	[Approval] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerPayments]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerPayments](
	[CustomerPaymentID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [varchar](50) NULL,
	[AccountNo] [varchar](50) NULL,
	[ReceiptNo] [varchar](20) NULL,
	[PaymentMode] [int] NULL,
	[DocumentPath] [varchar](max) NULL,
	[Cashier] [varchar](50) NULL,
	[CashOffice] [int] NULL,
	[DocumentName] [varchar](max) NULL,
	[RecievedDate] [datetime] NULL,
	[PaymentType] [int] NULL,
	[PaidAmount] [decimal](18, 2) NULL,
	[BatchNo] [int] NULL,
	[ActivestatusId] [int] NULL,
	[ReceivedDevice] [int] NULL,
	[Remarks] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Tbl_CustomerPayments] PRIMARY KEY CLUSTERED 
(
	[CustomerPaymentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerNameChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerNameChangeLogs](
	[NameChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[OldTitle] [varchar](10) NULL,
	[NewTitle] [varchar](10) NULL,
	[OldFirstName] [varchar](50) NULL,
	[NewFirstName] [varchar](50) NULL,
	[OldMiddleName] [varchar](50) NULL,
	[NewMiddleName] [varchar](50) NULL,
	[OldLastName] [varchar](50) NULL,
	[NewLastName] [varchar](50) NULL,
	[OldKnownAs] [varchar](150) NULL,
	[NewKnownAs] [varchar](150) NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[NameChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerMeterInfoChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerMeterInfoChangeLogs](
	[MeterInfoChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[OldMeterNo] [varchar](100) NULL,
	[NewMeterNo] [varchar](100) NULL,
	[OldMeterTypeId] [int] NULL,
	[NewMeterTypeId] [int] NULL,
	[OldDials] [int] NULL,
	[NewDials] [int] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[OldMeterReading] [varchar](20) NULL,
	[NewMeterReading] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[MeterChangedDate] [datetime] NULL,
	[InitialBillingkWh] [int] NULL,
	[NewMeterInitialReading] [varchar](20) NULL,
	[NewMeterReadingDate] [datetime] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
	[IsCAPMIMeter] [bit] NULL,
	[CAPMIAmount] [decimal](18, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterInfoChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerReadingsLog]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerReadingsLog](
	[ReadingLogId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNumber] [varchar](50) NULL,
	[LatestReadDate] [datetime] NULL,
	[ReadBy] [varchar](50) NULL,
	[PreviousReading] [numeric](20, 6) NULL,
	[PresentReading] [numeric](20, 6) NULL,
	[ReadType] [int] NULL,
	[IsBilled] [bit] NULL,
	[BilledBy] [varchar](50) NULL,
	[BilledDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerReadings]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerReadings](
	[CustomerReadingId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[AccountNumber] [varchar](50) NULL,
	[ReadDate] [datetime] NULL,
	[ReadBy] [varchar](50) NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Usage] [numeric](20, 2) NULL,
	[TotalReadingEnergies] [decimal](18, 2) NULL,
	[TotalReadings] [int] NULL,
	[Multiplier] [int] NULL,
	[ReadType] [int] NULL,
	[IsBilled] [bit] NULL,
	[BilledBy] [varchar](50) NULL,
	[BilledDate] [varchar](50) NULL,
	[AverageReading] [varchar](50) NULL,
	[BillNo] [varchar](50) NULL,
	[UsageForBilling] [decimal](18, 2) NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[IsTamper] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[MeterNumber] [varchar](50) NULL,
	[MeterReadingFrom] [int] NULL,
	[IsRollOver] [bit] NULL,
	[IsReadingWrong] [bit] NULL,
 CONSTRAINT [PK__Tbl_Cust__BF1C43EB033C6B35] PRIMARY KEY CLUSTERED 
(
	[CustomerReadingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerReadingApprovalLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerReadingApprovalLogs](
	[CustomerReadingLogId] [int] IDENTITY(1,1) NOT NULL,
	[ReadDate] [datetime] NULL,
	[ReadBy] [varchar](50) NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Usage] [numeric](18, 0) NULL,
	[TotalReadingEnergies] [decimal](18, 0) NULL,
	[TotalReadings] [int] NULL,
	[Multiplier] [int] NULL,
	[ReadType] [int] NULL,
	[AverageReading] [varchar](50) NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[IsTamper] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[MeterNumber] [varchar](50) NULL,
	[MeterReadingFrom] [int] NULL,
	[IsRollOver] [bit] NULL,
	[ApproveStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerReadingLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerTypeChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerTypeChangeLogs](
	[CustomerTypeChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[OldCustomerTypeId] [int] NULL,
	[NewCustomerTypeId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[ApprovalStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerTypeChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_DirectCustomerEnergyDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_DirectCustomerEnergyDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[AccountNo] [varchar](50) NULL,
	[EnergyCharges] [int] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_DirectCustomerEnergyCharges]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_DirectCustomerEnergyCharges](
	[DirectCustEnergyID] [int] IDENTITY(1,1) NOT NULL,
	[TariffID] [int] NULL,
	[EnergyInKW] [int] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[DirectCustEnergyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ReportBillingStatisticsByTariffId]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ReportBillingStatisticsByTariffId](
	[ReportBillingStatisticsByTariffId] [bigint] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](50) NULL,
	[BusinessUnitName] [varchar](100) NULL,
	[TariffId] [varchar](50) NULL,
	[TariffName] [varchar](100) NULL,
	[YearId] [int] NULL,
	[MonthId] [int] NULL,
	[ActiveCustomersCount] [bigint] NULL,
	[InActiveCustomersCount] [bigint] NULL,
	[HoldCustomersCount] [bigint] NULL,
	[TotalPopulationCount] [bigint] NULL,
	[NoOfBilledCustomers] [bigint] NULL,
	[EnergyDelivered] [decimal](20, 4) NULL,
	[FixedCharges] [decimal](20, 4) NULL,
	[TotalAmountBilled] [decimal](20, 4) NULL,
	[MINFCCustomersCount] [bigint] NULL,
	[ReadCustomersCount] [bigint] NULL,
	[ESTCustomersCount] [bigint] NULL,
	[DirectCustomersCount] [bigint] NULL,
	[EnergyBilled] [decimal](20, 4) NULL,
	[RevenueBilled] [decimal](20, 4) NULL,
	[Payments] [decimal](20, 4) NULL,
	[OpeningBalance] [decimal](20, 4) NULL,
	[ClosingBalance] [decimal](20, 4) NULL,
	[RevenueCollected] [decimal](20, 4) NULL,
	[KVASold] [bigint] NULL,
	[TotalAmountCollected] [decimal](20, 4) NULL,
	[NoOfStubs] [bigint] NULL,
	[WeightedAvg] [decimal](20, 4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[AdjustmentAmount] [decimal](18, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ReportBillingStatisticsByTariffId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ReportBillingStatisticsBySCId]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ReportBillingStatisticsBySCId](
	[ReportBillingStatisticsBySCId] [bigint] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](50) NULL,
	[BusinessUnitName] [varchar](100) NULL,
	[SU_ID] [varchar](50) NULL,
	[ServiceUnitName] [varchar](100) NULL,
	[SC_ID] [varchar](50) NULL,
	[ServiceCenterName] [varchar](100) NULL,
	[CustomerTypeId] [int] NULL,
	[CustomerType] [varchar](100) NULL,
	[YearId] [int] NULL,
	[MonthId] [int] NULL,
	[ActiveCustomersCount] [bigint] NULL,
	[InActiveCustomersCount] [bigint] NULL,
	[HoldCustomersCount] [bigint] NULL,
	[TotalPopulationCount] [bigint] NULL,
	[EnergyDelivered] [decimal](20, 4) NULL,
	[FixedCharges] [decimal](20, 4) NULL,
	[NoOfBilledCustomers] [bigint] NULL,
	[TotalAmountBilled] [decimal](20, 4) NULL,
	[MINFCCustomersCount] [bigint] NULL,
	[ReadCustomersCount] [bigint] NULL,
	[ESTCustomersCount] [bigint] NULL,
	[DirectCustomersCount] [bigint] NULL,
	[EnergyBilled] [decimal](20, 4) NULL,
	[RevenueBilled] [decimal](20, 4) NULL,
	[Payments] [decimal](20, 4) NULL,
	[OpeningBalance] [decimal](20, 4) NULL,
	[ClosingBalance] [decimal](20, 4) NULL,
	[RevenueCollected] [decimal](20, 4) NULL,
	[KVASold] [bigint] NULL,
	[TotalAmountCollected] [decimal](20, 4) NULL,
	[NoOfStubs] [bigint] NULL,
	[WeightedAvg] [decimal](20, 4) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[AdjustmentAmount] [decimal](18, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ReportBillingStatisticsBySCId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ReadToDirectCustomerActivityLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ReadToDirectCustomerActivityLogs](
	[ReadToDirectId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNo] [varchar](50) NULL,
	[MeterNo] [varchar](50) NULL,
	[Remarks] [varchar](max) NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ReadToDirectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ReadToDirectCustomerActivity]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ReadToDirectCustomerActivity](
	[RDID] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNo] [varchar](50) NULL,
	[MeterNumber] [varchar](50) NULL,
	[IsLatest] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[RDID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PresentReadingAdjustmentLog]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PresentReadingAdjustmentLog](
	[GlobalAccountNumber] [varchar](50) NULL,
	[Name] [varchar](max) NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[NewPresentReading] [varchar](50) NULL,
	[PreviousReadDate] [varchar](50) NULL,
	[PresentReadDate] [varchar](50) NULL,
	[MeterNumber] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PresentReadingId] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PresentReadingAdjustmentApprovalLog]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PresentReadingAdjustmentApprovalLog](
	[PresentReadingAdjustmentLogId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[NewPresentReading] [varchar](50) NULL,
	[PreviousReadDate] [datetime] NULL,
	[PresentReadDate] [datetime] NULL,
	[MeterNumber] [varchar](50) NULL,
	[Remarks] [varchar](max) NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[CurrentAppovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[PresentReadingAdjustmentLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PoleDescriptionTable]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PoleDescriptionTable](
	[PoleMasterId] [int] NULL,
	[Name] [varchar](150) NULL,
	[Description] [varchar](150) NULL,
	[Code] [varchar](50) NULL,
	[Comments] [varchar](150) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PoleId] [int] IDENTITY(1,1) NOT NULL,
	[PoleOrder] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserSpecialPermissions]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserSpecialPermissions](
	[UserPermissionId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](50) NULL,
	[BudgetSegmentWiseMargin] [bit] NULL,
	[CustomerDetailsModification] [bit] NULL,
	[BillAdjustment] [bit] NULL,
	[TariffAdjustments] [bit] NULL,
	[PaymentsAdjustments] [bit] NULL,
	[Disconnection] [bit] NULL,
	[ReConnections] [bit] NULL,
	[BillGeneration] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserPermissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserServiceUnits]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserServiceUnits](
	[UserServiceUnitId] [int] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserServiceUnitId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserServiceCenters]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserServiceCenters](
	[UserServiceCenterId] [int] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserServiceCenterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserRoles]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserRoles](
	[UserRoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserDocuments]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserDocuments](
	[UserDocumentId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](50) NULL,
	[DocumentName] [varchar](max) NULL,
	[Path] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserDocumentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserCashOffices]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserCashOffices](
	[UserCashOfficeId] [int] IDENTITY(1,1) NOT NULL,
	[CashOfficeId] [int] NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserCashOfficeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserBusinessUnits]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserBusinessUnits](
	[UserBusinessUnitId] [int] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserBusinessUnitId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_TariffRequestMessages]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_TariffRequestMessages](
	[MessageId] [int] IDENTITY(1,1) NOT NULL,
	[Message] [varchar](max) NULL,
	[TariffChangeRequestId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MessageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_TamperedCustomers]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_TamperedCustomers](
	[TamperedId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](20) NULL,
	[MeterNo] [varchar](50) NULL,
	[MeterReaderId] [int] NULL,
	[ReadingDate] [datetime] NULL,
	[Reason] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[TamperedId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_States]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_States](
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[StateCode] [varchar](20) NOT NULL,
	[StateName] [varchar](50) NULL,
	[DisplayCode] [varchar](20) NULL,
	[CountryCode] [varchar](20) NULL,
	[StateDetails] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[LGAId] [int] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[RegionId] [int] NULL,
 CONSTRAINT [PK_StateId] PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ServiceUnits]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ServiceUnits](
	[IdentitySU_ID] [int] IDENTITY(1,1) NOT NULL,
	[SU_ID] [varchar](20) NOT NULL,
	[ServiceUnitName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[BU_ID] [varchar](20) NULL,
	[SUCode] [varchar](10) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[ZIP] [varchar](100) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[SU_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ServiceCenter]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ServiceCenter](
	[IdentitySC_ID] [int] IDENTITY(1,1) NOT NULL,
	[ServiceCenterId] [varchar](20) NOT NULL,
	[ServiceCenterName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[SU_ID] [varchar](20) NULL,
	[SCCode] [varchar](10) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[ZIP] [varchar](100) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ServiceCenterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ReadingUploadBatches]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ReadingUploadBatches](
	[RUploadBatchId] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [varchar](12) NULL,
	[BatchDate] [datetime] NULL,
	[TotalCustomers] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastModifyDate] [datetime] NULL,
	[LastModifyBy] [varchar](50) NULL,
	[Notes] [varchar](200) NULL,
	[IsClosedBatch] [bit] NULL,
	[BU_ID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[RUploadBatchId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaymentUploadBatches]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaymentUploadBatches](
	[PUploadBatchId] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [varchar](12) NULL,
	[BatchDate] [datetime] NULL,
	[TotalCustomers] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastModifyDate] [datetime] NULL,
	[LastModifyBy] [varchar](50) NULL,
	[Notes] [varchar](200) NULL,
	[IsClosedBatch] [bit] NULL,
	[BU_ID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[PUploadBatchId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Cycles]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Cycles](
	[IdentityCycleId] [int] IDENTITY(1,1) NOT NULL,
	[CycleId] [varchar](20) NOT NULL,
	[CycleName] [varchar](300) NULL,
	[DetailsOfCycle] [varchar](max) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[ContactName] [varchar](200) NULL,
	[ContactNo] [varchar](20) NULL,
	[CycleCode] [varchar](10) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CycleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomerTenentDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomerTenentDetails](
	[TenentId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](100) NULL,
	[MiddleName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[PhoneNumber] [varchar](20) NULL,
	[AlternatePhoneNumber] [varchar](20) NULL,
	[EmailID] [varchar](max) NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[Title] [varchar](15) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[TenentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomersDetail]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomersDetail](
	[CustomerId] [bigint] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[Title] [varchar](10) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[KnownAs] [varchar](150) NULL,
	[EmailId] [varchar](max) NULL,
	[HomeContactNumber] [varchar](20) NULL,
	[BusinessContactNumber] [varchar](20) NULL,
	[OtherContactNumber] [varchar](20) NULL,
	[IsSameAsService] [bit] NULL,
	[ServiceAddressID] [int] NULL,
	[PostalAddressID] [int] NULL,
	[IsSameAsTenent] [bit] NULL,
	[DocumentNo] [varchar](50) NULL,
	[ApplicationDate] [datetime] NULL,
	[ConnectionDate] [datetime] NULL,
	[SetupDate] [datetime] NULL,
	[IsBEDCEmployee] [bit] NULL,
	[EmployeeCode] [int] NULL,
	[IsVIPCustomer] [bit] NULL,
	[TenentId] [int] NULL,
	[OldAccountNo] [varchar](20) NULL,
	[OrganizationCode] [varchar](20) NULL,
	[ConnectionReasonId] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[Service_HouseNo] [varchar](100) NULL,
	[Service_StreetName] [varchar](255) NULL,
	[Service_City] [varchar](100) NULL,
	[Service_Landmark] [varchar](100) NULL,
	[Service_AreaCode] [varchar](100) NULL,
	[Service_ZipCode] [varchar](100) NULL,
	[Postal_HouseNo] [varchar](100) NULL,
	[Postal_StreetName] [varchar](255) NULL,
	[Postal_City] [varchar](100) NULL,
	[Postal_Landmark] [varchar](100) NULL,
	[Postal_AreaCode] [varchar](100) NULL,
	[Postal_ZipCode] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[GlobalAccountNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomerProceduralDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomerProceduralDetails](
	[CustomerProcedureId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[CustomerTypeId] [int] NULL,
	[TariffClassID] [int] NULL,
	[RouteSequenceNumber] [bigint] NULL,
	[IsEmbassyCustomer] [bit] NULL,
	[EmbassyCode] [varchar](50) NULL,
	[PhaseId] [int] NULL,
	[ReadCodeID] [int] NULL,
	[SortOrder] [int] NULL,
	[IsBookNoChanged] [bit] NULL,
	[ClusterCategoryId] [int] NULL,
	[BookNo] [varchar](20) NULL,
	[PoleID] [varchar](50) NULL,
	[MeterNumber] [varchar](50) NULL,
	[AccountTypeId] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerProcedureId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails](
	[HouseNo] [varchar](200) NULL,
	[StreetName] [varchar](255) NULL,
	[City] [varchar](200) NULL,
	[Landmark] [varchar](200) NULL,
	[Details] [varchar](200) NULL,
	[AreaCode] [int] NULL,
	[Latitude] [varchar](50) NULL,
	[Longitude] [varchar](50) NULL,
	[IsServiceAddress] [bit] NULL,
	[ZipCode] [varchar](50) NULL,
	[AddressID] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[IsCommunication] [bit] NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AddressID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomerIdentityDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails](
	[IdentityId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[IdentityNumber] [varchar](150) NULL,
	[IdentityTypeId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdentityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomerActiveDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomerActiveDetails](
	[GlobalAccountNumber] [varchar](50) NULL,
	[CustomerActiveId] [int] IDENTITY(1,1) NOT NULL,
	[MeterAmount] [decimal](18, 2) NULL,
	[InitialBillingKWh] [bigint] NULL,
	[InitialReading] [bigint] NULL,
	[PresentReading] [bigint] NULL,
	[Highestconsumption] [decimal](18, 2) NULL,
	[OutStandingAmount] [decimal](18, 2) NULL,
	[AvgReading] [bigint] NULL,
	[IsCAPMI] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[OpeningBalance] [decimal](18, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerActiveId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_AverageUploadBatches]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_AverageUploadBatches](
	[AvgUploadBatchId] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [varchar](12) NULL,
	[BatchDate] [datetime] NULL,
	[TotalCustomers] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastModifyDate] [datetime] NULL,
	[LastModifyBy] [varchar](50) NULL,
	[Notes] [varchar](200) NULL,
	[IsClosedBatch] [bit] NULL,
	[BU_ID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[AvgUploadBatchId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_ApplicationProcessDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_ApplicationProcessDetails](
	[GlobalAccountNumber] [varchar](50) NULL,
	[Bedcinfoid] [int] IDENTITY(1,1) NOT NULL,
	[CertifiedBy] [varchar](50) NULL,
	[Seal1] [varchar](50) NULL,
	[Seal2] [varchar](50) NULL,
	[ApplicationProcessedBy] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Bedcinfoid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Agencies]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Agencies](
	[AgencyId] [int] IDENTITY(1,1) NOT NULL,
	[AgencyName] [varchar](300) NULL,
	[Details] [varchar](max) NULL,
	[ContactNo1] [varchar](20) NULL,
	[ContactNo2] [varchar](20) NULL,
	[ContactPersonName] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AgencyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_FunctionApprovalRole]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FunctionApprovalRole](
	[ApprovalRoleId] [int] IDENTITY(1,1) NOT NULL,
	[FunctionId] [int] NULL,
	[RoleId] [int] NULL,
	[Level] [int] NULL,
	[IsFinalApproval] [bit] NULL,
	[UserIds] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BU_ID] [varchar](20) NOT NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ApprovalRoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_MRegion]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MRegion](
	[RegionId] [int] IDENTITY(1,1) NOT NULL,
	[StateCode] [varchar](20) NULL,
	[RegionName] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[CountryCode] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[RegionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_AverageUploadFiles]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_AverageUploadFiles](
	[AvgUploadFileId] [int] IDENTITY(1,1) NOT NULL,
	[AvgUploadBatchId] [int] NULL,
	[FilePath] [varchar](200) NULL,
	[TotalCustomers] [int] NULL,
	[TotalSucessTransactions] [int] NULL,
	[TotalFailureTransactions] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[IsTrasactionsCompleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[AvgUploadFileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails](
	[Bedcinfoid] [int] NULL,
	[Bedconfid] [int] IDENTITY(1,1) NOT NULL,
	[ContactName] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[InstalledBy] [int] NULL,
	[AgencyId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Bedconfid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_MeterReadingsLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MeterReadingsLogs](
	[MeterReadingLogId] [bigint] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NOT NULL,
	[AccountNo] [varchar](50) NOT NULL,
	[MeterNumber] [varchar](20) NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Multiplier] [int] NULL,
	[AverageUsage] [numeric](20, 2) NULL,
	[Usage] [numeric](20, 2) NULL,
	[ReadDate] [datetime] NULL,
	[ReadBy] [varchar](50) NULL,
	[ReadType] [int] NOT NULL,
	[IsTamper] [bit] NULL,
	[TamperRemarks] [varchar](200) NULL,
	[MeterReadingFromId] [int] NULL,
	[IsRollOver] [bit] NULL,
	[PresentApprovalRole] [int] NOT NULL,
	[NextApprovalRole] [int] NOT NULL,
	[ApproveStatusId] [int] NOT NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterReadingLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_DirectCustomersAverageChangeLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_DirectCustomersAverageChangeLogs](
	[AverageChangeLogId] [bigint] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NOT NULL,
	[AccountNo] [varchar](50) NOT NULL,
	[PreviousAverage] [decimal](18, 2) NULL,
	[NewAverage] [decimal](18, 2) NULL,
	[PresentApprovalRole] [int] NOT NULL,
	[NextApprovalRole] [int] NOT NULL,
	[ApproveStatusId] [int] NOT NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[AverageChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_AdjustmentsLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_AdjustmentsLogs](
	[AdjustmentLogId] [bigint] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NOT NULL,
	[AccountNo] [varchar](50) NOT NULL,
	[BillNumber] [varchar](50) NULL,
	[BillAdjustmentTypeId] [int] NULL,
	[MeterNumber] [varchar](20) NULL,
	[PreviousReading] [varchar](20) NULL,
	[CurrentReadingAfterAdjustment] [varchar](50) NULL,
	[CurrentReadingBeforeAdjustment] [varchar](50) NULL,
	[AdjustedUnits] [decimal](18, 2) NULL,
	[TaxEffected] [decimal](18, 2) NULL,
	[TotalAmountEffected] [decimal](18, 2) NULL,
	[EnergryCharges] [decimal](18, 2) NULL,
	[AdditionalCharges] [decimal](18, 2) NULL,
	[PresentApprovalRole] [int] NOT NULL,
	[NextApprovalRole] [int] NOT NULL,
	[ApproveStatusId] [int] NOT NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BatchId] [bigint] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[AdjustmentLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [TR_InsertAuditStates]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <19-MAR-2014>
-- Description:	<Insert Previous data of States into Audit table Before Update>
-- =============================================

CREATE TRIGGER [dbo].[TR_InsertAuditStates]   ON  [dbo].[Tbl_States]
  INSTEAD OF UPDATE
AS 
BEGIN
	
	DECLARE 
	 @StateCode   varchar (20)   ,
	 @StateName   varchar (50)  ,
	 @DisplayCode   varchar (20)  ,
	 @CountryCode   varchar (20)  ,
	 @StateDetails   varchar (max)  ,
	 @Notes   varchar (max)  ,
	 @ModifiedBy   varchar (50)  ,
	 @IsActive   bit   ;

SELECT
 @StateCode =I.StateCode
 ,@StateName=I.StateName
 ,@DisplayCode=I.DisplayCode
 ,@CountryCode=I.CountryCode
 ,@StateDetails=I.StateDetails
 ,@Notes=I.Notes
 ,@ModifiedBy=I.ModifiedBy
 ,@IsActive=I.IsActive
FROM inserted I;

INSERT INTO Tbl_Audit_States
(
StateCode
,StateName
,DisplayCode
,CountryCode
,StateDetails
,Notes
,CreatedBy
,CreatedDate
,ModifiedBy
,ModifiedDate
,IsActive
)
SELECT [StateCode]
      ,[StateName]
      ,[DisplayCode]
      ,[CountryCode]
      ,[StateDetails]
      ,[Notes]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[IsActive]
       FROM Tbl_States WHERE StateCode=@StateCode;
UPDATE Tbl_States SET
[StateName]=@StateName
      ,[DisplayCode]=@DisplayCode
      ,[CountryCode]=@CountryCode
      ,[StateDetails]=@StateDetails
      ,[Notes]=@Notes
      ,[ModifiedBy]=@ModifiedBy
      ,[ModifiedDate]=GETDATE()
      ,[IsActive]=@IsActive WHERE StateCode=@StateCode;

END
GO
/****** Object:  Trigger [TR_InsertAuditServiceUnits]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <19-MAR-2014>
-- Description:	<Insert Previous data of ServiceUnits into Audit table Before Update>
-- Modified By: Padmini
--Modified Date: 19th Feb 2015 
-- =============================================
CREATE TRIGGER [dbo].[TR_InsertAuditServiceUnits]  ON  [dbo].[Tbl_ServiceUnits]
  INSTEAD OF UPDATE
AS 
BEGIN
	
	DECLARE  
	 @SU_ID   varchar (20)   ,
	 @ServiceUnitName   varchar (300)  ,
	 @Notes   varchar (max)  ,
	 @BU_ID   varchar (20)  ,
	 @SUCode   varchar (10)  ,
	 @ActiveStatusId   int   ,
	 @Address1 varchar(100) ,
	 @Address2 varchar(100),
	 @City varchar(100),
	 @ZIP varchar(100),
	 @ModifiedBy   varchar (50) ;
	 
	 SELECT 
	 @SU_ID =I.SU_ID
	 ,@ServiceUnitName=I.ServiceUnitName
	 ,@Notes=I.Notes
	 ,@BU_ID=I.BU_ID
	 ,@SUCode=I.SUCode
	 ,@Address1=I.Address1
	  ,@Address2=I.Address2
	  ,@City=I.City
	  ,@ZIP=I.ZIP
	 ,@ActiveStatusId=I.ActiveStatusId
	 ,@ModifiedBy=I.ModifiedBy
	 FROM inserted I; 
	 
	 INSERT INTO Tbl_Audit_ServiceUnits
	 (
		SU_ID
		,ServiceUnitName
		,Notes
		,BU_ID
		,ActiveStatusId
		,CreatedBy
		,CreatedDate
		,ModifiedBy
		,ModifiedDate
		,SUCode
		,Address1
		,Address2
		,City
		,ZIP

	 )
	 SELECT [SU_ID]
      ,[ServiceUnitName]
      ,[Notes]
      ,[BU_ID]
      ,[ActiveStatusId]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[SUCode] 
      ,Address1
      ,Address2
      ,City
      ,ZIP
      FROM Tbl_ServiceUnits WHERE SU_ID=@SU_ID;
	 
	 UPDATE Tbl_ServiceUnits SET
							   [ServiceUnitName]=@ServiceUnitName
							  ,[Notes]=@Notes
							  ,[BU_ID]=@BU_ID
							  ,[SUCode]=@SUCode
							  ,[ActiveStatusId]=@ActiveStatusId
							  ,[Address1]=@Address1
							  ,Address2=@Address2
						       ,City=@City
							   ,ZIP=@ZIP
							  ,[ModifiedBy]=@ModifiedBy
							  ,[ModifiedDate]=GETDATE() 
	 WHERE SU_ID=@SU_ID;

END
GO
/****** Object:  Trigger [TR_InsertAuditServiceCenter]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <19-MAR-2014>
-- Description:	<Insert Previous data of ServiceCenter into Audit table Before Update>
-- Modified By: Padmini
--Modified Date: 19th Feb 2015 
-- =============================================
CREATE TRIGGER [dbo].[TR_InsertAuditServiceCenter]   ON  [dbo].[Tbl_ServiceCenter]
   INSTEAD OF UPDATE
AS 
BEGIN

DECLARE  
     @ServiceCenterId   varchar(20)   ,
	 @ServiceCenterName   varchar(300)  ,
	 @Notes   varchar(max)  ,
	 @SU_ID   varchar(20)  ,
	 @SCCode   varchar(10)  ,
	 @Address1 varchar(100) ,
	 @Address2 varchar(100),
	 @City varchar(100),
	 @ZIP varchar(100),
	 @ActiveStatusId   int   ,
	 @ModifiedBy   varchar(50) ;
	 
	 SELECT
	  @ServiceCenterId =I.ServiceCenterId
	  ,@ServiceCenterName=I.ServiceCenterName
	  ,@Notes=I.Notes
	  ,@SU_ID=I.SU_ID
	  ,@ActiveStatusId=I.ActiveStatusId
	  ,@ModifiedBy=I.ModifiedBy
	  ,@SCCode=I.SCCode
	  ,@Address1=I.Address1
	  ,@Address2=I.Address2
	  ,@City=I.City
	  ,@ZIP=I.ZIP
	 FROM inserted I;
	 
	 INSERT INTO Tbl_Audit__ServiceCenter
	 (
		ServiceCenterId
		,ServiceCenterName
		,Notes
		,SU_ID
		,ActiveStatusId
		,CreatedBy
		,CreatedDate
		,ModifiedBy
		,ModifiedDate
		,SCCode
		,Address1
		,Address2
		,City
		,ZIP
	 )
	 SELECT [ServiceCenterId]
      ,[ServiceCenterName]
      ,[Notes]
      ,[SU_ID]
      ,[ActiveStatusId]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[SCCode]
      ,Address1
      ,Address2
      ,City
      ,ZIP
       FROM Tbl_ServiceCenter WHERE ServiceCenterId=@ServiceCenterId;
	 
	 UPDATE Tbl_ServiceCenter SET
	 [ServiceCenterName]=@ServiceCenterName
      ,[Notes]=@Notes
      ,[SU_ID]=@SU_ID
      ,[ActiveStatusId]=@ActiveStatusId
      ,[ModifiedBy]=@ModifiedBy
      ,[SCCode]=@SCCode
      ,[Address1]=@Address1
      ,Address2=@Address2
      ,City=@City
      ,ZIP=@ZIP
      ,[ModifiedDate]=dbo.fn_GetCurrentDateTime() 
     WHERE ServiceCenterId=@ServiceCenterId  
      
END
GO
/****** Object:  Trigger [TR_InsertAuditCycles]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <18-MAR-2014>
-- Description:	<Insert Previous data of Cycles into Audit table Before Update>
-- =============================================
CREATE TRIGGER [dbo].[TR_InsertAuditCycles]
   ON  [dbo].[Tbl_Cycles]
   INSTEAD OF UPDATE
AS 
BEGIN
DECLARE
	@CycleId varchar(20) ,
	@CycleName varchar(300) ,
	@DetailsOfCycle varchar(max) ,
	@ActiveStatusId int ,
	@ModifiedBy varchar(50) ,
	--@BU_ID varchar(20) ,
	--@SU_ID varchar(20) ,
	@ServiceCenterId varchar(20) ,
	@ContactName varchar(200) ,
	@ContactNo varchar(20) ,
	@CycleNo int ,
	@CycleCode VARCHAR(10) ;
SELECT @CycleId=I.CycleId  ,
		@CycleName =I.CycleName ,
		@DetailsOfCycle=I.DetailsOfCycle ,
		@ActiveStatusId=I.ActiveStatusId  ,
		@ModifiedBy=I.ModifiedBy  ,
		--@BU_ID=I.BU_ID  ,
		--@SU_ID=I.SU_ID ,
		@ServiceCenterId =I.ServiceCenterId ,
		@ContactName=I.ContactName ,
		@ContactNo=I.ContactNo  ,
		@CycleCode=I.CycleCode FROM inserted I ;
		
		INSERT INTO Tbl_Audit_Cycles
		SELECT [CycleId]
      ,[CycleName]
      ,[DetailsOfCycle]
      --,[BU_ID]
      --,[SU_ID]
      ,[ServiceCenterId]
      ,[ContactName]
      ,[ContactNo]
      ,[CycleCode]
       ,[ActiveStatusId]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]  FROM Tbl_Cycles WHERE CycleId=@CycleId;
		
		UPDATE Tbl_Cycles SET
							   [CycleName]=@CycleName
							  ,[DetailsOfCycle]=@DetailsOfCycle
							  ,[ActiveStatusId]=@ActiveStatusId
							  ,[ModifiedBy]=@ModifiedBy
							  ,[ModifiedDate]=GETDATE()
							  --,[BU_ID]=@BU_ID
							  --,[SU_ID]=@SU_ID
							  ,[ServiceCenterId]=@ServiceCenterId
							  ,[ContactName]=@ContactName
							  ,[ContactNo]=@ContactNo
							  ,[CycleCode]=@CycleCode WHERE CycleId=@CycleId;
END
GO
/****** Object:  Trigger [TR_INSERTAUDITAGENCIES]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_INSERTAUDITAGENCIES] ON [dbo].[Tbl_Agencies]
INSTEAD OF UPDATE
AS
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <18-MAR-2014>
-- Description:	<Insert Previous data of Agencies into Audit table Before Update>
-- =============================================
DECLARE @ID INT
    , @AgencyName  VARCHAR(300)
      , @Details  VARCHAR(MAX)
      , @ContactNo1   VARCHAR(20)
      , @ContactNo2   VARCHAR(20)
      , @ContactPersonName   VARCHAR(50)
      ,@ActiveStatusId  INT
      , @ModifedBy   VARCHAR(50);
      

SELECT @ID=I.AgencyId FROM inserted I;
SELECT @AgencyName=I.AgencyName FROM inserted I;

SELECT @Details=I.Details FROM inserted I;

SELECT @ContactNo1=I.ContactNo1 FROM inserted I;

SELECT @ContactNo2=I.ContactNo2 FROM inserted I;

SELECT @ContactPersonName=I.ContactPersonName FROM inserted I;

SELECT @ActiveStatusId=I.ActiveStatusId FROM inserted I;
SELECT @ModifedBy=I.ModifedBy FROM inserted I;


INSERT INTO Tbl_Audit_Agencies
(
AgencyId
,AgencyName
,Details
,ContactNo1
,ContactNo2
,ContactPersonName
,ActiveStatusId
,CreatedDate
,CreatedBy
,ModifedBy
,ModifiedDate
)
SELECT 
		AgencyId
      ,[AgencyName]
      ,[Details]
      ,[ContactNo1]
      ,[ContactNo2]
      ,[ContactPersonName]
      ,[ActiveStatusId]
      ,[CreatedDate]
      ,[CreatedBy]
      ,[ModifedBy]
      ,[ModifiedDate] FROM Tbl_Agencies WHERE AgencyId=@ID
      
      
 UPDATE Tbl_Agencies SET [AgencyName]=@AgencyName
      ,[Details]=@Details
      ,[ContactNo1]=@ContactNo1
      ,[ContactNo2]=@ContactNo2
      ,[ContactPersonName]=@ContactPersonName
      ,[ActiveStatusId]=@ActiveStatusId
      ,[ModifedBy]=@ModifedBy
      ,[ModifiedDate]=GETDATE() WHERE AgencyId=@ID
GO
/****** Object:  Table [dbo].[Tbl_ReadingUploadFiles]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ReadingUploadFiles](
	[RUploadFileId] [int] IDENTITY(1,1) NOT NULL,
	[RUploadBatchId] [int] NULL,
	[FilePath] [varchar](200) NULL,
	[TotalCustomers] [int] NULL,
	[TotalSucessTransactions] [int] NULL,
	[TotalFailureTransactions] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[IsTrasactionsCompleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[RUploadFileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaymentsLogs]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaymentsLogs](
	[PaymentLogId] [bigint] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NOT NULL,
	[AccountNo] [varchar](50) NOT NULL,
	[ReceiptNumber] [varchar](20) NULL,
	[PaymentMode] [int] NOT NULL,
	[PaidAmount] [decimal](18, 2) NOT NULL,
	[PadiDate] [datetime] NOT NULL,
	[PresentApprovalRole] [int] NOT NULL,
	[NextApprovalRole] [int] NOT NULL,
	[ApproveStatusId] [int] NOT NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PaymentFromId] [int] NULL,
	[DocumentPath] [varchar](max) NULL,
	[Cashier] [varchar](50) NULL,
	[CashOffice] [int] NULL,
	[DocumentName] [varchar](max) NULL,
	[BatchNo] [int] NULL,
	[PaymentType] [int] NULL,
	[CurrentApprovalLevel] [int] NULL,
	[IsLocked] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaymentUploadFiles]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaymentUploadFiles](
	[PUploadFileId] [int] IDENTITY(1,1) NOT NULL,
	[PUploadBatchId] [int] NULL,
	[FilePath] [varchar](200) NULL,
	[TotalCustomers] [int] NULL,
	[TotalSucessTransactions] [int] NULL,
	[TotalFailureTransactions] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[IsTrasactionsCompleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[PUploadFileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ReadingTransactions]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ReadingTransactions](
	[ReadingTransactionId] [int] IDENTITY(1,1) NOT NULL,
	[RUploadFileId] [int] NULL,
	[SNO] [varchar](50) NULL,
	[AccountNo] [varchar](100) NULL,
	[CurrentReading] [varchar](100) NULL,
	[ReadingDate] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ReadingTransactionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ReadingSucessTransactions]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ReadingSucessTransactions](
	[ReadingSucessTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[RUploadFileId] [int] NULL,
	[SNO] [varchar](50) NULL,
	[AccountNo] [varchar](100) NULL,
	[CurrentReading] [varchar](100) NULL,
	[ReadingDate] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[Comments] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ReadingSucessTransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ReadingFailureTransactions]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ReadingFailureTransactions](
	[ReadingFailureransactionID] [int] IDENTITY(1,1) NOT NULL,
	[RUploadFileId] [int] NULL,
	[SNO] [varchar](50) NULL,
	[AccountNo] [varchar](100) NULL,
	[CurrentReading] [varchar](100) NULL,
	[ReadingDate] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[Comments] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ReadingFailureransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaymentTransactions]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaymentTransactions](
	[PaymentTransactionId] [int] IDENTITY(1,1) NOT NULL,
	[PUploadFileId] [int] NULL,
	[SNO] [varchar](50) NULL,
	[AccountNo] [varchar](100) NULL,
	[AmountPaid] [varchar](100) NULL,
	[ReceiptNo] [varchar](100) NULL,
	[ReceivedDate] [varchar](100) NULL,
	[PaymentMode] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentTransactionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaymentSucessTransactions]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaymentSucessTransactions](
	[PaymentSucessTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[PUploadFileId] [int] NULL,
	[SNO] [varchar](50) NULL,
	[AccountNo] [varchar](100) NULL,
	[AmountPaid] [varchar](100) NULL,
	[ReceiptNo] [varchar](100) NULL,
	[ReceivedDate] [varchar](100) NULL,
	[PaymentMode] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[Comments] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentSucessTransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaymentFailureTransactions]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaymentFailureTransactions](
	[PaymentFailureransactionID] [int] IDENTITY(1,1) NOT NULL,
	[PUploadFileId] [int] NULL,
	[SNO] [varchar](50) NULL,
	[AccountNo] [varchar](100) NULL,
	[AmountPaid] [varchar](100) NULL,
	[ReceiptNo] [varchar](100) NULL,
	[ReceivedDate] [varchar](100) NULL,
	[PaymentMode] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[Comments] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentFailureransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_AverageTransactions]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_AverageTransactions](
	[AvgTransactionId] [int] IDENTITY(1,1) NOT NULL,
	[AvgUploadFileId] [int] NULL,
	[SNO] [varchar](50) NULL,
	[AccountNo] [varchar](100) NULL,
	[AverageReading] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[AvgTransactionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_AverageSucessTransactions]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_AverageSucessTransactions](
	[AvgSucessTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[AvgUploadFileId] [int] NULL,
	[SNO] [varchar](50) NULL,
	[AccountNo] [varchar](100) NULL,
	[AverageReading] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[Comments] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[AvgSucessTransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_AverageFailureTransactions]    Script Date: 08/07/2015 19:02:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_AverageFailureTransactions](
	[AvgFailureransactionID] [int] IDENTITY(1,1) NOT NULL,
	[AvgUploadFileId] [int] NULL,
	[SNO] [varchar](50) NULL,
	[AccountNo] [varchar](100) NULL,
	[AverageReading] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[Comments] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[AvgFailureransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF__Tbl_Appro__IsLoc__6C98FCFF]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApprovalRegistration] ADD  DEFAULT ((0)) FOR [IsLocked]
GO
/****** Object:  Default [DF__Tbl_Appro__Activ__6D8D2138]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApprovalRegistration] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [Defalut_CPD]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] ADD  CONSTRAINT [Defalut_CPD]  DEFAULT ((1)) FOR [IsServiceAddress]
GO
/****** Object:  Default [DF__Tbl_Custo__IsCom__22CA2527]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] ADD  DEFAULT ((0)) FOR [IsCommunication]
GO
/****** Object:  Default [DF__Tbl_Custo__IsAct__23BE4960]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Agenc__Activ__24B26D99]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Agencies] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Assig__Appro__1B68FA81]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AssignedMeterLogs] ADD  DEFAULT ((2)) FOR [ApprovalStatusId]
GO
/****** Object:  Default [DF__Tbl_Assig__Prese__1C5D1EBA]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AssignedMeterLogs] ADD  DEFAULT ((0)) FOR [PresentApprovalRole]
GO
/****** Object:  Default [DF__Tbl_Assig__NextA__1D5142F3]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AssignedMeterLogs] ADD  DEFAULT ((0)) FOR [NextApprovalRole]
GO
/****** Object:  Default [DF__Tbl_Assig__IsCAP__25476A76]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AssignedMeterLogs] ADD  DEFAULT ((0)) FOR [IsCAPMIMeter]
GO
/****** Object:  Default [DF__Tbl_Audit__IsCAP__263B8EAF]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Audit_AssignedMeterLogs] ADD  DEFAULT ((0)) FOR [IsCAPMIMeter]
GO
/****** Object:  Default [DF__Tbl_Audit__Appro__7F8BD5E2]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Audit_BookNoChangeLogs] ADD  DEFAULT ((0)) FOR [ApprovalStatusId]
GO
/****** Object:  Default [DF__Tbl_Audit__Prese__007FFA1B]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Audit_BookNoChangeLogs] ADD  DEFAULT ((0)) FOR [PresentApprovalRole]
GO
/****** Object:  Default [DF__Tbl_Audit__NextA__01741E54]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Audit_BookNoChangeLogs] ADD  DEFAULT ((0)) FOR [NextApprovalRole]
GO
/****** Object:  Default [DF__Tbl_Audit__IsBoo__25A691D2]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Audit_CustomerDetails] ADD  DEFAULT ((0)) FOR [IsBookNoChanged]
GO
/****** Object:  Default [DF__Tbl_Audit__IsCAP__2ED0D4B0]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Audit_CustomerMeterInfoChangeLogs] ADD  DEFAULT ((0)) FOR [IsCAPMIMeter]
GO
/****** Object:  Default [DF__Tbl_Avera__IsClo__6A5BAED2]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageUploadBatches] ADD  DEFAULT ((0)) FOR [IsClosedBatch]
GO
/****** Object:  Default [DF__Tbl_Avera__IsTra__7108AC61]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageUploadFiles] ADD  DEFAULT ((0)) FOR [IsTrasactionsCompleted]
GO
/****** Object:  Default [dt_Tbl_BATCH_ADJUSTMENT]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BATCH_ADJUSTMENT] ADD  CONSTRAINT [dt_Tbl_BATCH_ADJUSTMENT]  DEFAULT ((1)) FOR [BatchStatusID]
GO
/****** Object:  Default [DF_Tbl_BillAdjustmentDetails_AdditionalChargesID]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BillAdjustmentDetails] ADD  CONSTRAINT [DF_Tbl_BillAdjustmentDetails_AdditionalChargesID]  DEFAULT ((0)) FOR [AdditionalChargesID]
GO
/****** Object:  Default [DF_Tbl_BillAdjustments_ApprovalStatusId]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BillAdjustments] ADD  CONSTRAINT [DF_Tbl_BillAdjustments_ApprovalStatusId]  DEFAULT ((0)) FOR [ApprovalStatusId]
GO
/****** Object:  Default [DF__Tbl_Billi__IsAct__2882FE7D]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BillingDisabledBooks] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [IsPartialBillDefault0]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BillingDisabledBooks] ADD  CONSTRAINT [IsPartialBillDefault0]  DEFAULT ((0)) FOR [IsPartialBill]
GO
/****** Object:  Default [DF__Tbl_Billi__Appro__297722B6]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BillingDisabledBooks] ADD  DEFAULT ((2)) FOR [ApproveStatusId]
GO
/****** Object:  Default [DF__Tbl_BookN__Appro__7BBB44FE]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BookNoChangeLogs] ADD  DEFAULT ((0)) FOR [ApprovalStatusId]
GO
/****** Object:  Default [DF__Tbl_BookN__Prese__7CAF6937]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BookNoChangeLogs] ADD  DEFAULT ((0)) FOR [PresentApprovalRole]
GO
/****** Object:  Default [DF__Tbl_BookN__NextA__7DA38D70]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BookNoChangeLogs] ADD  DEFAULT ((0)) FOR [NextApprovalRole]
GO
/****** Object:  Default [DF__Tbl_BookN__Activ__2B5F6B28]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BookNumbers] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__TBL_BU_He__Activ__2C538F61]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[TBL_BU_HeadOffice] ADD  DEFAULT ((1)) FOR [ActiveStatusID]
GO
/****** Object:  Default [DF_Tbl_BUNotice_IsAllBuss]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BUNotice] ADD  CONSTRAINT [DF_Tbl_BUNotice_IsAllBuss]  DEFAULT ((0)) FOR [IsAllBuss]
GO
/****** Object:  Default [DF_Tbl_BUNotice_Activestatus]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BUNotice] ADD  CONSTRAINT [DF_Tbl_BUNotice_Activestatus]  DEFAULT ((1)) FOR [Activestatus]
GO
/****** Object:  Default [DF__Tbl_Bussi__Activ__2F2FFC0C]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_BussinessUnits] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__TBL_Bussi__Activ__30242045]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[TBL_BussinessUnitsEBllMails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Cashi__Activ__2A0C1F93]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Cashier] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_CashO__Activ__3118447E]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CashOffices] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Check__IsAct__320C68B7]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CheckMeterDetails] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Count__IsAct__33F4B129]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Countries] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Count__Creat__33008CF0]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Countries] ADD  DEFAULT ('Admin') FOR [CreatedBy]
GO
/****** Object:  Default [DF__Tbl_Custo__Appro__34E8D562]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerActiveStatusChangeLogs] ADD  DEFAULT ((2)) FOR [ApproveStatusId]
GO
/****** Object:  Default [DF__Tbl_Custo__Activ__35DCF99B]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerBills] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Custo__Activ__36D11DD4]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerDetails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Custo__OutSt__37C5420D]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerDetails] ADD  DEFAULT ((0)) FOR [OutStandingAmount]
GO
/****** Object:  Default [DF__Tbl_Custo__IsBoo__38B96646]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerDetails] ADD  DEFAULT ((0)) FOR [IsBookNoChanged]
GO
/****** Object:  Default [DF__Tbl_Custo__Activ__39AD8A7F]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerDocuments] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Custo__Appro__3AA1AEB8]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerMeterInfoChangeLogs] ADD  DEFAULT ((2)) FOR [ApproveStatusId]
GO
/****** Object:  Default [DF__Tbl_Custo__IsCAP__2DDCB077]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerMeterInfoChangeLogs] ADD  DEFAULT ((0)) FOR [IsCAPMIMeter]
GO
/****** Object:  Default [DT_BLA]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerPayments] ADD  CONSTRAINT [DT_BLA]  DEFAULT ((1)) FOR [ActivestatusId]
GO
/****** Object:  Default [DF__Tbl_Custo__Recei__3C89F72A]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerPayments] ADD  DEFAULT ((2)) FOR [ReceivedDevice]
GO
/****** Object:  Default [DF_Tbl_CustomerReadings_IsBilled]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerReadings] ADD  CONSTRAINT [DF_Tbl_CustomerReadings_IsBilled]  DEFAULT ((0)) FOR [IsBilled]
GO
/****** Object:  Default [DF__Tbl_Custo__IsTam__3E723F9C]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerReadings] ADD  DEFAULT ((0)) FOR [IsTamper]
GO
/****** Object:  Default [DF__Tbl_Custo__IsRol__7266E4EE]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerReadings] ADD  DEFAULT ((0)) FOR [IsRollOver]
GO
/****** Object:  Default [DF__Tbl_Custo__IsRea__57D2EA43]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerReadings] ADD  DEFAULT ((0)) FOR [IsReadingWrong]
GO
/****** Object:  Default [DF_Tbl_CustomerReadingsLog_IsBilled]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_CustomerReadingsLog] ADD  CONSTRAINT [DF_Tbl_CustomerReadingsLog_IsBilled]  DEFAULT ((0)) FOR [IsBilled]
GO
/****** Object:  Default [DF__Tbl_Cycle__Activ__405A880E]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Cycles] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Direc__Activ__4D9F7493]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_DirectCustomersAvgReadings] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Distr__Activ__58520D30]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_DistrictMessages] ADD  CONSTRAINT [DF__Tbl_Distr__Activ__58520D30]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__TBL_EBill__IsMai__4242D080]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[TBL_EBillMailLogs] ADD  DEFAULT ((0)) FOR [IsMailSent]
GO
/****** Object:  Default [DF__Tbl_EditL__IsAct__2FDA0782]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_EditListReportSummary] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Estim__IsAct__4336F4B9]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_EstimatedEnergy] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [df_ConsAct]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_EstimationSettings] ADD  CONSTRAINT [df_ConsAct]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__TBL_Funct__IsFin__43D61337]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[TBL_FunctionApprovalRole] ADD  DEFAULT ((0)) FOR [IsFinalApproval]
GO
/****** Object:  Default [DF__TBL_Funct__IsAct__61915EA7]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[TBL_FunctionApprovalRole] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Globa__Activ__451F3D2B]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_GlobalMessages] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF_Tbl_HHInputFiles_ActiveStatus]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_HHInputFiles] ADD  CONSTRAINT [DF_Tbl_HHInputFiles_ActiveStatus]  DEFAULT ((1)) FOR [ActiveStatus]
GO
/****** Object:  Default [DF__Tbl_LAddi__IsAct__4707859D]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_LAdditionalClassCharges] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_LEner__IsAct__47FBA9D6]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_LEnergyClassCharges] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Marke__Activ__47A6A41B]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Marketers] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Meter__Activ__4AD81681]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_MeterInformation] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Meter__IsCAP__2176D992]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_MeterInformation] ADD  DEFAULT ((0)) FOR [IsCAPMIMeter]
GO
/****** Object:  Default [DF__Tbl_MRegi__Activ__59C55456]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_MRegion] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF_Tbl_MRoutes_ActiveStatusId]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_MRoutes] ADD  CONSTRAINT [DF_Tbl_MRoutes_ActiveStatusId]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_NewCu__Appro__4BCC3ABA]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_NewCustomerLocator] ADD  DEFAULT ((0)) FOR [ApprovalStatus]
GO
/****** Object:  Default [DF_Tbl_Notifications_activestatus]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_Notifications] ADD  CONSTRAINT [DF_Tbl_Notifications_activestatus]  DEFAULT ((1)) FOR [Activestatus]
GO
/****** Object:  Default [DF__Tbl_PaidM__Activ__4DB4832C]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaidMeterDetails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Payme__IsClo__1486F2C8]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentUploadBatches] ADD  DEFAULT ((0)) FOR [IsClosedBatch]
GO
/****** Object:  Default [DF__Tbl_Payme__IsTra__1B33F057]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentUploadFiles] ADD  DEFAULT ((0)) FOR [IsTrasactionsCompleted]
GO
/****** Object:  Default [ActiveStatusId_Default_Value]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PoleDescriptionTable] ADD  CONSTRAINT [ActiveStatusId_Default_Value]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Readi__IsClo__330B79E8]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingUploadBatches] ADD  DEFAULT ((0)) FOR [IsClosedBatch]
GO
/****** Object:  Default [DF__Tbl_Readi__IsTra__39B87777]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingUploadFiles] ADD  DEFAULT ((0)) FOR [IsTrasactionsCompleted]
GO
/****** Object:  Default [DF__TBL_ReadT__IsLat__61A66D40]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[TBL_ReadToDirectCustomerActivity] ADD  DEFAULT ((1)) FOR [IsLatest]
GO
/****** Object:  Default [DF__Tbl_ReadT__Appro__2215F810]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadToDirectCustomerActivityLogs] ADD  DEFAULT ((2)) FOR [ApprovalStatusId]
GO
/****** Object:  Default [DF__Tbl_ReadT__Prese__230A1C49]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadToDirectCustomerActivityLogs] ADD  DEFAULT ((0)) FOR [PresentApprovalRole]
GO
/****** Object:  Default [DF__Tbl_ReadT__NextA__23FE4082]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadToDirectCustomerActivityLogs] ADD  DEFAULT ((0)) FOR [NextApprovalRole]
GO
/****** Object:  Default [DF__Tbl_Servi__Activ__4F9CCB9E]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ServiceCenter] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Servi__Activ__5090EFD7]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ServiceUnits] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_State__IsAct__52793849]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_States] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_State__Creat__51851410]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_States] ADD  DEFAULT ('Admin') FOR [CreatedBy]
GO
/****** Object:  Default [DF__Tbl_UserB__Activ__477C86E9]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_UserBusinessUnits] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_UserC__Activ__546180BB]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_UserCashOffices] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_UserD__Activ__5555A4F4]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_UserDocuments] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_UserR__Activ__5649C92D]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_UserRoles] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_UserS__Activ__573DED66]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_UserServiceCenters] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_UserS__Activ__5832119F]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_UserServiceUnits] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId18]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId18] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId18]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId719]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId719] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId719]
GO
/****** Object:  ForeignKey [FK_AgencyId]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails]  WITH CHECK ADD  CONSTRAINT [FK_AgencyId] FOREIGN KEY([AgencyId])
REFERENCES [dbo].[Tbl_Agencies] ([AgencyId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails] CHECK CONSTRAINT [FK_AgencyId]
GO
/****** Object:  ForeignKey [FK_BEDCEmpId]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails]  WITH CHECK ADD  CONSTRAINT [FK_BEDCEmpId] FOREIGN KEY([InstalledBy])
REFERENCES [EMPLOYEE].[Tbl_MBEDCEmployeeDetails] ([BEDCEmpId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails] CHECK CONSTRAINT [FK_BEDCEmpId]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId20]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId20] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId20]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId21]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId21] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId21]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId24]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerActiveDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId24] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerActiveDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId24]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId25]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerActiveDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId25] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerActiveDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId25]
GO
/****** Object:  ForeignKey [FK_IdentityTypeId]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails]  WITH CHECK ADD  CONSTRAINT [FK_IdentityTypeId] FOREIGN KEY([IdentityTypeId])
REFERENCES [dbo].[Tbl_MIdentityTypes] ([IdentityId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails] CHECK CONSTRAINT [FK_IdentityTypeId]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId5]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId5] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId5]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId6]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId6] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId6]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId12]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId12] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId12]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId13]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId13] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId13]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId7]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerProceduralDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId7] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerProceduralDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId7]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId8]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerProceduralDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId8] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerProceduralDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId8]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId3]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomersDetail]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId3] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomersDetail] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId3]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId4]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomersDetail]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId4] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomersDetail] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId4]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId14]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerTenentDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId14] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerTenentDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId14]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId15]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerTenentDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId15] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerTenentDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId15]
GO
/****** Object:  ForeignKey [Tbl_AdjustmentsLogs_ApproveStatusId_FK]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AdjustmentsLogs]  WITH CHECK ADD  CONSTRAINT [Tbl_AdjustmentsLogs_ApproveStatusId_FK] FOREIGN KEY([ApproveStatusId])
REFERENCES [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId])
GO
ALTER TABLE [dbo].[Tbl_AdjustmentsLogs] CHECK CONSTRAINT [Tbl_AdjustmentsLogs_ApproveStatusId_FK]
GO
/****** Object:  ForeignKey [Tbl_AdjustmentsLogs_BillAdjustmentTypeId_FK]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AdjustmentsLogs]  WITH CHECK ADD  CONSTRAINT [Tbl_AdjustmentsLogs_BillAdjustmentTypeId_FK] FOREIGN KEY([BillAdjustmentTypeId])
REFERENCES [dbo].[Tbl_BillAdjustmentType] ([BATID])
GO
ALTER TABLE [dbo].[Tbl_AdjustmentsLogs] CHECK CONSTRAINT [Tbl_AdjustmentsLogs_BillAdjustmentTypeId_FK]
GO
/****** Object:  ForeignKey [Tbl_AdjustmentsLogs_GlobalAccountNumber_FK]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AdjustmentsLogs]  WITH CHECK ADD  CONSTRAINT [Tbl_AdjustmentsLogs_GlobalAccountNumber_FK] FOREIGN KEY([GlobalAccountNumber])
REFERENCES [CUSTOMERS].[Tbl_CustomersDetail] ([GlobalAccountNumber])
GO
ALTER TABLE [dbo].[Tbl_AdjustmentsLogs] CHECK CONSTRAINT [Tbl_AdjustmentsLogs_GlobalAccountNumber_FK]
GO
/****** Object:  ForeignKey [FK__Tbl_Avera__Creat__02333863]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageFailureTransactions]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Avera__PUplo__013F142A]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageFailureTransactions]  WITH CHECK ADD FOREIGN KEY([AvgUploadFileId])
REFERENCES [dbo].[Tbl_AverageUploadFiles] ([AvgUploadFileId])
GO
/****** Object:  ForeignKey [FK__Tbl_Avera__AvgUp__7B863AD4]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageSucessTransactions]  WITH CHECK ADD FOREIGN KEY([AvgUploadFileId])
REFERENCES [dbo].[Tbl_AverageUploadFiles] ([AvgUploadFileId])
GO
/****** Object:  ForeignKey [FK__Tbl_Avera__Creat__7C7A5F0D]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageSucessTransactions]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Avera__AvgUp__75CD617E]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageTransactions]  WITH CHECK ADD FOREIGN KEY([AvgUploadFileId])
REFERENCES [dbo].[Tbl_AverageUploadFiles] ([AvgUploadFileId])
GO
/****** Object:  ForeignKey [FK__Tbl_Avera__Creat__76C185B7]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageTransactions]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Avera__Creat__68736660]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageUploadBatches]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Avera__LastM__69678A99]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageUploadBatches]  WITH CHECK ADD FOREIGN KEY([LastModifyBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Avera__AvgUp__6F2063EF]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageUploadFiles]  WITH CHECK ADD FOREIGN KEY([AvgUploadBatchId])
REFERENCES [dbo].[Tbl_AverageUploadBatches] ([AvgUploadBatchId])
GO
/****** Object:  ForeignKey [FK__Tbl_Avera__Creat__70148828]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_AverageUploadFiles]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [Tbl_DirectCustomersAverageChangeLogs_ApproveStatusId_FK]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_DirectCustomersAverageChangeLogs]  WITH CHECK ADD  CONSTRAINT [Tbl_DirectCustomersAverageChangeLogs_ApproveStatusId_FK] FOREIGN KEY([ApproveStatusId])
REFERENCES [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId])
GO
ALTER TABLE [dbo].[Tbl_DirectCustomersAverageChangeLogs] CHECK CONSTRAINT [Tbl_DirectCustomersAverageChangeLogs_ApproveStatusId_FK]
GO
/****** Object:  ForeignKey [Tbl_DirectCustomersAverageChangeLogs_GlobalAccountNumber_FK]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_DirectCustomersAverageChangeLogs]  WITH CHECK ADD  CONSTRAINT [Tbl_DirectCustomersAverageChangeLogs_GlobalAccountNumber_FK] FOREIGN KEY([GlobalAccountNumber])
REFERENCES [CUSTOMERS].[Tbl_CustomersDetail] ([GlobalAccountNumber])
GO
ALTER TABLE [dbo].[Tbl_DirectCustomersAverageChangeLogs] CHECK CONSTRAINT [Tbl_DirectCustomersAverageChangeLogs_GlobalAccountNumber_FK]
GO
/****** Object:  ForeignKey [FK__TBL_Funct__BU_ID__5DC0CDC3]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[TBL_FunctionApprovalRole]  WITH CHECK ADD FOREIGN KEY([BU_ID])
REFERENCES [dbo].[Tbl_BussinessUnits] ([BU_ID])
GO
/****** Object:  ForeignKey [Tbl_MeterReadingsLogs_ApproveStatusId_FK]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_MeterReadingsLogs]  WITH CHECK ADD  CONSTRAINT [Tbl_MeterReadingsLogs_ApproveStatusId_FK] FOREIGN KEY([ApproveStatusId])
REFERENCES [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId])
GO
ALTER TABLE [dbo].[Tbl_MeterReadingsLogs] CHECK CONSTRAINT [Tbl_MeterReadingsLogs_ApproveStatusId_FK]
GO
/****** Object:  ForeignKey [Tbl_MeterReadingsLogs_GlobalAccountNumber_FK]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_MeterReadingsLogs]  WITH CHECK ADD  CONSTRAINT [Tbl_MeterReadingsLogs_GlobalAccountNumber_FK] FOREIGN KEY([GlobalAccountNumber])
REFERENCES [CUSTOMERS].[Tbl_CustomersDetail] ([GlobalAccountNumber])
GO
ALTER TABLE [dbo].[Tbl_MeterReadingsLogs] CHECK CONSTRAINT [Tbl_MeterReadingsLogs_GlobalAccountNumber_FK]
GO
/****** Object:  ForeignKey [Tbl_MeterReadingsLogs_MeterReadingFromId_FK]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_MeterReadingsLogs]  WITH CHECK ADD  CONSTRAINT [Tbl_MeterReadingsLogs_MeterReadingFromId_FK] FOREIGN KEY([MeterReadingFromId])
REFERENCES [MASTERS].[Tbl_MMeterReadingsFrom] ([MeterReadingFromId])
GO
ALTER TABLE [dbo].[Tbl_MeterReadingsLogs] CHECK CONSTRAINT [Tbl_MeterReadingsLogs_MeterReadingFromId_FK]
GO
/****** Object:  ForeignKey [FK__Tbl_MRegi__Activ__662B2B3B]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_MRegion]  WITH CHECK ADD FOREIGN KEY([ActiveStatusId])
REFERENCES [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId])
GO
/****** Object:  ForeignKey [FK__Tbl_Payme__Creat__2C5E7C59]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentFailureTransactions]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Payme__PUplo__2B6A5820]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentFailureTransactions]  WITH CHECK ADD FOREIGN KEY([PUploadFileId])
REFERENCES [dbo].[Tbl_PaymentUploadFiles] ([PUploadFileId])
GO
/****** Object:  ForeignKey [Tbl_PaymentsLogs_ApproveStatusId_FK]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentsLogs]  WITH CHECK ADD  CONSTRAINT [Tbl_PaymentsLogs_ApproveStatusId_FK] FOREIGN KEY([ApproveStatusId])
REFERENCES [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId])
GO
ALTER TABLE [dbo].[Tbl_PaymentsLogs] CHECK CONSTRAINT [Tbl_PaymentsLogs_ApproveStatusId_FK]
GO
/****** Object:  ForeignKey [Tbl_PaymentsLogs_GlobalAccountNumber_FK]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentsLogs]  WITH CHECK ADD  CONSTRAINT [Tbl_PaymentsLogs_GlobalAccountNumber_FK] FOREIGN KEY([GlobalAccountNumber])
REFERENCES [CUSTOMERS].[Tbl_CustomersDetail] ([GlobalAccountNumber])
GO
ALTER TABLE [dbo].[Tbl_PaymentsLogs] CHECK CONSTRAINT [Tbl_PaymentsLogs_GlobalAccountNumber_FK]
GO
/****** Object:  ForeignKey [Tbl_PaymentsLogs_PaymentMode_FK]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentsLogs]  WITH CHECK ADD  CONSTRAINT [Tbl_PaymentsLogs_PaymentMode_FK] FOREIGN KEY([PaymentMode])
REFERENCES [dbo].[Tbl_MPaymentMode] ([PaymentModeId])
GO
ALTER TABLE [dbo].[Tbl_PaymentsLogs] CHECK CONSTRAINT [Tbl_PaymentsLogs_PaymentMode_FK]
GO
/****** Object:  ForeignKey [FK__Tbl_Payme__Creat__26A5A303]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentSucessTransactions]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Payme__PUplo__25B17ECA]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentSucessTransactions]  WITH CHECK ADD FOREIGN KEY([PUploadFileId])
REFERENCES [dbo].[Tbl_PaymentUploadFiles] ([PUploadFileId])
GO
/****** Object:  ForeignKey [FK__Tbl_Payme__Creat__20ECC9AD]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentTransactions]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Payme__PUplo__1FF8A574]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentTransactions]  WITH CHECK ADD FOREIGN KEY([PUploadFileId])
REFERENCES [dbo].[Tbl_PaymentUploadFiles] ([PUploadFileId])
GO
/****** Object:  ForeignKey [FK__Tbl_Payme__Creat__129EAA56]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentUploadBatches]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Payme__LastM__1392CE8F]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentUploadBatches]  WITH CHECK ADD FOREIGN KEY([LastModifyBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Payme__Creat__1A3FCC1E]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentUploadFiles]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Payme__PUplo__194BA7E5]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_PaymentUploadFiles]  WITH CHECK ADD FOREIGN KEY([PUploadBatchId])
REFERENCES [dbo].[Tbl_PaymentUploadBatches] ([PUploadBatchId])
GO
/****** Object:  ForeignKey [FK__Tbl_Readi__Creat__4AE30379]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingFailureTransactions]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Readi__PUplo__49EEDF40]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingFailureTransactions]  WITH CHECK ADD FOREIGN KEY([RUploadFileId])
REFERENCES [dbo].[Tbl_ReadingUploadFiles] ([RUploadFileId])
GO
/****** Object:  ForeignKey [FK__Tbl_Readi__Creat__452A2A23]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingSucessTransactions]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Readi__PUplo__443605EA]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingSucessTransactions]  WITH CHECK ADD FOREIGN KEY([RUploadFileId])
REFERENCES [dbo].[Tbl_ReadingUploadFiles] ([RUploadFileId])
GO
/****** Object:  ForeignKey [FK__Tbl_Readi__Creat__3F7150CD]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingTransactions]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Readi__PUplo__3E7D2C94]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingTransactions]  WITH CHECK ADD FOREIGN KEY([RUploadFileId])
REFERENCES [dbo].[Tbl_ReadingUploadFiles] ([RUploadFileId])
GO
/****** Object:  ForeignKey [FK__Tbl_Readi__Creat__31233176]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingUploadBatches]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Readi__LastM__321755AF]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingUploadBatches]  WITH CHECK ADD FOREIGN KEY([LastModifyBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Readi__Creat__38C4533E]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingUploadFiles]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
/****** Object:  ForeignKey [FK__Tbl_Readi__RUplo__37D02F05]    Script Date: 08/07/2015 19:02:51 ******/
ALTER TABLE [dbo].[Tbl_ReadingUploadFiles]  WITH CHECK ADD FOREIGN KEY([RUploadBatchId])
REFERENCES [dbo].[Tbl_ReadingUploadBatches] ([RUploadBatchId])
GO
