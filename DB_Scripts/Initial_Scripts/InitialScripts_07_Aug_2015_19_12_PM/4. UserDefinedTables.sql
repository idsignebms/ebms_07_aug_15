GO
/****** Object:  UserDefinedTableType [dbo].[BillFixedCharges]    Script Date: 08/07/2015 17:56:29 ******/
CREATE TYPE [dbo].[BillFixedCharges] AS TABLE(
	[ChargeId] [int] NULL,
	[Amount] [decimal](18, 2) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TblAvgBulkUpload]    Script Date: 08/07/2015 17:56:29 ******/
CREATE TYPE [dbo].[TblAvgBulkUpload] AS TABLE(
	[SNo] [int] NULL,
	[AccNum] [varchar](50) NULL,
	[AverageReading] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TblBulkCustomerPayments]    Script Date: 08/07/2015 17:56:29 ******/
CREATE TYPE [dbo].[TblBulkCustomerPayments] AS TABLE(
	[RowNumber] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[OldAccountNo] [varchar](50) NULL,
	[PaidAmount] [decimal](18, 2) NULL,
	[ReceiptNO] [varchar](20) NULL,
	[PaymentDate] [datetime] NULL,
	[ReceivedDate] [datetime] NULL,
	[IsDuplicate] [bit] NULL,
	[IsExists] [bit] NULL,
	[PaymentMode] [varchar](50) NULL,
	[PaymentModeId] [int] NULL,
	[IsValid] [bit] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TblConsumptionAvgData]    Script Date: 08/07/2015 17:56:29 ******/
CREATE TYPE [dbo].[TblConsumptionAvgData] AS TABLE(
	[SNO] [int] NULL,
	[Global_Account_Number] [varchar](50) NULL,
	[Average_Reading] [varchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TblMeterRadingUploadedData]    Script Date: 08/07/2015 17:56:29 ******/
CREATE TYPE [dbo].[TblMeterRadingUploadedData] AS TABLE(
	[SNO] [int] NULL,
	[Account_Number] [varchar](50) NULL,
	[Current_Reading] [varchar](50) NULL,
	[Read_Date] [datetime] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TblMeterReadingsBulkUpload]    Script Date: 08/07/2015 17:56:29 ******/
CREATE TYPE [dbo].[TblMeterReadingsBulkUpload] AS TABLE(
	[SNO] [varchar](10) NULL,
	[Account_Number] [varchar](50) NULL,
	[Current_Reading] [varchar](50) NULL,
	[Read_Date] [varchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TblMeterReadingsUpload]    Script Date: 08/07/2015 17:56:29 ******/
CREATE TYPE [dbo].[TblMeterReadingsUpload] AS TABLE(
	[SNo] [int] NULL,
	[AccNum] [varchar](50) NULL,
	[TotalReadings] [int] NULL,
	[AverageReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Usage] [varchar](50) NULL,
	[IsTamper] [bit] NULL,
	[PreviousReading] [varchar](50) NULL,
	[IsExists] [bit] NULL,
	[Multiplier] [int] NULL,
	[ReadDate] [datetime] NULL,
	[ReadBy] [varchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TblPaymentsUpload]    Script Date: 08/07/2015 17:56:30 ******/
CREATE TYPE [dbo].[TblPaymentsUpload] AS TABLE(
	[SNO] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[AmountPaid] [decimal](18, 2) NULL,
	[ReceiptNO] [varchar](50) NULL,
	[ReceivedDate] [datetime] NULL,
	[PaymentModeId] [varchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TblPaymentUploads_TempType]    Script Date: 08/07/2015 17:56:30 ******/
CREATE TYPE [dbo].[TblPaymentUploads_TempType] AS TABLE(
	[SNO] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[AmountPaid] [varchar](50) NULL,
	[ReceiptNO] [varchar](50) NULL,
	[ReceivedDate] [varchar](50) NULL,
	[PaymentMode] [varchar](50) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[TypeCustomerAverageUpload]    Script Date: 08/07/2015 17:56:30 ******/
CREATE TYPE [dbo].[TypeCustomerAverageUpload] AS TABLE(
	[SNO] [varchar](20) NULL,
	[Global_Account_Number] [varchar](50) NULL,
	[Average_Reading] [varchar](50) NULL
)
GO
