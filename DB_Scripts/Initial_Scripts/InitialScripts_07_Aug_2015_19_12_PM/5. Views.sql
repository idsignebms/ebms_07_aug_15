GO
/****** Object:  View [dbo].[UDV_CustomerInformation]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_CustomerInformation] 
AS
	SELECT
	  CD.GlobalAccountNumber 
	 ,CD.DocumentNo
	 ,CD.AccountNo
	 ,CD.OldAccountNo
	 ,CD.Title
	 ,CD.FirstName
	 ,CD.MiddleName
	 ,CD.LastName
	 ,CD.KnownAs
	 ,CD.EmployeeCode
	 ,ISNULL(CD.HomeContactNumber,'--') AS HomeContactNumber    
	 ,ISNULL(CD.BusinessContactNumber,'--') AS BusinessContactNumber    
	 ,ISNULL(CD.OtherContactNumber,'--') AS OtherContactNumber
	 ,TD.PhoneNumber
	 ,TD.AlternatePhoneNumber
	 ,CD.ActiveStatusId
	 ,PD.BookNo
	 ,PD.PoleID
	 ,PD.TariffClassID AS TariffClassID
	 ,PD.IsBookNoChanged
	 ,PD.ReadCodeID
	 ,PD.SortOrder
	 ,CAD.Highestconsumption
	 ,CAD.OutStandingAmount
	 ,CD.EmailId
	 ,PD.MeterNumber 
	 ,PD.PhaseId
	 ,PD.ClusterCategoryId
	 ,CAD.InitialReading
	 ,CAD.InitialBillingKWh AS  InitialBillingKWh
	 ,CAD.AvgReading
	 ,PD.RouteSequenceNumber AS RouteSequenceNumber
	 ,CD.ConnectionDate
	 ,CD.CreatedDate
	 ,CD.CreatedBy
	 ,PD.CustomerTypeId
FROM CUSTOMERS.Tbl_CustomersDetail CD
JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD	ON PD.GlobalAccountNumber=CD.GlobalAccountNumber
LEFT JOIN  CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CD.GlobalAccountNumber
LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails TD ON TD.TenentId=CD.TenentId
GO
/****** Object:  View [dbo].[UDV_CustomerDetailsForBillPDF]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_CustomerDetailsForBillPDF] AS  
  
 SELECT   
   CD.GlobalAccountNumber  
  ,CD.AccountNo  
  ,CD.OldAccountNo  
  ,dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName) AS FullName  
  ,ISNULL(CD.HomeContactNumber,ISNULL(CD.BusinessContactNumber,ISNULL(CD.OtherContactNumber,'--'))) AS ContactNo  
  ,CD.ActiveStatusId  
  ,PD.PoleID  
  ,[dbo].[fn_GetCustomerPoleNo_ByPoleId](PD.PoleID) AS PoleNo  
  ,PD.TariffClassID AS TariffId  
  ,TC.ClassName AS TariffName  
  ,TCL.ClassName AS TariffCategory  
  ,PD.ReadCodeID  
  ,CAD.OutStandingAmount  
  ,BN.BookNo 
  ,BN.ID + '(' + BN.BookCode + ')' AS BookName
  ,BU.BU_ID  
  ,BU.BusinessUnitName  
  ,SU.SU_ID  
  ,SU.ServiceUnitName  
  ,SC.ServiceCenterId  
  ,SC.ServiceCenterName  
  ,C.CycleId  
  ,C.CycleName  
  ,CD.EmailId  
  ,PD.MeterNumber  
  ,ISNULL(MI.IsCAPMIMeter,0) AS IsCAPMIMeter  
  ,ISNULL(MI.CAPMIAmount,0) AS CAPMIAmount  
  ,PD.PhaseId  
  ,PH.Phase  
  ,CAD.InitialBillingKWh   
  ,PD.RouteSequenceNumber  
  ,R.RouteName    
  ,PD.CustomerTypeId  
  ,CT.CustomerType  
  ,CAD.OpeningBalance  
  ,[dbo].[fn_GetCustomerAddressFormat](PAD.HouseNo,PAD.StreetName,PAD.City,PAD.ZipCode) AS ServiceAddress  
  ,(CASE WHEN ISNULL(PAD.IsCommunication,0) = 1   
    THEN [dbo].[fn_GetCustomerAddressFormat](PAD.HouseNo,PAD.StreetName,PAD.City,PAD.ZipCode)  
    ELSE (SELECT [dbo].[fn_GetCustomerAddressFormat](PAD2.HouseNo,PAD2.StreetName,PAD2.City,PAD2.ZipCode)  
       FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails AS PAD2   
       WHERE PAD2.AddressID = CD.PostalAddressID)  
    END) AS BillingAddress  
 FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
 LEFT JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber   
 LEFT JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber  
 LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId  
 LEFT JOIN CUSTOMERS.Tbl_CustomerPostalAddressDetails AS PAD ON PAD.AddressID = CD.ServiceAddressID   
 INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo   
 INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId   
 INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
 INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID   
 INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID   
 INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID   
 LEFT JOIN dbo.Tbl_MTariffClasses AS TCL ON TCL.ClassID = TC.RefClassID   
 LEFT JOIN dbo.Tbl_MActiveStatusDetails AS MS ON CD.ActiveStatusId = MS.ActiveStatusId   
 LEFT JOIN dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo  
 LEFT JOIN dbo.Tbl_MCustomerTypes AS CT ON CT.CustomerTypeId = PD.CustomerTypeId  
 LEFT JOIN dbo.Tbl_MRoutes AS R ON R.RouteId = PD.RouteSequenceNumber  
 LEFT JOIN dbo.Tbl_MPhases AS PH ON PH.PhaseId = PD.PhaseId  
 WHERE PAD.IsActive = 1
GO
/****** Object:  View [dbo].[UDV_CustomerDescription]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_CustomerDescription]    
AS    
SELECT     CD.GlobalAccountNumber, CD.DocumentNo, CD.AccountNo, CD.OldAccountNo, CD.Title, CD.FirstName, CD.MiddleName, CD.LastName, CD.KnownAs,     
                      CD.EmployeeCode, ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo, ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo,     
                      ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo, TD.PhoneNumber, TD.AlternatePhoneNumber, CD.ActiveStatusId, PD.PoleID, PD.TariffClassID AS TariffId,     
                      TC.ClassName, PD.IsBookNoChanged, PD.ReadCodeID, PD.SortOrder, CAD.Highestconsumption, CAD.OutStandingAmount, BN.BookNo, BN.BookCode, BU.BU_ID,     
                      BU.BusinessUnitName, BU.BUCode, SU.SU_ID, SU.ServiceUnitName, SU.SUCode, SC.ServiceCenterId, SC.ServiceCenterName, SC.SCCode, C.CycleId, C.CycleName,     
                      C.CycleCode, MS.StatusName AS ActiveStatus, CD.EmailId, PD.MeterNumber, MI.MeterType AS MeterTypeId, PD.PhaseId, PD.ClusterCategoryId, CAD.InitialReading,     
                      CAD.InitialBillingKWh AS MinimumReading, CAD.AvgReading, PD.RouteSequenceNumber AS RouteSequenceNo, CD.ConnectionDate, CD.CreatedDate, CD.CreatedBy,CAD.PresentReading,     
                      PD.CustomerTypeId, C.ActiveStatusId AS CylceActiveStatusId, CD.ServiceAddressID    
                      ,CD.Service_HouseNo       
   ,CD.Service_StreetName        
   ,CD.Service_City       
   ,CD.Service_ZipCode               
        
   ,CD.Postal_ZipCode    
   ,CD.Postal_HouseNo       
   ,CD.Postal_StreetName        
   ,CD.Postal_City    
   ,CD.Postal_Landmark    
   ,Cd.Service_Landmark    
   ,Cd.ApplicationDate    
   ,Cd.SetupDate    
   ,CD.TenentId    
   ,CD.IsVIPCustomer    
   ,IsCAPMI    
   ,CD.IsBEDCEmployee    
   ,MeterAmount    
   ,MS.StatusId as CustomerStatusID    
   ,PD.IsEmbassyCustomer    
   ,BN.ID AS BookId
   ,BN.SortOrder AS BookSortOrder  
   ,BN.Details AS BookDetails  
   ,CD.IsSameAsService
       
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN    
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN    
                      CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN    
                      CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId INNER JOIN    
                      dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo INNER JOIN    
                      dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId INNER JOIN    
                      dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId INNER JOIN    
                      dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID INNER JOIN    
                      dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID INNER JOIN    
                      dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID LEFT OUTER JOIN    
                      dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId LEFT OUTER JOIN    
                      dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo    
    
    
    
    
----------------------------------------------------------------------------------------------------
GO
/****** Object:  View [dbo].[UDV_CustDetailsForBillGen]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_CustDetailsForBillGen] AS

SELECT		
 
			CD.GlobalAccountNumber
			--, CD.DocumentNo
			--, CD.AccountNo
			, CD.OldAccountNo
			, CD.Title
			, CD.FirstName
			, CD.MiddleName
			, CD.LastName
			--, CD.KnownAs
			--, CD.EmployeeCode
			--, ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo, ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo
			, ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo
			--, TD.PhoneNumber
			--, TD.AlternatePhoneNumber
			, CD.ActiveStatusId
			, PD.PoleID
			, PD.TariffClassID AS TariffId
			--, TC.ClassName
			--, PD.IsBookNoChanged
			 , PD.ReadCodeID
			, PD.SortOrder
			--, CAD.Highestconsumption
			, CAD.OutStandingAmount
			, BN.BookNo
			--, BN.BookCode
			, BU.BU_ID
			, BU.BusinessUnitName
			--, BU.BUCode
			, SU.SU_ID
			--, SU.ServiceUnitName
			--, SU.SUCode
			, SC.ServiceCenterId
			--, SC.ServiceCenterName
			--, SC.SCCode
			, C.CycleId
			--, C.CycleName
			--, C.CycleCode
			--, MS.[Status] AS ActiveStatus
			, CD.EmailId
			, PD.MeterNumber
			--, MI.MeterType AS MeterTypeId
			--, PD.PhaseId
			--, PD.ClusterCategoryId
			--, CAD.InitialReading
			, CAD.InitialBillingKWh 
			--, CAD.AvgReading
			--, PD.RouteSequenceNumber AS RouteSequenceNo
			--, CD.ConnectionDate
			--, CD.CreatedDate
			--, CD.CreatedBy
			, PD.CustomerTypeId
			,PD.ClusterCategoryId
			,PD.IsEmbassyCustomer
			,CAD.Highestconsumption
			,CD.ServiceAddressID
			,CD.PostalAddressID
			 
			,CAD.OpeningBalance
			,CD.Service_HouseNo   
			,CD.Service_StreetName    
			,CD.Service_City   
			,CD.Service_ZipCode           
			,TC.ClassName  
			,TC.ClassID
			,CD.Postal_ZipCode
			,CD.Postal_HouseNo   
			,CD.Postal_StreetName    
			,CD.Postal_City
			,CD.Postal_Landmark
			,Cd.Service_Landmark 
			,CD.SetupDate   
			,CD.ConnectionDate
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber 
                      LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber 
                      LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId 
                      INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo 
                      INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
                      INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
                      INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
                      INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
                      INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID 
                      LEFT OUTER JOIN dbo.Tbl_MActiveStatusDetails AS MS ON CD.ActiveStatusId = MS.ActiveStatusId 
                      LEFT OUTER JOIN dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo
GO
/****** Object:  View [dbo].[UDV_BookNumberDetails]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_BookNumberDetails]  
AS  
 SELECT   
 BU.BU_ID,BU.BUCode,BU.BusinessUnitName  
 ,SU.SU_ID,SU.SUCode,SU.ServiceUnitName  
 ,SC.ServiceCenterId,SC.SCCode,SC.ServiceCenterName  
 ,C.CycleId,C.CycleCode,C.CycleName  
 ,B.BookNo,B.BookCode,B.ID
 ,M.MarketerId
 ,B.SortOrder as BookSortOrder
 ,dbo.fn_GetMarketersFullName_New(M.FirstName,M.MiddleName,M.LastName) AS MarketerName   
 FROM Tbl_BookNumbers B 
 INNER JOIN Tbl_Cycles C ON C.CycleId=B.CycleId  
 INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId  
 INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID  
 INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = SU.BU_ID
 LEFT JOIN Tbl_Marketers M ON M.MarketerId = B.MarketerId
GO
/****** Object:  View [dbo].[UDV_PrebillingRpt]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_PrebillingRpt]    
  
AS   
 SELECT CD.GlobalAccountNumber  
 ,CD.AccountNo  
 ,CD.OldAccountNo  
 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName  
 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
             ,CD.Service_Landmark  
             ,CD.Service_City,'',  
             CD.Service_ZipCode) AS [ServiceAddress]   
   ,PD.SortOrder  
   ,BN.SortOrder AS BookSortOrder  
   ,CD.ConnectionDate  
   ,PD.PoleID    
   ,PD.TariffClassID AS TariffId    
   ,TC.ClassName    
   ,PD.ReadCodeID    
   ,BN.BookNo    
   ,BN.BookCode  
   ,BN.ID AS BookId    
   ,BU.BU_ID    
   ,BU.BusinessUnitName    
   ,BU.BUCode    
   ,SU.SU_ID    
   ,SU.ServiceUnitName    
   ,SU.SUCode    
   ,SC.ServiceCenterId    
   ,SC.ServiceCenterName    
   ,SC.SCCode    
   ,C.CycleId    
   ,C.CycleName    
   ,C.CycleCode  
   ,Pd.MeterNumber
   ,PD.ActiveStatusId   
   ,CAD.InitialBillingKWh as DeafaultUsage  
   ,CustomerStatus.StatusId as CustomerStatusID  
   ,PD.IsEmbassyCustomer
   , BDB.DisableTypeId  as BoolDisableTypeId
   ,BDB.IsPartialBill  as IsPartialBook
   ,InitialBillingKWh
   ,CustomerTypeId
      
  FROM CUSTOMERS.Tbl_CustomerSDetail(NOLOCK) AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber  
  INNER Join CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber  
  INNER JOIN  dbo.Tbl_BookNumbers(NOLOCK) AS BN ON BN.BookNo = PD.BookNo   
  INNER JOIN  dbo.Tbl_Cycles(NOLOCK) AS C ON C.CycleId = BN.CycleId   
  INNER JOIN  dbo.Tbl_ServiceCenter(NOLOCK) AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
  INNER JOIN  dbo.Tbl_ServiceUnits(NOLOCK) AS SU ON SU.SU_ID = SC.SU_ID   
  INNER JOIN  dbo.Tbl_BussinessUnits(NOLOCK) AS BU ON BU.BU_ID = SU.BU_ID  
  INNER JOIN dbo.Tbl_MTariffClasses(NOLOCK) AS TC ON PD.TariffClassID = TC.ClassID  
  INNER JOIn Tbl_MCustomerStatus(NOLOCK) AS CustomerStatus ON   CD.ActiveStatusId = CustomerStatus.StatusId
  LEFT JOIN Tbl_BillingDisabledBooks(NOLOCK) BDB ON BDB.BookNo=Pd.BookNo  and IsActive=1 
----------------------------------------------------------------------------------------------------
GO
/****** Object:  View [dbo].[UDV_IsCustomerExists]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_IsCustomerExists]  
AS  
 SELECT CD.GlobalAccountNumber,  
  CD.OldAccountNo,  
  PD.MeterNumber  
  ,CD.ActiveStatusId  
  ,SU.BU_ID  
  ,PD.ReadCodeID  
  ,PD.BookNo  
  ,PD.CustomerTypeId
 FROM CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN  
         CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber  
         INNER JOIN Tbl_BookNumbers BN ON PD.BookNo=BN.BookNo  
         INNER JOIN Tbl_Cycles CY ON CY.CycleId=BN.CycleId  
         INNER JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=CY.ServiceCenterId  
         INNER JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
GO
/****** Object:  View [dbo].[UDV_EditListReport]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_EditListReport]    
AS         
  
 SELECT   
	 CD.GlobalAccountNumber  
	 ,CD.AccountNo  
	 ,CD.OldAccountNo  
	 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName  
	 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
				 ,CD.Service_Landmark  
				 ,CD.Service_City,'',  
				CD.Service_ZipCode) AS [ServiceAddress]   
	 ,PD.SortOrder  
	 ,BN.SortOrder AS BookSortOrder  
	 ,PD.TariffClassID AS TariffId    
	 ,C.CycleId    
	 ,C.CycleName    
	 ,C.CycleCode  
	 ,BU.BU_ID
	 ,BU.BusinessUnitName
	 ,PD.MeterNumber
  FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber  
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo   
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId   
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID   
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID  
   
 ----------------------------------------------------------------------------------------------------
GO
/****** Object:  View [dbo].[UDV_CustomerPDFReport]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_CustomerPDFReport]    
AS    
SELECT     CD.GlobalAccountNumber, CD.AccountNo, CD.OldAccountNo, 
					(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName, CD.KnownAs, 
                      PD.TariffClassID AS TariffId,     
                      TC.ClassName, PD.ReadCodeID, PD.SortOrder, CAD.OutStandingAmount, BN.BookNo, BN.BookCode, BU.BU_ID,     
                      BU.BusinessUnitName, BU.BUCode, SU.SU_ID, SU.ServiceUnitName, SU.SUCode, SC.ServiceCenterId, SC.ServiceCenterName, SC.SCCode, C.CycleId, C.CycleName,     
                      C.CycleCode, MS.StatusName AS ActiveStatus, PD.MeterNumber, PD.ClusterCategoryId, CAD.InitialReading,     
                      CAD.InitialBillingKWh AS MinimumReading, CAD.AvgReading, CD.ConnectionDate, CAD.PresentReading,     
                      PD.CustomerTypeId, CD.ServiceAddressID   
   ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName  
				 ,CD.Service_Landmark  
				 ,CD.Service_City,'',  
				CD.Service_ZipCode) AS [ServiceAddress] 
   ,BN.ID AS BookId
   ,BN.SortOrder AS BookSortOrder  
   ,BN.Details AS BookDetails        
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN    
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber LEFT OUTER JOIN    
                      CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber INNER JOIN    
                      dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo INNER JOIN    
                      dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId INNER JOIN    
                      dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId INNER JOIN    
                      dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID INNER JOIN    
                      dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID INNER JOIN    
                      dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID LEFT OUTER JOIN    
                      dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId 
    
    
    
    
----------------------------------------------------------------------------------------------------
GO
/****** Object:  View [dbo].[UDV_RptCustomerDesc]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_RptCustomerDesc]  
AS  
 SELECT       
    CD.GlobalAccountNumber  
   ,CD.DocumentNo  
   ,CD.AccountNo  
   ,CD.OldAccountNo  
   ,CD.Title  
   ,CD.FirstName  
   ,CD.MiddleName  
   ,CD.LastName  
   ,CD.KnownAs  
   ,CD.EmployeeCode  
   ,ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo  
   ,ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo  
   ,ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo  
   ,TD.PhoneNumber  
   ,TD.AlternatePhoneNumber  
   ,CD.ActiveStatusId  
   ,PD.PoleID  
   ,PD.TariffClassID AS TariffId  
   ,TC.ClassName  
   ,PD.IsBookNoChanged  
   ,PD.ReadCodeID  
   ,PD.SortOrder  
   ,CAD.Highestconsumption  
   ,CAD.OutStandingAmount  
   ,BN.BookNo  
   ,BN.BookCode
   ,BN.ID AS BookId    
   ,BU.BU_ID  
   ,BU.BusinessUnitName  
   ,BU.BUCode  
   ,SU.SU_ID  
   ,SU.ServiceUnitName  
   ,SU.SUCode  
   ,SC.ServiceCenterId  
   ,SC.ServiceCenterName  
   ,SC.SCCode  
   ,C.CycleId  
   ,C.CycleName  
   ,C.CycleCode  
   ,MS.StatusName AS ActiveStatus  
   ,CD.EmailId  
   ,PD.MeterNumber  
   ,MI.MeterType AS MeterTypeId  
   ,PD.PhaseId  
   ,PD.ClusterCategoryId  
   ,CAD.InitialReading  
   ,CAD.InitialBillingKWh AS MinimumReading  
   ,CAD.AvgReading  
   ,PD.RouteSequenceNumber AS RouteSequenceNo  
   ,CD.ConnectionDate  
   ,CD.CreatedDate  
   ,CD.CreatedBy  
   ,CAD.PresentReading  
   ,PD.CustomerTypeId  
   ,C.ActiveStatusId AS CylceActiveStatusId  
   ,CD.ServiceAddressID  
   ,Service_HouseNo  
   ,Service_StreetName  
   ,Service_Landmark  
   ,Service_City  
   ,Service_ZipCode  
   ,Postal_HouseNo  
   ,Postal_StreetName  
   ,Postal_Landmark  
   ,Postal_City  
   ,Postal_ZipCode  
   ,BN.SortOrder AS BookSortOrder  
   ,TC.RefClassID  
  FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber   
  LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber   
  LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId   
  INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo   
  LEFT JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId   
  LEFT JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId   
  LEFT JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID   
  LEFT JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID   
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID   
  LEFT OUTER JOIN dbo.Tbl_MCustomerStatus AS MS ON CD.ActiveStatusId = MS.StatusId   
  LEFT OUTER JOIN dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo  
------------------------------------------------------------------------------------------------------
GO
/****** Object:  View [dbo].[UDV_RptCustomerBookInfo]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_RptCustomerBookInfo]  
AS  
 SELECT       
    PD.GlobalAccountNumber  
   ,PD.PoleID  
   ,PD.TariffClassID AS TariffId  
   ,PD.ClusterCategoryId 
   ,TC.ClassName  
   ,PD.ReadCodeID  
   ,CD.ActiveStatusId 
   ,BN.BookNo  
   ,BN.BookCode
   ,BN.ID AS BookId  
   ,BU.BU_ID  
   ,BU.BusinessUnitName  
   ,BU.BUCode  
   ,SU.SU_ID  
   ,SU.ServiceUnitName  
   ,SU.SUCode  
   ,SC.ServiceCenterId  
   ,SC.ServiceCenterName  
   ,SC.SCCode  
   ,C.CycleId  
   ,C.CycleName  
   ,C.CycleCode
  FROM CUSTOMERS.Tbl_CustomerSDetail CD
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON CD.GlobalAccountNumber=PD.GlobalAccountNumber
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo 
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID
GO
/****** Object:  View [dbo].[UDV_RptCustomerAndBookInfo]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_RptCustomerAndBookInfo]      
AS      
 SELECT CD.GlobalAccountNumber    
 ,CD.AccountNo    
 ,CD.OldAccountNo    
 ,(dbo.fn_GetCustomerFullName_New(CD.Title,CD.FirstName,CD.MiddleName,CD.LastName)) AS CustomerFullName    
 ,dbo.fn_GetCustomerServiceAddress_New( Cd.Service_HouseNo,CD.Service_StreetName    
             ,CD.Service_Landmark    
             ,CD.Service_City,'',    
             CD.Service_ZipCode) AS [ServiceAddress]     
   ,PD.SortOrder    
   ,BN.SortOrder AS BookSortOrder    
   ,CD.ConnectionDate    
   ,PD.PoleID      
   ,PD.TariffClassID AS TariffId      
   ,TC.ClassName      
   ,PD.ReadCodeID      
   ,BN.BookNo      
   ,BN.BookCode    
   ,BN.ID AS BookId      
   ,BU.BU_ID      
   ,BU.BusinessUnitName      
   ,BU.BUCode      
   ,SU.SU_ID      
   ,SU.ServiceUnitName      
   ,SU.SUCode      
   ,SC.ServiceCenterId      
   ,SC.ServiceCenterName      
   ,SC.SCCode      
   ,C.CycleId      
   ,C.CycleName      
   ,C.CycleCode 
   ,PD.MeterNumber
   ,CA.OutStandingAmount
   ,CA.AvgReading
   ,CA.InitialReading
   ,CA.PresentReading
   ,CA.InitialBillingKWh
  FROM CUSTOMERS.Tbl_CustomerSDetail AS CD
  INNER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CA ON CA.GlobalAccountNumber=CD.GlobalAccountNumber
  INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber    
  INNER JOIN  dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo     
  INNER JOIN  dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId     
  INNER JOIN  dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId     
  INNER JOIN  dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID     
  INNER JOIN  dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID    
  INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID     
----------------------------------------------------------------------------------------------------
GO
/****** Object:  View [dbo].[UDV_ServiceCenterDetails]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_ServiceCenterDetails]
AS
	SELECT 
	SC.ServiceCenterId,SC.SCCode,SC.ServiceCenterName
	,SU.SU_ID,SU.SUCode,SU.ServiceUnitName
	,BU.BU_ID,BU.BUCode,BU.BusinessUnitName
	FROM Tbl_ServiceCenter SC 
	JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
	JOIN Tbl_BussinessUnits BU ON BU.BU_ID = SU.BU_ID
GO
/****** Object:  View [dbo].[UDV_SearchCustomer]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_SearchCustomer]     
AS    
   
 SELECT    DISTINCT
  CPD.GlobalAccountNumber,
  (CD.AccountNo+' - '+CPD.GlobalAccountNumber) AS AccNoAndGlobalAccNo,
  CD.AccountNo,  
  CD.OldAccountNo,  
  dbo.fn_GetCustomerFullName_New(   
    CD.Title,    
    CD.FirstName,    
    CD.MiddleName,    
    CD.LastName) AS Name,   
  CD.HomeContactNumber,  
  BusinessContactNumber,  
  CD.OtherContactNumber,   
  BookDetails.BusinessUnitName,  
  Dbo.fn_GetCustomerServiceAddress_New(Service_HouseNo,Service_StreetName,Service_Landmark,Service_City,'',Service_ZipCode) As ServiceAddress,  
  BookDetails.ServiceUnitName,  
  BookDetails.ServiceCenterName,  
  TariffClasses.ClassName,     
  CStatus.StatusName,  
  CPD.IsBookNoChanged,  
  CPD.MeterNumber,  
  CD.ActiveStatusId  
  ,BookDetails.BU_ID  
  ,BookDetails.SU_ID  
  ,BookDetails.ServiceCenterId  
  ,BookDetails.CycleId  
  ,CPD.TariffClassID as TariffId  
  ,ReadCodeId  
  ,CAD.CreatedDate  
  ,BookDetails.BookCode  
  ,BookDetails.ID
  ,BookDetails.BookNo  
  ,BookDetails.BookSortOrder  
  ,CPD.SortOrder  
  ,CAD.InitialReading
  ,BookDetails.CycleName  
 FROM CUSTOMERS.Tbl_CustomerActiveDetails(NOLOCK) CAD    
 INNER JOIN CUSTOMERS.Tbl_CustomerProceduralDetails(NOLOCK) CPD ON CAD.GlobalAccountNumber=CPD.GlobalAccountNumber  
 INNER JOIN CUSTOMERS.Tbl_CustomersDetail(NOLOCK) CD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber  
 INNER JOIN  dbo.UDV_BookNumberDetails(NOLOCK) BookDetails ON BookDetails.BookNo=CPD.BookNo  
 INNER JOIN  Tbl_MCustomerStatus(NOLOCK) CStatus ON CStatus.StatusId=CD.ActiveStatusId  
 INNER JOIN  Tbl_MTariffClasses(NOLOCK) TariffClasses ON TariffClasses.ClassID=CPD.TariffClassID 
 INNER JOIN TBL_BussinessUnits AS BU ON BookDetails.BU_ID =BU.BU_ID AND BU.ActiveStatusId=1-- Neeraj ID-077 18-May-15
GO
/****** Object:  View [dbo].[UDV_ReadCustomerDetailsWithMeterDetails]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[UDV_ReadCustomerDetailsWithMeterDetails]   
AS
  
 SELECT  
   CD.GlobalAccountNumber   
  ,CD.AccountNo  
  ,CD.OldAccountNo  
  ,CD.Title  
  ,CD.FirstName  
  ,CD.MiddleName  
  ,CD.LastName  
  ,CD.KnownAs  
  ,CD.ActiveStatusId  
  ,PD.TariffClassID AS TariffId  
  ,TC.ClassName  
  ,PD.ReadCodeID  
  ,PD.SortOrder  
  ,AD.Highestconsumption  
  ,BN.BookNo  
  ,BN.BookCode  
  ,BN.CycleId  
  ,BN.CycleCode  
  ,BN.CycleName  
  ,BN.SCCode  
  ,BN.ServiceCenterId  
  ,BN.ServiceCenterName  
  ,BN.SUCode  
  ,BN.ServiceUnitName  
  ,BN.SU_ID  
  ,BN.BUCode  
  ,BN.BU_ID  
  ,BN.BusinessUnitName  
  ,MS.[StatusName] AS ActiveStatus  
  ,CD.EmailId  
  ,PD.MeterNumber   
  ,MI.MeterType AS MeterTypeId  
  ,PD.PhaseId  
  ,AD.InitialReading   
  ,AD.PresentReading   
  ,AD.AvgReading  
  ,PD.RouteSequenceNumber AS RouteSequenceNo  
  ,PD.CustomerTypeId  
  ,CD.CreatedDate  
  ,CD.CreatedBy  
  ,CD.ModifiedDate  
  ,CD.ModifedBy   
FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
INNER JOIN  CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber 
and Pd.ReadCodeID=2
ANd PD.ActiveStatusId=1 -- ReadCustomers
INNER JOIN  CUSTOMERS.Tbl_CustomerActiveDetails AS AD ON AD.GlobalAccountNumber = CD.GlobalAccountNumber	
INNER JOIN  UDV_BookNumberDetails BN ON BN.BookNo=PD.BookNo   
INNER JOIN Tbl_MTariffClasses AS TC ON PD.TariffClassID=TC.ClassID  
INNER JOIN  Tbl_MCustomerStatus MS ON CD.ActiveStatusId=MS.StatusId   
Inner JOIN Tbl_MeterInformation MI ON PD.MeterNumber=MI.MeterNo  and MI.MeterDials >0
GO
/****** Object:  View [dbo].[UDV_CustomerMeterInformation]    Script Date: 08/07/2015 19:03:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_CustomerMeterInformation]   
AS  
 SELECT  
   CD.GlobalAccountNumber   
  ,CD.AccountNo  
  ,CD.OldAccountNo  
  ,CD.Title  
  ,CD.FirstName  
  ,CD.MiddleName  
  ,CD.LastName  
  ,CD.KnownAs  
  ,CD.ActiveStatusId  
  ,PD.TariffClassID AS TariffId  
  ,TC.ClassName  
  ,PD.ReadCodeID  
  ,PD.SortOrder  
  ,AD.Highestconsumption  
  ,BN.BookNo  
  ,BN.BookCode  
  ,BN.CycleId  
  ,BN.CycleCode  
  ,BN.CycleName  
  ,BN.SCCode  
  ,BN.ServiceCenterId  
  ,BN.ServiceCenterName  
  ,BN.SUCode  
  ,BN.ServiceUnitName  
  ,BN.SU_ID  
  ,BN.BUCode  
  ,BN.BU_ID  
  ,BN.BusinessUnitName  
  ,MS.[StatusName] AS ActiveStatus  
  ,CD.EmailId  
  ,PD.MeterNumber   
  ,MI.MeterType AS MeterTypeId  
  ,PD.PhaseId  
  ,AD.InitialReading   
 ,AD.PresentReading   
  ,AD.AvgReading  
  ,PD.RouteSequenceNumber AS RouteSequenceNo  
  ,PD.CustomerTypeId  
 ,CD.CreatedDate  
 ,CD.CreatedBy  
 ,CD.ModifiedDate  
 ,CD.ModifedBy 
 ,MI.MeterDials
 ,MI.Decimals
 ,BN.MarketerId
 ,BN.MarketerName
 ,MI.MeterMultiplier
FROM CUSTOMERS.Tbl_CustomerSDetail AS CD   
INNER JOIN  CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber   
INNER JOIN  CUSTOMERS.Tbl_CustomerActiveDetails AS AD ON AD.GlobalAccountNumber = CD.GlobalAccountNumber   
INNER JOIN  UDV_BookNumberDetails BN ON BN.BookNo=PD.BookNo   
INNER JOIN Tbl_MTariffClasses AS TC ON PD.TariffClassID=TC.ClassID  
INNER JOIN  Tbl_MCustomerStatus MS ON CD.ActiveStatusId=MS.StatusId   
LEFT JOIN Tbl_MeterInformation MI ON PD.MeterNumber=MI.MeterNo
GO
