GO
/****** Object:  Table [dbo].[Tbl_MReadCodes]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MReadCodes](
	[ReadCodeId] [int] IDENTITY(1,1) NOT NULL,
	[ReadCode] [varchar](100) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[DisplayCode] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[ReadCodeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MReadCodes] ON
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [ActiveStatusId], [DisplayCode]) VALUES (1, N'Direct', NULL, 1, N'D')
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [ActiveStatusId], [DisplayCode]) VALUES (2, N'Read', NULL, 1, N'R')
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [ActiveStatusId], [DisplayCode]) VALUES (3, N'Estimate', NULL, 1, N'E')
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [ActiveStatusId], [DisplayCode]) VALUES (4, N'Minimum', NULL, 1, N'M')
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [ActiveStatusId], [DisplayCode]) VALUES (5, N'Average', NULL, 1, N'A')
INSERT [dbo].[Tbl_MReadCodes] ([ReadCodeId], [ReadCode], [Details], [ActiveStatusId], [DisplayCode]) VALUES (6, N'High Consumption', NULL, 1, N'HC')
SET IDENTITY_INSERT [dbo].[Tbl_MReadCodes] OFF
/****** Object:  Table [dbo].[Tbl_MPoleMasterDetails]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MPoleMasterDetails](
	[PoleMasterId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Description] [varchar](150) NULL,
	[PoleMasterOrderID] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PoleMasterCodeLength] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PoleMasterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MPoleMasterDetails] ON
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1007, N'TCN', N'TCN - 02', 1, 1, N'Admin', CAST(0x0000A40A00F08BEC AS DateTime), NULL, NULL, 2)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1008, N'TCN Power TR', N'TCN Power TR - 01', 2, 1, N'Admin', CAST(0x0000A40A00F153E4 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1009, N'33 KV Feeder', N'33 KV Feeder - 01', 3, 1, N'Admin', CAST(0x0000A40A00F17518 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1010, N'33/11 Inj. S/S', N'33/11 Inj. S/S - 02', 4, 1, N'Admin', CAST(0x0000A40A00F197CC AS DateTime), NULL, NULL, 2)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1011, N'33/11 Power TR', N'33/11 Power TR - 01', 5, 1, N'Admin', CAST(0x0000A40A00F1B849 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1012, N'11 KV Feeder', N'11 KV Feeder - 01', 6, 1, N'Admin', CAST(0x0000A40A00F1CE59 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1013, N'11/0.45KV TR', N'11/0.45KV TR - 02', 7, 1, N'Admin', CAST(0x0000A40A00F1EA1D AS DateTime), NULL, NULL, 3)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1014, N'LV Upraiser', N'LV Upraiser - 01', 8, 1, N'Admin', CAST(0x0000A40A00F20661 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_MPoleMasterDetails] ([PoleMasterId], [Name], [Description], [PoleMasterOrderID], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [PoleMasterCodeLength]) VALUES (1015, N'Pole No', N'Pole No – 03', 9, 1, N'Admin', CAST(0x0000A40A00F21B27 AS DateTime), N'Admin', CAST(0x0000A434011016FA AS DateTime), 3)
SET IDENTITY_INSERT [dbo].[Tbl_MPoleMasterDetails] OFF
/****** Object:  Table [dbo].[Tbl_MPhases]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MPhases](
	[PhaseId] [int] IDENTITY(1,1) NOT NULL,
	[Phase] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[PhaseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MPhases] ON
INSERT [dbo].[Tbl_MPhases] ([PhaseId], [Phase], [Details], [ActiveStatus]) VALUES (1, N'Single Phase','S', 1)
INSERT [dbo].[Tbl_MPhases] ([PhaseId], [Phase], [Details], [ActiveStatus]) VALUES (2, N'Three Phase','T', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MPhases] OFF
/****** Object:  Table [dbo].[Tbl_MPaymentMode]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MPaymentMode](
	[PaymentModeId] [int] IDENTITY(1,1) NOT NULL,
	[PaymentMode] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[IsDefault] [bit] NULL,
 CONSTRAINT [PK_Table_MPaymentMode] PRIMARY KEY CLUSTERED 
(
	[PaymentModeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MPaymentMode] ON
INSERT [dbo].[Tbl_MPaymentMode] ([PaymentModeId], [PaymentMode], [ActiveStatusId], [IsDefault]) VALUES (1, N'Cash', 1, 1)
INSERT [dbo].[Tbl_MPaymentMode] ([PaymentModeId], [PaymentMode], [ActiveStatusId], [IsDefault]) VALUES (2, N'Cheque', 1, NULL)
INSERT [dbo].[Tbl_MPaymentMode] ([PaymentModeId], [PaymentMode], [ActiveStatusId], [IsDefault]) VALUES (3, N'DD', 1, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_MPaymentMode] OFF
/****** Object:  Table [dbo].[Tbl_MOrganizationTypes]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MOrganizationTypes](
	[OrganizationTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](300) NULL,
	[Details] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[OrganizationTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_MOrganizations]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MOrganizations](
	[OrganizationCode] [varchar](50) NOT NULL,
	[NameOfOrganization] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[Contact1] [varchar](20) NULL,
	[Contact2] [varchar](20) NULL,
	[EmailId] [varchar](500) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ActiveStatusId] [int] NULL,
	[TypeOfOrganizationId] [int] NULL,
	[OrganizationId] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[OrganizationCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_MMeterTypes]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MMeterTypes](
	[MeterTypeId] [int] IDENTITY(1,1) NOT NULL,
	[MeterType] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatus] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsMaster] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MMeterTypes] ON
INSERT [dbo].[Tbl_MMeterTypes] ([MeterTypeId], [MeterType], [Details], [ActiveStatus], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [IsMaster]) VALUES (1, N'Prepaid Meter', NULL, 1, N'Admin', NULL, NULL, CAST(0x0000A41100CAEAED AS DateTime), 1)
INSERT [dbo].[Tbl_MMeterTypes] ([MeterTypeId], [MeterType], [Details], [ActiveStatus], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [IsMaster]) VALUES (2, N'Credit Meter', N'sfdsfdsfsfd', 1, N'Admin', N'Admin', NULL, CAST(0x0000A42F00DB3046 AS DateTime), 1)
INSERT [dbo].[Tbl_MMeterTypes] ([MeterTypeId], [MeterType], [Details], [ActiveStatus], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [IsMaster]) VALUES (3, N'Bulk Meter', N'sfdsfdsfsfds', 1, N'Admin', N'Admin', NULL, CAST(0x0000A42F00DB3A4F AS DateTime), 1)
INSERT [dbo].[Tbl_MMeterTypes] ([MeterTypeId], [MeterType], [Details], [ActiveStatus], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [IsMaster]) VALUES (4, N'Digital Meter', N'New Digital Meter.', 1, N'admin', N'Admin', CAST(0x0000A41800E1C5E5 AS DateTime), CAST(0x0000A43101510AD0 AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Tbl_MMeterTypes] OFF
/****** Object:  Table [dbo].[Tbl_MMeterStatus]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MMeterStatus](
	[MeterStatusId] [int] IDENTITY(1,1) NOT NULL,
	[MeterStatus] [varchar](100) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MMeterStatus] ON
INSERT [dbo].[Tbl_MMeterStatus] ([MeterStatusId], [MeterStatus], [Details], [ActiveStatusId]) VALUES (1, N'New Meter', NULL, 1)
INSERT [dbo].[Tbl_MMeterStatus] ([MeterStatusId], [MeterStatus], [Details], [ActiveStatusId]) VALUES (2, N'Old Meter', NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MMeterStatus] OFF
/****** Object:  Table [dbo].[Tbl_MLGA]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MLGA](
	[LGAId] [int] IDENTITY(1,1) NOT NULL,
	[LGAName] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ActiveStatusId] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MLGA] ON
INSERT [dbo].[Tbl_MLGA] ([LGAId], [LGAName], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ActiveStatusId]) VALUES (1, N'LGA', N'Admin', CAST(0x0000A41500DDA3D8 AS DateTime), N'Admin', CAST(0x0000A42800DF623E AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Tbl_MLGA] OFF
/****** Object:  Table [dbo].[Tbl_MIdentityTypes]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MIdentityTypes](
	[IdentityId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdentityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MIdentityTypes] ON
INSERT [dbo].[Tbl_MIdentityTypes] ([IdentityId], [Type], [Details], [ActiveStatusId]) VALUES (1, N'License', NULL, 1)
INSERT [dbo].[Tbl_MIdentityTypes] ([IdentityId], [Type], [Details], [ActiveStatusId]) VALUES (2, N'Passport', NULL, 1)
INSERT [dbo].[Tbl_MIdentityTypes] ([IdentityId], [Type], [Details], [ActiveStatusId]) VALUES (3, N'PanCard', NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MIdentityTypes] OFF
/****** Object:  Table [dbo].[Tbl_MGovtAccountTypes]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MGovtAccountTypes](
	[GActTypeID] [int] IDENTITY(1,1) NOT NULL,
	[AccountType] [varchar](100) NULL,
	[ActiveStatusId] [int] NULL,
	[Details] [varchar](max) NULL,
	[RefActTypeID] [int] NULL,
	[AccountCode] [varchar](10) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[GActTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MGovtAccountTypes] ON
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [ActiveStatusId], [Details], [RefActTypeID], [AccountCode], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Govt', 1, N'', NULL, N'', NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [ActiveStatusId], [Details], [RefActTypeID], [AccountCode], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Fedral', 1, N'Under Govt', 1, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MGovtAccountTypes] ([GActTypeID], [AccountType], [ActiveStatusId], [Details], [RefActTypeID], [AccountCode], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate]) VALUES (3, N'Local', 1, N'Under Govt', 1, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_MGovtAccountTypes] OFF
/****** Object:  Table [dbo].[Tbl_MGender]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MGender](
	[GenderId] [int] IDENTITY(1,1) NOT NULL,
	[GenderName] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[GenderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MGender] ON
INSERT [dbo].[Tbl_MGender] ([GenderId], [GenderName]) VALUES (1, N'Male')
INSERT [dbo].[Tbl_MGender] ([GenderId], [GenderName]) VALUES (2, N'Female')
SET IDENTITY_INSERT [dbo].[Tbl_MGender] OFF
/****** Object:  Table [dbo].[TBl_MDisableType]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBl_MDisableType](
	[DisableTypeId] [int] IDENTITY(1,1) NOT NULL,
	[DisableType] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[DisableTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBl_MDisableType] ON
INSERT [dbo].[TBl_MDisableType] ([DisableTypeId], [DisableType]) VALUES (1, N'No Power')
INSERT [dbo].[TBl_MDisableType] ([DisableTypeId], [DisableType]) VALUES (2, N'Temporary Close')
SET IDENTITY_INSERT [dbo].[TBl_MDisableType] OFF
/****** Object:  Table [dbo].[Tbl_MDesignations]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MDesignations](
	[DesignationId] [int] IDENTITY(1,1) NOT NULL,
	[DesignationName] [varchar](200) NULL,
	[ActiveStatusId] [int] NULL,
	[Details] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[DesignationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MDesignations] ON
INSERT [dbo].[Tbl_MDesignations] ([DesignationId], [DesignationName], [ActiveStatusId], [Details]) VALUES (1, N'Manager', 1, NULL)
INSERT [dbo].[Tbl_MDesignations] ([DesignationId], [DesignationName], [ActiveStatusId], [Details]) VALUES (2, N'Employee', 1, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_MDesignations] OFF
/****** Object:  Table [dbo].[Tbl_MCustomerTypes]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MCustomerTypes](
	[CustomerTypeId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerType] [varchar](100) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[IsMaster] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerTypes] ON
INSERT [dbo].[Tbl_MCustomerTypes] ([CustomerTypeId], [CustomerType], [Details], [ActiveStatusId], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [IsMaster]) VALUES (1, N'MD', NULL, 1, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_MCustomerTypes] ([CustomerTypeId], [CustomerType], [Details], [ActiveStatusId], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [IsMaster]) VALUES (2, N'NON MD', NULL, 1, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_MCustomerTypes] ([CustomerTypeId], [CustomerType], [Details], [ActiveStatusId], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy], [IsMaster]) VALUES (3, N'Pre Paid', NULL, 1, NULL, NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerTypes] OFF
/****** Object:  Table [dbo].[Tbl_MCustomerType]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MCustomerType](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerTypeName] [varchar](100) NULL,
	[IsAcitve] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerType] ON
INSERT [dbo].[Tbl_MCustomerType] ([CustomerId], [CustomerTypeName], [IsAcitve]) VALUES (1, N'Demand', 1)
INSERT [dbo].[Tbl_MCustomerType] ([CustomerId], [CustomerTypeName], [IsAcitve]) VALUES (2, N'Consumption', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerType] OFF
/****** Object:  Table [dbo].[Tbl_MCustomerStatus]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MCustomerStatus](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusName] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerStatus] ON
INSERT [dbo].[Tbl_MCustomerStatus] ([StatusId], [StatusName]) VALUES (1, N'Active')
INSERT [dbo].[Tbl_MCustomerStatus] ([StatusId], [StatusName]) VALUES (2, N'Inactive')
INSERT [dbo].[Tbl_MCustomerStatus] ([StatusId], [StatusName]) VALUES (3, N'Vacation')
INSERT [dbo].[Tbl_MCustomerStatus] ([StatusId], [StatusName]) VALUES (4, N'Reactivated')
INSERT [dbo].[Tbl_MCustomerStatus] ([StatusId], [StatusName]) VALUES (5, N'Closed')
INSERT [dbo].[Tbl_MCustomerStatus] ([StatusId], [StatusName]) VALUES (6, N'Hold')
SET IDENTITY_INSERT [dbo].[Tbl_MCustomerStatus] OFF
/****** Object:  Table [MASTERS].[Tbl_MControlRefMaster]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MASTERS].[Tbl_MControlRefMaster](
	[MRefId] [int] IDENTITY(1,1) NOT NULL,
	[UDCId] [int] NULL,
	[VText] [varchar](150) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [varchar](30) NULL,
	[ModifiedBy] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[MRefId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [MASTERS].[Tbl_MControlRefMaster] ON
INSERT [MASTERS].[Tbl_MControlRefMaster] ([MRefId], [UDCId], [VText], [IsActive], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy]) VALUES (1, 2, N'License', 1, NULL, CAST(0x0000A43A01231921 AS DateTime), NULL, N'Admin')
SET IDENTITY_INSERT [MASTERS].[Tbl_MControlRefMaster] OFF
/****** Object:  Table [dbo].[Tbl_MConnectionReasons]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MConnectionReasons](
	[ConnectionReasonId] [int] IDENTITY(1,1) NOT NULL,
	[Reason] [varchar](500) NULL,
	[Notes] [varchar](max) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatus] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ConnectionReasonId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MConnectionReasons] ON
INSERT [dbo].[Tbl_MConnectionReasons] ([ConnectionReasonId], [Reason], [Notes], [Details], [ActiveStatus]) VALUES (1, N'New', NULL, NULL, 1)
INSERT [dbo].[Tbl_MConnectionReasons] ([ConnectionReasonId], [Reason], [Notes], [Details], [ActiveStatus]) VALUES (2, N'Disconnected', NULL, NULL, 1)
INSERT [dbo].[Tbl_MConnectionReasons] ([ConnectionReasonId], [Reason], [Notes], [Details], [ActiveStatus]) VALUES (3, N'Replacement', NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MConnectionReasons] OFF
/****** Object:  Table [MASTERS].[Tbl_MClusterCategories]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MASTERS].[Tbl_MClusterCategories](
	[ClusterCategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [varchar](50) NULL,
	[CategoryDescription] [varchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[ClusterCategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [MASTERS].[Tbl_MClusterCategories] ON
INSERT [MASTERS].[Tbl_MClusterCategories] ([ClusterCategoryId], [CategoryName], [CategoryDescription]) VALUES (1, N'A', N'High Usage')
INSERT [MASTERS].[Tbl_MClusterCategories] ([ClusterCategoryId], [CategoryName], [CategoryDescription]) VALUES (2, N'B', N'Medium Usage')
INSERT [MASTERS].[Tbl_MClusterCategories] ([ClusterCategoryId], [CategoryName], [CategoryDescription]) VALUES (3, N'C', N'Low Usage')
SET IDENTITY_INSERT [MASTERS].[Tbl_MClusterCategories] OFF
/****** Object:  Table [dbo].[Tbl_MChargeIds]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MChargeIds](
	[ChargeId] [int] IDENTITY(1,1) NOT NULL,
	[ChargeName] [varchar](100) NULL,
	[IsAcitve] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ChargeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MChargeIds] ON
INSERT [dbo].[Tbl_MChargeIds] ([ChargeId], [ChargeName], [IsAcitve], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Additional Charge', 0, NULL, NULL, N'Admin', CAST(0x0000A42F00E58723 AS DateTime))
INSERT [dbo].[Tbl_MChargeIds] ([ChargeId], [ChargeName], [IsAcitve], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'Meter Maintenance Charge', 0, NULL, NULL, N'Admin', CAST(0x0000A42F00E58351 AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_MChargeIds] OFF
/****** Object:  Table [dbo].[Tbl_MBillStatus]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillStatus](
	[BillStatusId] [int] IDENTITY(1,1) NOT NULL,
	[BillStatus] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillStatus] ON
INSERT [dbo].[Tbl_MBillStatus] ([BillStatusId], [BillStatus]) VALUES (1, N'Paid')
INSERT [dbo].[Tbl_MBillStatus] ([BillStatusId], [BillStatus]) VALUES (2, N'Due')
SET IDENTITY_INSERT [dbo].[Tbl_MBillStatus] OFF
/****** Object:  Table [dbo].[Tbl_MBillProcessType]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillProcessType](
	[BillProcessTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillProcessTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillProcessType] ON
INSERT [dbo].[Tbl_MBillProcessType] ([BillProcessTypeId], [Type]) VALUES (1, N'CycleWise')
INSERT [dbo].[Tbl_MBillProcessType] ([BillProcessTypeId], [Type]) VALUES (2, N'FeederWise')
SET IDENTITY_INSERT [dbo].[Tbl_MBillProcessType] OFF
/****** Object:  Table [dbo].[Tbl_MBillMonthOpenStatus]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillMonthOpenStatus](
	[OpenStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[OpenStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillMonthOpenStatus] ON
INSERT [dbo].[Tbl_MBillMonthOpenStatus] ([OpenStatusId], [Status]) VALUES (1, N'Opened')
INSERT [dbo].[Tbl_MBillMonthOpenStatus] ([OpenStatusId], [Status]) VALUES (2, N'Closed')
INSERT [dbo].[Tbl_MBillMonthOpenStatus] ([OpenStatusId], [Status]) VALUES (3, N'Cancelled')
SET IDENTITY_INSERT [dbo].[Tbl_MBillMonthOpenStatus] OFF
/****** Object:  Table [dbo].[Tbl_MBillingTypes]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillingTypes](
	[BillingTypeId] [int] IDENTITY(1,1) NOT NULL,
	[BillingType] [varchar](500) NULL,
	[Notes] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[BillingTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillingTypes] ON
INSERT [dbo].[Tbl_MBillingTypes] ([BillingTypeId], [BillingType], [Notes], [ActiveStatusId]) VALUES (1, N'Mobile', NULL, 1)
INSERT [dbo].[Tbl_MBillingTypes] ([BillingTypeId], [BillingType], [Notes], [ActiveStatusId]) VALUES (2, N'Web', NULL, 1)
INSERT [dbo].[Tbl_MBillingTypes] ([BillingTypeId], [BillingType], [Notes], [ActiveStatusId]) VALUES (3, N'Spot', NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MBillingTypes] OFF
/****** Object:  Table [dbo].[Tbl_MBillingRules]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillingRules](
	[RuleID] [int] IDENTITY(1,1) NOT NULL,
	[RuleName] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[RuleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillingRules] ON
INSERT [dbo].[Tbl_MBillingRules] ([RuleID], [RuleName]) VALUES (1, N'As Per Settings Rule')
INSERT [dbo].[Tbl_MBillingRules] ([RuleID], [RuleName]) VALUES (2, N'Avg Reading Rule')
SET IDENTITY_INSERT [dbo].[Tbl_MBillingRules] OFF
/****** Object:  Table [dbo].[Tbl_MBillGenarationStatus]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBillGenarationStatus](
	[BillGenarationStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillGenarationStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBillGenarationStatus] ON
INSERT [dbo].[Tbl_MBillGenarationStatus] ([BillGenarationStatusId], [Status]) VALUES (1, N'BillingGenarationCompleted')
INSERT [dbo].[Tbl_MBillGenarationStatus] ([BillGenarationStatusId], [Status]) VALUES (2, N'FileCreated')
INSERT [dbo].[Tbl_MBillGenarationStatus] ([BillGenarationStatusId], [Status]) VALUES (3, N'EmailSent')
INSERT [dbo].[Tbl_MBillGenarationStatus] ([BillGenarationStatusId], [Status]) VALUES (4, N'BillingQueeClosed')
INSERT [dbo].[Tbl_MBillGenarationStatus] ([BillGenarationStatusId], [Status]) VALUES (5, N'BillGenarationPending')
SET IDENTITY_INSERT [dbo].[Tbl_MBillGenarationStatus] OFF
/****** Object:  Table [EMPLOYEE].[Tbl_MBEDCEmployeeDetails]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [EMPLOYEE].[Tbl_MBEDCEmployeeDetails](
	[BEDCEmpId] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeName] [varchar](50) NULL,
	[EmployeeDesignation] [varchar](50) NULL,
	[EmployeeLocation] [varchar](50) NULL,
	[EmployeCode] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ActiveStatusId] [int] NULL,
	[ContactNo] [varchar](20) NULL,
	[AnotherContactNo] [varchar](20) NULL,
	[Address] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[BEDCEmpId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [EMPLOYEE].[Tbl_MBEDCEmployeeDetails] ON
INSERT [EMPLOYEE].[Tbl_MBEDCEmployeeDetails] ([BEDCEmpId], [EmployeeName], [EmployeeDesignation], [EmployeeLocation], [EmployeCode], [CreatedDate], [CreatedBy], [ModifedBy], [ModifiedDate], [ActiveStatusId], [ContactNo], [AnotherContactNo], [Address]) VALUES (1, N'Admin', N'Manger', N'Hyderabad', N'BEDC_001', NULL, NULL, N'Admin', CAST(0x0000A43A011F67CB AS DateTime), 1, N'123-123123', N'123-123123', N'Manger')
SET IDENTITY_INSERT [EMPLOYEE].[Tbl_MBEDCEmployeeDetails] OFF
/****** Object:  Table [dbo].[Tbl_MBatchStatus]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MBatchStatus](
	[BatchStatusId] [int] IDENTITY(1,1) NOT NULL,
	[BatchStatus] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[BatchStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MBatchStatus] ON
INSERT [dbo].[Tbl_MBatchStatus] ([BatchStatusId], [BatchStatus]) VALUES (1, N'Pending')
INSERT [dbo].[Tbl_MBatchStatus] ([BatchStatusId], [BatchStatus]) VALUES (2, N'Finished')
INSERT [dbo].[Tbl_MBatchStatus] ([BatchStatusId], [BatchStatus]) VALUES (3, N'Bill Generated')
SET IDENTITY_INSERT [dbo].[Tbl_MBatchStatus] OFF
/****** Object:  Table [dbo].[Tbl_Marketers]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Marketers](
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[ContactNumber] [varchar](20) NULL,
	[AlternateContactNumber] [varchar](20) NULL,
	[Address1] [varchar](max) NULL,
	[Address2] [varchar](max) NULL,
	[City] [varchar](50) NULL,
	[Zipcode] [varchar](50) NULL,
	[EmailId] [varchar](max) NULL,
	[Photo] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BU_ID] [varchar](20) NULL,
	[MarketerId] [int] IDENTITY(1,1) NOT NULL,
	[ActiveStatusId] [int] NULL,
	[ModifiedBy] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_Marketers] ON
SET IDENTITY_INSERT [dbo].[Tbl_Marketers] OFF
/****** Object:  Table [dbo].[Tbl_MApprovalStatus]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MApprovalStatus](
	[ApprovalStatusId] [int] IDENTITY(0,1) NOT NULL,
	[ApprovalStatus] [varchar](50) NULL,
	[StatusCode] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[ApprovalStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MApprovalStatus] ON
INSERT [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId], [ApprovalStatus], [StatusCode]) VALUES (0, N'New', N'N')
INSERT [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId], [ApprovalStatus], [StatusCode]) VALUES (1, N'Process', N'P')
INSERT [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId], [ApprovalStatus], [StatusCode]) VALUES (2, N'Approved', N'A')
INSERT [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId], [ApprovalStatus], [StatusCode]) VALUES (3, N'Rejected', N'R')
INSERT [dbo].[Tbl_MApprovalStatus] ([ApprovalStatusId], [ApprovalStatus], [StatusCode]) VALUES (4, N'Hold', N'H')
SET IDENTITY_INSERT [dbo].[Tbl_MApprovalStatus] OFF
/****** Object:  Table [dbo].[Tbl_MApplicationProccessedBy]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MApplicationProccessedBy](
	[ProccessedById] [int] IDENTITY(1,1) NOT NULL,
	[ProcessedBy] [varchar](200) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProccessedById] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MApplicationProccessedBy] ON
INSERT [dbo].[Tbl_MApplicationProccessedBy] ([ProccessedById], [ProcessedBy], [Details], [ActiveStatusId]) VALUES (1, N'BEDC', NULL, 1)
INSERT [dbo].[Tbl_MApplicationProccessedBy] ([ProccessedById], [ProcessedBy], [Details], [ActiveStatusId]) VALUES (2, N'Others', NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MApplicationProccessedBy] OFF
/****** Object:  Table [dbo].[Tbl_MActiveStatusDetails]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MActiveStatusDetails](
	[ActiveStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Status] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[ActiveStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MActiveStatusDetails] ON
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (1, N'Active')
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (2, N'DeActive')
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (3, N'Delete')
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (4, N'ReDeactivate')
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (5, N'Close')
INSERT [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId], [Status]) VALUES (6, N'Block')
SET IDENTITY_INSERT [dbo].[Tbl_MActiveStatusDetails] OFF
/****** Object:  Table [dbo].[Tbl_MAccountTypes]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MAccountTypes](
	[AccountTypeId] [int] IDENTITY(1,1) NOT NULL,
	[AccountType] [varchar](100) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[AccountCode] [varchar](10) NULL,
	[IsMaster] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[AccountTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MAccountTypes] ON
INSERT [dbo].[Tbl_MAccountTypes] ([AccountTypeId], [AccountType], [Details], [ActiveStatusId], [AccountCode], [IsMaster], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy]) VALUES (1, N'Commercial', N'---', 1, N'C', 1, NULL, CAST(0x0000A43100C28D06 AS DateTime), NULL, N'admin')
INSERT [dbo].[Tbl_MAccountTypes] ([AccountTypeId], [AccountType], [Details], [ActiveStatusId], [AccountCode], [IsMaster], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy]) VALUES (2, N'Govt', NULL, 1, N'G', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MAccountTypes] ([AccountTypeId], [AccountType], [Details], [ActiveStatusId], [AccountCode], [IsMaster], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy]) VALUES (3, N'Street', NULL, 1, N'S', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MAccountTypes] ([AccountTypeId], [AccountType], [Details], [ActiveStatusId], [AccountCode], [IsMaster], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy]) VALUES (4, N'Personal', NULL, 1, N'P', 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MAccountTypes] ([AccountTypeId], [AccountType], [Details], [ActiveStatusId], [AccountCode], [IsMaster], [CreatedDate], [ModifiedDate], [CreatedBy], [ModifiedBy]) VALUES (5, N'Industrial', NULL, 1, N'D', 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Tbl_MAccountTypes] OFF
/****** Object:  Table [dbo].[TBL_FunctionApprovalRole]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FunctionApprovalRole](
	[ApprovalRoleId] [int] IDENTITY(1,1) NOT NULL,
	[FunctionId] [int] NULL,
	[RoleId] [int] NULL,
	[Level] [int] NULL,
	[IsFinalApproval] [bit] NULL,
	[UserIds] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ApprovalRoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_FunctionApprovalRole] ON
SET IDENTITY_INSERT [dbo].[TBL_FunctionApprovalRole] OFF
/****** Object:  Table [dbo].[TBL_FunctionalAccessPermission]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_FunctionalAccessPermission](
	[FunctionId] [int] IDENTITY(1,1) NOT NULL,
	[Function] [varchar](200) NULL,
	[AccessLevels] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[FunctionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_FunctionalAccessPermission] ON
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [IsActive]) VALUES (1, N'Bill Adjustment', 2, CAST(0x0000A3E300000000 AS DateTime), N'Admin', N'Admin', CAST(0x0000A3F200C8FF15 AS DateTime), 1)
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [IsActive]) VALUES (2, N'Payment Entry', 2, CAST(0x0000A3E300000000 AS DateTime), N'Admin', N'Admin', CAST(0x0000A3F9010247CF AS DateTime), 1)
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [IsActive]) VALUES (3, N'Change Customer Tariff', 3, CAST(0x0000A3E300000000 AS DateTime), N'Admin', N'Admin', CAST(0x0000A3FB00D6F9BC AS DateTime), 1)
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [IsActive]) VALUES (4, N'Chnage Customer BookNo', 1, CAST(0x0000A3E300000000 AS DateTime), N'Admin', N'Admin', CAST(0x0000A3F000C4A9BC AS DateTime), 0)
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [IsActive]) VALUES (5, N'BookNo Disabled', 1, CAST(0x0000A3E300000000 AS DateTime), N'Admin', N'Admin', CAST(0x0000A3F10138A4B1 AS DateTime), 1)
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [IsActive]) VALUES (6, N'Change Customer Address', 1, CAST(0x0000A3E300000000 AS DateTime), N'Admin', N'Admin', CAST(0x0000A3F000C4A9BC AS DateTime), 0)
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [IsActive]) VALUES (7, N'Change Customer MeterNo', 3, CAST(0x0000A3E300000000 AS DateTime), N'Admin', N'Admin', CAST(0x0000A3F200C1E579 AS DateTime), 1)
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [IsActive]) VALUES (8, N'Change Customer Name', 2, CAST(0x0000A3E300000000 AS DateTime), N'Admin', N'admin', CAST(0x0000A43D00D5A4AB AS DateTime), 0)
INSERT [dbo].[TBL_FunctionalAccessPermission] ([FunctionId], [Function], [AccessLevels], [CreatedDate], [CreatedBy], [ModifiedBy], [ModifiedDate], [IsActive]) VALUES (9, N'Change Customer Status', 1, CAST(0x0000A3E300000000 AS DateTime), N'Admin', N'Admin', CAST(0x0000A3F000C4A9BC AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[TBL_FunctionalAccessPermission] OFF
/****** Object:  Table [dbo].[Tbl_CustomerBillPaymentsApproval]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerBillPaymentsApproval](
	[CustomerBillPaymentId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerPaymentId] [int] NULL,
	[PaidAmount] [decimal](20, 4) NULL,
	[BillNo] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[Approval] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [MASTERS].[Tbl_ControlRefMaster]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MASTERS].[Tbl_ControlRefMaster](
	[ControlTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ControlTypeName] [varchar](50) NULL,
	[IsHavingRef] [bit] NULL,
	[ActiveStatusId] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ControlTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [MASTERS].[Tbl_ControlRefMaster] ON
INSERT [MASTERS].[Tbl_ControlRefMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef], [ActiveStatusId]) VALUES (1, N'TextBox', 0, 1)
INSERT [MASTERS].[Tbl_ControlRefMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef], [ActiveStatusId]) VALUES (2, N'DropDownList', 1, 1)
INSERT [MASTERS].[Tbl_ControlRefMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef], [ActiveStatusId]) VALUES (3, N'RadioButtonList', 1, 0)
INSERT [MASTERS].[Tbl_ControlRefMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef], [ActiveStatusId]) VALUES (4, N'CheckBoxList', 1, 0)
SET IDENTITY_INSERT [MASTERS].[Tbl_ControlRefMaster] OFF
/****** Object:  Table [dbo].[Tbl_CompanyDetails]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CompanyDetails](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](300) NULL,
	[Details] [varchar](max) NULL,
	[Address] [varchar](max) NULL,
	[Logo] [varchar](max) NULL,
	[KeyCode] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BillPaymentModeId] [int] NULL,
	[IsEmbassyHaveTax] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_CompanyDetails] ON
INSERT [dbo].[Tbl_CompanyDetails] ([CompanyId], [CompanyName], [Details], [Address], [Logo], [KeyCode], [ActiveStatusId], [CreatedDate], [CreatedBy], [ModifedBy], [ModifiedDate], [BillPaymentModeId], [IsEmbassyHaveTax]) VALUES (1, N'BEDC', NULL, NULL, NULL, N'BEDC', 1, NULL, NULL, NULL, NULL, 2, 1)
SET IDENTITY_INSERT [dbo].[Tbl_CompanyDetails] OFF
/****** Object:  Table [dbo].[Tbl_BillPaymentModes]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillPaymentModes](
	[BillPaymentModeId] [int] IDENTITY(1,1) NOT NULL,
	[BillPaymentMode] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillPaymentModeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_BillPaymentModes] ON
INSERT [dbo].[Tbl_BillPaymentModes] ([BillPaymentModeId], [BillPaymentMode]) VALUES (1, N'Bill-Bill')
INSERT [dbo].[Tbl_BillPaymentModes] ([BillPaymentModeId], [BillPaymentMode]) VALUES (2, N'FIFO')
SET IDENTITY_INSERT [dbo].[Tbl_BillPaymentModes] OFF
/****** Object:  Table [dbo].[Tbl_BillAdjustmentType]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillAdjustmentType](
	[BATID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_BillAdjustmentType] PRIMARY KEY CLUSTERED 
(
	[BATID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_BillAdjustmentType] ON
INSERT [dbo].[Tbl_BillAdjustmentType] ([BATID], [Name], [Status]) VALUES (1, N'Meter Reading Adjustment', 1)
INSERT [dbo].[Tbl_BillAdjustmentType] ([BATID], [Name], [Status]) VALUES (2, N'Additional Charges Adjustment', 1)
INSERT [dbo].[Tbl_BillAdjustmentType] ([BATID], [Name], [Status]) VALUES (3, N'Consumption Adjustment', 1)
INSERT [dbo].[Tbl_BillAdjustmentType] ([BATID], [Name], [Status]) VALUES (4, N'Bill Adjustment', 1)
SET IDENTITY_INSERT [dbo].[Tbl_BillAdjustmentType] OFF
/****** Object:  Table [dbo].[Tbl_WebServiceWorkingKey]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_WebServiceWorkingKey](
	[WSWorkingKey] [int] IDENTITY(1,1) NOT NULL,
	[DeviceName] [varchar](50) NULL,
	[Key] [char](32) NULL,
 CONSTRAINT [PK_Tbl_WebServiceWorkingKey] PRIMARY KEY CLUSTERED 
(
	[WSWorkingKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_WebServiceWorkingKey] ON
INSERT [dbo].[Tbl_WebServiceWorkingKey] ([WSWorkingKey], [DeviceName], [Key]) VALUES (1, N'WC', N'KJFDKFD5455544                  ')
SET IDENTITY_INSERT [dbo].[Tbl_WebServiceWorkingKey] OFF
/****** Object:  Table [dbo].[Tbl_UserLoginDetails]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserLoginDetails](
	[UserId] [varchar](50) NOT NULL,
	[Password] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentityUserId] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_UserLoginDetails] ON
INSERT [dbo].[Tbl_UserLoginDetails] ([UserId], [Password], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IdentityUserId]) VALUES (N'Admin', N'cTtGyvnFUpHfxPNt+CFvh9UsfbkFe1HQSpcigAT8IKA=', 1, NULL, NULL, N'Admin', CAST(0x0000A42700D6AD69 AS DateTime), 1)
INSERT [dbo].[Tbl_UserLoginDetails] ([UserId], [Password], [ActiveStatusId], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [IdentityUserId]) VALUES (N'SuperAdmin', N'cTtGyvnFUpHfxPNt+CFvh9UsfbkFe1HQSpcigAT8IKA=', 1, N'Admin', CAST(0x0000A41700F83079 AS DateTime), NULL, NULL, 2)
SET IDENTITY_INSERT [dbo].[Tbl_UserLoginDetails] OFF
/****** Object:  Table [dbo].[Tbl_UserDetails]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserDetails](
	[UserDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](50) NULL,
	[Name] [varchar](300) NULL,
	[SurName] [varchar](300) NULL,
	[PrimaryContact] [varchar](20) NULL,
	[SecondaryContact] [varchar](20) NULL,
	[PrimaryEmailId] [varchar](500) NULL,
	[SecondaryEmailId] [varchar](500) NULL,
	[Address] [varchar](max) NULL,
	[Photo] [varchar](max) NULL,
	[ScannedDocument] [varchar](max) NULL,
	[GenderId] [int] NULL,
	[Details] [varchar](max) NULL,
	[RoleId] [int] NULL,
	[DesignationId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[FilePath] [varchar](max) NULL,
	[IsMobileAccess] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserDetailsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_UserDetails] ON
INSERT [dbo].[Tbl_UserDetails] ([UserDetailsId], [UserId], [Name], [SurName], [PrimaryContact], [SecondaryContact], [PrimaryEmailId], [SecondaryEmailId], [Address], [Photo], [ScannedDocument], [GenderId], [Details], [RoleId], [DesignationId], [CreatedDate], [CreatedBy], [ModifedBy], [ModifiedDate], [FilePath], [IsMobileAccess]) VALUES (2, N'Admin', N'Admin', N'Adfgdf', N'123-45612345', N'123-456123556', N'bedc.billing@gmail.com', N'bedc.billing@gmail.com', N'Hyderabad', N'lord_rama_by_molee_19_02_2015_11_39_47.png', NULL, 1, N'Best Employee', 1, 1, CAST(0x0000A2E30146A4BE AS DateTime), N'Admin', N'Admin', CAST(0x0000A44500C8F556 AS DateTime), NULL, NULL)
INSERT [dbo].[Tbl_UserDetails] ([UserDetailsId], [UserId], [Name], [SurName], [PrimaryContact], [SecondaryContact], [PrimaryEmailId], [SecondaryEmailId], [Address], [Photo], [ScannedDocument], [GenderId], [Details], [RoleId], [DesignationId], [CreatedDate], [CreatedBy], [ModifedBy], [ModifiedDate], [FilePath], [IsMobileAccess]) VALUES (3, N'SuperAdmin', N'Bhimaraju', N'Vanka', N'123456789012', NULL, NULL, NULL, N'India', NULL, NULL, 1, NULL, 12, 1, CAST(0x0000A41700F83079 AS DateTime), N'Admin', NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_UserDetails] OFF
/****** Object:  Table [dbo].[Tbl_USerDefinedValueDetails]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_USerDefinedValueDetails](
	[UD_Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[UDCId] [int] NULL,
	[Value] [varchar](150) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UD_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_USerDefinedValueDetails] ON
SET IDENTITY_INSERT [dbo].[Tbl_USerDefinedValueDetails] OFF
/****** Object:  Table [MASTERS].[Tbl_USerDefinedControlDetails]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MASTERS].[Tbl_USerDefinedControlDetails](
	[UDCId] [int] IDENTITY(1,1) NOT NULL,
	[FieldName] [varchar](50) NULL,
	[ControlTypeId] [int] NULL,
	[IsMandatory] [bit] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UDCId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [MASTERS].[Tbl_USerDefinedControlDetails] ON
INSERT [MASTERS].[Tbl_USerDefinedControlDetails] ([UDCId], [FieldName], [ControlTypeId], [IsMandatory], [ModifiedBy], [ModifiedDate], [IsActive], [CreatedDate], [CreatedBy]) VALUES (1, N'Name', 1, 0, N'admin', CAST(0x0000A44801191C00 AS DateTime), 0, NULL, NULL)
SET IDENTITY_INSERT [MASTERS].[Tbl_USerDefinedControlDetails] OFF
/****** Object:  Table [dbo].[TBL_ReasonCode]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_ReasonCode](
	[RCID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonCode] [varchar](50) NULL,
	[DESCRIPTION] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[RCID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBL_ReasonCode] ON
INSERT [dbo].[TBL_ReasonCode] ([RCID], [ReasonCode], [DESCRIPTION]) VALUES (1, N'Over Estimation', N'Wrong Estimation')
INSERT [dbo].[TBL_ReasonCode] ([RCID], [ReasonCode], [DESCRIPTION]) VALUES (2, N'Under Estimation', N'Wrong Estimation')
SET IDENTITY_INSERT [dbo].[TBL_ReasonCode] OFF
/****** Object:  Table [dbo].[Tbl_NewPagePermissions]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_NewPagePermissions](
	[Page_PermissionId] [int] IDENTITY(1,1) NOT NULL,
	[Role_Id] [int] NULL,
	[MenuId] [int] NULL,
	[View] [bit] NULL,
	[Create] [bit] NULL,
	[Created_By] [varchar](50) NULL,
	[Created_Date] [datetime] NULL,
	[Modified_By] [varchar](50) NULL,
	[Modified_Date] [datetime] NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Page_PermissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_NewPagePermissions] ON
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1, 12, 1, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (2, 12, 2, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (3, 12, 3, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (4, 12, 4, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (5, 12, 5, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (6, 12, 6, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (7, 12, 7, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (8, 12, 8, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (9, 12, 9, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (10, 12, 10, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (11, 12, 11, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (12, 12, 12, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (13, 12, 13, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (14, 12, 14, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (15, 12, 15, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (16, 12, 16, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (17, 12, 17, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (18, 12, 18, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (19, 12, 19, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (20, 12, 20, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (21, 12, 21, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (22, 12, 22, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (23, 12, 23, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (24, 12, 24, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (25, 12, 25, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (26, 12, 26, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (27, 12, 27, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (28, 12, 28, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (29, 12, 29, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (30, 12, 30, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (31, 12, 31, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (32, 12, 32, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (33, 12, 33, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (34, 12, 34, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (35, 12, 35, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (36, 12, 36, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (37, 12, 37, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (38, 12, 38, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (39, 12, 39, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (40, 12, 40, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (41, 12, 41, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (42, 12, 42, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (43, 12, 43, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (44, 12, 44, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (45, 12, 45, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (46, 12, 46, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (47, 12, 47, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (48, 12, 48, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (49, 12, 49, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (50, 12, 50, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (51, 12, 51, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (52, 12, 52, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (53, 12, 53, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (54, 12, 54, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (55, 12, 55, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (56, 12, 56, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (57, 12, 57, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (58, 12, 58, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (59, 12, 59, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (60, 12, 60, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (61, 12, 61, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (62, 12, 62, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (63, 12, 63, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (64, 12, 64, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (65, 12, 65, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (66, 12, 66, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (67, 12, 67, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (68, 12, 68, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (69, 12, 69, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (70, 12, 70, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (71, 12, 71, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (72, 12, 72, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (73, 12, 73, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (74, 12, 74, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (75, 12, 75, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (76, 12, 76, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (77, 12, 77, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (78, 12, 78, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (79, 12, 79, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (80, 12, 80, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (81, 12, 81, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (82, 12, 82, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (83, 12, 83, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (84, 12, 84, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (85, 12, 85, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (86, 12, 86, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (87, 12, 87, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (88, 12, 88, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (89, 12, 89, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (90, 12, 90, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (91, 12, 91, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (92, 12, 92, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (93, 12, 93, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (94, 12, 94, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (95, 12, 95, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (96, 12, 96, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (97, 12, 97, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (98, 12, 98, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (99, 12, 99, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (100, 12, 100, 1, 1, N'Admin', NULL, NULL, NULL, 1)
GO
print 'Processed 100 total records'
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (101, 12, 101, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (102, 12, 102, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (103, 12, 103, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (104, 12, 104, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (105, 12, 105, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (106, 12, 106, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (107, 12, 107, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (108, 12, 108, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (109, 12, 109, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (110, 12, 110, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (111, 12, 111, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (112, 12, 112, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (113, 12, 113, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (114, 12, 114, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (115, 12, 115, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (116, 12, 116, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (117, 12, 117, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (118, 12, 118, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (119, 12, 119, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (120, 12, 120, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (121, 12, 121, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (122, 12, 122, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (123, 12, 123, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (124, 12, 124, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (125, 12, 125, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (126, 12, 126, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (127, 12, 127, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (128, 12, 128, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (129, 12, 129, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (130, 12, 130, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (131, 12, 131, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (132, 12, 132, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (133, 12, 133, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (134, 12, 134, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (135, 12, 135, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (136, 12, 136, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (137, 12, 137, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (138, 12, 138, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (139, 12, 139, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (140, 12, 140, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (141, 12, 141, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (142, 12, 142, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (143, 12, 143, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (144, 12, 144, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (145, 12, 145, 1, 1, N'Admin', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1139, 1, 1, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1140, 1, 10, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1141, 1, 12, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1142, 1, 13, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1143, 1, 14, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1144, 1, 15, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1145, 1, 16, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1146, 1, 17, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1147, 1, 18, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1148, 1, 19, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1149, 1, 20, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1150, 1, 21, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1151, 1, 22, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1152, 1, 23, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1153, 1, 24, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1154, 1, 25, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1155, 1, 9, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1156, 1, 145, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1157, 1, 4, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1158, 1, 37, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1159, 1, 40, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1160, 1, 43, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1161, 1, 44, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1162, 1, 51, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1163, 1, 56, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1164, 1, 57, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1165, 1, 59, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1166, 1, 5, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1167, 1, 52, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1168, 1, 53, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1169, 1, 54, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1170, 1, 6, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1171, 1, 75, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1172, 1, 76, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1173, 1, 77, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1174, 1, 78, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1175, 1, 55, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1176, 1, 60, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1177, 1, 58, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1178, 1, 8, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1179, 1, 125, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1180, 1, 126, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1181, 1, 127, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1182, 1, 129, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1183, 1, 130, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1184, 1, 131, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1185, 1, 132, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1186, 1, 133, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1187, 1, 134, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1188, 1, 135, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1189, 1, 136, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1190, 1, 137, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1191, 1, 138, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1192, 1, 139, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1193, 1, 140, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1194, 1, 141, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
GO
print 'Processed 200 total records'
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1195, 1, 142, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1196, 1, 143, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1197, 1, 144, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1198, 1, 45, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1199, 1, 7, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1200, 1, 79, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1201, 1, 80, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1202, 1, 81, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1203, 1, 82, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1204, 1, 83, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1205, 1, 84, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1206, 1, 85, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1207, 1, 86, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1208, 1, 87, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1209, 1, 88, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1210, 1, 89, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1211, 1, 90, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1212, 1, 91, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1213, 1, 92, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1214, 1, 93, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1215, 1, 94, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1216, 1, 95, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1217, 1, 96, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1218, 1, 97, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1219, 1, 98, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1220, 1, 99, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1221, 1, 100, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1222, 1, 101, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1223, 1, 102, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1224, 1, 103, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1225, 1, 104, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1226, 1, 105, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1227, 1, 106, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1228, 1, 107, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1229, 1, 108, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1230, 1, 109, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1231, 1, 110, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1232, 1, 111, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1233, 1, 112, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1234, 1, 113, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1235, 1, 114, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1236, 1, 115, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1237, 1, 116, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1238, 1, 117, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1239, 1, 118, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1240, 1, 119, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1241, 1, 120, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1242, 1, 121, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A43F01375231 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1243, 1, 122, NULL, NULL, NULL, NULL, N'admin', NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1264, 18, 122, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A44101316016 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1265, 18, 7, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A44101316016 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1266, 18, 112, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A44101316016 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1267, 18, 113, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A44101316016 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1268, 18, 114, NULL, NULL, NULL, NULL, N'Admin', CAST(0x0000A44101316016 AS DateTime), 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1269, 19, 122, NULL, NULL, N'Admin', CAST(0x0000A448014AA1B1 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1270, 19, 7, NULL, NULL, N'Admin', CAST(0x0000A448014AA1B1 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1271, 19, 112, NULL, NULL, N'Admin', CAST(0x0000A448014AA1B1 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1272, 19, 113, NULL, NULL, N'Admin', CAST(0x0000A448014AA1B1 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1273, 19, 114, NULL, NULL, N'Admin', CAST(0x0000A448014AA1B1 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1274, 19, 115, NULL, NULL, N'Admin', CAST(0x0000A448014AA1B1 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1275, 19, 116, NULL, NULL, N'Admin', CAST(0x0000A448014AA1B1 AS DateTime), NULL, NULL, 1)
INSERT [dbo].[Tbl_NewPagePermissions] ([Page_PermissionId], [Role_Id], [MenuId], [View], [Create], [Created_By], [Created_Date], [Modified_By], [Modified_Date], [IsActive]) VALUES (1276, 19, 117, NULL, NULL, N'Admin', CAST(0x0000A448014AA1B1 AS DateTime), NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_NewPagePermissions] OFF
/****** Object:  Table [dbo].[Tbl_MUserControlMaster]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MUserControlMaster](
	[ControlTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ControlTypeName] [varchar](50) NULL,
	[IsHavingRef] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ControlTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MUserControlMaster] ON
INSERT [dbo].[Tbl_MUserControlMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef]) VALUES (1, N'TextBox', 1)
INSERT [dbo].[Tbl_MUserControlMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef]) VALUES (2, N'DropDownList', 1)
INSERT [dbo].[Tbl_MUserControlMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef]) VALUES (3, N'RadioButtonList', 1)
INSERT [dbo].[Tbl_MUserControlMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef]) VALUES (4, N'CheckBoxList', 1)
INSERT [dbo].[Tbl_MUserControlMaster] ([ControlTypeId], [ControlTypeName], [IsHavingRef]) VALUES (5, N'CheckBox', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MUserControlMaster] OFF
/****** Object:  Table [dbo].[Tbl_MTaxTypeDetails]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MTaxTypeDetails](
	[TaxTypeId] [int] IDENTITY(1,1) NOT NULL,
	[TaxType] [varchar](50) NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[TaxTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MTaxTypeDetails] ON
INSERT [dbo].[Tbl_MTaxTypeDetails] ([TaxTypeId], [TaxType], [IsActive]) VALUES (1, N'Pecentage', 1)
INSERT [dbo].[Tbl_MTaxTypeDetails] ([TaxTypeId], [TaxType], [IsActive]) VALUES (2, N'Amount', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MTaxTypeDetails] OFF
/****** Object:  Table [dbo].[Tbl_MTaxDetails]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MTaxDetails](
	[TaxId] [int] IDENTITY(1,1) NOT NULL,
	[TaxName] [varchar](100) NULL,
	[TaxValue] [decimal](18, 0) NULL,
	[IsAcitive] [bit] NULL,
	[TaxTypeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[TaxId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MTaxDetails] ON
INSERT [dbo].[Tbl_MTaxDetails] ([TaxId], [TaxName], [TaxValue], [IsAcitive], [TaxTypeId]) VALUES (1, N'VAT', CAST(5 AS Decimal(18, 0)), 1, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MTaxDetails] OFF
/****** Object:  Table [dbo].[Tbl_MTariffClasses]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MTariffClasses](
	[ClassID] [int] IDENTITY(1,1) NOT NULL,
	[ClassName] [varchar](200) NULL,
	[Description] [varchar](max) NULL,
	[Remarks] [varchar](max) NULL,
	[IsActiveClass] [bit] NULL,
	[RefClassID] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK__Tbl_MTar__CB1927A00DAF0CB0] PRIMARY KEY CLUSTERED 
(
	[ClassID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MTariffClasses] ON
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (1, N'Residential', N'Residential', NULL, 1, NULL, NULL, NULL, N'Admin', CAST(0x0000A43100F6878C AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (2, N'R1', NULL, NULL, 1, 1, NULL, NULL, N'Admin', CAST(0x0000A42E00C90459 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (3, N'R2', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (4, N'R3', NULL, NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (5, N'R4', NULL, NULL, 1, 1, NULL, NULL, N'Admin', CAST(0x0000A42D00C6DDB5 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (6, N'Commercial', N'Commercial', NULL, 1, NULL, NULL, NULL, N'Admin', CAST(0x0000A43100F68979 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (7, N'C1', NULL, NULL, 1, 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (8, N'C2', NULL, NULL, 1, 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (9, N'C3', NULL, NULL, 1, 6, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (10, N'Industrial', N'Industrial', NULL, 1, NULL, NULL, NULL, N'Admin', CAST(0x0000A43100F68B32 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (11, N'D1', NULL, NULL, 1, 10, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (12, N'D2', NULL, NULL, 1, 10, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (13, N'D3', NULL, NULL, 1, 10, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (14, N'Special', N'Special', NULL, 1, NULL, NULL, NULL, N'Admin', CAST(0x0000A43100F68CD3 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (15, N'A1', NULL, NULL, 1, 14, NULL, NULL, NULL, NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (16, N'A2', NULL, NULL, 1, 14, NULL, NULL, N'admin', NULL)
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (17, N'A4', NULL, NULL, 0, 14, NULL, NULL, N'admin', CAST(0x0000A44100FF3104 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (18, N'Street Light', N'Street Light', NULL, 0, NULL, NULL, NULL, N'Admin', CAST(0x0000A448010F1234 AS DateTime))
INSERT [dbo].[Tbl_MTariffClasses] ([ClassID], [ClassName], [Description], [Remarks], [IsActiveClass], [RefClassID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate]) VALUES (19, N'S1', N's1', NULL, 1, 18, NULL, NULL, N'Admin', CAST(0x0000A42C011865DA AS DateTime))
SET IDENTITY_INSERT [dbo].[Tbl_MTariffClasses] OFF
/****** Object:  Table [dbo].[Tbl_MRoutes]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MRoutes](
	[RouteId] [int] IDENTITY(1,1) NOT NULL,
	[RouteName] [varchar](500) NULL,
	[AvgMaxLimit] [numeric](20, 4) NULL,
	[ActiveStatusId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[Details] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[RouteCode] [varchar](10) NULL,
 CONSTRAINT [PK__Tbl_MRou__80979B4D7F6BDA51] PRIMARY KEY CLUSTERED 
(
	[RouteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MRoutes] ON
SET IDENTITY_INSERT [dbo].[Tbl_MRoutes] OFF
/****** Object:  Table [dbo].[Tbl_MRoles]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MRoles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](100) NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MRoles] ON
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (1, N'Admin', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (2, N'BU Manager', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (3, N'Commercial Manager', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (4, N'Cashier', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (5, N'Data Entry Operator', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (6, N'SU Manager', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (7, N'Meter Reader', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (8, N'Head Office', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (9, N'Accountant', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (10, N'Customer Service', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (11, N'Supervisor', 1)
INSERT [dbo].[Tbl_MRoles] ([RoleId], [RoleName], [IsActive]) VALUES (12, N'SuperAdmin', 1)
SET IDENTITY_INSERT [dbo].[Tbl_MRoles] OFF
/****** Object:  Table [dbo].[Tbl_MRegion]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MRegion](
	[RegionId] [int] IDENTITY(1,1) NOT NULL,
	[StateCode] [varchar](20) NULL,
	[RegionName] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[RegionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tbl_MRegion] ON
INSERT [dbo].[Tbl_MRegion] ([RegionId], [StateCode], [RegionName], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ActiveStatusId]) VALUES (1, N'01', N'Edo', N'ADMIN', NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_MRegion] ([RegionId], [StateCode], [RegionName], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ActiveStatusId]) VALUES (2, N'02', N'Delta', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_MRegion] ([RegionId], [StateCode], [RegionName], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ActiveStatusId]) VALUES (3, N'03', N'Ondo', NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Tbl_MRegion] ([RegionId], [StateCode], [RegionName], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ActiveStatusId]) VALUES (4, N'04', N'Ekiti', NULL, NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Tbl_MRegion] OFF
/****** Object:  Table [MASTERS].[Tbl_MAreaDetails]    Script Date: 02/24/2015 19:06:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [MASTERS].[Tbl_MAreaDetails](
	[AreaCode] [int] IDENTITY(1,1) NOT NULL,
	[AreaDetails] [varchar](100) NULL,
	[ZipCode] [varchar](20) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AreaCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [MASTERS].[Tbl_MAreaDetails] ON
INSERT [MASTERS].[Tbl_MAreaDetails] ([AreaCode], [AreaDetails], [ZipCode], [CreatedDate], [CreatedBy], [ModifedBy], [ModifiedDate]) VALUES (10, N'Hyderabad', N'482001', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [MASTERS].[Tbl_MAreaDetails] OFF
/****** Object:  Default [DF_BillAdjustmentType_Status]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_BillAdjustmentType] ADD  CONSTRAINT [DF_BillAdjustmentType_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF__Tbl_Compa__Activ__07F6335A]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_CompanyDetails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__TBL_Funct__IsAct__0DAF0CB0]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[TBL_FunctionalAccessPermission] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__TBL_Funct__IsFin__1273C1CD]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[TBL_FunctionApprovalRole] ADD  DEFAULT ((0)) FOR [IsFinalApproval]
GO
/****** Object:  Default [DF__Tbl_MAcco__Activ__173876EA]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MAccountTypes] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MAcco__IsMas__6033261A]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MAccountTypes] ADD  DEFAULT ((0)) FOR [IsMaster]
GO
/****** Object:  Default [DF__Tbl_MAppl__Activ__1FCDBCEB]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MApplicationProccessedBy] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Marke__Activ__52D92AFC]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_Marketers] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MBill__Activ__33D4B598]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MBillingTypes] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MChar__IsAci__440B1D61]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MChargeIds] ADD  DEFAULT ((1)) FOR [IsAcitve]
GO
/****** Object:  Default [DF__Tbl_MConn__Activ__4C2C2D6D]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MConnectionReasons] ADD  DEFAULT ((1)) FOR [ActiveStatus]
GO
/****** Object:  Default [DF__Tbl_MCust__IsAci__4CA06362]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MCustomerType] ADD  DEFAULT ((1)) FOR [IsAcitve]
GO
/****** Object:  Default [DF__Tbl_MCust__Activ__5165187F]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MCustomerTypes] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MCust__IsMas__64F7DB37]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MCustomerTypes] ADD  DEFAULT ((0)) FOR [IsMaster]
GO
/****** Object:  Default [DF__Tbl_MDesi__Activ__5629CD9C]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MDesignations] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DefaultActivestatus]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MGovtAccountTypes] ADD  CONSTRAINT [DefaultActivestatus]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MIden__Activ__6C190EBB]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MIdentityTypes] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MLGA__Active__681373AD]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MLGA] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MMete__Activ__70DDC3D8]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MMeterStatus] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MMete__Activ__75A278F5]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MMeterTypes] ADD  DEFAULT ((1)) FOR [ActiveStatus]
GO
/****** Object:  Default [DF__Tbl_MMete__IsMas__5E4ADDA8]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MMeterTypes] ADD  DEFAULT ((0)) FOR [IsMaster]
GO
/****** Object:  Default [DF__Tbl_MOrga__Activ__4C6B5938]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MOrganizations] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF_Table_MPaymentMode_ActiveStatusId]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MPaymentMode] ADD  CONSTRAINT [DF_Table_MPaymentMode_ActiveStatusId]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MPhas__Activ__06CD04F7]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MPhases] ADD  DEFAULT ((1)) FOR [ActiveStatus]
GO
/****** Object:  Default [df_ConstraintNAme1]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MPoleMasterDetails] ADD  CONSTRAINT [df_ConstraintNAme1]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MRead__Activ__0B91BA14]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MReadCodes] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MRegi__Activ__719CDDE7]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MRegion] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MRole__IsAct__10566F31]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MRoles] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_Tbl_MRoutes_ActiveStatusId]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MRoutes] ADD  CONSTRAINT [DF_Tbl_MRoutes_ActiveStatusId]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MTari__IsAci__0F975522]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MTariffClasses] ADD  CONSTRAINT [DF__Tbl_MTari__IsAci__0F975522]  DEFAULT ((1)) FOR [IsActiveClass]
GO
/****** Object:  Default [DF__Tbl_MTaxD__IsAci__151B244E]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MTaxDetails] ADD  DEFAULT ((1)) FOR [IsAcitive]
GO
/****** Object:  Default [DF__Tbl_MTaxT__IsAct__19DFD96B]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MTaxTypeDetails] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_Tbl_NewPagePermissions_IsActive]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_NewPagePermissions] ADD  CONSTRAINT [DF_Tbl_NewPagePermissions_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_UserD__IsMob__2B0A656D]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_UserDetails] ADD  DEFAULT ((0)) FOR [IsMobileAccess]
GO
/****** Object:  Default [DF__Tbl_UserL__Activ__2FCF1A8A]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_UserLoginDetails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MBEDC__Activ__367C1819]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [EMPLOYEE].[Tbl_MBEDCEmployeeDetails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Contr__Activ__79F2F81D]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [MASTERS].[Tbl_ControlRefMaster] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_MCont__IsAct__46B27FE2]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [MASTERS].[Tbl_MControlRefMaster] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_USerD__IsAct__4B7734FF]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [MASTERS].[Tbl_USerDefinedControlDetails] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  ForeignKey [FK__Tbl_MRegi__Activ__04AFB25B]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [dbo].[Tbl_MRegion]  WITH CHECK ADD FOREIGN KEY([ActiveStatusId])
REFERENCES [dbo].[Tbl_MActiveStatusDetails] ([ActiveStatusId])
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId10]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [MASTERS].[Tbl_MAreaDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId10] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [MASTERS].[Tbl_MAreaDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId10]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId11]    Script Date: 02/24/2015 19:06:40 ******/
ALTER TABLE [MASTERS].[Tbl_MAreaDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId11] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [MASTERS].[Tbl_MAreaDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId11]
GO
