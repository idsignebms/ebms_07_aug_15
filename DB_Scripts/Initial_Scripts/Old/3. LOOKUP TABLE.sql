GO
/****** Object:  Table [dbo].[TBL_Customer_Additionalcharges]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Customer_Additionalcharges](
	[CustomerAdditioalCharges] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [varchar](50) NULL,
	[AccountNO] [varchar](50) NULL,
	[CustomerBillId] [int] NULL,
	[TariffId] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBY] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[ChargeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerAdditioalCharges] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Countries]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Countries](
	[CountryCode] [varchar](20) NOT NULL,
	[CountryName] [varchar](50) NULL,
	[CountryDetails] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[CountryId] [int] IDENTITY(1,1) NOT NULL,
	[Currency] [varchar](10) NULL,
	[CurrencySymbol] [nvarchar](20) NULL,
 CONSTRAINT [PK_CountryId] PRIMARY KEY CLUSTERED 
(
	[CountryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CheckMeterReadings]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CheckMeterReadings](
	[CheckMeterReadingId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[BillNo] [varchar](50) NULL,
	[MeterPreviousReading] [varchar](50) NULL,
	[MeterCurrentReading] [varchar](50) NULL,
	[CheckMeterPreviousReading] [varchar](50) NULL,
	[CheckMeterCurrentReading] [varchar](50) NULL,
	[BillDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CheckMeterReadingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CheckMeterDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CheckMeterDetails](
	[CheckMeterDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[CheckMeterNumber] [varchar](50) NULL,
	[CycleId] [varchar](20) NULL,
	[InitialReading] [varchar](50) NULL,
	[CheckMeterReadingId] [int] NULL,
	[IsActive] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CheckMeterDetailsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CashOffices]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CashOffices](
	[CashOfficeId] [int] IDENTITY(1,1) NOT NULL,
	[CashOffice] [varchar](300) NULL,
	[StateCode] [varchar](20) NULL,
	[DistrictCode] [varchar](20) NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[Details] [varchar](max) NULL,
	[ContactNo1] [varchar](20) NULL,
	[ContactNo2] [varchar](20) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CashOfficeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_BussinessUnitsEBllMails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_BussinessUnitsEBllMails](
	[BUEBMID] [int] IDENTITY(1,1) NOT NULL,
	[NAME] [varchar](20) NULL,
	[TO] [varchar](max) NULL,
	[CC] [varchar](max) NULL,
	[BCC] [varchar](max) NULL,
	[CreatedDate] [smalldatetime] NULL,
	[ActiveStatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[BUEBMID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BussinessUnits]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BussinessUnits](
	[BU_ID] [varchar](20) NOT NULL,
	[BusinessUnitName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[StateCode] [varchar](20) NULL,
	[BUCode] [varchar](10) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[ZIP] [varchar](100) NULL,
	[RegionId] [int] NULL,
	[IdentityBU_ID] [int] IDENTITY(1,1) NOT NULL,
	[PoleId] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[BU_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BUNotice]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BUNotice](
	[BUNoticeID] [int] IDENTITY(1,1) NOT NULL,
	[Notice_ID] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[IsAllBuss] [bit] NULL,
	[Activestatus] [int] NULL,
	[ModifiedDate] [smalldatetime] NULL,
	[ModifiedBy] [varchar](20) NULL,
	[CreatedBy] [varchar](20) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Tbl_BUNotice] PRIMARY KEY CLUSTERED 
(
	[BUNoticeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_BU_HeadOffice]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TBL_BU_HeadOffice](
	[HeadOfficeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NULL,
	[TO] [varchar](50) NULL,
	[CC] [varchar](50) NULL,
	[BCC] [varchar](50) NULL,
	[CreatedDate] [smalldatetime] NULL,
	[ActiveStatusID] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[HeadOfficeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BookNumbers]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BookNumbers](
	[BookNo] [varchar](20) NOT NULL,
	[ID] [varchar](20) NULL,
	[CycleId] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[NoOfAccounts] [int] NULL,
	[Details] [varchar](max) NULL,
	[BookCode] [varchar](10) NULL,
	[SortOrder] [int] NULL,
	[MarketerId] [int] NULL,
	[IDENTITYBOOKID] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BookNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BookNoChangeLogs]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BookNoChangeLogs](
	[BookNoChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[OldBookNo] [varchar](20) NULL,
	[NewBookNo] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[BookNoChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillUnlockRemarks]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillUnlockRemarks](
	[BillUnlockRemarkId] [int] IDENTITY(1,1) NOT NULL,
	[BillingQueueScheduleId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[BillUnlockRemarkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillingQueueSchedule]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillingQueueSchedule](
	[BillingQueueScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[BillingMonth] [int] NULL,
	[BillingYear] [int] NULL,
	[BillSendingMode] [int] NULL,
	[BillProcessTypeId] [int] NULL,
	[TotalCustomersInQueue] [int] NULL,
	[OpenStatusId] [int] NULL,
	[ServiceStartDate] [datetime] NULL,
	[CycleId] [varchar](20) NULL,
	[BillGenarationStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[BillingQueueScheduleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillingQueeCustomers]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillingQueeCustomers](
	[BillQueueCustomerId] [int] IDENTITY(1,1) NOT NULL,
	[BillingQueuescheduleId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[Month] [int] NULL,
	[Year] [int] NULL,
	[BillGenarationStatusId] [int] NULL,
	[BillFile] [varchar](max) NULL,
 CONSTRAINT [PK_Tbl_BillingQueeCustomers] PRIMARY KEY CLUSTERED 
(
	[BillQueueCustomerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillingMonths]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillingMonths](
	[BillMonthID] [int] IDENTITY(1,1) NOT NULL,
	[Year] [int] NULL,
	[Month] [int] NULL,
	[OpenStatusId] [int] NULL,
	[Details] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[BillMonthID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillingDisabledBooks]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillingDisabledBooks](
	[DisabledBookId] [int] IDENTITY(1,1) NOT NULL,
	[BookNo] [varchar](20) NULL,
	[YearId] [int] NULL,
	[MonthId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[IsActive] [bit] NULL,
	[ApproveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[DisableTypeId] [int] NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[IsPartialBill] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[DisabledBookId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillFixedChagesDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillFixedChagesDetails](
	[BillDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerBillId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[TariffId] [int] NULL,
	[Amount] [decimal](20, 4) NULL,
	[ActivestatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[BillDetailsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillEnergyChargesDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillEnergyChargesDetails](
	[BillDetailsId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerBillId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[EnergyFrom] [int] NULL,
	[EnergyTo] [int] NULL,
	[ChargePerKW] [decimal](20, 4) NULL,
	[TotalCharges] [decimal](20, 4) NULL,
	[TariffId] [int] NULL,
	[ActivestatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[BillDetailsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillAdjustments]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillAdjustments](
	[BillAdjustmentId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerBillId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerId] [varchar](50) NULL,
	[Remarks] [varchar](max) NULL,
	[AmountEffected] [decimal](18, 4) NULL,
	[ApprovalStatusId] [int] NULL,
	[BillAdjustmentType] [int] NULL,
	[TaxEffected] [decimal](18, 2) NULL,
	[AdjustedUnits] [int] NULL,
	[TotalAmountEffected] [decimal](18, 2) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[ApprovedDate] [varchar](50) NULL,
	[EffectedBillId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[BatchNo] [int] NULL,
 CONSTRAINT [PK_Tbl_BillAdjustments] PRIMARY KEY CLUSTERED 
(
	[BillAdjustmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BillAdjustmentDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BillAdjustmentDetails](
	[BADID] [int] IDENTITY(1,1) NOT NULL,
	[BillAdjustmentId] [int] NULL,
	[PreviousReading] [varchar](50) NULL,
	[CurrentReadingAfterAdjustment] [varchar](50) NULL,
	[EnergryCharges] [decimal](20, 4) NULL,
	[AdditionalCharges] [decimal](20, 4) NULL,
	[AdditionalChargesID] [int] NULL,
	[CurrentReadingBeforeAdjustment] [varchar](50) NULL,
 CONSTRAINT [PK_Tbl_BillAdjustmentDetails] PRIMARY KEY CLUSTERED 
(
	[BADID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_BillAdjustmentApproval]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_BillAdjustmentApproval](
	[BAAID] [int] IDENTITY(1,1) NOT NULL,
	[BAID] [int] NULL,
	[Approval] [int] NULL,
	[Remark] [int] NULL,
	[ApprovalClosed] [bit] NULL,
 CONSTRAINT [PK_TBL_BillAdjustmentApproval] PRIMARY KEY CLUSTERED 
(
	[BAAID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tbl_BatchVsAccountNo]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BatchVsAccountNo](
	[CustBatchId] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustBatchId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BatchPayment]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BatchPayment](
	[BatchPaymentId] [int] IDENTITY(1,1) NOT NULL,
	[BatchId] [int] NULL,
	[PaymentId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_Tbl_BatchPayment] PRIMARY KEY CLUSTERED 
(
	[BatchPaymentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BatchDetailsApproval]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BatchDetailsApproval](
	[BatchID] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [int] NOT NULL,
	[BatchName] [varchar](50) NULL,
	[BatchDate] [datetime] NULL,
	[CashOffice] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BatchReceivedDate] [datetime] NULL,
	[BatchTotal] [numeric](20, 4) NULL,
	[Description] [varchar](max) NULL,
	[BatchStatus] [int] NULL,
	[CashierID] [varchar](50) NULL,
	[Approval] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BatchDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BatchDetails](
	[BatchID] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [int] NOT NULL,
	[BatchName] [varchar](50) NULL,
	[BatchDate] [datetime] NULL,
	[CashOffice] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BatchReceivedDate] [datetime] NULL,
	[BatchTotal] [numeric](20, 4) NULL,
	[Description] [varchar](max) NULL,
	[BatchStatus] [int] NULL,
	[CashierID] [varchar](50) NULL,
 CONSTRAINT [PK_BatchNo] PRIMARY KEY CLUSTERED 
(
	[BatchNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Batch_CustBulkUpload]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Batch_CustBulkUpload](
	[UploadBatchId] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [int] NULL,
	[BatchDate] [datetime] NULL,
	[BatchName] [varchar](500) NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UploadBatchId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_BATCH_ADJUSTMENT]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_BATCH_ADJUSTMENT](
	[BatchID] [int] IDENTITY(1,1) NOT NULL,
	[BatchNo] [int] NULL,
	[Reason] [varchar](100) NULL,
	[BatchTotal] [decimal](18, 2) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[BatchDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[BatchID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[BatchNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_UserSpecialPermissions]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserSpecialPermissions](
	[UserPermissionId] [int] NOT NULL,
	[UserId] [varchar](50) NULL,
	[BudgetSegmentWiseMargin] [bit] NULL,
	[CustomerDetailsModification] [bit] NULL,
	[BillAdjustment] [bit] NULL,
	[TariffAdjustments] [bit] NULL,
	[PaymentsAdjustments] [bit] NULL,
	[Disconnection] [bit] NULL,
	[ReConnections] [bit] NULL,
	[BillGeneration] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentityUserPermissionId] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_UserServiceUnits]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserServiceUnits](
	[UserServiceUnitId] [int] NOT NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentityUserSU_ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_UserServiceCenters]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserServiceCenters](
	[UserServiceCenterId] [int] NOT NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentityUserSC_ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_UserLoginDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserLoginDetails](
	[UserId] [varchar](50) NOT NULL,
	[Password] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentityUserId] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_UserDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserDetails](
	[UserDetailsId] [int] NOT NULL,
	[UserId] [varchar](50) NULL,
	[Name] [varchar](300) NULL,
	[SurName] [varchar](300) NULL,
	[PrimaryContact] [varchar](20) NULL,
	[SecondaryContact] [varchar](20) NULL,
	[PrimaryEmailId] [varchar](500) NULL,
	[SecondaryEmailId] [varchar](500) NULL,
	[Address] [varchar](max) NULL,
	[Photo] [varchar](max) NULL,
	[ScannedDocument] [varchar](max) NULL,
	[GenderId] [int] NULL,
	[Details] [varchar](max) NULL,
	[RoleId] [int] NULL,
	[DesignationId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[FilePath] [varchar](max) NULL,
	[IdentityUserId] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_UserBusinessUnits]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_UserBusinessUnits](
	[UserBusinessUnitId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentityUserBU_ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_States]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_States](
	[StateCode] [varchar](20) NOT NULL,
	[StateName] [varchar](50) NULL,
	[DisplayCode] [varchar](20) NULL,
	[CountryCode] [varchar](20) NULL,
	[StateDetails] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IdentityStateId] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_ServiceUnits]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_ServiceUnits](
	[SU_ID] [varchar](20) NOT NULL,
	[ServiceUnitName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[BU_ID] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentitySU_ID] [int] IDENTITY(1,1) NOT NULL,
	[SUCode] [varchar](10) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[ZIP] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_MMeterTypes]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_MMeterTypes](
	[MeterTypeId] [int] NOT NULL,
	[MeterType] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[ActiveStatus] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentityMeterId] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_LEnergyClassCharges]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_LEnergyClassCharges](
	[EnergyClassChargeID] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[FromKW] [int] NULL,
	[ToKW] [int] NULL,
	[FromDate] [datetime] NULL,
	[Todate] [datetime] NULL,
	[ClassID] [int] NULL,
	[IsActive] [bit] NULL,
	[TaxId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentityChargeId] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_AUDIT_LAdditionalClassCharges]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_AUDIT_LAdditionalClassCharges](
	[AdditionalChargeID] [int] NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[ClassID] [int] NULL,
	[IsActive] [bit] NULL,
	[ChargeId] [int] NULL,
	[TaxId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentityChargeId] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_FunctionApprovalRole]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_FunctionApprovalRole](
	[ApprovalRoleId] [int] NULL,
	[FunctionId] [int] NULL,
	[RoleId] [int] NULL,
	[Level] [int] NULL,
	[IsFinalApproval] [bit] NULL,
	[UserIds] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_FunctionalAccessPermission]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_FunctionalAccessPermission](
	[FunctionId] [int] NULL,
	[Function] [varchar](200) NULL,
	[AccessLevels] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_Cycles]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_Cycles](
	[CycleId] [varchar](20) NOT NULL,
	[CycleName] [varchar](300) NULL,
	[DetailsOfCycle] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[ContactName] [varchar](200) NULL,
	[ContactNo] [varchar](20) NULL,
	[IdentityCycleId] [int] IDENTITY(1,1) NOT NULL,
	[CycleCode] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerTariffChangeRequest]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerTariffChangeRequest](
	[TariffChangeRequestId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[PreviousTariffId] [int] NULL,
	[ChangeRequestedTariffId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[Remarks] [varchar](max) NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerReadings]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerReadings](
	[AuditCustomerReadingId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerReadingId] [int] NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[AccountNumber] [varchar](50) NULL,
	[ReadDate] [datetime] NULL,
	[ReadBy] [varchar](50) NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Usage] [numeric](20, 4) NULL,
	[TotalReadingEnergies] [decimal](18, 2) NULL,
	[TotalReadings] [int] NULL,
	[Multiplier] [int] NULL,
	[ReadType] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsBilled] [bit] NULL,
	[BilledBy] [varchar](50) NULL,
	[BilledDate] [varchar](50) NULL,
	[AverageReading] [varchar](50) NULL,
	[StatusId] [varchar](50) NULL,
	[StatusDate] [datetime] NULL,
	[BillNo] [varchar](50) NULL,
	[UsageForBilling] [decimal](18, 2) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerMeterInfoChangeLogs]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerMeterInfoChangeLogs](
	[MeterInfoChangeLogId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[OldMeterNo] [varchar](100) NULL,
	[NewMeterNo] [varchar](100) NULL,
	[OldMeterTypeId] [int] NULL,
	[NewMeterTypeId] [int] NULL,
	[OldDials] [int] NULL,
	[NewDials] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[OldMeterReading] [varchar](20) NULL,
	[NewMeterReading] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerDetails](
	[Audit_Identity] [int] IDENTITY(1,1) NOT NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[AccountNo] [varchar](50) NULL,
	[Title] [varchar](10) NULL,
	[Name] [varchar](100) NULL,
	[SurName] [varchar](100) NULL,
	[KnownAs] [varchar](100) NULL,
	[EmailId] [varchar](max) NULL,
	[DocumentNo] [varchar](100) NULL,
	[EmployeeCode] [varchar](100) NULL,
	[HomeContactNo] [varchar](20) NULL,
	[BusinessContactNo] [varchar](20) NULL,
	[OtherContactNo] [varchar](20) NULL,
	[PostalLandMark] [varchar](200) NULL,
	[PostalStreet] [varchar](200) NULL,
	[PostalCity] [varchar](200) NULL,
	[PostalHouseNo] [varchar](200) NULL,
	[PostalDetails] [varchar](max) NULL,
	[PostalZipCode] [varchar](50) NULL,
	[IdentityNo] [varchar](100) NULL,
	[IdentityId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[BookNo] [varchar](20) NULL,
	[InjectionSubStationId] [varchar](20) NULL,
	[FeederId] [varchar](20) NULL,
	[TransformerId] [varchar](20) NULL,
	[PoleId] [varchar](20) NULL,
	[ServiceLandMark] [varchar](200) NULL,
	[ServiceStreet] [varchar](200) NULL,
	[ServiceCity] [varchar](200) NULL,
	[ServiceHouseNo] [varchar](200) NULL,
	[ServiceDetails] [varchar](max) NULL,
	[ServiceZipCode] [varchar](50) NULL,
	[ServiceEmailId] [varchar](max) NULL,
	[MobileNo] [varchar](20) NULL,
	[RouteSequenceNo] [int] NULL,
	[NeighborAccountNo] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
	[Longitude] [varchar](50) NULL,
	[TariffId] [int] NULL,
	[MultiplicationFactor] [decimal](18, 4) NULL,
	[AccountTypeId] [int] NULL,
	[ApplicationDate] [datetime] NULL,
	[ReadCodeId] [int] NULL,
	[OrganizationCode] [varchar](50) NULL,
	[CustomerTypeId] [int] NULL,
	[OldAccountNo] [varchar](50) NULL,
	[MeterTypeId] [int] NULL,
	[PhaseId] [int] NULL,
	[IsEmbassyCustomer] [bit] NULL,
	[EmbassyCode] [varchar](50) NULL,
	[IsVIPCustomer] [bit] NULL,
	[MeterNo] [varchar](100) NULL,
	[MeterSerialNo] [varchar](100) NULL,
	[ConnectionDate] [datetime] NULL,
	[MeterStatusId] [int] NULL,
	[ConnectionReasonId] [int] NULL,
	[InitialReading] [varchar](max) NULL,
	[CurrentReading] [varchar](max) NULL,
	[AverageReading] [varchar](max) NULL,
	[SetupDate] [datetime] NULL,
	[CertifiedBy] [varchar](100) NULL,
	[ProccessedById] [int] NULL,
	[InstalledBy] [varchar](100) NULL,
	[AgencyId] [int] NULL,
	[ContactPersonName] [varchar](100) NULL,
	[AgencyDetails] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[RouteNo] [int] NULL,
	[CustomerSequenceNo] [bigint] NULL,
	[CustomerUniqueId] [int] NULL,
	[StateCode] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[MinimumReading] [varchar](max) NULL,
	[Highestconsumption] [varchar](15) NULL,
	[SortOrder] [int] NULL,
	[StatusId] [varchar](10) NULL,
	[IsBookNoChanged] [bit] NULL,
	[OutStandingAmount] [decimal](18, 2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_CustomerBills]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_CustomerBills](
	[AuditCustomerBillId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerBillId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerId] [varchar](50) NULL,
	[TotalBillAmount] [decimal](20, 4) NULL,
	[ServiceAddress] [varchar](max) NULL,
	[MeterNo] [varchar](50) NULL,
	[Dials] [int] NULL,
	[NetArrears] [decimal](20, 4) NULL,
	[NetEnergyCharges] [decimal](20, 4) NULL,
	[NetFixedCharges] [decimal](20, 4) NULL,
	[VAT] [decimal](20, 4) NULL,
	[VATPercentage] [decimal](20, 4) NULL,
	[Messages] [varchar](max) NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[SubStationId] [varchar](20) NULL,
	[FeederId] [varchar](20) NULL,
	[TransFormerId] [varchar](20) NULL,
	[PoleId] [varchar](20) NULL,
	[BillGeneratedBy] [varchar](50) NULL,
	[BillGeneratedDate] [datetime] NULL,
	[PaymentLastDate] [datetime] NULL,
	[TariffId] [int] NULL,
	[BillYear] [int] NULL,
	[BillMonth] [int] NULL,
	[CycleId] [varchar](20) NULL,
	[TotalBillAmountWithArrears] [decimal](20, 4) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BillNo] [varchar](50) NULL,
	[PaymentStatusID] [int] NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Usage] [decimal](20, 4) NULL,
	[AverageReading] [decimal](20, 4) NULL,
	[TotalBillAmountWithTax] [decimal](20, 4) NULL,
	[EstimatedUsage] [int] NULL,
	[ReadCodeId] [int] NULL,
	[ReadType] [int] NULL,
	[StatusId] [varchar](50) NULL,
	[StatusDate] [datetime] NULL,
	[AdjustmentAmmount] [decimal](18, 2) NULL,
	[Remarks] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_Customer_Additionalcharges]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_Customer_Additionalcharges](
	[AuditCustomerAdditioalCharges] [int] IDENTITY(1,1) NOT NULL,
	[CustomerAdditioalCharges] [int] NULL,
	[CustomerID] [varchar](50) NULL,
	[AccountNO] [varchar](50) NULL,
	[CustomerBillId] [int] NULL,
	[TariffId] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBY] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
	[ChargeId] [int] NULL,
	[StatusId] [varchar](50) NULL,
	[StatusDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_Countries]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_Countries](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [varchar](20) NOT NULL,
	[CountryName] [varchar](50) NULL,
	[CountryDetails] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_BussinessUnits]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_BussinessUnits](
	[BU_Audit_Id] [int] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](20) NOT NULL,
	[BusinessUnitName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentityBU_ID] [int] NULL,
	[StateCode] [varchar](20) NULL,
	[BUCode] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_BookNumbers]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_BookNumbers](
	[Audit_Identity] [int] IDENTITY(1,1) NOT NULL,
	[BookNo] [varchar](20) NULL,
	[ID] [varchar](20) NULL,
	[CycleId] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[NoOfAccounts] [int] NULL,
	[Details] [varchar](max) NULL,
	[IdentityBookId] [int] NULL,
	[BookCode] [varchar](10) NULL,
	[SortOrder] [int] NULL,
	[StatusId] [varchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_BillingQueueSchedule]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_BillingQueueSchedule](
	[AuditBillingQueueScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[BillingQueueScheduleId] [int] NULL,
	[BillingMonth] [int] NULL,
	[BillingYear] [int] NULL,
	[BillSendingMode] [int] NULL,
	[BillProcessTypeId] [int] NULL,
	[TotalCustomersInQueue] [int] NULL,
	[OpenStatusId] [int] NULL,
	[ServiceStartDate] [datetime] NULL,
	[FeederId] [varchar](20) NULL,
	[CycleId] [varchar](20) NULL,
	[BillGenarationStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedOn] [datetime] NULL,
	[StatusId] [varchar](50) NULL,
	[StatusDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_BillingQueeCustomers]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_BillingQueeCustomers](
	[AuditBillQueueCustomerId] [int] IDENTITY(1,1) NOT NULL,
	[BillQueueCustomerId] [int] NULL,
	[BillingQueuescheduleId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[Month] [int] NULL,
	[Year] [int] NULL,
	[BillGenarationStatusId] [int] NULL,
	[BillFile] [varchar](max) NULL,
	[StatusId] [varchar](50) NULL,
	[StatusDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_BillAdjustments]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_BillAdjustments](
	[AuditBillAdjustmentId] [int] IDENTITY(1,1) NOT NULL,
	[BillAdjustmentId] [int] NULL,
	[CustomerBillId] [int] NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerId] [varchar](50) NULL,
	[Remarks] [varchar](max) NULL,
	[AmountEffected] [decimal](18, 4) NULL,
	[ApprovalStatusId] [int] NULL,
	[BillAdjustmentType] [int] NULL,
	[TaxEffected] [decimal](18, 2) NULL,
	[AdjustedUnits] [int] NULL,
	[TotalAmountEffected] [decimal](18, 2) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[ApprovedDate] [varchar](50) NULL,
	[EffectedBillId] [int] NULL,
	[StatusId] [varchar](10) NULL,
	[StatusDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit_Agencies]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit_Agencies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AgencyId] [int] NULL,
	[AgencyName] [varchar](300) NULL,
	[Details] [varchar](max) NULL,
	[ContactNo1] [varchar](20) NULL,
	[ContactNo2] [varchar](20) NULL,
	[ContactPersonName] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Audit__ServiceCenter]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Audit__ServiceCenter](
	[ServiceCenterId] [varchar](20) NOT NULL,
	[ServiceCenterName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[SU_ID] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentitySC_ID] [int] IDENTITY(1,1) NOT NULL,
	[SCCode] [varchar](10) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[ZIP] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TestBook]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TestBook](
	[BookNo] [varchar](20) NOT NULL,
	[BookCode] [varchar](10) NULL,
	[CycleId] [varchar](20) NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserSpecialPermissions]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserSpecialPermissions](
	[UserPermissionId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](50) NULL,
	[BudgetSegmentWiseMargin] [bit] NULL,
	[CustomerDetailsModification] [bit] NULL,
	[BillAdjustment] [bit] NULL,
	[TariffAdjustments] [bit] NULL,
	[PaymentsAdjustments] [bit] NULL,
	[Disconnection] [bit] NULL,
	[ReConnections] [bit] NULL,
	[BillGeneration] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserPermissionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserServiceUnits]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserServiceUnits](
	[UserServiceUnitId] [int] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserServiceUnitId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserServiceCenters]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserServiceCenters](
	[UserServiceCenterId] [int] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserServiceCenterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserRoles]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserRoles](
	[UserRoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserDocuments]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserDocuments](
	[UserDocumentId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](50) NULL,
	[DocumentName] [varchar](max) NULL,
	[Path] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserDocumentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserCashOffices]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserCashOffices](
	[UserCashOfficeId] [int] IDENTITY(1,1) NOT NULL,
	[CashOfficeId] [int] NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserCashOfficeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_UserBusinessUnits]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_UserBusinessUnits](
	[UserBusinessUnitId] [int] IDENTITY(1,1) NOT NULL,
	[BU_ID] [varchar](20) NULL,
	[UserId] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserBusinessUnitId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_TestPole]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_TestPole](
	[PoleId] [int] NULL,
	[PoleMasterId] [int] NULL,
	[Name] [varchar](150) NULL,
	[Description] [varchar](150) NULL,
	[Code] [varchar](50) NULL,
	[Comments] [varchar](150) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PoleOrder] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_TariffRequestMessages]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_TariffRequestMessages](
	[MessageId] [int] IDENTITY(1,1) NOT NULL,
	[Message] [varchar](max) NULL,
	[TariffChangeRequestId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MessageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_TamperedCustomers]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_TamperedCustomers](
	[TamperedId] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](20) NULL,
	[MeterNo] [varchar](50) NULL,
	[MeterReaderId] [int] NULL,
	[ReadingDate] [datetime] NULL,
	[Reason] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[TamperedId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerDocuments]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerDocuments](
	[CustomerDocumentId] [int] IDENTITY(1,1) NOT NULL,
	[DocumentName] [varchar](max) NULL,
	[Path] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[GlobalAccountNo] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerDocumentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerDetails](
	[CustomerUniqueNo] [varchar](50) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[Title] [varchar](10) NULL,
	[Name] [varchar](100) NULL,
	[SurName] [varchar](100) NULL,
	[KnownAs] [varchar](100) NULL,
	[EmailId] [varchar](max) NULL,
	[DocumentNo] [varchar](100) NULL,
	[EmployeeCode] [varchar](100) NULL,
	[HomeContactNo] [varchar](20) NULL,
	[BusinessContactNo] [varchar](20) NULL,
	[OtherContactNo] [varchar](20) NULL,
	[PostalLandMark] [varchar](200) NULL,
	[PostalStreet] [varchar](200) NULL,
	[PostalCity] [varchar](200) NULL,
	[PostalHouseNo] [varchar](200) NULL,
	[PostalDetails] [varchar](max) NULL,
	[PostalZipCode] [varchar](50) NULL,
	[IdentityNo] [varchar](100) NULL,
	[IdentityId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[BookNo] [varchar](20) NULL,
	[InjectionSubStationId] [varchar](20) NULL,
	[FeederId] [varchar](20) NULL,
	[TransformerId] [varchar](20) NULL,
	[PoleId] [varchar](20) NULL,
	[ServiceLandMark] [varchar](200) NULL,
	[ServiceStreet] [varchar](200) NULL,
	[ServiceCity] [varchar](200) NULL,
	[ServiceHouseNo] [varchar](200) NULL,
	[ServiceDetails] [varchar](max) NULL,
	[ServiceZipCode] [varchar](50) NULL,
	[ServiceEmailId] [varchar](max) NULL,
	[MobileNo] [varchar](20) NULL,
	[RouteSequenceNo] [int] NULL,
	[NeighborAccountNo] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
	[Longitude] [varchar](50) NULL,
	[TariffId] [int] NULL,
	[MultiplicationFactor] [decimal](18, 4) NULL,
	[AccountTypeId] [int] NULL,
	[ApplicationDate] [datetime] NULL,
	[ReadCodeId] [int] NULL,
	[OrganizationCode] [varchar](50) NULL,
	[CustomerTypeId] [int] NULL,
	[OldAccountNo] [varchar](50) NULL,
	[MeterTypeId] [int] NULL,
	[PhaseId] [int] NULL,
	[IsEmbassyCustomer] [bit] NULL,
	[EmbassyCode] [varchar](50) NULL,
	[IsVIPCustomer] [bit] NULL,
	[MeterNo] [varchar](100) NULL,
	[MeterSerialNo] [varchar](100) NULL,
	[ConnectionDate] [datetime] NULL,
	[MeterStatusId] [int] NULL,
	[ConnectionReasonId] [int] NULL,
	[InitialReading] [varchar](max) NULL,
	[CurrentReading] [varchar](max) NULL,
	[AverageReading] [varchar](max) NULL,
	[SetupDate] [datetime] NULL,
	[CertifiedBy] [varchar](100) NULL,
	[ProccessedById] [int] NULL,
	[InstalledBy] [varchar](100) NULL,
	[AgencyId] [int] NULL,
	[ContactPersonName] [varchar](100) NULL,
	[AgencyDetails] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[RouteNo] [int] NULL,
	[CustomerSequenceNo] [bigint] NULL,
	[CustomerUniqueId] [int] IDENTITY(1,1) NOT NULL,
	[StateCode] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[MinimumReading] [varchar](max) NULL,
	[Highestconsumption] [varchar](15) NULL,
	[SortOrder] [int] NULL,
	[OutStandingAmount] [decimal](18, 2) NULL,
	[IsBookNoChanged] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerUniqueNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UX_Constraint_AccountNo] UNIQUE NONCLUSTERED 
(
	[AccountNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerBills]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerBills](
	[CustomerBillId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[CustomerId] [varchar](50) NULL,
	[TotalBillAmount] [decimal](20, 4) NULL,
	[ServiceAddress] [varchar](max) NULL,
	[MeterNo] [varchar](50) NULL,
	[Dials] [int] NULL,
	[NetArrears] [decimal](20, 4) NULL,
	[NetEnergyCharges] [decimal](20, 4) NULL,
	[NetFixedCharges] [decimal](20, 4) NULL,
	[VAT] [decimal](20, 4) NULL,
	[VATPercentage] [decimal](20, 4) NULL,
	[Messages] [varchar](max) NULL,
	[BU_ID] [varchar](20) NULL,
	[SU_ID] [varchar](20) NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[PoleId] [varchar](20) NULL,
	[BillGeneratedBy] [varchar](50) NULL,
	[BillGeneratedDate] [datetime] NULL,
	[PaymentLastDate] [datetime] NULL,
	[TariffId] [int] NULL,
	[BillYear] [int] NULL,
	[BillMonth] [int] NULL,
	[CycleId] [varchar](20) NULL,
	[TotalBillAmountWithArrears] [decimal](20, 4) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BillNo] [varchar](50) NULL,
	[PaymentStatusID] [int] NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Usage] [decimal](20, 4) NULL,
	[AverageReading] [decimal](20, 4) NULL,
	[TotalBillAmountWithTax] [decimal](20, 4) NULL,
	[EstimatedUsage] [int] NULL,
	[ReadCodeId] [int] NULL,
	[ReadType] [int] NULL,
	[AdjustmentAmmount] [decimal](18, 2) NULL,
	[Remarks] [varchar](max) NULL,
	[BalanceUsage] [int] NULL,
	[BillingTypeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerBillId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerBillPayments]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerBillPayments](
	[CustomerBillPaymentId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerPaymentId] [int] NULL,
	[PaidAmount] [decimal](20, 4) NULL,
	[BillNo] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerBillPaymentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerAddressChangeLogs]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerAddressChangeLogs](
	[AddressChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[OldHouseNo] [varchar](200) NULL,
	[NewHouseNo] [varchar](200) NULL,
	[OldStreetName] [varchar](50) NULL,
	[NewStreetName] [varchar](50) NULL,
	[OldCity] [varchar](50) NULL,
	[NewCity] [varchar](50) NULL,
	[OldLandmark] [varchar](50) NULL,
	[NewLandmark] [varchar](50) NULL,
	[OldAreaCode] [varchar](50) NULL,
	[NewAreaCode] [varchar](50) NULL,
	[OldZipCode] [varchar](50) NULL,
	[NewZipCode] [varchar](50) NULL,
	[OldServiceAddressID] [int] NULL,
	[NewServiceAddressID] [int] NULL,
	[OldPostalAddressID] [int] NULL,
	[NewPostalAddressID] [int] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsServiceAddress] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[AddressChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerActiveStatusChangeLogs]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerActiveStatusChangeLogs](
	[ActiveStatusChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[OldStatus] [int] NULL,
	[NewStatus] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ActiveStatusChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerPaymentsApproval]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerPaymentsApproval](
	[CustomerPaymentID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [varchar](50) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[ReceiptNo] [varchar](20) NULL,
	[PaymentMode] [int] NULL,
	[DocumentPath] [varchar](max) NULL,
	[Cashier] [varchar](50) NULL,
	[CashOffice] [int] NULL,
	[DocumentName] [varchar](max) NULL,
	[RecievedDate] [datetime] NULL,
	[PaymentType] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PaidAmount] [decimal](18, 2) NULL,
	[BatchNo] [int] NULL,
	[ActivestatusId] [int] NULL,
	[Approval] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerPayments]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerPayments](
	[CustomerPaymentID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [varchar](50) NULL,
	[AccountNo] [varchar](50) NULL,
	[ReceiptNo] [varchar](20) NULL,
	[PaymentMode] [int] NULL,
	[DocumentPath] [varchar](max) NULL,
	[Cashier] [varchar](50) NULL,
	[CashOffice] [int] NULL,
	[DocumentName] [varchar](max) NULL,
	[RecievedDate] [datetime] NULL,
	[PaymentType] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PaidAmount] [decimal](18, 2) NULL,
	[BatchNo] [int] NULL,
	[ActivestatusId] [int] NULL,
	[ReceivedDevice] [int] NULL,
	[Remarks] [varchar](50) NULL,
 CONSTRAINT [PK_Tbl_CustomerPayments] PRIMARY KEY CLUSTERED 
(
	[CustomerPaymentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerNameChangeLogs]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerNameChangeLogs](
	[NameChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[OldTitle] [varchar](10) NULL,
	[NewTitle] [varchar](10) NULL,
	[OldFirstName] [varchar](50) NULL,
	[NewFirstName] [varchar](50) NULL,
	[OldMiddleName] [varchar](50) NULL,
	[NewMiddleName] [varchar](50) NULL,
	[OldLastName] [varchar](50) NULL,
	[NewLastName] [varchar](50) NULL,
	[OldKnownAs] [varchar](50) NULL,
	[NewKnownAs] [varchar](150) NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[NameChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerMeterInfoChangeLogs]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerMeterInfoChangeLogs](
	[MeterInfoChangeLogId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[OldMeterNo] [varchar](100) NULL,
	[NewMeterNo] [varchar](100) NULL,
	[OldMeterTypeId] [int] NULL,
	[NewMeterTypeId] [int] NULL,
	[OldDials] [int] NULL,
	[NewDials] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ApproveStatusId] [int] NULL,
	[Remarks] [varchar](max) NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
	[OldMeterReading] [varchar](20) NULL,
	[NewMeterReading] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterInfoChangeLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerReadingsLog]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerReadingsLog](
	[ReadingLogId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNumber] [varchar](50) NULL,
	[LatestReadDate] [datetime] NULL,
	[ReadBy] [varchar](50) NULL,
	[PreviousReading] [numeric](20, 6) NULL,
	[PresentReading] [numeric](20, 6) NULL,
	[ReadType] [int] NULL,
	[IsBilled] [bit] NULL,
	[BilledBy] [varchar](50) NULL,
	[BilledDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_CustomerReadings]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_CustomerReadings](
	[CustomerReadingId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[AccountNumber] [varchar](50) NULL,
	[ReadDate] [datetime] NULL,
	[ReadBy] [varchar](50) NULL,
	[PreviousReading] [varchar](50) NULL,
	[PresentReading] [varchar](50) NULL,
	[Usage] [numeric](20, 4) NULL,
	[TotalReadingEnergies] [decimal](18, 2) NULL,
	[TotalReadings] [int] NULL,
	[Multiplier] [int] NULL,
	[ReadType] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsBilled] [bit] NULL,
	[BilledBy] [varchar](50) NULL,
	[BilledDate] [varchar](50) NULL,
	[AverageReading] [varchar](50) NULL,
	[BillNo] [varchar](50) NULL,
	[UsageForBilling] [decimal](18, 2) NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[IsTamper] [bit] NULL,
 CONSTRAINT [PK__Tbl_Cust__BF1C43EB033C6B35] PRIMARY KEY CLUSTERED 
(
	[CustomerReadingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PoleDescriptionTable]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PoleDescriptionTable](
	[PoleId] [int] NULL,
	[PoleMasterId] [int] NULL,
	[Name] [varchar](150) NULL,
	[Description] [varchar](150) NULL,
	[Code] [varchar](50) NULL,
	[Comments] [varchar](150) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PoleOrder] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PasswordStrength]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PasswordStrength](
	[StrengthTypeId] [int] IDENTITY(1,1) NOT NULL,
	[StrengthName] [varchar](200) NULL,
	[MinLength] [varchar](10) NULL,
	[MaxLength] [varchar](10) NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[StrengthTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaidMeterPaymentDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaidMeterPaymentDetails](
	[CapnyMeterPaymentsId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[MeterNo] [varchar](100) NULL,
	[Amount] [decimal](18, 2) NULL,
	[BillNo] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[CapnyMeterPaymentsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaidMeterDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaidMeterDetails](
	[MeterId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[MeterNo] [varchar](100) NULL,
	[MeterTypeId] [int] NULL,
	[MeterCost] [decimal](18, 2) NULL,
	[OutStandingAmount] [decimal](18, 2) NULL,
	[ActiveStatusId] [int] NULL,
	[MeterAssignedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_PaidMeterAdjustmentDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_PaidMeterAdjustmentDetails](
	[MeterAdjustmentId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[MeterNo] [varchar](100) NULL,
	[AdjustedAmount] [decimal](18, 2) NULL,
	[ApprovalStatusId] [int] NULL,
	[Reason] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterAdjustmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Notifications]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Notifications](
	[NoticeID] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [varchar](500) NULL,
	[Details] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[FilePath] [varchar](50) NULL,
	[activestatus] [int] NULL,
 CONSTRAINT [PK_Tbl_Notifications] PRIMARY KEY CLUSTERED 
(
	[NoticeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_NewCustomerLocator]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_NewCustomerLocator](
	[NewCustomerLocatorId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Primary_ContactNo] [varchar](20) NULL,
	[Secondary_ContactNo] [varchar](20) NULL,
	[CycleID] [varchar](20) NULL,
	[MarketerName] [varchar](20) NULL,
	[Note] [varchar](500) NULL,
	[Longitude] [varchar](50) NULL,
	[Latitude] [varchar](50) NULL,
	[ApprovalStatus] [int] NULL,
	[CreatedDate] [smalldatetime] NULL,
	[CreatedBy] [varchar](20) NULL,
	[ModifiedDate] [smalldatetime] NULL,
	[ModifiedBy] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[NewCustomerLocatorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_MeterInformation]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_MeterInformation](
	[MeterType] [int] NULL,
	[MeterSize] [varchar](50) NULL,
	[MeterDetails] [varchar](50) NULL,
	[MeterMultiplier] [varchar](50) NULL,
	[MeterBrand] [varchar](50) NULL,
	[MeterStatus] [int] NULL,
	[MeterSerialNo] [varchar](50) NULL,
	[MeterRating] [varchar](50) NULL,
	[NextCalibrationDate] [datetime] NULL,
	[ServiceHoursBeforeCalibration] [varchar](50) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[MeterDials] [int] NULL,
	[Decimals] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
	[MeterId] [int] IDENTITY(1,1) NOT NULL,
	[MeterNo] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MeterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Menus]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Menus](
	[MenuId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](1000) NULL,
	[Path] [varchar](max) NULL,
	[Menu_Order] [int] NULL,
	[ReferenceMenuId] [int] NULL,
	[Icon1] [varchar](500) NULL,
	[Icon2] [varchar](500) NULL,
	[Page_Order] [int] NULL,
	[IsActive] [bit] NULL,
	[ActiveHeader] [bit] NOT NULL,
	[Menu_Image] [varchar](200) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifiedBy] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_LEnergyClassCharges]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_LEnergyClassCharges](
	[EnergyClassChargeID] [int] IDENTITY(1,1) NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[FromKW] [int] NULL,
	[ToKW] [int] NULL,
	[FromDate] [datetime] NULL,
	[Todate] [datetime] NULL,
	[ClassID] [int] NULL,
	[IsActive] [bit] NULL,
	[TaxId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EnergyClassChargeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_LCustomerTariffChangeRequest]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_LCustomerTariffChangeRequest](
	[TariffChangeRequestId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[PreviousTariffId] [int] NULL,
	[ChangeRequestedTariffId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[Remarks] [varchar](max) NULL,
	[ApprovalStatusId] [int] NULL,
	[PresentApprovalRole] [int] NULL,
	[NextApprovalRole] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[TariffChangeRequestId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_LAdditionalClassCharges]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_LAdditionalClassCharges](
	[AdditionalChargeID] [int] IDENTITY(1,1) NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[ClassID] [int] NULL,
	[IsActive] [bit] NULL,
	[ChargeId] [int] NULL,
	[TaxId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AdditionalChargeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_HHInputFiles]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_HHInputFiles](
	[HHInputFileID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](100) NULL,
	[CycleID] [varchar](50) NULL,
	[Month] [int] NULL,
	[Year] [int] NULL,
	[ActiveStatus] [int] NULL,
	[CreatedBy] [varchar](20) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](20) NULL,
	[ModifiedDate] [datetime] NULL,
	[Notes] [varchar](250) NULL,
 CONSTRAINT [PK_Tbl_HHInputFiles] PRIMARY KEY CLUSTERED 
(
	[HHInputFileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_GovtCustomers]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_GovtCustomers](
	[GlobalAccountNumber] [varchar](50) NULL,
	[MGActTypeID] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_GlobalMessages]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_GlobalMessages](
	[GlobalMessageId] [int] IDENTITY(1,1) NOT NULL,
	[Message] [varchar](max) NULL,
	[StateCode] [varchar](20) NULL,
	[BillingTypeId] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[BU_ID] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[GlobalMessageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_EstimationSettings]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_EstimationSettings](
	[EstimationSettingId] [int] IDENTITY(1,1) NOT NULL,
	[BillingYear] [int] NULL,
	[BillingMonth] [int] NULL,
	[BillingRule] [int] NULL,
	[CycleId] [varchar](20) NULL,
	[TariffId] [int] NULL,
	[EnergytoCalculate] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ClusterCategoryId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[EstimationSettingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_EstimatedEnergy]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_EstimatedEnergy](
	[EstimatedEnergyId] [int] IDENTITY(1,1) NOT NULL,
	[EnergyToEstimate] [int] NULL,
	[BillYear] [int] NULL,
	[BillMonth] [int] NULL,
	[TariffId] [int] NULL,
	[FeederId] [varchar](20) NULL,
	[CycleId] [varchar](20) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[EstimatedEnergyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_EstimatedCustomerRule]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_EstimatedCustomerRule](
	[EstCustomerRuleId] [int] IDENTITY(1,1) NOT NULL,
	[TariffID] [int] NULL,
	[BillingMonth] [int] NULL,
	[BillingYEar] [int] NULL,
	[RuleId] [int] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[EstCustomerRuleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_EBillMailLogs]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_EBillMailLogs](
	[EBillMailLogsId] [int] IDENTITY(1,1) NOT NULL,
	[BUEBMID] [int] NULL,
	[BU_TxtFile] [varchar](max) NULL,
	[BU_XlsFile] [varchar](max) NULL,
	[ConsolidatedFile] [varchar](max) NULL,
	[ConsolidatedDuplicateFile] [varchar](max) NULL,
	[CreatedDate] [smalldatetime] NULL,
	[IsMailSent] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[EBillMailLogsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_DistrictMessages]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_DistrictMessages](
	[DistrictMessageId] [int] IDENTITY(1,1) NOT NULL,
	[Message] [varchar](max) NULL,
	[DistrictCode] [varchar](20) NULL,
	[IsGlobalMessage] [bit] NULL,
	[BillingTypeId] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[GlobalMessageId] [int] NULL,
	[BU_ID] [varchar](20) NULL,
 CONSTRAINT [PK__Tbl_Dist__4F012867538D5813] PRIMARY KEY CLUSTERED 
(
	[DistrictMessageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_DirectCustomerEnergyDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_DirectCustomerEnergyDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerUniqueNo] [varchar](50) NULL,
	[AccountNo] [varchar](50) NULL,
	[EnergyCharges] [int] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_DirectCustomerEnergyCharges]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_DirectCustomerEnergyCharges](
	[DirectCustEnergyID] [int] IDENTITY(1,1) NOT NULL,
	[TariffID] [int] NULL,
	[EnergyInKW] [int] NULL,
	[CreatedBy] [varchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](100) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[DirectCustEnergyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Cycles]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Cycles](
	[CycleId] [varchar](20) NOT NULL,
	[CycleName] [varchar](300) NULL,
	[DetailsOfCycle] [varchar](max) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ServiceCenterId] [varchar](20) NULL,
	[ContactName] [varchar](200) NULL,
	[ContactNo] [varchar](20) NULL,
	[IdentityCycleId] [int] IDENTITY(1,1) NOT NULL,
	[CycleCode] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[CycleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomerTenentDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomerTenentDetails](
	[TenentId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](100) NULL,
	[MiddleName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[PhoneNumber] [varchar](20) NULL,
	[AlternatePhoneNumber] [varchar](20) NULL,
	[EmailID] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[Title] [varchar](15) NULL,
PRIMARY KEY CLUSTERED 
(
	[TenentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomerSDetail]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomerSDetail](
	[CustomerId] [bigint] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NOT NULL,
	[AccountNo] [varchar](50) NULL,
	[Title] [varchar](10) NULL,
	[FirstName] [varchar](50) NULL,
	[MiddleName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[KnownAs] [varchar](150) NULL,
	[EmailId] [varchar](max) NULL,
	[HomeContactNumber] [varchar](20) NULL,
	[BusinessContactNumber] [varchar](20) NULL,
	[OtherContactNumber] [varchar](20) NULL,
	[IsSameAsService] [bit] NULL,
	[ServiceAddressID] [int] NULL,
	[IsSameAsTenent] [bit] NULL,
	[DocumentNo] [varchar](50) NULL,
	[ApplicationDate] [datetime] NULL,
	[ConnectionDate] [datetime] NULL,
	[SetupDate] [datetime] NULL,
	[ActiveStatusId] [int] NULL,
	[IsBEDCEmployee] [bit] NULL,
	[EmployeeCode] [int] NULL,
	[IsVIPCustomer] [bit] NULL,
	[TenentId] [int] NULL,
	[OldAccountNo] [varchar](20) NULL,
	[OrganizationCode] [varchar](20) NULL,
	[ConnectionReasonId] [varchar](20) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[PostalAddressID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[GlobalAccountNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomerProceduralDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomerProceduralDetails](
	[GlobalAccountNumber] [varchar](50) NULL,
	[CustomerTypeId] [int] NULL,
	[TariffClassID] [int] NULL,
	[RouteSequenceNumber] [bigint] NULL,
	[IsEmbassyCustomer] [bit] NULL,
	[EmbassyCode] [varchar](50) NULL,
	[PhaseId] [int] NULL,
	[ActiveStatusId] [int] NULL,
	[ReadCodeID] [int] NULL,
	[SortOrder] [int] NULL,
	[IsBookNoChanged] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ClusterCategoryId] [int] NULL,
	[BookNo] [varchar](20) NULL,
	[PoleID] [varchar](50) NULL,
	[MeterNumber] [varchar](50) NULL,
	[AccountTypeId] [int] NULL,
	[CustomerProcedureId] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerProcedureId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails](
	[HouseNo] [varchar](200) NULL,
	[StreetName] [varchar](200) NULL,
	[City] [varchar](200) NULL,
	[Landmark] [varchar](200) NULL,
	[Details] [varchar](200) NULL,
	[AreaCode] [int] NULL,
	[Latitude] [varchar](50) NULL,
	[Longitude] [varchar](50) NULL,
	[IsServiceAddress] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[ZipCode] [varchar](50) NULL,
	[AddressID] [int] IDENTITY(1,1) NOT NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[IsCommunication] [bit] NULL,
	[IsActive] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[AddressID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomerIdentityDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails](
	[GlobalAccountNumber] [varchar](50) NULL,
	[IdentityNumber] [varchar](150) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentityId] [int] IDENTITY(1,1) NOT NULL,
	[IdentityTypeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdentityId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_CustomerActiveDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_CustomerActiveDetails](
	[GlobalAccountNumber] [varchar](50) NULL,
	[CustomerActiveId] [int] IDENTITY(1,1) NOT NULL,
	[MeterAmount] [decimal](18, 2) NULL,
	[InitialBillingKWh] [bigint] NULL,
	[InitialReading] [bigint] NULL,
	[PresentReading] [bigint] NULL,
	[Highestconsumption] [decimal](18, 2) NULL,
	[OutStandingAmount] [decimal](18, 2) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[AvgReading] [bigint] NULL,
	[IsCAPMI] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerActiveId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [CUSTOMERS].[Tbl_ApplicationProcessDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_ApplicationProcessDetails](
	[GlobalAccountNumber] [varchar](50) NULL,
	[Bedcinfoid] [int] IDENTITY(1,1) NOT NULL,
	[CertifiedBy] [varchar](50) NULL,
	[Seal1] [varchar](50) NULL,
	[Seal2] [varchar](50) NULL,
	[ApplicationProcessedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Bedcinfoid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_Agencies]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_Agencies](
	[AgencyId] [int] IDENTITY(1,1) NOT NULL,
	[AgencyName] [varchar](300) NULL,
	[Details] [varchar](max) NULL,
	[ContactNo1] [varchar](20) NULL,
	[ContactNo2] [varchar](20) NULL,
	[ContactPersonName] [varchar](50) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AgencyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_States]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_States](
	[StateCode] [varchar](20) NOT NULL,
	[StateName] [varchar](50) NULL,
	[DisplayCode] [varchar](20) NULL,
	[CountryCode] [varchar](20) NULL,
	[StateDetails] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[LGAId] [int] NULL,
 CONSTRAINT [PK_StateId] PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ServiceUnits]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ServiceUnits](
	[SU_ID] [varchar](20) NOT NULL,
	[ServiceUnitName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[BU_ID] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentitySU_ID] [int] IDENTITY(1,1) NOT NULL,
	[SUCode] [varchar](10) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[ZIP] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[SU_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tbl_ServiceCenter]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tbl_ServiceCenter](
	[ServiceCenterId] [varchar](20) NOT NULL,
	[ServiceCenterName] [varchar](300) NULL,
	[Notes] [varchar](max) NULL,
	[SU_ID] [varchar](20) NULL,
	[ActiveStatusId] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[IdentitySC_ID] [int] IDENTITY(1,1) NOT NULL,
	[SCCode] [varchar](10) NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](100) NULL,
	[ZIP] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ServiceCenterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Trigger [TR_InsertAuditStates]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <19-MAR-2014>
-- Description:	<Insert Previous data of States into Audit table Before Update>
-- =============================================

CREATE TRIGGER [dbo].[TR_InsertAuditStates]   ON  [dbo].[Tbl_States]
  INSTEAD OF UPDATE
AS 
BEGIN
	
	DECLARE 
	 @StateCode   varchar (20)   ,
	 @StateName   varchar (50)  ,
	 @DisplayCode   varchar (20)  ,
	 @CountryCode   varchar (20)  ,
	 @StateDetails   varchar (max)  ,
	 @Notes   varchar (max)  ,
	 @ModifiedBy   varchar (50)  ,
	 @IsActive   bit   ;

SELECT
 @StateCode =I.StateCode
 ,@StateName=I.StateName
 ,@DisplayCode=I.DisplayCode
 ,@CountryCode=I.CountryCode
 ,@StateDetails=I.StateDetails
 ,@Notes=I.Notes
 ,@ModifiedBy=I.ModifiedBy
 ,@IsActive=I.IsActive
FROM inserted I;

INSERT INTO Tbl_Audit_States
SELECT [StateCode]
      ,[StateName]
      ,[DisplayCode]
      ,[CountryCode]
      ,[StateDetails]
      ,[Notes]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[IsActive]
       FROM Tbl_States WHERE StateCode=@StateCode;
UPDATE Tbl_States SET
[StateName]=@StateName
      ,[DisplayCode]=@DisplayCode
      ,[CountryCode]=@CountryCode
      ,[StateDetails]=@StateDetails
      ,[Notes]=@Notes
      ,[ModifiedBy]=@ModifiedBy
      ,[ModifiedDate]=GETDATE()
      ,[IsActive]=@IsActive WHERE StateCode=@StateCode;

END
GO
/****** Object:  Trigger [TR_InsertAuditServiceUnits]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <19-MAR-2014>
-- Description:	<Insert Previous data of ServiceUnits into Audit table Before Update>
-- Modified By: Padmini
--Modified Date: 19th Feb 2015 
-- =============================================
CREATE TRIGGER [dbo].[TR_InsertAuditServiceUnits]  ON  [dbo].[Tbl_ServiceUnits]
  INSTEAD OF UPDATE
AS 
BEGIN
	
	DECLARE  
	 @SU_ID   varchar (20)   ,
	 @ServiceUnitName   varchar (300)  ,
	 @Notes   varchar (max)  ,
	 @BU_ID   varchar (20)  ,
	 @SUCode   varchar (10)  ,
	 @ActiveStatusId   int   ,
	 @Address1 varchar(100) ,
	 @Address2 varchar(100),
	 @City varchar(100),
	 @ZIP varchar(100),
	 @ModifiedBy   varchar (50) ;
	 
	 SELECT 
	 @SU_ID =I.SU_ID
	 ,@ServiceUnitName=I.ServiceUnitName
	 ,@Notes=I.Notes
	 ,@BU_ID=I.BU_ID
	 ,@SUCode=I.SUCode
	 ,@Address1=I.ActiveStatusId
	  ,@Address2=I.Address2
	  ,@City=I.City
	  ,@ZIP=I.ZIP
	 ,@ActiveStatusId=I.ActiveStatusId
	 ,@ModifiedBy=I.ModifiedBy
	 FROM inserted I; 
	 
	 INSERT INTO Tbl_Audit_ServiceUnits
	 SELECT [SU_ID]
      ,[ServiceUnitName]
      ,[Notes]
      ,[BU_ID]
      ,[ActiveStatusId]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[SUCode] 
      ,Address1
      ,Address2
      ,City
      ,ZIP
      FROM Tbl_ServiceUnits WHERE SU_ID=@SU_ID;
	 
	 UPDATE Tbl_ServiceUnits SET
							   [ServiceUnitName]=@ServiceUnitName
							  ,[Notes]=@Notes
							  ,[BU_ID]=@BU_ID
							  ,[SUCode]=@SUCode
							  ,[ActiveStatusId]=@ActiveStatusId
							  ,[Address1]=@Address1
							  ,Address2=@Address2
						       ,City=@City
							   ,ZIP=@ZIP
							  ,[ModifiedBy]=@ModifiedBy
							  ,[ModifiedDate]=GETDATE() 
	 WHERE SU_ID=@SU_ID;

END
GO
/****** Object:  Trigger [TR_InsertAuditServiceCenter]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <19-MAR-2014>
-- Description:	<Insert Previous data of ServiceCenter into Audit table Before Update>
-- Modified By: Padmini
--Modified Date: 19th Feb 2015 
-- =============================================
CREATE TRIGGER [dbo].[TR_InsertAuditServiceCenter]   ON  [dbo].[Tbl_ServiceCenter]
   INSTEAD OF UPDATE
AS 
BEGIN

DECLARE  
     @ServiceCenterId   varchar(20)   ,
	 @ServiceCenterName   varchar(300)  ,
	 @Notes   varchar(max)  ,
	 @SU_ID   varchar(20)  ,
	 @SCCode   varchar(10)  ,
	 @Address1 varchar(100) ,
	 @Address2 varchar(100),
	 @City varchar(100),
	 @ZIP varchar(100),
	 @ActiveStatusId   int   ,
	 @ModifiedBy   varchar(50) ;
	 
	 SELECT
	  @ServiceCenterId =I.ServiceCenterId
	  ,@ServiceCenterName=I.ServiceCenterName
	  ,@Notes=I.Notes
	  ,@SU_ID=I.SU_ID
	  ,@ActiveStatusId=I.ActiveStatusId
	  ,@ModifiedBy=I.ModifiedBy
	  ,@SCCode=I.SCCode
	  ,@Address1=I.ActiveStatusId
	  ,@Address2=I.Address2
	  ,@City=I.City
	  ,@ZIP=I.ZIP
	 FROM inserted I;
	 
	 INSERT INTO Tbl_Audit__ServiceCenter
	 SELECT [ServiceCenterId]
      ,[ServiceCenterName]
      ,[Notes]
      ,[SU_ID]
      ,[ActiveStatusId]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[SCCode]
      ,Address1
      ,Address2
      ,City
      ,ZIP
       FROM Tbl_ServiceCenter WHERE ServiceCenterId=@ServiceCenterId;
	 
	 UPDATE Tbl_ServiceCenter SET
	 [ServiceCenterName]=@ServiceCenterName
      ,[Notes]=@Notes
      ,[SU_ID]=@SU_ID
      ,[ActiveStatusId]=@ActiveStatusId
      ,[ModifiedBy]=@ModifiedBy
      ,[SCCode]=@SCCode
      ,[Address1]=@Address1
      ,Address2=@Address2
      ,City=@City
      ,ZIP=@ZIP
      ,[ModifiedDate]=dbo.fn_GetCurrentDateTime() 
     WHERE ServiceCenterId=@ServiceCenterId  
      
END
GO
/****** Object:  Trigger [TR_InsertAuditCycles]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <18-MAR-2014>
-- Description:	<Insert Previous data of Cycles into Audit table Before Update>
-- =============================================
CREATE TRIGGER [dbo].[TR_InsertAuditCycles]
   ON  [dbo].[Tbl_Cycles]
   INSTEAD OF UPDATE
AS 
BEGIN
DECLARE
	@CycleId varchar(20) ,
	@CycleName varchar(300) ,
	@DetailsOfCycle varchar(max) ,
	@ActiveStatusId int ,
	@ModifiedBy varchar(50) ,
	--@BU_ID varchar(20) ,
	--@SU_ID varchar(20) ,
	@ServiceCenterId varchar(20) ,
	@ContactName varchar(200) ,
	@ContactNo varchar(20) ,
	@CycleNo int ,
	@CycleCode VARCHAR(10) ;
SELECT @CycleId=I.CycleId  ,
		@CycleName =I.CycleName ,
		@DetailsOfCycle=I.DetailsOfCycle ,
		@ActiveStatusId=I.ActiveStatusId  ,
		@ModifiedBy=I.ModifiedBy  ,
		--@BU_ID=I.BU_ID  ,
		--@SU_ID=I.SU_ID ,
		@ServiceCenterId =I.ServiceCenterId ,
		@ContactName=I.ContactName ,
		@ContactNo=I.ContactNo  ,
		@CycleCode=I.CycleCode FROM inserted I ;
		
		INSERT INTO Tbl_Audit_Cycles
		SELECT [CycleId]
      ,[CycleName]
      ,[DetailsOfCycle]
      ,[ActiveStatusId]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      --,[BU_ID]
      --,[SU_ID]
      ,[ServiceCenterId]
      ,[ContactName]
      ,[ContactNo]
      ,[CycleCode] FROM Tbl_Cycles WHERE CycleId=@CycleId;
		
		UPDATE Tbl_Cycles SET
							   [CycleName]=@CycleName
							  ,[DetailsOfCycle]=@DetailsOfCycle
							  ,[ActiveStatusId]=@ActiveStatusId
							  ,[ModifiedBy]=@ModifiedBy
							  ,[ModifiedDate]=GETDATE()
							  --,[BU_ID]=@BU_ID
							  --,[SU_ID]=@SU_ID
							  ,[ServiceCenterId]=@ServiceCenterId
							  ,[ContactName]=@ContactName
							  ,[ContactNo]=@ContactNo
							  ,[CycleCode]=@CycleCode WHERE CycleId=@CycleId;
END
GO
/****** Object:  Trigger [TR_INSERTAUDITAGENCIES]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TR_INSERTAUDITAGENCIES] ON [dbo].[Tbl_Agencies]
INSTEAD OF UPDATE
AS
-- =============================================
-- Author:		<RamaDevi M>
-- Create date: <18-MAR-2014>
-- Description:	<Insert Previous data of Agencies into Audit table Before Update>
-- =============================================
DECLARE @ID INT
    , @AgencyName  VARCHAR(300)
      , @Details  VARCHAR(MAX)
      , @ContactNo1   VARCHAR(20)
      , @ContactNo2   VARCHAR(20)
      , @ContactPersonName   VARCHAR(50)
      ,@ActiveStatusId  INT
      , @ModifedBy   VARCHAR(50);
      

SELECT @ID=I.AgencyId FROM inserted I;
SELECT @AgencyName=I.AgencyName FROM inserted I;

SELECT @Details=I.Details FROM inserted I;

SELECT @ContactNo1=I.ContactNo1 FROM inserted I;

SELECT @ContactNo2=I.ContactNo2 FROM inserted I;

SELECT @ContactPersonName=I.ContactPersonName FROM inserted I;

SELECT @ActiveStatusId=I.ActiveStatusId FROM inserted I;
SELECT @ModifedBy=I.ModifedBy FROM inserted I;


INSERT INTO Tbl_Audit_Agencies

SELECT 
		AgencyId
      ,[AgencyName]
      ,[Details]
      ,[ContactNo1]
      ,[ContactNo2]
      ,[ContactPersonName]
      ,[ActiveStatusId]
      ,[CreatedDate]
      ,[CreatedBy]
      ,[ModifedBy]
      ,[ModifiedDate] FROM Tbl_Agencies WHERE AgencyId=@ID
      
      
 UPDATE Tbl_Agencies SET [AgencyName]=@AgencyName
      ,[Details]=@Details
      ,[ContactNo1]=@ContactNo1
      ,[ContactNo2]=@ContactNo2
      ,[ContactPersonName]=@ContactPersonName
      ,[ActiveStatusId]=@ActiveStatusId
      ,[ModifedBy]=@ModifedBy
      ,[ModifiedDate]=GETDATE() WHERE AgencyId=@ID
GO
/****** Object:  Table [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails]    Script Date: 02/24/2015 19:01:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails](
	[Bedcinfoid] [int] NULL,
	[Bedconfid] [int] IDENTITY(1,1) NOT NULL,
	[ContactName] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ModifedBy] [varchar](50) NULL,
	[ModifiedDate] [datetime] NULL,
	[GlobalAccountNumber] [varchar](50) NULL,
	[InstalledBy] [int] NULL,
	[AgencyId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Bedconfid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [Defalut_CPD]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] ADD  CONSTRAINT [Defalut_CPD]  DEFAULT ((1)) FOR [IsServiceAddress]
GO
/****** Object:  Default [DF__Tbl_Custo__IsCom__752E4300]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] ADD  DEFAULT ((0)) FOR [IsCommunication]
GO
/****** Object:  Default [DF__Tbl_Custo__IsAct__78FED3E4]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Agenc__Activ__531856C7]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_Agencies] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Audit__IsBoo__147C05D0]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_Audit_CustomerDetails] ADD  DEFAULT ((0)) FOR [IsBookNoChanged]
GO
/****** Object:  Default [DF_Tbl_BillAdjustmentDetails_AdditionalChargesID]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_BillAdjustmentDetails] ADD  CONSTRAINT [DF_Tbl_BillAdjustmentDetails_AdditionalChargesID]  DEFAULT ((0)) FOR [AdditionalChargesID]
GO
/****** Object:  Default [DF_Tbl_BillAdjustments_ApprovalStatusId]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_BillAdjustments] ADD  CONSTRAINT [DF_Tbl_BillAdjustments_ApprovalStatusId]  DEFAULT ((0)) FOR [ApprovalStatusId]
GO
/****** Object:  Default [DF__Tbl_Billi__IsAct__184C96B4]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_BillingDisabledBooks] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Billi__Appro__1940BAED]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_BillingDisabledBooks] ADD  DEFAULT ((2)) FOR [ApproveStatusId]
GO
/****** Object:  Default [DF__Tbl_BookN__Appro__1A34DF26]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_BookNoChangeLogs] ADD  DEFAULT ((2)) FOR [ApproveStatusId]
GO
/****** Object:  Default [DF__Tbl_BookN__Activ__57DD0BE4]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_BookNumbers] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__TBL_BU_He__Activ__1B29035F]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[TBL_BU_HeadOffice] ADD  DEFAULT ((1)) FOR [ActiveStatusID]
GO
/****** Object:  Default [DF_Tbl_BUNotice_IsAllBuss]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_BUNotice] ADD  CONSTRAINT [DF_Tbl_BUNotice_IsAllBuss]  DEFAULT ((0)) FOR [IsAllBuss]
GO
/****** Object:  Default [DF_Tbl_BUNotice_Activestatus]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_BUNotice] ADD  CONSTRAINT [DF_Tbl_BUNotice_Activestatus]  DEFAULT ((1)) FOR [Activestatus]
GO
/****** Object:  Default [DF__Tbl_Bussi__Activ__5CA1C101]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_BussinessUnits] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__TBL_Bussi__Activ__1C1D2798]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[TBL_BussinessUnitsEBllMails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_CashO__Activ__725BF7F6]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CashOffices] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Check__IsAct__1D114BD1]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CheckMeterDetails] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Count__Creat__5F7E2DAC]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_Countries] ADD  DEFAULT ('Admin') FOR [CreatedBy]
GO
/****** Object:  Default [DF__Tbl_Count__IsAct__607251E5]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_Countries] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Custo__Appro__1E05700A]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerActiveStatusChangeLogs] ADD  DEFAULT ((2)) FOR [ApproveStatusId]
GO
/****** Object:  Default [DF__Tbl_Custo__Activ__0E04126B]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerBills] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Custo__Activ__1FEDB87C]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerDetails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Custo__OutSt__20E1DCB5]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerDetails] ADD  DEFAULT ((0)) FOR [OutStandingAmount]
GO
/****** Object:  Default [DF__Tbl_Custo__IsBoo__21D600EE]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerDetails] ADD  DEFAULT ((0)) FOR [IsBookNoChanged]
GO
/****** Object:  Default [DF__Tbl_Custo__Activ__178D7CA5]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerDocuments] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Custo__Appro__22CA2527]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerMeterInfoChangeLogs] ADD  DEFAULT ((2)) FOR [ApproveStatusId]
GO
/****** Object:  Default [DT_BLA]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerPayments] ADD  CONSTRAINT [DT_BLA]  DEFAULT ((1)) FOR [ActivestatusId]
GO
/****** Object:  Default [DF__Tbl_Custo__Recei__25A691D2]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerPayments] ADD  DEFAULT ((2)) FOR [ReceivedDevice]
GO
/****** Object:  Default [DF_Tbl_CustomerReadings_IsBilled]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerReadings] ADD  CONSTRAINT [DF_Tbl_CustomerReadings_IsBilled]  DEFAULT ((0)) FOR [IsBilled]
GO
/****** Object:  Default [DF__Tbl_Custo__IsTam__55B597A7]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerReadings] ADD  DEFAULT ((0)) FOR [IsTamper]
GO
/****** Object:  Default [DF_Tbl_CustomerReadingsLog_IsBilled]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_CustomerReadingsLog] ADD  CONSTRAINT [DF_Tbl_CustomerReadingsLog_IsBilled]  DEFAULT ((0)) FOR [IsBilled]
GO
/****** Object:  Default [DF__Tbl_Cycle__Activ__65370702]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_Cycles] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Distr__Activ__58520D30]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_DistrictMessages] ADD  CONSTRAINT [DF__Tbl_Distr__Activ__58520D30]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__TBL_EBill__IsMai__2A6B46EF]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[TBL_EBillMailLogs] ADD  DEFAULT ((0)) FOR [IsMailSent]
GO
/****** Object:  Default [DF__Tbl_Estim__IsAct__2B5F6B28]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_EstimatedEnergy] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [df_ConsAct]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_EstimationSettings] ADD  CONSTRAINT [df_ConsAct]  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Globa__Activ__4830B400]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_GlobalMessages] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF_Tbl_HHInputFiles_ActiveStatus]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_HHInputFiles] ADD  CONSTRAINT [DF_Tbl_HHInputFiles_ActiveStatus]  DEFAULT ((1)) FOR [ActiveStatus]
GO
/****** Object:  Default [DF__Tbl_LAddi__IsAct__4EDDB18F]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_LAdditionalClassCharges] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_LEner__IsAct__5772F790]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_LEnergyClassCharges] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_Tbl_Menus_IsActive]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_Menus] ADD  CONSTRAINT [DF_Tbl_Menus_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Menus__Activ__5FB337D6]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_Menus] ADD  DEFAULT ((1)) FOR [ActiveHeader]
GO
/****** Object:  Default [DF__Tbl_Meter__Activ__5C37ACAD]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_MeterInformation] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_NewCu__Appro__2D47B39A]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_NewCustomerLocator] ADD  DEFAULT ((0)) FOR [ApprovalStatus]
GO
/****** Object:  Default [DF_Tbl_Notifications_activestatus]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_Notifications] ADD  CONSTRAINT [DF_Tbl_Notifications_activestatus]  DEFAULT ((1)) FOR [activestatus]
GO
/****** Object:  Default [DF__Tbl_PaidM__Activ__2E3BD7D3]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_PaidMeterDetails] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Passw__IsAct__019419E5]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_PasswordStrength] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_Servi__Activ__7B264821]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_ServiceCenter] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_Servi__Activ__7FEAFD3E]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_ServiceUnits] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_State__Creat__02C769E9]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_States] ADD  DEFAULT ('Admin') FOR [CreatedBy]
GO
/****** Object:  Default [DF__Tbl_State__IsAct__03BB8E22]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_States] ADD  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF__Tbl_UserB__Activ__79C80F94]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_UserBusinessUnits] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_UserC__Activ__7E8CC4B1]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_UserCashOffices] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_UserD__Activ__035179CE]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_UserDocuments] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_UserR__Activ__2F2FFC0C]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_UserRoles] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_UserS__Activ__0BE6BFCF]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_UserServiceCenters] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  Default [DF__Tbl_UserS__Activ__30242045]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [dbo].[Tbl_UserServiceUnits] ADD  DEFAULT ((1)) FOR [ActiveStatusId]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId18]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId18] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId18]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId719]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId719] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId719]
GO
/****** Object:  ForeignKey [FK_AgencyId]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails]  WITH CHECK ADD  CONSTRAINT [FK_AgencyId] FOREIGN KEY([AgencyId])
REFERENCES [dbo].[Tbl_Agencies] ([AgencyId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails] CHECK CONSTRAINT [FK_AgencyId]
GO
/****** Object:  ForeignKey [FK_BEDCEmpId]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails]  WITH CHECK ADD  CONSTRAINT [FK_BEDCEmpId] FOREIGN KEY([InstalledBy])
REFERENCES [EMPLOYEE].[Tbl_MBEDCEmployeeDetails] ([BEDCEmpId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails] CHECK CONSTRAINT [FK_BEDCEmpId]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId20]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId20] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId20]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId21]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId21] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_ApplicationProcessPersonDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId21]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId24]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerActiveDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId24] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerActiveDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId24]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId25]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerActiveDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId25] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerActiveDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId25]
GO
/****** Object:  ForeignKey [FK_IdentityTypeId]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails]  WITH CHECK ADD  CONSTRAINT [FK_IdentityTypeId] FOREIGN KEY([IdentityTypeId])
REFERENCES [dbo].[Tbl_MIdentityTypes] ([IdentityId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails] CHECK CONSTRAINT [FK_IdentityTypeId]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId5]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId5] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId5]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId6]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId6] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerIdentityDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId6]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId12]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId12] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId12]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId13]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId13] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerPostalAddressDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId13]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId7]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerProceduralDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId7] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerProceduralDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId7]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId8]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerProceduralDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId8] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerProceduralDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId8]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId3]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerSDetail]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId3] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerSDetail] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId3]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId4]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerSDetail]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId4] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerSDetail] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId4]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId14]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerTenentDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId14] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerTenentDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId14]
GO
/****** Object:  ForeignKey [FK_Tbl_UserLoginDetails_UserId15]    Script Date: 02/24/2015 19:01:49 ******/
ALTER TABLE [CUSTOMERS].[Tbl_CustomerTenentDetails]  WITH CHECK ADD  CONSTRAINT [FK_Tbl_UserLoginDetails_UserId15] FOREIGN KEY([ModifedBy])
REFERENCES [dbo].[Tbl_UserLoginDetails] ([UserId])
GO
ALTER TABLE [CUSTOMERS].[Tbl_CustomerTenentDetails] CHECK CONSTRAINT [FK_Tbl_UserLoginDetails_UserId15]
GO
