GO
/****** Object:  View [dbo].[UDV_CustomerDescription]    Script Date: 02/24/2015 17:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_CustomerDescription] 
AS
	SELECT
	  CD.GlobalAccountNumber 
	 ,DocumentNo
	 ,AccountNo
	 ,OldAccountNo
	 ,CD.Title
	 ,CD.FirstName
	 ,CD.MiddleName
	 ,CD.LastName
	 ,KnownAs
	 ,EmployeeCode
	 ,ISNULL(HomeContactNumber,'--') AS HomeContactNo    
	 ,ISNULL(BusinessContactNumber,'--') AS BusinessContactNo    
	 ,ISNULL(OtherContactNumber,'--') AS OtherContactNo
	 ,PhoneNumber
	 ,AlternatePhoneNumber
	 ,CD.ActiveStatusId
	 ,PD.PoleID
	 ,TariffClassID AS TariffId
	 ,TC.ClassName AS ClassName
	 ,IsBookNoChanged
	 ,ReadCodeID
	 ,PD.SortOrder
	 ,Highestconsumption
	 ,OutStandingAmount
	 ,BN.BookNo
	 ,BN.BookCode
	 ,BU.BU_ID
	 ,BU.BusinessUnitName
	 ,BU.BUCode
	 ,SU.SU_ID
	 ,SU.ServiceUnitName
	 ,SU.SUCode
	 ,SC.ServiceCenterId
	 ,SC.ServiceCenterName
	 ,SC.SCCode
	 ,C.CycleId
	 ,C.CycleName
	 ,C.CycleCode
	 ,StatusName AS ActiveStatus
	 ,CD.EmailId
	 ,MeterNumber 
	 ,MeterType AS MeterTypeId
	 ,PhaseId
	 ,ClusterCategoryId
	 ,InitialReading
	 ,InitialBillingKWh AS  MinimumReading
	 ,AvgReading
	 ,RouteSequenceNumber AS RouteSequenceNo
	 ,ConnectionDate
	 ,CD.CreatedDate
	 ,CD.CreatedBy
	 ,CustomerTypeId
	 ,C.ActiveStatusId AS CylceActiveStatusId
FROM CUSTOMERS.Tbl_CustomersDetail CD
JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD	ON PD.GlobalAccountNumber=CD.GlobalAccountNumber
LEFT JOIN  CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CD.GlobalAccountNumber
LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails TD ON TD.TenentId=CD.TenentId
JOIN  Tbl_BookNumbers BN ON BN.BookNo=PD.BookNo
JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId
JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID
JOIN Tbl_MTariffClasses AS TC ON PD.TariffClassID=TC.ClassID
--LEFT JOIN  Tbl_MActiveStatusDetails MS ON CD.ActiveStatusId=MS.ActiveStatusId	
LEFT JOIN  Tbl_MCustomerStatus MS ON CD.ActiveStatusId=MS.StatusId	
LEFT JOIN Tbl_MeterInformation MI ON PD.MeterNumber=MI.MeterNo
----------------------------------------------------------------------------
--=============================================================================
	--SELECT
	--  CD.GlobalAccountNumber 
	-- ,DocumentNo
	-- ,AccountNo
	-- ,OldAccountNo
	-- ,CD.Title
	-- ,CD.FirstName
	-- ,CD.MiddleName
	-- ,CD.LastName
	-- ,KnownAs
	-- ,EmployeeCode
	-- ,ISNULL(HomeContactNumber,'--') AS HomeContactNo    
	-- ,ISNULL(BusinessContactNumber,'--') AS BusinessContactNo    
	-- ,ISNULL(OtherContactNumber,'--') AS OtherContactNo
	-- ,PhoneNumber
	-- ,AlternatePhoneNumber
	-- ,CD.ActiveStatusId
	-- ,PoleID
	-- ,TariffClassID AS TariffId
	-- ,ClassName
	-- ,IsBookNoChanged
	-- ,ReadCodeID
	-- ,CPD.SortOrder
	-- ,Highestconsumption
	-- ,OutStandingAmount
	-- ,BN.BookNo
	-- ,BN.BU_ID
	-- ,BN.SU_ID
	-- ,BN.ServiceCenterId
	-- ,BN.BookCode
	-- ,CycleId
	-- ,[Status] AS ActiveStatus
	-- ,CD.EmailId
	-- ,MeterNumber 
	-- ,MeterType AS MeterTypeId
	-- ,PhaseId
	-- ,ClusterCategoryId
	-- ,InitialReading
	-- ,InitialBillingKWh AS  MinimumReading
	-- ,AvgReading
	-- ,RouteNumber
	-- ,RouteSequenceNumber AS RouteSequenceNo
	-- ,ConnectionDate
	-- ,CD.CreatedDate
	-- ,CD.CreatedBy
	-- ,CustomerTypeId
	 
	--FROM [CUSTOMERS].[Tbl_CustomerSDetail] CD	
	--JOIN [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber 
	--JOIN [CUSTOMERS].[Tbl_CustomerActiveDetails] AC ON CD.GlobalAccountNumber=AC.GlobalAccountNumber
	--JOIN Tbl_BookNumbers BN ON BN.BookNo=CPD.BookNo
	--JOIN Tbl_MTariffClasses AS TC ON CPD.TariffClassID=TC.ClassID
	--JOIN Tbl_MActiveStatusDetails MS ON CD.ActiveStatusId=MS.ActiveStatusId	
	--JOIN Tbl_MeterInformation MI ON CPD.MeterNumber=MI.MeterNo
	--JOIN [CUSTOMERS].[Tbl_CustomerTenentDetails]  CT ON CD.GlobalAccountNumber=CT.GlobalAccountNumber
--=============================================================================
GO
/****** Object:  View [dbo].[UDV_CustDetailsForBillGen]    Script Date: 02/24/2015 17:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_CustDetailsForBillGen] AS

SELECT			
			CD.GlobalAccountNumber
			--, CD.DocumentNo
			--, CD.AccountNo
			, CD.OldAccountNo
			--, CD.Title
			--, CD.FirstName
			--, CD.MiddleName
			--, CD.LastName
			--, CD.KnownAs
			--, CD.EmployeeCode
			--, ISNULL(CD.HomeContactNumber, '--') AS HomeContactNo, ISNULL(CD.BusinessContactNumber, '--') AS BusinessContactNo
			, ISNULL(CD.OtherContactNumber, '--') AS OtherContactNo
			--, TD.PhoneNumber
			--, TD.AlternatePhoneNumber
			, CD.ActiveStatusId
			, PD.PoleID
			, PD.TariffClassID AS TariffId
			--, TC.ClassName
			--, PD.IsBookNoChanged
			--, PD.ReadCodeID
			, PD.SortOrder
			--, CAD.Highestconsumption
			, CAD.OutStandingAmount
			, BN.BookNo
			--, BN.BookCode
			, BU.BU_ID
			--, BU.BusinessUnitName
			--, BU.BUCode
			, SU.SU_ID
			--, SU.ServiceUnitName
			--, SU.SUCode
			, SC.ServiceCenterId
			--, SC.ServiceCenterName
			--, SC.SCCode
			, C.CycleId
			--, C.CycleName
			--, C.CycleCode
			--, MS.[Status] AS ActiveStatus
			, CD.EmailId
			, PD.MeterNumber
			--, MI.MeterType AS MeterTypeId
			--, PD.PhaseId
			--, PD.ClusterCategoryId
			--, CAD.InitialReading
			--, CAD.InitialBillingKWh AS MinimumReading
			--, CAD.AvgReading
			--, PD.RouteSequenceNumber AS RouteSequenceNo
			--, CD.ConnectionDate
			--, CD.CreatedDate
			--, CD.CreatedBy
			--, PD.CustomerTypeId
			
FROM         CUSTOMERS.Tbl_CustomerSDetail AS CD INNER JOIN
                      CUSTOMERS.Tbl_CustomerProceduralDetails AS PD ON PD.GlobalAccountNumber = CD.GlobalAccountNumber 
                      LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerActiveDetails AS CAD ON CAD.GlobalAccountNumber = CD.GlobalAccountNumber 
                      LEFT OUTER JOIN CUSTOMERS.Tbl_CustomerTenentDetails AS TD ON TD.TenentId = CD.TenentId 
                      INNER JOIN dbo.Tbl_BookNumbers AS BN ON BN.BookNo = PD.BookNo 
                      INNER JOIN dbo.Tbl_Cycles AS C ON C.CycleId = BN.CycleId 
                      INNER JOIN dbo.Tbl_ServiceCenter AS SC ON SC.ServiceCenterId = C.ServiceCenterId 
                      INNER JOIN dbo.Tbl_ServiceUnits AS SU ON SU.SU_ID = SC.SU_ID 
                      INNER JOIN dbo.Tbl_BussinessUnits AS BU ON BU.BU_ID = SU.BU_ID 
                      INNER JOIN dbo.Tbl_MTariffClasses AS TC ON PD.TariffClassID = TC.ClassID 
                      LEFT OUTER JOIN dbo.Tbl_MActiveStatusDetails AS MS ON CD.ActiveStatusId = MS.ActiveStatusId 
                      LEFT OUTER JOIN dbo.Tbl_MeterInformation AS MI ON PD.MeterNumber = MI.MeterNo
GO
/****** Object:  View [dbo].[UDV_BookNumberDetails]    Script Date: 02/24/2015 17:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_BookNumberDetails]
AS
	SELECT 
	SC.ServiceCenterId,SC.SCCode,SC.ServiceCenterName
	,SU.SU_ID,SU.SUCode,SU.ServiceUnitName
	,BU.BU_ID,BU.BUCode,BU.BusinessUnitName
	FROM Tbl_BookNumbers B
	JOIN Tbl_Cycles C ON C.CycleId=B.CycleId
	JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
	JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
	JOIN Tbl_BussinessUnits BU ON BU.BU_ID = SU.BU_ID
GO
/****** Object:  View [dbo].[UDV_ServiceCenterDetails]    Script Date: 02/24/2015 17:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_ServiceCenterDetails]
AS
	SELECT 
	SC.ServiceCenterId,SC.SCCode,SC.ServiceCenterName
	,SU.SU_ID,SU.SUCode,SU.ServiceUnitName
	,BU.BU_ID,BU.BUCode,BU.BusinessUnitName
	FROM Tbl_ServiceCenter SC 
	JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
	JOIN Tbl_BussinessUnits BU ON BU.BU_ID = SU.BU_ID
GO
/****** Object:  View [dbo].[UDV_CustomerInformation]    Script Date: 02/24/2015 17:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UDV_CustomerInformation] 
AS
	SELECT
	  CD.GlobalAccountNumber 
	 ,CD.DocumentNo
	 ,CD.AccountNo
	 ,CD.OldAccountNo
	 ,CD.Title
	 ,CD.FirstName
	 ,CD.MiddleName
	 ,CD.LastName
	 ,CD.KnownAs
	 ,CD.EmployeeCode
	 ,ISNULL(CD.HomeContactNumber,'--') AS HomeContactNumber    
	 ,ISNULL(CD.BusinessContactNumber,'--') AS BusinessContactNumber    
	 ,ISNULL(CD.OtherContactNumber,'--') AS OtherContactNumber
	 ,TD.PhoneNumber
	 ,TD.AlternatePhoneNumber
	 ,CD.ActiveStatusId
	 ,PD.BookNo
	 ,PD.PoleID
	 ,PD.TariffClassID AS TariffClassID
	 ,PD.IsBookNoChanged
	 ,PD.ReadCodeID
	 ,PD.SortOrder
	 ,CAD.Highestconsumption
	 ,CAD.OutStandingAmount
	 ,CD.EmailId
	 ,PD.MeterNumber 
	 ,PD.PhaseId
	 ,PD.ClusterCategoryId
	 ,CAD.InitialReading
	 ,CAD.InitialBillingKWh AS  InitialBillingKWh
	 ,CAD.AvgReading
	 ,PD.RouteSequenceNumber AS RouteSequenceNumber
	 ,CD.ConnectionDate
	 ,CD.CreatedDate
	 ,CD.CreatedBy
	 ,PD.CustomerTypeId
FROM CUSTOMERS.Tbl_CustomersDetail CD
JOIN CUSTOMERS.Tbl_CustomerProceduralDetails PD	ON PD.GlobalAccountNumber=CD.GlobalAccountNumber
LEFT JOIN  CUSTOMERS.Tbl_CustomerActiveDetails CAD ON CAD.GlobalAccountNumber=CD.GlobalAccountNumber
LEFT JOIN CUSTOMERS.Tbl_CustomerTenentDetails TD ON TD.TenentId=CD.TenentId
GO
--/****** Object:  View [dbo].[UDV_CustomerDetails]    Script Date: 02/24/2015 17:38:49 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE VIEW [dbo].[UDV_CustomerDetails] 
--AS
--	SELECT
--	  CD.GlobalAccountNumber 
--	 ,CD.DocumentNo
--	 ,CD.AccountNo
--	 ,CD.OldAccountNo
--	 ,CD.Title
--	 ,CD.FirstName
--	 ,CD.MiddleName
--	 ,CD.LastName
--	 ,CD.KnownAs
--	 ,CD.EmployeeCode
--	 ,ISNULL(CD.HomeContactNumber,'--') AS HomeContactNo    
--	 ,ISNULL(CD.BusinessContactNumber,'--') AS BusinessContactNo    
--	 ,ISNULL(CD.OtherContactNumber,'--') AS OtherContactNo
--	 ,CD.PhoneNumber
--	 ,CD.AlternatePhoneNumber
--	 ,CD.ActiveStatusId
--	 ,CD.PoleID
--	 ,CD.TariffClassID AS TariffId
--	 ,TC.ClassName
--	 ,CD.IsBookNoChanged
--	 ,CD.ReadCodeID
--	 ,CD.SortOrder
--	 ,CD.Highestconsumption
--	 ,CD.OutStandingAmount
--	 ,BN.BookNo
--	 ,BN.BookCode
--	 ,BN.CycleId
--	 ,BN.CycleCode
--	 ,BN.CycleName
--	 ,BN.SCCode
--	 ,BN.ServiceCenterId
--	 ,BN.ServiceCenterName
--	 ,BN.SUCode
--	 ,BN.ServiceUnitName
--	 ,BN.SU_ID
--	 ,BN.BUCode
--	 ,BN.BU_ID
--	 ,BN.BusinessUnitName
--	 ,MS.[Status] AS ActiveStatus
--	 ,CD.EmailId
--	 ,CD.MeterNumber 
--	 ,MI.MeterType AS MeterTypeId
--	 ,CD.PhaseId
--	 ,CD.ClusterCategoryId
--	 ,CD.InitialReading
--	 ,CD.InitialBillingKWh AS  MinimumReading
--	 ,CD.AvgReading
--	 ,CD.RouteSequenceNumber AS RouteSequenceNo
--	 ,CD.ConnectionDate
--	 ,CD.CreatedDate
--	 ,CD.CreatedBy
--	 ,CD.CustomerTypeId
--FROM UDV_CustomerInformation CD
--JOIN  UDV_BookNumberDetails BN ON BN.BookNo=CD.BookNo 
--JOIN Tbl_MTariffClasses AS TC ON CD.TariffClassID=TC.ClassID
--LEFT JOIN  Tbl_MActiveStatusDetails MS ON CD.ActiveStatusId=MS.ActiveStatusId	
--LEFT JOIN Tbl_MeterInformation MI ON CD.MeterNumber=MI.MeterNo
--GO
