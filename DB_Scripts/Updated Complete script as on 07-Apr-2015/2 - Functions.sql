GO
/****** Object:  UserDefinedFunction [dbo].[fn_BillDueStatus]    Script Date: 04/07/2015 03:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 12 Apr 2014
-- Description:	The purpose of this function is to get the Due Statuses maintain at one place
-- =============================================
ALTER FUNCTION [dbo].[fn_BillDueStatus]()
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @DueStatus VARCHAR(MAX) = '2'
		
	RETURN @DueStatus

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GenerateCustomerUniqueNo]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 01-03-2014
-- Description:	The purpose of this function is to generate Customer UniqueNo
-- =============================================
ALTER FUNCTION [dbo].[fn_GenerateCustomerUniqueNo]
(
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @CustomerUniqueNo VARCHAR(50)
	RETURN @CustomerUniqueNo

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCurrentDateTime]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V Bhima Raju
-- Create date: 29-03-2014
-- Description:	to Get the Current date
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCurrentDateTime]()
RETURNS DATETIME
AS
BEGIN
	DECLARE @ResultDate DATETIME = GETDATE()
	
	RETURN @ResultDate 
	
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerAddressFormat_bill]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya
-- Create date: 10-Mar-2015
-- Description:	The purpose of this function is to get the Customer Service Address

-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerAddressFormat_bill]
(

@HouseNo VARCHAR(50)
,@StreetName VARCHAR(50)
--,@Landmark VARCHAR(50)
,@City VARCHAR(50)
--,@Details VARCHAR(50)
,@zipcode VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @ServiceAddress VARCHAR(MAX)


			SELECT @ServiceAddress =	LTRIM(RTRIM(
				CASE WHEN @HouseNo IS NULL OR @HouseNo = '' THEN '' ELSE LTRIM(RTRIM(@HouseNo)) + ',   '  END + 
				CASE WHEN @StreetName IS NULL OR @StreetName = '' THEN '' ELSE  LTRIM(RTRIM(@StreetName)) + ', \n' END + 
				--CASE WHEN @Landmark IS NULL OR @Landmark = '' THEN '' ELSE LTRIM(RTRIM(@Landmark)) + ', '  END + 
				CASE WHEN @City IS NULL OR @City = '' THEN '' ELSE  LTRIM(RTRIM(@City)) + ',  ' END  + 
				--CASE WHEN @Details IS NULL OR @Details = '' THEN '' ELSE  LTRIM(RTRIM(@Details))+ ', ' END   + 
				CASE WHEN @zipcode IS NULL OR @zipcode = '' THEN '' ELSE  LTRIM(RTRIM(@zipcode))+ ', \n ' END )) 
			 
	RETURN @ServiceAddress
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerAddressFormat]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Satya
-- Create date: 10-Mar-2015
-- Description:	The purpose of this function is to get the Customer Service Address

-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerAddressFormat]
(

@HouseNo VARCHAR(50)
,@StreetName VARCHAR(50)
--,@Landmark VARCHAR(50)
,@City VARCHAR(50)
--,@Details VARCHAR(50)
,@zipcode VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @ServiceAddress VARCHAR(MAX)


			SELECT @ServiceAddress =	LTRIM(RTRIM(
				CASE WHEN @HouseNo IS NULL OR @HouseNo = '' THEN '' ELSE LTRIM(RTRIM(@HouseNo)) + ', '  END + 
				CASE WHEN @StreetName IS NULL OR @StreetName = '' THEN '' ELSE  LTRIM(RTRIM(@StreetName)) + ', ' END + 
				--CASE WHEN @Landmark IS NULL OR @Landmark = '' THEN '' ELSE LTRIM(RTRIM(@Landmark)) + ', '  END + 
				CASE WHEN @City IS NULL OR @City = '' THEN '' ELSE  LTRIM(RTRIM(@City)) + ', ' END  + 
				--CASE WHEN @Details IS NULL OR @Details = '' THEN '' ELSE  LTRIM(RTRIM(@Details))+ ', ' END   + 
				CASE WHEN @zipcode IS NULL OR @zipcode = '' THEN '' ELSE  LTRIM(RTRIM(@zipcode))+ ', ' END )) 
			 
	RETURN @ServiceAddress
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAmount]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 04-02-2014
-- Description:	The purpose of this function is to get amount with two decimal points
-- =============================================
ALTER FUNCTION [dbo].[fn_GetAmount]
(
	@Amount FLOAT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @FinalAmount VARCHAR(MAX)
	
SELECT @FinalAmount=(CASE CHARINDEX('.',ISNULL(CONVERT(VARCHAR(MAX),@Amount),'')) WHEN 0 
			THEN CONVERT(VARCHAR(MAX),@Amount)+'.00'
			ELSE
				SUBSTRING(CONVERT(VARCHAR(MAX),@Amount),1,CHARINDEX('.',CONVERT(VARCHAR(MAX),@Amount))+2)
			END)
			
	RETURN @FinalAmount

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerFullName_New]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 18-10-2014
-- Description:	To Get FullName Of the Customer Title+Name+SurName

-- Modified By: Faiz-ID103-----(Bhargav)
-- Modified Date: 10-Mar-2015
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerFullName_New]
(
	@Title VARCHAR(50),@FirstName VARCHAR(50) ,@MiddleName VARCHAR(50),@LastName VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Name VARCHAR(MAX)

		SELECT @Name =	LTRIM(RTRIM(
		CASE WHEN @Title IS NULL OR @Title = '' THEN '' ELSE LTRIM(RTRIM(@Title+'. ')) END + 
		CASE WHEN @FirstName IS NULL OR @FirstName = '' THEN '' ELSE  LTRIM(RTRIM(@FirstName)) END + 
		CASE WHEN @MiddleName IS NULL OR @MiddleName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(@MiddleName)) END +
		CASE WHEN @LastName IS NULL OR @LastName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(@LastName)) END))
 
	RETURN @Name
END
----------------------------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCurrentYearMonths_ByCurrentDate]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 25-09-2014
-- Description:	To get the list of last n months details from current month
-- select * from dbo.fn_GetLast_N_Months(3)
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCurrentYearMonths_ByCurrentDate]
(
 @FromDate DATETIME
)
RETURNS @ResultTable TABLE(Id INT IDENTITY(1,1),[Year] INT,[Month] INT)
AS
BEGIN

	DECLARE @Month INT
			,@Year INT
			,@MonthStartDate DATETIME 
		
			
	SET @MonthStartDate = CONVERT(VARCHAR(20),DATEPART(YEAR,@FromDate))+'-'+CONVERT(VARCHAR(20),01)+'-01'    
	
	DECLARE @monthCount INT = 0

	WHILE(@MonthStartDate <= @FromDate)
		BEGIN
			INSERT INTO @ResultTable([Month],[Year])
			SELECT  DATEPART(MM,@MonthStartDate), DATEPART(YY,@MonthStartDate)
			SET @MonthStartDate = DATEADD(MONTH,1,@MonthStartDate)
		END			
			
	RETURN 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetDaysInMonth]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author     :	Suresh Kumar Dasi
-- Create date: 14 JULY 2014
-- Description:	The purpose of this function is to get the total days in Month
-- =============================================
ALTER FUNCTION [dbo].[fn_GetDaysInMonth] (@pDate    DATETIME )
RETURNS INT
AS
BEGIN

    RETURN CASE WHEN MONTH(@pDate) IN (1, 3, 5, 7, 8, 10, 12) THEN 31
                WHEN MONTH(@pDate) IN (4, 6, 9, 11) THEN 30
                ELSE CASE WHEN (YEAR(@pDate) % 4    = 0 AND
                                YEAR(@pDate) % 100 != 0) OR
                               (YEAR(@pDate) % 400  = 0)
                          THEN 29
                          ELSE 28
                     END
           END

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerServiceAddress_New]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Faiz-ID103---(Bhargav)
-- Create date: 10-Mar-2015
-- Description:	The purpose of this function is to get the Customer Service Address

-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerServiceAddress_New]
(

@HouseNo VARCHAR(50)
,@StreetName VARCHAR(50)
,@Landmark VARCHAR(50)
,@City VARCHAR(50)
,@Details VARCHAR(50)
,@zipcode VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @ServiceAddress VARCHAR(MAX)


			SELECT @ServiceAddress =	LTRIM(RTRIM(
				CASE WHEN @HouseNo IS NULL OR @HouseNo = '' THEN '' ELSE LTRIM(RTRIM(@HouseNo)) + ', '  END + 
				CASE WHEN @StreetName IS NULL OR @StreetName = '' THEN '' ELSE  LTRIM(RTRIM(@StreetName)) + ', ' END + 
				CASE WHEN @Landmark IS NULL OR @Landmark = '' THEN '' ELSE LTRIM(RTRIM(@Landmark)) + ', '  END + 
				CASE WHEN @City IS NULL OR @City = '' THEN '' ELSE  LTRIM(RTRIM(@City)) + ', ' END  + 
				CASE WHEN @Details IS NULL OR @Details = '' THEN '' ELSE  LTRIM(RTRIM(@Details))+ ', ' END   + 
				CASE WHEN @zipcode IS NULL OR @zipcode = '' THEN '' ELSE  LTRIM(RTRIM(@zipcode))+ ', ' END )) 
			 
	RETURN @ServiceAddress
END


---------------------------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetFilteredMonths]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju.Vanka
-- Create date: 31-05-2014
-- Description:	To get the Details of Last 3 months with years
-- =============================================
ALTER FUNCTION [dbo].[fn_GetFilteredMonths]
(
	 @Year INT
	,@Month INT
)

RETURNS @FilteredMonths TABLE(YearId INT,MonthId INT)
AS
BEGIN

	DECLARE @Count INT = 0
	--DECLARE 

		WHILE(@Count <= 3)
			BEGIN
				IF(@Month=0)
					BEGIN
						SET @Month = 12
						INSERT INTO @FilteredMonths
						SELECT @Year - 1,@Month
						SET @Year = @Year - 1				
					END
				ELSE 
					BEGIN
						INSERT INTO @FilteredMonths
						SELECT @Year,@Month
					END	
				SET @Count = @Count + 1
				SET @Month = @Month - 1
			END					
	RETURN 
	--(SELECT * FROM @FilteredMonths)
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetHugeMildReadingDetails]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar
-- Create date: 07-10-2014
-- Description:	The purpose of this function is to get Huge or Mild Details
-- =============================================
ALTER FUNCTION [dbo].[fn_GetHugeMildReadingDetails]
(
@newAvgUsage DECIMAL(18,0)
,@prevAvgUsage DECIMAL(18,0)
,@HugePercentage DECIMAL(18,2)
)
RETURNS VARCHAR(20)
AS
BEGIN
	DECLARE @HugeAmount DECIMAL(18,0)
			,@MildAmount DECIMAL(18,0)
			,@IsLowHighReading VARCHAR(20) = NULL
			
	SET @HugeAmount = @prevAvgUsage + (@prevAvgUsage * @HugePercentage / 100);		
	SET @MildAmount = @prevAvgUsage - (@prevAvgUsage * @HugePercentage / 100);		

	IF (@newAvgUsage > @HugeAmount)
		BEGIN
			SET @IsLowHighReading = 'HUGE'
		END
	ELSE IF(@newAvgUsage < @MildAmount)	
		BEGIN
			SET @IsLowHighReading = 'MILD'
		END
	
	RETURN @IsLowHighReading

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthShortName]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		JEEVAN REDDY
-- Create date: 25-10-2014
-- Description:	Get three Character Month Code
-- =============================================
ALTER FUNCTION [dbo].[fn_GetMonthShortName] 
(
	@Month INT
)
RETURNS VARCHAR(20)
AS
BEGIN
 RETURN CASE @Month 
		WHEN 1 THEN 'JAN'
		WHEN 2 THEN 'FEB'
		WHEN 3 THEN 'MAR'
		WHEN 4 THEN 'APR'
		WHEN 5 THEN 'MAY'
		WHEN 6 THEN 'JUN'
		WHEN 7 THEN 'JUL'
		WHEN 8 THEN 'AUG'
		WHEN 9 THEN 'SEP'
		WHEN 10 THEN 'OCT'
		WHEN 11 THEN 'NOV'
		WHEN 12 THEN 'DEC'
		END 
	END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthName]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		:	Suresh Kumar Dasi
-- Create date	:	13 May 2014
-- Description	:	To Get the Month Name With MonthId
-- =============================================
ALTER FUNCTION [dbo].[fn_GetMonthName]
(
	@Month INT
)
RETURNS VARCHAR(50)
AS
BEGIN
	
	DECLARE @MonthName VARCHAR(50) = ''				
		SET @MonthName =	
				CASE @Month
				WHEN 1 THEN 'January'
				WHEN 2 THEN 'February'
				WHEN 3 THEN 'March'
				WHEN 4 THEN 'April'
				WHEN 5 THEN 'May'
				WHEN 6 THEN 'June'
				WHEN 7 THEN 'July'
				WHEN 8 THEN  'August'
				WHEN 9 THEN 'September'
				WHEN 10 THEN  'October'
				WHEN 11 THEN  'November'
				WHEN 12 THEN  'December'
			END
	RETURN @MonthName
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetLast_N_Months_ByDate]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 25-09-2014
-- Description:	To get the list of last n months details from current month
-- select * from dbo.fn_GetLast_N_Months(3)
-- =============================================
ALTER FUNCTION [dbo].[fn_GetLast_N_Months_ByDate]
(
 @Last_N_Months INT
 ,@FromDate DATETIME
)
RETURNS @ResultTable TABLE(Id INT IDENTITY(1,1),[Year] INT,[Month] INT)
AS
BEGIN

	DECLARE @Month INT
			,@Year INT
			
	--DECLARE 
		DECLARE @monthCount INT = 0

		WHILE(@monthCount < @Last_N_Months)
			BEGIN
				INSERT INTO @ResultTable([Month],[Year])
				SELECT  DATEPART(MM,DATEADD(MONTH,-1*@monthCount,@FromDate)), DATEPART(YY,DATEADD(MONTH,-1*@monthCount,@FromDate))
				SET @monthCount = @monthCount + 1
			END
			
	RETURN 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetSpotBillingMessages]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 02-09-2014
-- Description:	The purpose of this function is to get the MessageS
-- =============================================
ALTER FUNCTION [dbo].[fn_GetSpotBillingMessages]
(
@MessageNo INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Message VARCHAR(20) 
	
	IF(@MessageNo = 1)
		BEGIN
			SET @Message = 'Message1'
		END
	ELSE IF(@MessageNo = 2)
		BEGIN
			SET @Message = 'Message2'
		END
	ELSE IF(@MessageNo = 3)
		BEGIN
			SET @Message = 'Message3'
		END
	ELSE IF(@MessageNo = 4)
		BEGIN
			SET @Message = 'Message4'
		END	
		
	RETURN @Message

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_splitTwo]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
--select * from dbo.[fn_splitTwo]('1,2,3,4,4,5,5,5,5,5,5,5,6,5','a,c,d,f,f,f,f,f,ds,h,s,s,s,l',',')
-- =============================================
ALTER FUNCTION [dbo].[fn_splitTwo](
@String nvarchar (MAX),
@String1 nvarchar (MAX),
@Delimiter nvarchar (10)
)
returns @ValueTable table ([Value1] varchar(MAX),[Value2] varchar(MAX))
begin


declare @NextString nvarchar(MAX)
declare @NextString1 nvarchar(MAX)
declare @Pos int
declare @Pos1 int
 
declare @NextPos int
declare @NextPos1 int
 
declare @CommaCheck nvarchar(1)
 declare @CommaCheck1 nvarchar(1)
--Initialize
set @NextString = ''
set @NextString1 = ''
set @CommaCheck = right(@String,1)
set @CommaCheck1 = right(@String1,1)
 --Check for trailing Comma, if not exists, INSERT
if (@CommaCheck <> @Delimiter ) 
set @String = @String + @Delimiter
if (@CommaCheck <> @Delimiter ) 
set @String1 = @String1 + @Delimiter

--Get position of first Comma
set @Pos = charindex(@Delimiter,@String)
set @Pos1 = charindex(@Delimiter,@String1)
 
 set @NextPos = 1
 set @NextPos1 = 1

--Loop while there is still a comma in the String of levels
while (@pos <> 0 and @pos1<>0)
begin
set @NextString = substring(@String,1,@Pos - 1)
set @NextString1 = substring(@String1,1,@Pos1 - 1)
insert into @ValueTable([Value1],[Value2]) Values (@NextString,@NextString1)
set @String = substring(@String,@pos +1,len(@String))
set @String1 = substring(@String1,@pos1 +1,len(@String1))
set @NextPos = @Pos
set @NextPos1 = @Pos1

set @pos = charindex(@Delimiter,@String)
set @pos1 = charindex(@Delimiter,@String1)
 

end

return
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_splitThree]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[fn_splitThree](
@String nvarchar (MAX),
@String1 nvarchar (MAX),
@String2 nvarchar(MAX),
@Delimiter nvarchar (10)
)
returns @ValueTable table ([Value1] varchar(MAX),[Value2] varchar(MAX),[Value3] varchar(MAX))
begin


declare @NextString nvarchar(MAX)
declare @NextString1 nvarchar(MAX)
declare @NextString2 nvarchar(MAX)
declare @Pos int
declare @Pos1 int
declare @Pos2 int
 
declare @NextPos int
declare @NextPos1 int
declare @NextPos2 int
 
declare @CommaCheck nvarchar(1)
 declare @CommaCheck1 nvarchar(1)
 declare @CommaCheck2 nvarchar(1)
--Initialize
set @NextString = ''
set @NextString1 = ''
set @NextString2 = ''
set @CommaCheck = right(@String,1)
set @CommaCheck1 = right(@String1,1)
set @CommaCheck2 = right(@String2,1)
 --Check for trailing Comma, if not exists, INSERT
if (@CommaCheck <> @Delimiter ) 
set @String = @String + @Delimiter
if (@CommaCheck <> @Delimiter ) 
set @String1 = @String1 + @Delimiter
if(@CommaCheck <> @Delimiter )
set @String2 = @String2 + @Delimiter

--Get position of first Comma
set @Pos = charindex(@Delimiter,@String)
set @Pos1 = charindex(@Delimiter,@String1)
set @Pos2 = CHARINDEX(@Delimiter,@String2)
 
 set @NextPos = 1
 set @NextPos1 = 1
 set @NextPos2 = 1

--Loop while there is still a comma in the String of levels
while (@pos <> 0 and @pos1<>0 and @Pos2<>0)
begin
set @NextString = substring(@String,1,@Pos - 1)
set @NextString1 = substring(@String1,1,@Pos1 - 1)
set @NextString2 = SUBSTRING(@string2,1,@Pos2 - 1)
insert into @ValueTable([Value1],[Value2],[Value3]) Values (@NextString,@NextString1,@NextString2)
set @String = substring(@String,@pos +1,len(@String))
set @String1 = substring(@String1,@pos1 +1,len(@String1))
set @String2 = SUBSTRING(@String2,@pos2 +1,len(@String2))
set @NextPos = @Pos
set @NextPos1 = @Pos1
set @NextPos2 = @Pos2

set @pos = charindex(@Delimiter,@String)
set @pos1 = charindex(@Delimiter,@String1)
set @Pos2 = CHARINDEX(@Delimiter, @String2)

end

return
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_splitMenuId]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Faiz - ID103>  
-- Create date: <10-Jan-2015>  
-- Description: <TO split the permissions ID in list for MenuId>  
--select * from dbo.[fn_splitTwo]('1,2,3,4,4,5,5,5,5,5,5,5,6,5','a,c,d,f,f,f,f,f,ds,h,s,s,s,l',',')  
-- =============================================  
ALTER FUNCTION [dbo].[fn_splitMenuId](  
@String nvarchar (MAX),   
@Delimiter nvarchar (10)  
)  
returns @ValueTable table ([MenuId] varchar(MAX))
begin  
  
  
declare @NextString nvarchar(MAX)  
declare @Pos int  
   
declare @NextPos int  
   
declare @CommaCheck nvarchar(1)  
--Initialize  
set @NextString = ''  
set @CommaCheck = right(@String,1)    
 --Check for trailing Comma, if not exists, INSERT  
if (@CommaCheck <> @Delimiter )   
set @String = @String + @Delimiter   
  
--Get position of first Comma  
set @Pos = charindex(@Delimiter,@String)   
   
 set @NextPos = 1  
  
--Loop while there is still a comma in the String of levels  
while (@pos <> 0 )  
begin  
set @NextString = substring(@String,1,@Pos - 1)   
insert into @ValueTable([MenuId]) Values (@NextString)  
set @String = substring(@String,@pos +1,len(@String))    
set @NextPos = @Pos  
  
set @pos = charindex(@Delimiter,@String) 
   
  
end  
  
return  
end  
  
  
  
  ------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Split]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[fn_Split](
@String varchar(MAX),
@Delimiter nvarchar (10)
)
returns @ValueTable table ([com] varchar(MAX))
begin
declare @NextString varchar(MAX)
declare @Pos int
declare @NextPos int
declare @CommaCheck nvarchar(1)

--Initialize
set @NextString = ''
set @CommaCheck = right(@String,1)

--Check for trailing Comma, if not exists, INSERT
if (@CommaCheck <> @Delimiter ) 
set @String = @String + @Delimiter

--Get position of first Comma
set @Pos = charindex(@Delimiter,@String)
set @NextPos = 1

--Loop while there is still a comma in the String of levels
while (@pos <> 0)
begin
set @NextString = substring(@String,1,@Pos - 1)
insert into @ValueTable ( [com]) Values (@NextString)
set @String = substring(@String,@pos +1,len(@String))
set @NextPos = @Pos
set @pos = charindex(@Delimiter,@String)
end

return
end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsTariffNameExists]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Bhimaraju,,V>
-- Create date: <11 Feb 2014>
-- Description:	This function is used to Check the TariffName is existing or not
--Select dbo.fn_IsTariffNameExists
-- =============================================
ALTER FUNCTION [dbo].[fn_IsTariffNameExists]
(
@TariffName VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	DECLARE @Result BIT				
		IF EXISTs(select 0 from Tbl_TariffTypes where TariffName= @TariffName)
			BEGIN
				SET @Result = 1
			END
		ELSE
			BEGIN
				SET @Result = 0
			END	
	RETURN @Result

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsPreviousBillMonthOpen]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: <Suresh Kumar Dasi>
-- Create date  : <11-JUL-2014>
-- Description	: This function is used to Check the customer billed month is Locked or not ?
-- select dbo.[fn_IsPreviousBillMonthOpen](2015,1)
-- =============================================
ALTER FUNCTION [dbo].[fn_IsPreviousBillMonthOpen]
(
@Year INT
,@Month INT
)
RETURNS BIT
AS
BEGIN
		DECLARE @PreviousYear INT		
				,@PreviousMonth INT		
				,@IsOpen BIT = 0
				,@MonthStartDate VARCHAR(50)     
   
		SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
				

---- Old COde Only For Previous one month checking
		--IF(@Month = 1)
		--	BEGIN
		--		SET @PreviousMonth = 12
		--		SET @PreviousYear = @Year - 1 
		--	END
		--ELSE	
		--	BEGIN		
		--		SET @PreviousMonth = @Month - 1
		--		SET @PreviousYear = @Year
		--	END

			
		--IF NOT EXISTS(SELECT 0 FROM Tbl_BillingMonths WHERE OpenStatusId = 2 AND [MONTH] = @PreviousMonth AND [YEAR] = @PreviousYear)
		--	BEGIN
		--		SET @IsOpen = 1
		--	END			
			
			
			IF EXISTS( SELECT 0 FROM Tbl_BillingMonths WHERE OpenStatusId = 1 
				AND CONVERT(DATE,CONVERT(VARCHAR(20),[Year])+'-'+CONVERT(VARCHAR(20),[Month])+'-01') < CONVERT(DATE,@MonthStartDate))
				BEGIN
					SET @IsOpen = 1
				END
				
	RETURN @IsOpen

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsPaidMeterCustomer]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: <Suresh Kumar Dasi>
-- Create date	: <10-NOV-2014>
-- Description	: This function is used to Check the customers is paid meter customer or not
-- =============================================
ALTER FUNCTION [dbo].[fn_IsPaidMeterCustomer]
(
@AccountNo VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
		DECLARE @Result BIT				
		
		IF EXISTS(SELECT 0 FROM Tbl_PaidMeterDetails WHERE AccountNo = @AccountNo AND ActiveStatusId = 1)
			BEGIN
				SET @Result = 1
			END
		ELSE
			BEGIN
				SET @Result = 0
			END	
			
	RETURN @Result

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsHighEstimation]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 08-OCT-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION [dbo].[fn_IsHighEstimation]
(
 @AccountNo VARCHAR(50)
 ,@EstimationUsage NUMERIC
 ,@ReportMonths INT
 ,@FromDate DATETIME
)
RETURNS BIT
AS
BEGIN
	DECLARE @AcountNo VARCHAR(50)
		,@OldAcountNo VARCHAR(50)
		,@MonthId INT
		,@PresentYear INT
		,@PresentMonth INT
		,@TotalUsage DECIMAL(18,2)
		,@TotalCount DECIMAL(18,2)
		,@IsHighEstimate BIT = 0
		,@OpenMonth INT
		,@OpenYear INT
		
	DECLARE @ResultedMonths TABLE(Id INT IDENTITY(1,1),[Year] INT,[Month] INT)
	DECLARE @CustomerBills TABLE(Usage DECIMAL(18,0))
    
    INSERT INTO @ResultedMonths
	SELECT [YEAR],[MONTH] FROM dbo.fn_GetLast_N_Months_ByDate(@ReportMonths,@FromDate) ORDER BY Id DESC
	
	SELECT TOP(1) @MonthId = Id FROM @ResultedMonths ORDER BY Id ASC    
	---- Collection all the transaction Start
	WHILE(EXISTS(SELECT TOP(1) Id FROM @ResultedMonths WHERE Id >= @MonthId ORDER BY Id ASC))    
	  BEGIN   
		SELECT @PresentMonth = [Month],@PresentYear = [Year] FROM @ResultedMonths WHERE Id = @MonthId   
	  
		INSERT INTO @CustomerBills
		SELECT Usage FROM Tbl_CustomerBills 
		WHERE AccountNo = @AcountNo AND BillMonth = @PresentMonth AND BillYear = @PresentYear 
			  
	    IF(@MonthId = (SELECT TOP(1) Id FROM @ResultedMonths ORDER BY Id DESC))    
			  BREAK    
		  ELSE    
			   BEGIN    
					SET @MonthId = (SELECT TOP(1) Id FROM @ResultedMonths WHERE  Id > @MonthId ORDER BY Id ASC)    
					IF(@MonthId IS NULL) break;    
					  Continue    
			   END    
	  END      
	  
	  SELECT @TotalCount = COUNT(0) FROM @CustomerBills
	  SELECT @TotalUsage = SUM(Usage) FROM @CustomerBills
	--  SELECT  (Usage) FROM @CustomerBills
	  
	   IF(ISNULL(CONVERT(DECIMAL(18,2), @TotalUsage/@TotalCount),0)<@EstimationUsage)
		BEGIN
			SET @IsHighEstimate = 1
		END
			
	RETURN @IsHighEstimate 

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsGreaterBillMonthOpen]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: Bhimaraju.V
-- Create date  : 30-JUL-2014
-- Description	: This function is used to Check the customer billed month is Locked or not ?
-- select dbo.[fn_IsGreaterBillMonthOpen](2015,1)
-- =============================================
ALTER FUNCTION [dbo].[fn_IsGreaterBillMonthOpen]
(
@Year INT
,@Month INT
)
RETURNS BIT
AS
BEGIN
		DECLARE @PreviousYear INT		
				,@PreviousMonth INT		
				,@IsOpen BIT = 0
				,@MonthStartDate VARCHAR(50)     
   
		SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
				
		IF EXISTS( SELECT 0 FROM Tbl_BillingMonths WHERE OpenStatusId = 1 
					AND CONVERT(DATE,CONVERT(VARCHAR(20),[Year])+'-'+CONVERT(VARCHAR(20),[Month])+'-01') > CONVERT(DATE,@MonthStartDate))
			BEGIN
				SET @IsOpen = 1
			END
				
	RETURN @IsOpen

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsFirstTimeCustomerForBilling]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Rajaiah>
-- Create date: <30-JUL-2014>
-- Description	: This function is used to Check the Is First Time Customer For Billing.
-- =============================================
ALTER FUNCTION [dbo].[fn_IsFirstTimeCustomerForBilling]
(
	@GlobalAccountNumber VARCHAR(50)	
)
RETURNS BIT
AS
BEGIN
		DECLARE @IsFirstTimeCustomer BIT = 1 		
				
		IF EXISTS(SELECT CustomerBillId FROM Tbl_CustomerBills WHERE AccountNo = @GlobalAccountNumber)
		BEGIN
			SET @IsFirstTimeCustomer = 0
		END 		
			
	RETURN @IsFirstTimeCustomer

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsDuplicatePayment]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Suresh Kumar D
-- Create date : 30-10-2014
-- Description : to check the duplicate payment or not ?
-- =============================================
ALTER FUNCTION [dbo].[fn_IsDuplicatePayment]
(
@AccountNo VARCHAR(20)
,@PaidDate DATETIME
,@PaidAmount DECIMAL(18,2)
,@ReceiptNumber VARCHAR(20)
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @IsPaymentExists BIT = 0
	DECLARE @GlobalAccountNo VARCHAR(50)

	IF EXISTS(SELECT 0 FROM Tbl_CustomerPayments WHERE AccountNo = @AccountNo 
					AND ActivestatusId = 1 AND CONVERT(DATE,RecievedDate) = CONVERT(DATE,@PaidDate) 
					AND PaidAmount = @PaidAmount AND ReceiptNo=@ReceiptNumber) 
		SET @IsPaymentExists  = 1
		
	RETURN @IsPaymentExists 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsCustomerMonthBilled]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author     :	Suresh Kumar Dasi
-- Create date: 25-08-2014
-- Description:	This function is used to Check the The Customer having Bill Or not for this month
-- =============================================
ALTER FUNCTION [dbo].[fn_IsCustomerMonthBilled]
(
@AccountNo VARCHAR(50)	
,@Year INT
,@Month INT
)
RETURNS BIT
AS
BEGIN
	DECLARE @Result BIT = 0
		
	IF EXISTS(SELECT 0 FROM Tbl_CustomerBills B WHERE B.AccountNo = @AccountNo AND BillMonth = @Month AND BillYear = @Year) 		
		BEGIN--- Checking CustomerBills Table
			SET @Result = 1
		END
	ELSE
		BEGIN--- Checking MeterReaing Table
			IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings B WHERE B.GlobalAccountNumber = @AccountNo AND IsBilled = 1 AND DATEPART(YY,ReadDate) = @Year AND DATEPART(MM,ReadDate) = @Month)
				BEGIN
					SET @Result = 1					
				END
		END			
			
	RETURN @Result

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsCustomerHaveLatestBill_ReadDate]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: <Suresh Kumar Dasi>
-- Create date  : <22-AUG-2014>
-- Description	: This Function is used to Check the is Latesrt Bill exists.
-- select dbo.fn_IsCustomerHaveLatestBill_ReadDate('BEDC_CAN_0042','21-06-2014')
-- =============================================
ALTER FUNCTION [dbo].[fn_IsCustomerHaveLatestBill_ReadDate]
(
@AccountNo VARCHAR(50)
,@ReadDate DATE
)
RETURNS BIT
AS
BEGIN
		DECLARE @IsLatested BIT = 0 		
				,@SelectedDate DATETIME    
				,@Date DATETIME    
				,@MonthStartDate VARCHAR(50)     
				,@Month INT    
				,@Year INT 
		
		SELECT TOP(1) @Month = BillMonth ,@Year = BillYear FROM Tbl_CustomerBills WHERE AccountNo = @AccountNo ORDER BY CustomerBillId DESC
		SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
        --SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(4),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   
        SET @SelectedDate = CONVERT(DATETIME,CONVERT(VARCHAR(4),DATEPART(YEAR,@ReadDate))+'-'+CONVERT(VARCHAR(2),DATEPART(MONTH,@ReadDate))+'-01')   
   
							
		 IF(CONVERT(DATE,@MonthStartDate) > CONVERT(DATE,@SelectedDate))
			BEGIN
				SET @IsLatested = 1
			END 		
			
	RETURN @IsLatested

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsCustomerBilledMonthLocked]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Suresh Kumar Dasi>  
-- Create date: <07-JUL-2014>  
-- Description : This function is used to Check the customer billed month is Locked or not ?  
--select dbo.fn_IsCustomerBilledMonthLocked(2014,5,'BEDC_CAN_0040')  
-- =============================================  
ALTER FUNCTION [dbo].[fn_IsCustomerBilledMonthLocked]  
(  
@Year INT  
,@Month INT  
,@AccountNo VARCHAR(50)  
)  
RETURNS BIT  
AS  
BEGIN  
  DECLARE @Result BIT = 0    
  --  ,@BillingQueueScheduleId INT  
  --SELECT @BillingQueueScheduleId = BillingQueueScheduleId FROM Tbl_BillingQueueSchedule WHERE OpenStatusId = 2 AND BillingYear = @Year AND BillingMonth = @Month  
    
  IF EXISTS(SELECT 0 from Tbl_BillingQueeCustomers C 
  JOIN Tbl_BillingQueueSchedule S ON C.BillingQueuescheduleId = S.BillingQueueScheduleId  
      WHERE AccountNo = @AccountNo AND OpenStatusId in ( 2 , 3) AND BillingYear = @Year AND BillingMonth = @Month)  
   BEGIN  
    SET @Result = 1  
   END  
     
 RETURN @Result  
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsCustomerBill_Latest]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suresh Kumar Dasi>
-- Create date: <30-JUL-2014>
-- Description	: This function is used to Check the Bill is Latesrt one.
-- =============================================
ALTER FUNCTION [dbo].[fn_IsCustomerBill_Latest]
(
@CustomerBillId INT
,@AccountNo VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
		DECLARE @IsLatested BIT = 1 		
				
		 IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE CustomerBillId > @CustomerBillId AND AccountNo = @AccountNo)
			BEGIN
				SET @IsLatested = 0
			END 		
			
	RETURN @IsLatested

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PreviousBalance]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 20-FEB-2014
-- Description:	The purpose of this function to get previous balance
-- =============================================
ALTER FUNCTION [dbo].[fn_PreviousBalance]
(
	@GlobalAccountNo VARCHAR(50)
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	DECLARE
			@TotalBillAmountWithArrears DECIMAL(18,2)
			,@PreviousBillNo VARCHAR(50)
			,@TotalPayment DECIMAL(18,2)
			,@PreviousBalance DECIMAL(18,2)=0
		
		IF((SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo)>=2)
		BEGIN
			;WITH TBL AS
			(
				SELECT TOP 2 *, ROW_NUMBER() OVER(order by CustomerBillId DESC) AS ID FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo ORDER BY CustomerBillId DESC
			)
			SELECT @TotalBillAmountWithArrears= TotalBillAmountWithArrears,@PreviousBillNo=BillNo FROM TBL WHERE ID=2
			
			SELECT @TotalPayment=SUM(PaidAmount) FROM Tbl_CustomerBillPayments WHERE BillNo=@PreviousBillNo
			
			SET @PreviousBalance=@TotalBillAmountWithArrears-ISNULL(@TotalPayment,0)
		END
	RETURN @PreviousBalance
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PoleDescriptionCodeGenerate]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  FAIZ - ID103    
-- Create date: 25-DEC-2014    
-- Description: GENERATE POLE CODE    
-- =============================================    
    
ALTER FUNCTION [dbo].[fn_PoleDescriptionCodeGenerate](    
@PoleMasterId INT    
,@PoleOrder INT    
)    
RETURNS VARCHAR(20)    
AS    
BEGIN    
     
DECLARE @CODE Varchar(20)    
  ,@CODENew VARCHAR(20)    
  ,@LengthGlobal INT    
  ,@LengthCurrent INT    
      
 --Get current code Length of Master Pole    
 SET @LengthGlobal =(Select PoleMasterCodeLength from Tbl_MPoleMasterDetails Where PoleMasterId=@PoleMasterId)    
 --Declare @LengthGlobal INT = 0    
 --SET @LengthGlobal =(Select PoleMasterCodeLength from Tbl_MPoleMasterDetails Where PoleMasterId=1008)    
 --PRINT @LengthGlobal    
     
 --GET CURRENT Pole Code      
 SET @CODENew= (SELECT TOP 1 Code     
         FROM Tbl_PoleDescriptionTable     
         where PoleMasterId=@PoleMasterId    
         AND PoleOrder=@PoleOrder    
         ORDER BY PoleId DESC)     
             
     
 --Select * from Tbl_PoleDescriptionTable            
 --declare @CODENew INT=0    
 --SET @CODENew= (SELECT TOP 1 Code     
 --    FROM Tbl_PoleDescriptionTable     
 --    where PoleMasterId=1008    
 --    AND PoleOrder=4    
 --    ORDER BY PoleId DESC)    
 --if(@CODENew is null)     
 -- Print 0    
 -- else    
 -- print @CODENew           
                  
 --INCREMENT Business Unit Code BY 1    
 --IF(@CODENew IS NOT NULL)    
 SET @CODE =CONVERT(INT,ISNULL(@CODENew,0)) + 1    
     
 --ELSE     
 --SET @CODE=0;    
    
 SET @CODENew=@CODE    
     
 --GET LEGHT OF CURRENT POLE Code    
 SET @LengthCurrent=LEN(@CODENew)     
     
 --NEW Business Unit Code CAN BE GENERATE OR NOT    
 IF(@LengthCurrent<=@LengthGlobal)    
 BEGIN    
  --IF Business Unit Code    
  WHILE(@LengthCurrent < @LengthGlobal)    
  BEGIN    
   SET @CODENew={fn CONCAT('0',@CODENew)}    
   SET @LengthCurrent=@LengthCurrent+1;    
  END    
 END    
 ELSE    
 BEGIN    
  SET @CODENew=NULL    
 END    
     
 RETURN @CODENew    
 --PRINT @CODENew    
END    
    
----------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_PaymentSumAfterLastBill]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 18-FEB-2014
-- Description:	The purpose of this payment sum while generating bill.
-- =============================================
ALTER FUNCTION [dbo].[fn_PaymentSumAfterLastBill]
(
	@GlobalAccountNo VARCHAR(50)
   ,@PrevBM INT
   ,@PrevBY INT
   ,@PresBM INT
   ,@PresBY INT
   
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	DECLARE
			  @PresDate DATE
             ,@PrevDate DATE
			--@LastBillGenerated DATETIME
			,@TotalPayment DECIMAL(18,2)=0
			
		--//==Select last bill generated date==\\--
		--SET @LastBillGenerated=(SELECT TOP(1)  BillGeneratedDate FROM Tbl_CustomerBills      
		--						WHERE AccountNo = @GlobalAccountNo       
		--					ORDER BY CustomerBillId DESC)
		
		SET @PresDate= CONVERT(DATE,CONVERT(VARCHAR(20),@PresBY)+'-'+CONVERT(VARCHAR(20),@PresBM)+'-01') 
		SET @PrevDate= CONVERT(DATE,CONVERT(VARCHAR(20),@PrevBY)+'-'+CONVERT(VARCHAR(20),@PrevBM)+'-01') 
							
	--//==If customer's bills have generated more than once.==\\--							
	IF(@PresDate !='' )
		BEGIN		
			--//==Select all the payment made between last bill generated and current bill generated date.==\\--							
			SELECT 
			--	@TotalPayment=SUM(CONVERT(DECIMAL(18,2),PaidAmount)) 
			--	FROM Tbl_CustomerPayments 
			--	WHERE AccountNo=@GlobalAccountNo AND (RecievedDate BETWEEN @LastBillGenerated AND dbo.fn_GetCurrentDateTime())SELECT 
				@TotalPayment=SUM(CONVERT(DECIMAL(18,2),PaidAmount)) 
				FROM Tbl_CustomerPayments 
				WHERE AccountNo=@GlobalAccountNo AND (RecievedDate BETWEEN @PrevDate AND @PresDate)
		END
	--//==If customer's bill is generating first time==\\--							
	ELSE
		BEGIN
			--//==Select advance payment made by customer.==\\--							
			SELECT
				@TotalPayment=SUM(CONVERT(DECIMAL(18,2),PaidAmount)) 
				FROM Tbl_CustomerPayments WHERE AccountNo=@GlobalAccountNo
		END
	RETURN @TotalPayment
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_LastBillGeneratedReadType]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 30-12-2014
-- Description:	The purpose of this function is to get last billgenerated read type
-- =============================================
ALTER FUNCTION [dbo].[fn_LastBillGeneratedReadType] 
(
	@GlobalAccountNo VARCHAR(50)
)
RETURNS VARCHAR(200)
AS
BEGIN
	DECLARE @ReadCodeId INT=0,@ReadCode VARCHAR(200)='--'

	SELECT TOP 1 @ReadCodeId=ReadCodeId
	FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo AND ActiveStatusId=1
	ORDER BY CustomerBillId DESC

	SELECT @ReadCode=ReadCode FROM Tbl_MReadCodes WHERE ReadCodeId=@ReadCodeId

	RETURN @ReadCode
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsCountryExists]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Bhimaraju Vanka>
-- Create date: <12-FEB-2014>
-- Description	: This function is used to Check the CountryName,CountryCode is existing or not
--Select dbo.fn_IsCountryExists
-- =============================================
ALTER FUNCTION [dbo].[fn_IsCountryExists]
(
	 @CountryName VARCHAR(50)
	,@CountryCode VARCHAR(20)
)
RETURNS BIT
AS
BEGIN
		DECLARE @Result BIT				
		IF EXISTs(select 0 from Tbl_Countries where CountryName= @CountryName AND CountryCode=@CountryCode)
			BEGIN
				SET @Result = 1
			END
		ELSE
			BEGIN
				SET @Result = 0
			END	
	RETURN @Result

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsBillMonthOpened]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suresh Kumar Dasi>
-- Create date: <02-JUL-2014>
--select dbo.fn_IsBillMonthOpened('04/1/2014')
-- =============================================
ALTER FUNCTION [dbo].[fn_IsBillMonthOpened]
(
 @Date DATETIME
)
RETURNS BIT
AS
BEGIN
		DECLARE @Result BIT	= 0 	
		,@Year INT
		,@Month INT
		
		SET @Year = DATEPART(YY,@Date)
		SET @Month = DATEPART(MM,@Date)
		
				
		IF EXISTS(SELECT 0 FROM Tbl_BillingMonths WHERE OpenStatusId = 1 AND [YEAR] = @Year AND [MONTH] = @Month)
			BEGIN
				SET @Result = 1
			END
			
	RETURN @Result

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsBillGenerated_ByAccNo]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju.V
-- Create date: 20-06-2014
-- Description:	This function is used to Check the The Customer having Bill Or not
-- =============================================
ALTER FUNCTION [dbo].[fn_IsBillGenerated_ByAccNo]
(
@AccountNo VARCHAR(50)	
)
RETURNS INT
AS
BEGIN
	DECLARE @Result INT				
		
	SET @Result = (SELECT COUNT(IsBilled) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@AccountNo AND IsBilled=1)
			
	RETURN @Result

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsBillAdjusted]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suresh Kumar Dasi>
-- Create date: <24-JUL-2014>
-- Description	: This function is used to Check the Bill is Adjusted.
--Select dbo.fn_IsCountryExists
-- =============================================
ALTER FUNCTION [dbo].[fn_IsBillAdjusted]
(
@CustomerBillId INT
)
RETURNS BIT
AS
BEGIN
		DECLARE @IsAdjested BIT = 0 		
				
		 IF EXISTS(SELECT 0 FROM Tbl_BillAdjustments WHERE CustomerBillId = @CustomerBillId)
			BEGIN
				SET @IsAdjested	= 1
			END 		
			
	RETURN @IsAdjested

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsActiveBillMonth]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suresh Kumar Dasi>
-- Create date: <28-JUL-2014>
-- Description	: This function is used to Check the Month is Opened or Closed.
--select dbo.fn_IsActiveBillMonth(2014,04)
-- =============================================
ALTER FUNCTION [dbo].[fn_IsActiveBillMonth]
(
 @Year INT
 ,@Month INT
)
RETURNS BIT
AS
BEGIN
		DECLARE @Result BIT	= 0 	
				
		IF EXISTS(SELECT 0 FROM Tbl_BillingMonths WHERE OpenStatusId = 1 AND [YEAR] = @Year AND [MONTH] = @Month)
			BEGIN
				SET @Result = 1
			END
			
	RETURN @Result

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetTotalPaidAmountOfBill]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author  : NEERAJ KANOJIYA  
-- Create date : 4 JUNE 2014  
-- Description : The purpose of this function is to get the due amount for a bill  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetTotalPaidAmountOfBill]  
(  
@BillNo VARCHAR(50)  
)  
RETURNS VARCHAR(20)  
AS  
BEGIN  
 DECLARE @PaidAmount DECIMAL(18,2)  
   
 SELECT @PaidAmount=SUM(A.PaidAmount)   
 from Tbl_CustomerBillPayments AS A 
 WHERE A.BillNo=@BillNo 
 
 RETURN @PaidAmount   
  
END  
-----------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetNumOfBillCustomersInQueue]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 14 MAY 2014
-- Description:	The purpose of this function is to get the Total Count Bill Queuee Customers
-- =============================================
ALTER FUNCTION [dbo].[fn_GetNumOfBillCustomersInQueue]()
RETURNS INT
AS
BEGIN
	DECLARE @TotalCustomers INT = 0
	
	SELECT 
		@TotalCustomers = COUNT(0) 
	FROM Tbl_BillingQueeCustomers
	WHERE BillGenarationStatusId <> 4
		
	RETURN @TotalCustomers

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetPreviusMonth]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 22-05-2014
-- Description:	to Get the Bill Service Start date
--select dbo.fn_GetPreviusMonth(2014,1)
-- =============================================
ALTER FUNCTION [dbo].[fn_GetPreviusMonth]
(
@Year INT
,@Month INT
)
RETURNS VARCHAR(50)
AS
BEGIN
	
	DECLARE @Date DATETIME     
		    ,@MonthStartDate VARCHAR(50)     
			,@PrevMonth INT
			,@PrevYear INT
			,@Result VARCHAR(50)
			
	SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
	
	SELECT @PrevMonth = DATEPART(MONTH,(DATEADD(MONTH,-1,@MonthStartDate))),@PrevYear = DATEPART(YEAR,(DATEADD(MONTH,-1,@MonthStartDate)))

	SELECT 	@Result = (dbo.fn_GetMonthName(@PrevMonth))+ CONVERT(VARCHAR(5),@PrevYear)
		
		
	RETURN @Result
	
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetPreviuosTotalBillPayment]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Satya>
-- Create date: <10 MARCH 2015>
-- Description:	<TO GET TOTAL PAYMENT MADE BY CUSTOMER FROM LAST BIL GEN DATE TO FIX TEMP PURPOSE>
-- =============================================
ALTER FUNCTION [dbo].[fn_GetPreviuosTotalBillPayment]
(
	@GlobalAccountNumber varchar(50) 
)
RETURNS  Decimal(18,2)
AS
BEGIN
	Declare @PaidAmount  Decimal(18,2) =0
	 Declare @BillCount int =0
	 SET @BillCount= (SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNumber)
	 IF(@BillCount>=2)
		BEGIN
	 
		 
	
		 
		 Declare @Prev_BillDate datetime

		 Select @Prev_BillDate= MAX (BillGeneratedDate) FROM Tbl_CustomerBills where CustomerBillId Not IN (
		  Select MAX (BillGeneratedDate) FROM Tbl_CustomerBills where  AccountNo=@GlobalAccountNumber) and  AccountNo=@GlobalAccountNumber
		 
		 
		  Select @PaidAmount=SUM(PaidAmount)  from Tbl_CustomerPayments where  
		   CreatedDate >=@Prev_BillDate  and AccountNo=@GlobalAccountNumber
		 
		 
		END
		ELSE 
		BEGIN
	 
		 
		 Select @PaidAmount=SUM(PaidAmount)   from Tbl_CustomerPayments where   CreatedDate >=
			(select top 1 LastBillGeneratedDate from Tbl_LastBillGeneratedDate) and AccountNo=@GlobalAccountNumber
		End
	
	
	
	RETURN @PaidAmount

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetPreviousBalanceUsage]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================                
-- Author		: NEERAJ KANOJIYA              
-- Create date  : 22 Apr 2014                
-- Description  : THIS FUNCITON WILL GET CUSTOMER'S AND BALANCE USAGE BY ACCOUNT #         
-- ============================================= 
ALTER FUNCTION [dbo].[fn_GetPreviousBalanceUsage]
(
@AccountNo VARCHAR(20)
)

RETURNS INT
BEGIN
	DECLARE @BalanceUsage INT=0
	IF EXISTS(SELECT 0 FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo)
	BEGIN
		SET @BalanceUsage=(SELECT TOP 1 BU.BalanceUsage 
							FROM 
							(SELECT TOP 2 CustomerBillId,BalanceUsage FROM Tbl_CustomerBills
							WHERE AccountNo=@AccountNo ORDER BY CustomerBillId DESC)AS BU ORDER BY CustomerBillId ASC)
	END
	RETURN @BalanceUsage
END

-----------------------------------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetRegEstimationDetails]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: NEREAJ KANOJIYA
-- Create date	: 01-11-2014
-- Description	: Get the details of regular estimation.
-- Modified	By	: Neeraj Kanojiya
-- Modified Date: 3-11-2014	
-- =============================================
ALTER FUNCTION [dbo].[fn_GetRegEstimationDetails]
(
@AccountNo VARCHAR(50)
)
RETURNS @Table TABLE(MonthYear VARCHAR(20)
				   ,Usage INT
				   ,NetEnergyCharges INT
				   ,TariffClass VARCHAR(50)
				   ,Row INT )
AS
BEGIN
DECLARE @Month INT
		,@Year INT
		,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()
		,@Date DATETIME     
		,@MonthStartDate VARCHAR(50)     
	SELECT @Month = DATEPART(MONTH,@CurrentDate),@Year = DATEPART(YEAR,@CurrentDate)		
    SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
	
	DECLARE @Temp TABLE (CustomerBillId INT
						,ReadCodeId INT
						,BillMonth INT
						,BillYear INT
						,MonthYear VARCHAR(20)
						,Usage INT
						,NetEnergyCharges INT
						,TariffClass VARCHAR(50)
						,Row INT)

	INSERT INTO @Temp 
	SELECT 
		CustomerBillId
		,ReadCodeId 
		,BillMonth
		,BillYear
		,(SELECT dbo.fn_GetMonthName(BillMonth)+'-'+CONVERT(VARCHAR(50),BillYear)) AS MonthYear
		,Usage
		,NetEnergyCharges
		,(SELECT ClassName FROM Tbl_MTariffClasses WHERE ClassID=TariffId) AS ClassName 	  
		,ROW_NUMBER()OVER( ORDER BY CustomerBillId) AS Row
	FROM Tbl_CustomerBills 
	WHERE CONVERT(DATE,CONVERT(VARCHAR(20),BillYear)+'-'+CONVERT(VARCHAR(20),BillMonth)+'-01') <  CONVERT(DATE,@MonthStartDate)
	AND AccountNo = @AccountNo
	--SELECT * FROM @Temp
	DECLARE @CurrentMonthDate DATETIME
			,@CustomerBillId INT
			,@CurrentMonth INT
			,@CurrentYear INT
			,@Count INT = 0
		
	SELECT TOP(1) @CustomerBillId = CustomerBillId FROM @Temp ORDER BY CustomerBillId DESC  
	SELECT @CurrentMonth = DATEPART(MONTH,DATEADD(MONTH,-1,@CurrentDate)),@CurrentYear = DATEPART(YEAR,DATEADD(MONTH,-1,@CurrentDate))		
	
	IF EXISTS(SELECT 0 FROM @Temp WHERE ReadCodeId = 3 AND CustomerBillId = @CustomerBillId AND BillMonth = @CurrentMonth AND BillYear = @CurrentYear)
		BEGIN
			INSERT INTO @Table(MonthYear,Usage,NetEnergyCharges,TariffClass,Row)
					SELECT MonthYear,Usage,NetEnergyCharges,TariffClass,@Count FROM @Temp WHERE ReadCodeId = 3 AND CustomerBillId = @CustomerBillId AND BillMonth = @CurrentMonth AND BillYear = @CurrentYear
		END		
		
	
	 WHILE(EXISTS(SELECT TOP(1) CustomerBillId FROM @Temp WHERE CustomerBillId <= @CustomerBillId ORDER BY CustomerBillId ASC))    
		 BEGIN  	
			SELECT @Count = @Count + 1	
			IF EXISTS(SELECT 0 FROM @Temp WHERE ReadCodeId = 3 AND CustomerBillId = @CustomerBillId AND BillMonth = @CurrentMonth AND BillYear = @CurrentYear)
				BEGIN				
					INSERT INTO @Table(MonthYear,Usage,NetEnergyCharges,TariffClass,Row)
					SELECT MonthYear,Usage,NetEnergyCharges,TariffClass,@Count FROM @Temp WHERE ReadCodeId = 3 AND CustomerBillId = @CustomerBillId AND BillMonth = @CurrentMonth AND BillYear = @CurrentYear
					
					SELECT @CurrentMonthDate = CONVERT(VARCHAR(20),@CurrentYear)+'-'+CONVERT(VARCHAR(20),@CurrentMonth)+'-01'   		
					SELECT @CurrentMonth = DATEPART(MONTH,DATEADD(MONTH,-1,@CurrentMonthDate)),@CurrentYear = DATEPART(YEAR,DATEADD(MONTH,-1,@CurrentMonthDate))		
				END
			ELSE
				Break;	
			
			IF(@CustomerBillId = (SELECT TOP(1) CustomerBillId FROM @Temp ORDER BY CustomerBillId ASC))    
				   BREAK    
			  ELSE    
				   BEGIN    
						SET @CustomerBillId = (SELECT TOP(1) CustomerBillId FROM @Temp WHERE  CustomerBillId < @CustomerBillId ORDER BY CustomerBillId DESC)    
						IF(@CustomerBillId IS NULL) break;    
						  Continue    
				   END    
		 END	 
	
	RETURN

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetRegEstimationCount]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: NEREAJ KANOJIYA
-- Create date	: 01-11-2014
-- Description	: Get the count of regular estimation.
-- Modified	By	: Neeraj Kanojiya
-- Modified Date: 3-11-2014
-- Modified	By	: Jeevan
-- Modified Date: 4-11-2014
-- Modified	By	: NEERAJ KANOIYA
-- Modified Date: 5-11-2014
-- =============================================
ALTER FUNCTION [dbo].[fn_GetRegEstimationCount]
(
@AccountNo VARCHAR(50),@Month INT,@Year INT
)
RETURNS INT
AS
BEGIN
DECLARE 
		--@Month INT
		--,@Year INT
		@CurrentDate DATETIME = CONVERT(DATE,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01')-- dbo.fn_GetCurrentDateTime()
		,@Date DATETIME     
		,@MonthStartDate VARCHAR(50)     
   
	--SELECT @Month = DATEPART(MONTH,@CurrentDate),@Year = DATEPART(YEAR,@CurrentDate)		
    SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
	
	DECLARE @Temp TABLE (CustomerBillId INT,ReadCodeId INT,BillMonth INT,BillYear INT)

	INSERT INTO @Temp 
	SELECT 
		CustomerBillId
		,ReadCodeId 
		,BillMonth
		,BillYear
	FROM Tbl_CustomerBills 
	WHERE CONVERT(DATE,CONVERT(VARCHAR(20),BillYear)+'-'+CONVERT(VARCHAR(20),BillMonth)+'-01') <=  CONVERT(DATE,@MonthStartDate)
	AND AccountNo = @AccountNo
	
	DECLARE @CurrentMonthDate DATETIME
			,@CustomerBillId INT
			,@CurrentMonth INT
			,@CurrentYear INT
			,@Count INT = 0
		
	SELECT TOP(1) @CustomerBillId = CustomerBillId FROM @Temp ORDER BY CustomerBillId DESC  
		SELECT @CurrentMonth =@Month --DATEPART(MONTH,DATEADD(MONTH,-1,@CurrentDate))
			,@CurrentYear =@Year-- DATEPART(YEAR,DATEADD(MONTH,-1,@CurrentDate))		
	
		--IF EXISTS(SELECT 0 FROM @Temp WHERE ReadCodeId = 3 AND CustomerBillId = @CustomerBillId AND BillMonth = @CurrentMonth AND BillYear = @CurrentYear)
		--BEGIN
		--	SELECT @Count = @Count + 1	
		--END		
	
	
	 WHILE(EXISTS(SELECT TOP(1) CustomerBillId FROM @Temp WHERE CustomerBillId <= @CustomerBillId ORDER BY CustomerBillId ASC))    
		 BEGIN  	
			
			IF EXISTS(SELECT 0 FROM @Temp WHERE (ReadCodeId = 3 OR ReadCodeId = 5) AND CustomerBillId = @CustomerBillId AND BillMonth = @CurrentMonth AND BillYear = @CurrentYear)
				BEGIN
					SELECT @Count = @Count + 1
					SELECT @CurrentMonthDate = CONVERT(VARCHAR(20),@CurrentYear)+'-'+CONVERT(VARCHAR(20),@CurrentMonth)+'-01'   		
					SELECT @CurrentMonth = DATEPART(MONTH,DATEADD(MONTH,-1,@CurrentMonthDate)),@CurrentYear = DATEPART(YEAR,DATEADD(MONTH,-1,@CurrentMonthDate))		
				END
			ELSE
				Break;	
			
			IF(@CustomerBillId = (SELECT TOP(1) CustomerBillId FROM @Temp ORDER BY CustomerBillId ASC))    
				   BREAK    
			  ELSE    
				   BEGIN    
						SET @CustomerBillId = (SELECT TOP(1) CustomerBillId FROM @Temp WHERE  CustomerBillId < @CustomerBillId ORDER BY CustomerBillId DESC)    
						IF(@CustomerBillId IS NULL) break;    
						  Continue    
				   END    
		 END	
		   RETURN @Count;
	end
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMarketersFullName]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 18-10-2014
-- Description:	To Get FullName Of the Marketer FirstName+MiddleName+LastName
-- =============================================
ALTER FUNCTION [dbo].[fn_GetMarketersFullName]
(
	@MarketerId INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Name VARCHAR(MAX)

		SELECT @Name =	LTRIM(RTRIM(
		CASE WHEN FirstName IS NULL OR FirstName = '' THEN '' ELSE  LTRIM(RTRIM(FirstName)) END + 
		CASE WHEN MiddleName IS NULL OR MiddleName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(MiddleName)) END +
		CASE WHEN LastName IS NULL OR LastName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(LastName)) END))
	FROM [Tbl_Marketers]				
	WHERE MarketerId = @MarketerId
	RETURN @Name
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetLast_N_MonthsByFromAndToDates]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 06-11-2014
-- Description:	To get the list of last n months details between two dates
-- =============================================
ALTER FUNCTION [dbo].[fn_GetLast_N_MonthsByFromAndToDates]
(
 @FromDate DATE
 ,@ToDate DATE
)
RETURNS @ResultTable TABLE(Id INT IDENTITY(1,1),[Year] INT,[Month] INT)
AS
BEGIN

	DECLARE @Month INT
			,@Year INT
			,@CurrentDate DATETIME
			,@Last_N_Months int=DATEDIFF(MM,@FromDate,@ToDate)+1
	--DECLARE 
		SELECT @CurrentDate = dbo.fn_GetCurrentDateTime()
		DECLARE @monthCount INT = 0

		WHILE(@monthCount < @Last_N_Months)
			BEGIN
				INSERT INTO @ResultTable([Month],[Year])
				SELECT  DATEPART(MM,DATEADD(MONTH,-1*@monthCount,@ToDate)), DATEPART(YY,DATEADD(MONTH,-1*@monthCount,@ToDate))
				SET @monthCount = @monthCount + 1
			END
			
	RETURN 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetLast_N_Months]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 25-09-2014
-- Description:	To get the list of last n months details from current month
-- select * from dbo.fn_GetLast_N_Months(3)
-- =============================================
ALTER FUNCTION [dbo].[fn_GetLast_N_Months]
(
 @Last_N_Months INT
)
RETURNS @ResultTable TABLE(Id INT IDENTITY(1,1),[Year] INT,[Month] INT)
AS
BEGIN

	DECLARE @Month INT
			,@Year INT
			,@CurrentDate DATETIME

	--DECLARE 
		SELECT @CurrentDate = dbo.fn_GetCurrentDateTime()
		DECLARE @monthCount INT = 0

		WHILE(@monthCount < @Last_N_Months)
			BEGIN
				INSERT INTO @ResultTable([Month],[Year])
				SELECT  DATEPART(MM,DATEADD(MONTH,-1*@monthCount,@CurrentDate)), DATEPART(YY,DATEADD(MONTH,-1*@monthCount,@CurrentDate))
				SET @monthCount = @monthCount + 1
			END
			
	RETURN 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetFixedCharges]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER FUNCTION [dbo].[fn_GetFixedCharges]
(	
	-- Add the parameters for the function here
	 @ClassID INT
	,@MonthStartDate DATE
)
RETURNS @tblFixedCharges TABLE
(
     ClassID	int
	,Amount Decimal(18,2)
)
AS
BEGIN
	--DECLARE @FixedCharges DECIMAL
	---- Add the SELECT statement with parameter references here
	--SELECT @FixedCharges=SUM(Amount) FROM Tbl_LAdditionalClassCharges (NOLOCK)
	--WHERE  IsActive = 1   
	--AND  classID = @ClassID
	--AND @MonthStartDate BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)  
	
	INSERT INTO @tblFixedCharges(ClassID,Amount)
	SELECT chargeid,Amount FROM Tbl_LAdditionalClassCharges (NOLOCK)
	WHERE  IsActive = 1   
	AND  classID =@ClassID 
	AND @MonthStartDate BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)   

	RETURN 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetDirectCustomerConsumption]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[fn_GetDirectCustomerConsumption]
(
	-- Add the parameters for the function here
	@GlobalAccountNumber VARCHAR(50)	
	,@CycleId VARCHAR(100)
	,@ClusterCategoryId INT	
	,@Year INT
	,@Month INT
	,@InititalkWh INT
	,@TariffId INT
)
RETURNS DECIMAL
AS
BEGIN
	-- Declare the return variable here
	DECLARE @BillingRule INT, @Usage DECIMAL(20),@EstimationSettingId INT

	-- Add the T-SQL statements to compute the return value here
	
	SELECT @BillingRule=BillingRule,@EstimationSettingId=EstimationSettingId FROM tbl_EstimationSettings(NOLOCK) WHERE BillingYear=@Year AND BillingMonth=@Month 
		AND CycleId=@CycleId AND ClusterCategoryId=@ClusterCategoryId AND ActiveStatusID=1 AND TariffId=@TariffId
		
	IF @BillingRule=1 -- As Per Settings Rule from table "tbl_EstimationSettings" and "tbl_MBillingRules"
	BEGIN
		SELECT @Usage=EnergytoCalculate FROM tbl_EstimationSettings(NOLOCK) WHERE @EstimationSettingId=EstimationSettingId
	END
	ELSE
	BEGIN
		SELECT @Usage=AverageReading FROM tbl_DirectCustomersAvgReadings(NOLOCK) WHERE GlobalAccountNumber=@GlobalAccountNumber
		IF @Usage IS NULL
		BEGIN
			SELECT @Usage=@InititalkWh 
		END		
	END
	-- Return the result of the function
	RETURN @Usage

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetGlobalMessages_ByBu_Id]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  NEERAJ KANOJIYA
-- Create date: 9 MARCH 2014  
-- Description: The purpose of this function is to get the GLOBAL messages Based on BU_ID  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetGlobalMessages_ByBu_Id]  
(  
@BU_ID VARCHAR(50)  
)  
RETURNS VARCHAR(MAX)  
AS  
BEGIN  
 DECLARE @Message VARCHAR(MAX) 
		SET @Message=(SELECT TOP 1
									CONVERT(VARCHAR(50),GM1.[Message])
									+'|'+CONVERT(VARCHAR(50),GM2.[Message])
									+'|'+CONVERT(VARCHAR(50),GM3.[Message])
			FROM Tbl_GlobalMessages AS GM1  
			JOIN Tbl_GlobalMessages AS GM2 ON GM1.GlobalMessageId=GM2.GlobalMessageId-1
			JOIN Tbl_GlobalMessages AS GM3 ON GM1.GlobalMessageId=GM3.GlobalMessageId-2
			WHERE GM1.BU_ID=@BU_ID)
 RETURN @Message   
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetEstimationValue]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Suresh kumar>
-- Create date: <08-OCT-2014>
-- Description	: This function is used to Get the estimation value based on year, month and tariff id.
-- =============================================
ALTER FUNCTION [dbo].[fn_GetEstimationValue]
(
@Year INT 
,@Month INT 
,@TariffId INT
,@CycleId VARCHAR(50)
,@ClusterCategoryId INT
)
RETURNS DECIMAL(18,0)
AS
BEGIN
		DECLARE @Result DECIMAL(18,0) = 0

		IF EXISTS(SELECT 0 FROM Tbl_EstimationSettings WHERE BillingYear=@Year AND BillingMonth=@Month AND TariffId=@TariffId AND CycleId = @CycleId AND  ClusterCategoryId=@ClusterCategoryId AND ActiveStatusId=1)
			BEGIN
				SELECT @Result = EnergytoCalculate FROM Tbl_EstimationSettings WHERE BillingYear=@Year AND BillingMonth=@Month AND TariffId=@TariffId AND CycleId = @CycleId AND  ClusterCategoryId=@ClusterCategoryId AND ActiveStatusId=1
			END
	
	RETURN @Result 

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetEstimationExistence]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<NEERAJ KANOJIYA>
-- Create date: <29-JUL-2014>
-- Description	: This function is used to Get the existence of estimation based on year, month and tariff id.
-- Modified By : T.Karthik
--Modified Date : 29-12-2014  
-- =============================================
ALTER FUNCTION [dbo].[fn_GetEstimationExistence]
(
@Year INT 
,@Month INT 
,@TariffId INT
,@CycleId VARCHAR(50)
,@ClusterCategoryId INT
)
RETURNS BIT
AS
BEGIN
		DECLARE @Result BIT = 0

		IF EXISTS(SELECT 0 FROM Tbl_EstimationSettings WHERE BillingYear=@Year AND BillingMonth=@Month AND TariffId=@TariffId AND CycleId = @CycleId AND ClusterCategoryId=@ClusterCategoryId AND ActiveStatusId=1)
			BEGIN
				SET @Result = 1
			END
	
	RETURN @Result 

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetEnergyChanges]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 05-06-2014
-- Description:	The purpose of this function is to Calculate The Bill Amount Based on Consumption
-- Modified By : Padmini
-- Modified Date : 26-12-2014  

-- =============================================
ALTER FUNCTION [dbo].[fn_GetEnergyChanges]
(
	@AccountNumner VARCHAR(50)
	,@Consumption DECIMAL(18,2)
	,@Month INT 
	,@Year INT
	,@TariffClassID INT
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	DECLARE @Date VARCHAR(50) 
			,@RemaingUsage DECIMAL(20)
			,@TotalAmount DECIMAL(18,2)= 0
			,@PresentCharge INT
			,@FromUnit INT
			,@ToUnit INT
			,@Amount DECIMAL(18,2)
			
	SET @Date = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'
	DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)

	DELETE FROM @ChargeUnits 
	
	INSERT INTO @ChargeUnits 
	SELECT 
		ClassID,FromKW,ToKW,Amount,TaxId FROM Tbl_LEnergyClassCharges 
	WHERE ClassID =@TariffClassID
	AND IsActive = 1 AND CONVERT(DATE,@Date) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
	ORDER BY FromKW ASC		
	
	SET @RemaingUsage = @Consumption
	SET @TotalAmount = 0
			
	SELECT TOP(1) @PresentCharge = Id FROM @ChargeUnits ORDER BY Id ASC

	WHILE(EXISTS(SELECT TOP(1) Id FROM @ChargeUnits WHERE Id >= @PresentCharge ORDER BY Id ASC))
		BEGIN
		
		SELECT @FromUnit = FromKW,@ToUnit = ToKW,@Amount = Amount
		FROM @ChargeUnits
		WHERE Id = @PresentCharge
		
		IF(@ToUnit IS NOT NULL)
			BEGIN
				IF(@RemaingUsage > (@ToUnit - @FromUnit)+1)
					BEGIN					
						SET @TotalAmount = @TotalAmount + ((@ToUnit - @FromUnit)+1)* @Amount	
						SET @RemaingUsage = @RemaingUsage - ((@ToUnit - @FromUnit)+1)
					END	
				ELSE
					BEGIN
						SET @TotalAmount = @TotalAmount + @RemaingUsage * @Amount
						SET @RemaingUsage = 0
					END	
			END
		ELSE
			BEGIN
				SET @TotalAmount = @TotalAmount + @RemaingUsage * @Amount	
				 SET @RemaingUsage = @RemaingUsage - @RemaingUsage
			END			
		
	IF(@PresentCharge = (SELECT TOP(1) Id FROM @ChargeUnits ORDER BY Id DESC))
		BREAK
	ELSE
		BEGIN
			SET @PresentCharge = (SELECT TOP(1) Id FROM @ChargeUnits WHERE  Id > @PresentCharge ORDER BY Id ASC)
			IF(@PresentCharge IS NULL) break;
				Continue
		END
	END

	RETURN @TotalAmount
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustPresentReadingByReadDate]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 03-03-2015
-- Description:	The purpose of this function is to get customer
--				 Present Reading based on meter billing read date 				
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustPresentReadingByReadDate]
(
	@GlobalAccountNumber VARCHAR(50)
	,@ReadDate VARCHAR(20)
	,@MeterNumber VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	
	DECLARE @PresentReading VARCHAR(50)
		
	SET @PresentReading=(SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR 
									WHERE CR.GlobalAccountNumber  COLLATE DATABASE_DEFAULT =@GlobalAccountNumber
									AND CONVERT(DATE,CR.ReadDate) = CONVERT(DATE, @ReadDate) 
												 AND CR.MeterNumber=@MeterNumber
												 order by CustomerReadingId desc)
					  
	RETURN @PresentReading
	
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerYear_OpeningBalance]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author		: Suresh Kumar Dasi  
-- Create date	: 27 Sept 2014  
-- Description	: The purpose of this function is to get the total Pending Amonunt  
-- select * FROM dbo.fn_GetCustomerYear_OpeningBalance('BEDC_CAN_0042')
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerYear_OpeningBalance]
(  
@AccountNo VARCHAR(50)  
)  
RETURNS @ResultTable TABLE(OpeningDate DATE,TotalPendingAmount DECIMAL(18,2)) 
AS  
BEGIN   
 DECLARE @TotalPendingAmount DECIMAL(18,2)  
		 ,@TotalArrears DECIMAL(18,2)  
		 ,@TotalPaid DECIMAL(18,2)  
		 ,@AdvancedPaid DECIMAL(18,2)  
		 ,@DueStatus VARCHAR(MAX)
        ,@PresentYear INT
		,@PresentMonth INT
		,@Date DATETIME     
		,@CurrentDate DATETIME = (SELECT dbo.fn_GetCurrentDateTime())	   
		 
            
 SELECT @DueStatus = dbo.fn_BillDueStatus()
  
 SELECT  @PresentMonth = 01,@PresentYear =  DATEPART(YEAR,@CurrentDate)
	
 SET @Date = CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-01'    
 SELECT @DueStatus = dbo.fn_BillDueStatus()

IF(CONVERT(DATE,@Date) > CONVERT(DATE,dbo.fn_GetCurrentDateTime()))
	BEGIN
		SELECT @Date = dbo.fn_GetCurrentDateTime()
	END

 SELECT @TotalArrears = SUM(ISNULL(TotalBillAmountWithTax,0) - ISNULL(AdjustmentAmmount,0)) FROM Tbl_CustomerBills   
 Where PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,','))  
 AND AccountNo = @AccountNo 
 AND CONVERT(DATE,CreatedDate) <= CONVERT(DATE,@Date)
     
  SELECT 
	@TotalPaid = SUM(BP.PaidAmount) 
 FROM Tbl_CustomerPayments BP
 WHERE CustomerPaymentID IN (SELECT BP.CustomerPaymentId
							 FROM Tbl_CustomerBillPayments BP
							 JOIN Tbl_CustomerBills CB ON BP.BillNo = CB.BillNo AND AccountNo = @AccountNo
							 WHERE PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,',')) 
							)
       AND CONVERT(DATE,RecievedDate) <= CONVERT(DATE,@Date)
  
  SELECT @AdvancedPaid  = SUM(payments .PaidAmount) FROM Tbl_CustomerPayments payments WHERE AccountNo = @AccountNo 
  AND CustomerPaymentID NOT IN (SELECT BP.CustomerPaymentId
								 FROM Tbl_CustomerBillPayments BP
								 JOIN Tbl_CustomerBills CB ON BP.BillNo = CB.BillNo AND AccountNo = @AccountNo
								 WHERE PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,',')) 
								)
						 AND CONVERT(DATE,RecievedDate) <= CONVERT(DATE,@Date)
  	
       
 SET @TotalPendingAmount = (ISNULL(@TotalPaid,0)+ ISNULL(@AdvancedPaid,0)) - ISNULL(@TotalArrears,0) 
	
INSERT INTO @ResultTable(OpeningDate,TotalPendingAmount)
VALUES(CONVERT(DATE,@Date),@TotalPendingAmount)	
		    
	    
 RETURN   
  
END  
-----------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerTotalPaidAmount]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 23 Aug 2014  
-- Description: The purpose of this function is to get the total Paid Amonunt of a customer
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerTotalPaidAmount]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN   
 DECLARE @TotalPaidAmount DECIMAL(18,2)
 
	SELECT 
		@TotalPaidAmount = SUM(CP.PaidAmount) 
	  FROM Tbl_CustomerPayments CP
	  WHERE AccountNo = @AccountNo
	  
 RETURN @TotalPaidAmount  
  
END  
-----------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerTotalDueAmount]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author:  Suresh Kumar Dasi  
-- Create date: 12 Apr 2014  
-- Description: The purpose of this function is to get the total Pending Amonunt  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerTotalDueAmount]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN   
 DECLARE @TotalPendingAmount DECIMAL(18,2)  
		 ,@TotalArrears DECIMAL(18,2)  
		 ,@TotalPaid DECIMAL(18,2)  
		 ,@AdvancedPaid DECIMAL(18,2)  
		 ,@DueStatus VARCHAR(MAX)
            
            
 SELECT @DueStatus = dbo.fn_BillDueStatus()
  
 --SELECT @TotalArrears = SUM(ISNULL(TotalBillAmountWithArrears,0)) FROM Tbl_CustomerBills   
 --Where BillStatusId IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,','))  
 --AND AccountNo = @AccountNo 
 
  SELECT @TotalArrears = SUM(ISNULL(TotalBillAmountWithTax,0) - ISNULL(AdjustmentAmmount,0)) FROM Tbl_CustomerBills   
 Where PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,','))  
 AND AccountNo = @AccountNo --Raja-ID065
    
 --SELECT @TotalArrears = ISNULL((SELECT TOP(1) TotalBillAmountWithArrears FROM Tbl_CustomerBills WHERE AccountNo=@AccountNo ORDER BY CustomerBillId DESC),0) --Comment By Raja-ID065
    
 SELECT @TotalPaid = SUM(BP.PaidAmount) FROM Tbl_CustomerBillPayments BP
 WHERE BillNo IN (SELECT BillNo FROM Tbl_CustomerBills   
					 Where PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,','))  
					 AND AccountNo = @AccountNo)
					 
SELECT @AdvancedPaid  = SUM(payments .PaidAmount) FROM Tbl_CustomerPayments payments WHERE AccountNo = @AccountNo 
AND CustomerPaymentID NOT IN ( SELECT CustomerPaymentID FROM Tbl_CustomerBillPayments BP
								WHERE BillNo IN (SELECT BillNo FROM Tbl_CustomerBills   
													Where PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,','))  
													AND AccountNo = @AccountNo)
							 )
    
 SET @TotalPendingAmount = ISNULL(@TotalArrears,0) - (ISNULL(@TotalPaid,0)+ ISNULL(@AdvancedPaid,0)) 
    
 RETURN @TotalPendingAmount  
  
END  
-----------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerTotalBillsAmount]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author:  Bhimaraju Vanka
-- Create date: 23 Aug 2014  
-- Description: The purpose of this function is to get the total Bills Amonunt of a customer
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerTotalBillsAmount]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN   
 DECLARE @TotalBillsAmount DECIMAL(18,2)
 
	SELECT 
			@TotalBillsAmount = SUM(ISNULL(TotalBillAmountWithTax,0) - ISNULL(AdjustmentAmmount,0)) 
	  FROM Tbl_CustomerBills   
	  Where AccountNo = @AccountNo
	  
 RETURN @TotalBillsAmount  
  
END  
-----------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerPreviousReadDate]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author   : Suresh Kumar Dasi  
-- Create date: 12-08-2014  
-- Description: To Customer previous reading
--SELECT dbo.[fn_GetCustomerPreviousReadDate]('BEDC_CAN_0015',2014,8)  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerPreviousReadDate]
(  
 @AccountNo VARCHAR(50)  
 ,@Year INT
 ,@Month INT
)  
RETURNS VARCHAR(20)  
AS  
BEGIN  
 DECLARE @PreviousReadDate VARCHAR(20)
		 ,@MonthStartDate  VARCHAR(20)
		 ,@Date  VARCHAR(20)
		 ,@ReadDate DATETIME
			
   SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
   SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   
   
   IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo AND IsBilled = 1)
		BEGIN
			SELECT TOP(1)@ReadDate = ReadDate FROM Tbl_CustomerReadings 
			WHERE GlobalAccountNumber = @AccountNo
			AND CONVERT(DATE,ReadDate) < CONVERT(DATE,@Date)
			ORDER BY ReadDate DESC
			SELECT @PreviousReadDate = CONVERT(varchar,@ReadDate,103)
										--+ ' '
										--+ LEFT(CONVERT(varchar,@ReadDate,108),5)
		END
	ELSE
		BEGIN
			SELECT @PreviousReadDate = ''
		END
	--CONVERT(varchar,CreatedDate,103)
 --    + ' '
 --    +LEFT(CONVERT(varchar,CreatedDate,108),5)

	
 RETURN @PreviousReadDate 
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetDistrictMessages_ByBu_Id]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 22 MAY 2014
-- Description:	The purpose of this function is to get the District messages Based on BU_ID
-- =============================================

  
ALTER FUNCTION [dbo].[fn_GetDistrictMessages_ByBu_Id]
(
@BU_ID VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Message VARCHAR(MAX)
			
	
		SELECT @Message = SUBSTRING((SELECT ','+Message+'<\n>'
							  FROM Tbl_GlobalMessages 
							  WHERE BU_ID = @BU_ID
								AND ActiveStatusId = 1
							FOR XML PATH('')),2,200000)			
	RETURN @Message 
						   
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerHighConsumption]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Suresh Kumar Dasi  
-- Create date: 23 August 2014  
-- Description: The Purpose of this function is to get the Customer's Hieghest Consumption.  
-- Modified On: 3-Sep-2014  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerHighConsumption]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,0)  
AS  
BEGIN  
 DECLARE @HighestConsumption DECIMAL(18,0)  
  
   
   
 IF EXISTS (SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo)   
   SELECT @HighestConsumption = MAX(Usage) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo  
 ELSE  
   SELECT @HighestConsumption = MAX(Usage) FROM Tbl_CustomerBills WHERE AccountNo = @AccountNo  
     
 RETURN @HighestConsumption  
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustBillNoByReadDate]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 13-10-2014
-- Description:	The purpose of this function is to get customer
--				 bill no based on meter billing read date 				
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustBillNoByReadDate]
(
	@AccountNo VARCHAR(50)
	,@ReadDate VARCHAR(20)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @BillNo VARCHAR(50)=''

	SET @BillNo=(SELECT TOP(1) BillNo FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber = @AccountNo AND   
							 CONVERT(DATE,CR.ReadDate)<=CONVERT(DATE, @ReadDate) ORDER BY CR.ReadDate DESC)
	
	RETURN @BillNo

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCurrentReading_ByAdjustments]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar D
-- Create date: 04-08-2014
-- Description:	The purpose of this function is to get Adjusted Readings
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCurrentReading_ByAdjustments]
(
	@BillNo VARCHAR(50)
	,@PrevCurrentReading VARCHAR(20)
)
RETURNS VARCHAR(20)
AS
BEGIN
	DECLARE @AdjustedCurentReading VARCHAR(20)
			,@CustomerBillId INT
			
	IF(@BillNo IS NOT NULL)
		BEGIN
			SELECT @CustomerBillId = CustomerBillId FROM Tbl_CustomerBills WHERE BillNo = @BillNo
			SELECT 
				@AdjustedCurentReading = CurrentReadingAfterAdjustment 
			FROM Tbl_BillAdjustments BA 
			JOIN Tbl_BillAdjustmentDetails AD ON BA.BillAdjustmentId = AD.BillAdjustmentId
			WHERE CustomerBillId = @CustomerBillId
			
			IF(@AdjustedCurentReading IS NOT NULL)
				BEGIN
					SET @PrevCurrentReading  = @AdjustedCurentReading 
				END
		END	

	RETURN @PrevCurrentReading 

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillPendings]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 12 Apr 2014
-- Description:	The purpose of this function is to get the Pending A\C numbers
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerBillPendings]
(
@AccountNo VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @PendingCount INT
            ,@DueStatus VARCHAR(MAX)
            
	SELECT @DueStatus = dbo.fn_BillDueStatus()
	
	SELECT @PendingCount = COUNT(0) FROM Tbl_CustomerBills 
	Where PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,','))
	AND AccountNo = @AccountNo	
		
	RETURN @PendingCount

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerAverageReading_Updatting_New]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author   : Suresh Kumar Dasi  
-- Create date: 06-05-2014  
-- Description: To Customer Average Reading Calculations Based on Reading Date  
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0005',25)  
-- =============================================  
CREATE FUNCTION [dbo].[fn_GetCustomerAverageReading_Updatting_New]  
(  
 @GlobalAccountNumber VARCHAR(50)  
 ,@Usage NUMERIC  
 
)  
RETURNS VARCHAR(50)  
AS  
BEGIN  
  
  Declare @PreviousAverageUsage decimal(18,2)
  Declare @LatestAverage decimal(18,2) 
 

;WITH OrderedCinemas AS
(
   SELECT	TOP 2
       AverageReading, Usage, 
       ROW_NUMBER() OVER(ORDER BY CustomerReadingId DESC) AS 'RowNum'
   FROM Tbl_CustomerReadings
   where GlobalAccountNumber='0000057200'
)
SELECT 
   @PreviousAverageUsage=AverageReading 
FROM OrderedCinemas
WHERE RowNum = 2


if @PreviousAverageUsage IS NULL

BEGIN
  SET @LatestAverage=@Usage
   
END

ELSE
BEGIN
SET @LatestAverage=	 (@PreviousAverageUsage+100)/2

END

 

     
 RETURN @LatestAverage  
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerOverpayAmount]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author:  Suresh Kumar Dasi  
-- Create date: 12 Apr 2014  
-- Description: The purpose of this function is to get the total Pending Amonunt  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerOverpayAmount]
(  
@AccountNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN   
 DECLARE @TotalPendingAmount DECIMAL(18,2)  
		 ,@TotalBillAmount DECIMAL(18,2)  
		 ,@TotalPaid DECIMAL(18,2)  
		 ,@DueStatus VARCHAR(MAX)
            
            
 SELECT @DueStatus = dbo.fn_BillDueStatus()
  
	  SELECT 
			@TotalBillAmount = SUM(ISNULL(TotalBillAmountWithTax,0) - ISNULL(AdjustmentAmmount,0)) 
	  FROM Tbl_CustomerBills   
	  Where AccountNo = @AccountNo
		  
	  SELECT 
		@TotalPaid = SUM(CP.PaidAmount) 
	  FROM Tbl_CustomerPayments CP
	  WHERE AccountNo = @AccountNo
	  
	 -- SELECT 
		--@TotalPaid = SUM(BP.PaidAmount) 
	 -- FROM Tbl_CustomerBillPayments BP
	 -- WHERE BillNo IN (SELECT BillNo FROM Tbl_CustomerBills   
		--					 Where AccountNo = @AccountNo)
    
    SET @TotalPendingAmount =  ISNULL(@TotalPaid,0) - ISNULL(@TotalBillAmount,0) 
    
 RETURN (CASE WHEN @TotalPendingAmount > 0 THEN @TotalPendingAmount ELSE 0 END)  
  
END  
-----------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerOpeningBalance]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author		: Suresh Kumar Dasi  
-- Create date	: 27 Sept 2014  
-- Description	: The purpose of this function is to get the total Pending Amonunt  
-- select dbo.fn_GetCustomerOpeningBalance('BEDC_CAN_0042',0)
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerOpeningBalance]
(  
@AccountNo VARCHAR(50)  
,@ReportMonths INT
)  
RETURNS @ResultTable TABLE(OpeningDate DATE,TotalPendingAmount DECIMAL(18,2)) 
AS  
BEGIN   
 DECLARE @TotalPendingAmount DECIMAL(18,2)  
		 ,@TotalArrears DECIMAL(18,2)  
		 ,@TotalPaid DECIMAL(18,2)  
		 ,@AdvancedPaid DECIMAL(18,2)  
		 ,@DueStatus VARCHAR(MAX)
        ,@PresentYear INT
		,@PresentMonth INT
		,@Date DATETIME     
		,@MonthStartDate DATETIME 
		,@CurrentDate DATETIME = (SELECT dbo.fn_GetCurrentDateTime())	   
		 
            
 SELECT @DueStatus = dbo.fn_BillDueStatus()
  
 SELECT  @PresentMonth = DATEPART(MONTH,DATEADD(MONTH,(-1*@ReportMonths),@CurrentDate)),@PresentYear =  DATEPART(YEAR,DATEADD(MONTH,(-1*@ReportMonths),@CurrentDate))
	
 SET @MonthStartDate = CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-01'    
 SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@PresentYear)+'-'+CONVERT(VARCHAR(20),@PresentMonth)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   
 SELECT @DueStatus = dbo.fn_BillDueStatus()

IF(CONVERT(DATE,@Date) > CONVERT(DATE,dbo.fn_GetCurrentDateTime()))
	BEGIN
		SELECT @Date = dbo.fn_GetCurrentDateTime()
	END

 SELECT @TotalArrears = SUM(ISNULL(TotalBillAmountWithTax,0) - ISNULL(AdjustmentAmmount,0)) FROM Tbl_CustomerBills   
 Where PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,','))  
 AND AccountNo = @AccountNo 
 AND CONVERT(DATE,CreatedDate) <= CONVERT(DATE,@Date)
     
  SELECT 
	@TotalPaid = SUM(BP.PaidAmount) 
 FROM Tbl_CustomerPayments BP
 WHERE CustomerPaymentID IN (SELECT BP.CustomerPaymentId
							 FROM Tbl_CustomerBillPayments BP
							 JOIN Tbl_CustomerBills CB ON BP.BillNo = CB.BillNo AND AccountNo = @AccountNo
							 WHERE PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,',')) 
							)
       AND CONVERT(DATE,RecievedDate) <= CONVERT(DATE,@Date)
  
  SELECT @AdvancedPaid  = SUM(payments .PaidAmount) FROM Tbl_CustomerPayments payments WHERE AccountNo = @AccountNo 
  AND CustomerPaymentID NOT IN (SELECT BP.CustomerPaymentId
								 FROM Tbl_CustomerBillPayments BP
								 JOIN Tbl_CustomerBills CB ON BP.BillNo = CB.BillNo AND AccountNo = @AccountNo
								 WHERE PaymentStatusID IN (SELECT [com] FROM dbo.fn_Split(@DueStatus,',')) 
								)
						 AND CONVERT(DATE,RecievedDate) <= CONVERT(DATE,@Date)
  	
       
 SET @TotalPendingAmount = (ISNULL(@TotalPaid,0)+ ISNULL(@AdvancedPaid,0)) - ISNULL(@TotalArrears,0) 
	
INSERT INTO @ResultTable(OpeningDate,TotalPendingAmount)
VALUES(CONVERT(DATE,@Date),@TotalPendingAmount)	
		    
	    
 RETURN   
  
END  
-----------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastPaymentDate]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Suresh Kumar Dasi  
-- Create date: 12 Apr 2014  
-- Description: The purpose of this function is to get the Last Payment Date  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerLastPaymentDate]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS VARCHAR(20)  
AS  
BEGIN  
 DECLARE @LastPaymentDate VARCHAR(20)  
  
 -- SELECT TOP(1) @LastPaymentDate = CONVERT(VARCHAR(20),RecievedDate,103) FROM Tbl_CustomerPayments -- comented by Faiz-ID103
 SELECT TOP(1) @LastPaymentDate = CONVERT(VARCHAR(20),RecievedDate,106) FROM Tbl_CustomerPayments   
 Where AccountNo = @AccountNo   
 ORDER BY RecievedDate DESC   
 --ORDER BY CreatedDate DESC-- Raja -ID065  
    
 RETURN @LastPaymentDate   
  
END  
  
-----------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastPaidAmount]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Suresh Kumar Dasi  
-- Create date: 12 Apr 2014  
-- Description: The purpose of this function is to get the Last Paid Amount  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerLastPaidAmount]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
 DECLARE @LastPaymentDate VARCHAR(20)  
   ,@LastPaidAmount DECIMAL(18,2)  
  
 SELECT TOP(1) --@LastPaymentDate = CONVERT(VARCHAR(20),RecievedDate,106),  
 @LastPaidAmount = PaidAmount FROM Tbl_CustomerPayments   
 Where AccountNo = @AccountNo   
 ORDER BY CustomerPaymentID DESC   
 --ORDER BY CreatedDate DESC --Raja-ID065  
  
 SET @LastPaidAmount = ISNULL(@LastPaidAmount,0)  
      
 RETURN @LastPaidAmount   
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillGeneratedDate]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 12 Apr 2014
-- Modified By: T.Karthik
-- Modified date: 10-11-2014
-- Description:	The purpose of this function is to get the Last Bill Generated Date
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerLastBillGeneratedDate]
(
@AccountNo VARCHAR(50)
)
RETURNS VARCHAR(20)
AS
BEGIN
	DECLARE @LastBullGeneratedDate VARCHAR(20)

	SELECT TOP(1) @LastBullGeneratedDate  = CONVERT(VARCHAR(20),BillGeneratedDate,106) FROM Tbl_CustomerBills
	Where AccountNo = @AccountNo	
	ORDER BY CustomerBillId DESC
		
	RETURN @LastBullGeneratedDate  

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillAmountWithTax]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author   : M.Padmini  
-- Create date: 20-Feb-15
-- Description: This function is used to get customer previousbillamountwithtax.
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerLastBillAmountWithTax]  
(  
@GlobalAccountNo VARCHAR(50)
,@BillMonth INT
,@BillYear  INT
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
 DECLARE   
   @BillAmountWithTax DECIMAL(18,2)
  ,@MaxCustomerBillId INT 
  --,@PresDate DATE
  --,@PrevDate DATE
  
  --      SET @PresDate= CONVERT(DATE,CONVERT(VARCHAR(20),@BillYear)+'-'+CONVERT(VARCHAR(20),@BillMonth)+'-01') 
		--SET @PrevDate= CONVERT(DATE,CONVERT(VARCHAR(20),@BillYear)+'-'+CONVERT(VARCHAR(20),@BillMonth)+'-01') 
		
	 --================ Getting 2nd heighest CustomerBillId===================
	 SELECT  TOP 1 @MaxCustomerBillId= MAX(CustomerBillId)
	 FROM Tbl_CustomerBills   
	 WHERE AccountNo =@GlobalAccountNo 
	 AND CustomerBillId <(SELECT MAX(CustomerBillId) FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo)
	 GROUP BY CustomerBillId
	 ORDER BY CustomerBillId DESC 
	 
	 --================= Getting Previous Bill Amount===============
	 
	 SELECT @BillAmountWithTax= TotalBillAmountWithTax FROM Tbl_CustomerBills
	 WHERE CustomerBillId=@MaxCustomerBillId
	 AND AccountNo=@GlobalAccountNo
	 --AND BillMonth<DATEPART(MONTH, DATEADD(MONTH,+1,CONVERT(DATE, '2014-'+convert(VARCHAR(2),@BillMonth)+'-01'))) 
	 --AND BillYear=@BillYear
	 
 SET @BillAmountWithTax = ISNULL(@BillAmountWithTax,0) 
  
      
 RETURN @BillAmountWithTax   
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillAmountWithoutArrears]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  T.Karthik  
-- Create date: 10-11-2014  
-- Description: The purpose of this function is to get the Last Bill Amount Without Arrears 
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerLastBillAmountWithoutArrears]  
(  
@AccountNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
 DECLARE   
   @BillAmountWithOutArrears DECIMAL(18,2)  
  
 SELECT TOP(1) --@LastPaymentDate = CONVERT(VARCHAR(20),RecievedDate,106),  
 @BillAmountWithOutArrears = TotalBillAmount FROM Tbl_CustomerBills   
 Where AccountNo = @AccountNo   
 ORDER BY CustomerBillId DESC   
 --ORDER BY CreatedDate DESC --Raja-ID065  
  
 SET @BillAmountWithOutArrears = ISNULL(@BillAmountWithOutArrears,0)  
      
 RETURN @BillAmountWithOutArrears   
  
END
GO
/****** Object:  UserDefinedFunction [MASTERS].[fn_CountryCodeGenerate]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 11-DEC-2014
-- Description:	GENERATE COUNTRY CODE
-- =============================================
ALTER FUNCTION [MASTERS].[fn_CountryCodeGenerate]()
RETURNS VARCHAR(10)
AS
BEGIN
	
DECLARE @CountryCode INT
		,@CountryCodeNew VARCHAR(10)
		,@LengthGlobal INT =2
		,@LengthCurrent INT
		
	--GET CURRENT Service Center Code		
	SET @CountryCodeNew= (SELECT TOP 1 CountryCode 
									FROM Tbl_Countries 
									ORDER BY CountryId DESC)							
	--INCREMENT Service Center Code BY 1
	--IF(@CountryCodeNew IS NOT NULL)
	SET @CountryCode =CONVERT(INT,ISNULL(@CountryCodeNew,0)) + 1
	--ELSE 
	--	SET @CountryCode=0;

	SET @CountryCodeNew=@CountryCode
	
	--GET LEGHT OF CURRENT Service Center Code
	SET @LengthCurrent=LEN(@CountryCodeNew) 
	
	--NEW Service Center Code CAN BE GENERATE OR NOT
	IF(@LengthCurrent<=@LengthGlobal)
	BEGIN
		--IF Service Center Code
		WHILE(@LengthCurrent < @LengthGlobal)
		BEGIN
			SET @CountryCodeNew= {fn CONCAT('0',@CountryCodeNew)}
			SET @LengthCurrent=@LengthCurrent+1;
		END
	END
	ELSE
	BEGIN
		SET @CountryCodeNew=NULL
	END
	
	RETURN @CountryCodeNew
END
--====================================================================================================
GO
/****** Object:  UserDefinedFunction [dbo].[fn_Get_Customer_LastBill_TotalPayment]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER FUNCTION [dbo].[fn_Get_Customer_LastBill_TotalPayment]
(
	 @GlobalAccountNumber Varchar(50)
	,@PrevBillID INT
	,@PrevBillDate Datetime
)
RETURNS   decimal(18,2)
AS
BEGIN
	
	Declare @PrevBillTotalPaidAmount Decimal(18,2) 
	If @PrevBillID IS NULL or @PrevBillID=0
		BEGIN
			--Print 'No Bills For Customer'
			
			   select @PrevBillTotalPaidAmount=SUM(PaidAmount) from Tbl_CustomerPayments
			   where RecievedDate >= (select MAX(LastBillGeneratedDate) 
									  from Tbl_LastBillGeneratedDate
									  ) and AccountNo=@GlobalAccountNumber
			    
		END
	ELSE
		Begin
			--Print 'Bills Avaiable'
			select @PrevBillTotalPaidAmount=SUM(PaidAmount) from Tbl_CustomerPayments 
			where RecievedDate >= @PrevBillDate and AccountNo=@GlobalAccountNumber
		END
		 
	if @PrevBillTotalPaidAmount IS NULL 
		SET @PrevBillTotalPaidAmount=0
	RETURN @PrevBillTotalPaidAmount

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading_New]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
CREATE FUNCTION  [dbo].[fn_GetAverageReading_New]
(
 @AccountNo VARCHAR(50)
 ,@usage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	 
		 
 
 
Declare @NewAverage decimal(18,2 )

SELECT	Top 1    @NewAverage=AverageReading  
       
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
	 order by CustomerReadingId DESC

 ;WITH OrderedCinemas AS
(
   SELECT	Top 2
         AverageReading ,
       ROW_NUMBER() OVER(ORDER BY CustomerReadingId DESC) AS 'RowNum'
   FROM Tbl_CustomerReadings  where GlobalAccountNumber=@AccountNo
)
SELECT 
     @NewAverage=AverageReading
FROM OrderedCinemas
WHERE RowNum = 2

IF	@NewAverage IS NULL
SET @NewAverage =@usage
ELSE

SET		 @NewAverage = (@NewAverage+@usage)/2
			
	RETURN @NewAverage

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageReading]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 06-05-2014
-- Description:	To Customer Average Reading Calculations
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0034',25)
-- =============================================
ALTER FUNCTION [dbo].[fn_GetAverageReading]
(
 @AccountNo VARCHAR(50)
 ,@CurrentUsage NUMERIC
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @FinalReading VARCHAR(50)
			,@ReadingsCount INT
			
	SELECT @ReadingsCount = (COUNT(0)+ 1) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo
	
    SET @FinalReading = CONVERT(VARCHAR(50),CONVERT(DECIMAL(18,2),((SELECT (CASE WHEN SUM(Usage) IS NULL THEN 0 ELSE SUM(Usage) END)
						 FROM Tbl_CustomerReadings WHERE AccountNumber = @AccountNo)+@CurrentUsage)
						/@ReadingsCount))

			
	RETURN @FinalReading

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetBillServiceTime]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Suresh Kumar Dasi  
-- Create date: 22-05-2014  
-- Description: to Get the Bill Service Start date  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetBillServiceTime]()  
RETURNS DATETIME  
AS  
BEGIN  
 DECLARE @ResultDate DATETIME = DATEADD(MINUTE,5,(SELECT dbo.fn_GetCurrentDateTime()))  
   ,@TopServiceStartDate DATETIME   
   
 SELECT TOP(1) @TopServiceStartDate = ServiceStartDate from Tbl_BillingQueueSchedule ORDER BY ServiceStartDate DESC  
   
 IF(@TopServiceStartDate > @ResultDate)  
  BEGIN  
   SET @ResultDate = DATEADD(MINUTE,5,@TopServiceStartDate)  
  END  
    
 RETURN @ResultDate   
   
END  
---------------------------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetBillPayments]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 12 Apr 2014
-- Description:	The purpose of this function is to get the total payment for a bill
-- =============================================
ALTER FUNCTION [dbo].[fn_GetBillPayments]
(
@CustomerBillId VARCHAR(50)
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	DECLARE @TotalPayment DECIMAL(18,2)

	SELECT	@TotalPayment = Convert(Decimal(18,2),SUM(PaidAmount))
	FROM	TBL_CustomerBillPayments
	WHERE	BillNo = @CustomerBillId			
	RETURN	@TotalPayment 

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetBillPaymentDetails]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 09 JUL 2014
-- Description:	The purpose of this function is to Generated Bills payments based on paid amount
-- Modified By: Neeraj Kanojiya
-- Modified on: 21-July-2014
--=============================================
ALTER FUNCTION [dbo].[fn_GetBillPaymentDetails]
(
@AccountNo VARCHAR(50)
,@TotalPaidAmount DECIMAL(18,2)
)
RETURNS @ResultedTable TABLE(AccountNo VARCHAR(50),AmountPaid DECIMAL(18,2),          
					BillNo VARCHAR(50),BillStatus INT)
AS
BEGIN

	DECLARE @BillsTable TABLE(BillNo VARCHAR(50),BillAmount DECIMAL(18,2))--Declaring temp bill table
	DECLARE @PresentBill VARCHAR(50)
			,@RemainingAmount DECIMAL(18,2)
			,@BillAmount DECIMAL(18,2)
			
			--Inserting details in temp bill table
	INSERT INTO @BillsTable 
	SELECT BillNo ,(TotalBillAmountWithTax - ISNULL(AdjustmentAmmount,0) - (SELECT ISNULL(SUM(PaidAmount),0) from Tbl_CustomerBillPayments CP WHERE CP.BillNo = CB.BillNo))  
	FROM Tbl_CustomerBills CB WHERE PaymentStatusID = 2 AND AccountNo = @AccountNo
	
	
	SET @RemainingAmount = @TotalPaidAmount

	--Selecting first bill from temp bill table for payment.
	SELECT TOP(1) @PresentBill = BillNo FROM @BillsTable ORDER BY BillNo ASC  
	
	
	WHILE(EXISTS(SELECT TOP(1) BillNo FROM @BillsTable WHERE BillNo >= @PresentBill ORDER BY BillNo ASC))  --Is any bill left in the list comparing present bill.
		BEGIN 					 
			SELECT @BillAmount = BillAmount FROM @BillsTable WHERE BillNo = @PresentBill
			IF(@RemainingAmount >= @BillAmount)
				BEGIN
					IF NOT EXISTS(SELECT 0 FROM @BillsTable WHERE BillNo > @PresentBill)
						BEGIN
							INSERT INTO @ResultedTable(AccountNo,AmountPaid,BillNo,BillStatus)	
							VALUES(@AccountNo,@RemainingAmount,@PresentBill,1)
						END
					ELSE 
						BEGIN
							SET @RemainingAmount = @RemainingAmount - @BillAmount							
							INSERT INTO @ResultedTable(AccountNo,AmountPaid,BillNo,BillStatus)	
							VALUES(@AccountNo,@BillAmount,@PresentBill,1)							
						END 						
				END
			ELSE 
				BEGIN
					INSERT INTO @ResultedTable(AccountNo,AmountPaid,BillNo,BillStatus)	
					VALUES(@AccountNo,@RemainingAmount,@PresentBill,2) 
					SET @RemainingAmount = 0									
				END
				
			IF(@PresentBill = (SELECT TOP(1) BillNo FROM @BillsTable ORDER BY BillNo DESC))  --Setting next bill in the list as present bill.
				BREAK  
			ELSE  
				BEGIN  
					 SET @PresentBill = (SELECT TOP(1) BillNo FROM @BillsTable WHERE  BillNo > @PresentBill ORDER BY BillNo ASC)  
					 IF(@PresentBill IS NULL) break;  
					  Continue  
					END  
		END  	 	
		
	RETURN 

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetBillingMonthTotalAmount]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author:  Suresh Kumar Dasi  
-- Create date: 08 Jul 2014  
-- Description: The purpose of this function is to get the total Pending Amonunt  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetBillingMonthTotalAmount]
(  
@Year INT
,@Month INT
,@BillingQueueScheduleId INT
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
 DECLARE @TotalAmount DECIMAL(18,2)  
		
      SELECT @TotalAmount  = SUM(TotalBillAmountWithTax) FROM Tbl_CustomerBills 
      WHERE BillMonth = @Month AND BillYear = @Year 
      AND AccountNo IN (SELECT AccountNo FROM Tbl_BillingQueeCustomers WHERE BillingQueuescheduleId = @BillingQueueScheduleId)     
     
    
 RETURN @TotalAmount  
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCurrentBillSetting]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: Suresh Kumar Dasu      
-- Create date  : 23 AUG 2014                
-- Description  : THIS Function is used for getting currentbill rule
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetCurrentBillSetting]
(
@BillingMonth INT 
,@BillingYear INT 
,@CycleId VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @CurrentBillSetting INT = NULL
	
	SELECT 
		TOP (1) @CurrentBillSetting = BillingRule 
	FROM Tbl_EstimationSettings	
	WHERE BillingMonth = @BillingMonth 
	AND BillingYear = @BillingYear AND CycleId = @CycleId
	AND ActiveStatusId = 1
	
	IF(@CurrentBillSetting = NULL)
		BEGIN
			SET @CurrentBillSetting = 3
		END	
	
	RETURN @CurrentBillSetting
	
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCurrentApprovalRoles]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 17 NOV 2014
-- Modified date : 19-11-2014
-- Description:	The purpose of this function is to Get approval roles
--=============================================
ALTER FUNCTION [dbo].[fn_GetCurrentApprovalRoles]
(
@FunctionId INT
,@CurrentLevel INT
,@IsForward BIT
)
RETURNS @ResultTemp TABLE(PresentRoleId INT,NextRoleId INT)
AS
BEGIN

	DECLARE @PresentRoleId INT=0
			,@NextRoleId INT=0
			
	DECLARE @Temp TABLE([Level] INT,FunctionId INT,RoleId INT)
	DELETE FROM @ResultTemp 
	DELETE FROM @Temp 

	IF(@IsForward = 1)
		BEGIN
			INSERT INTO  @Temp 
			SELECT 
				TOP 2 [Level],FunctionId,RoleId
			FROM TBL_FunctionApprovalRole 
			WHERE FunctionId = @FunctionId
			AND [Level] > @CurrentLevel
			ORDER BY [Level] ASC


			SELECT TOP(1) @PresentRoleId = RoleId FROM @Temp ORDER BY [Level] ASC
			SELECT TOP(1) @NextRoleId = RoleId FROM @Temp ORDER BY [Level] DESC
		END
	ELSE
		BEGIN
			INSERT INTO  @Temp 
			SELECT 
				TOP 2 [Level],FunctionId,RoleId
			FROM TBL_FunctionApprovalRole 
			WHERE FunctionId = @FunctionId
			AND [Level] <= @CurrentLevel
			ORDER BY [Level] DESC


			SELECT TOP(1) @PresentRoleId = RoleId FROM @Temp ORDER BY [Level] ASC 
			SELECT TOP(1) @NextRoleId = RoleId FROM @Temp ORDER BY [Level] DESC
		END	

	INSERT INTO @ResultTemp
	SELECT @PresentRoleId,@NextRoleId 	
		
	RETURN 

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CheckNeighbourAccNoExists]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 25-03-2014
-- Description:	The purpose of this function is to check NeighbourAccount No Exists
-- =============================================
ALTER FUNCTION [dbo].[fn_CheckNeighbourAccNoExists]
(
	@NeighborAccountNo VARCHAR(50)
	,@CustomerUniqueNo VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	DECLARE @IsExists BIT=0
		IF(@CustomerUniqueNo='')
		BEGIN
			IF EXISTS(SELECT 0 FROM Tbl_CustomerDetails WHERE AccountNo=@NeighborAccountNo)
				SET @IsExists=1
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT 0 FROM Tbl_CustomerDetails WHERE AccountNo=@NeighborAccountNo AND CustomerUniqueNo!=@CustomerUniqueNo)
				SET @IsExists=1
		END
	RETURN @IsExists
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CheckIsPageAccess]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 02-May-2014
-- Description:	The purpose of this function is to check Page Access Or Not
-- =============================================
ALTER FUNCTION [dbo].[fn_CheckIsPageAccess]
(
			 @RoleId INT
			,@PageName VARCHAR(MAX)
)
RETURNS BIT
AS
BEGIN
	DECLARE @IsExists BIT=0
	
		IF EXISTS(	SELECT 
				-- P.MenuId 
				--,M.Name
				----,M.MenuId
				--,M.Path
				--,P.Role_Id
				--,P.IsActive
				TOP(1) P.IsActive
				
			FROM Tbl_NewPagePermissions P
			JOIN Tbl_Menus M ON M.MenuId=P.MenuId
			WHERE path like '%'+@PageName+'%' 
				AND P.IsActive=1 
				AND P.Role_Id=@RoleId
				AND M.IsActive=1)
				BEGIN
				SET @IsExists=1
				END
		ELSE
		BEGIN
		SET @IsExists=0
		END
				
	
	RETURN @IsExists

END
GO
/****** Object:  UserDefinedFunction [MASTERS].[fn_BusinessUnitCodeGenerate]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 22-DEC-2014
-- Description:	GENERATE BUSINESS CODE
-- ModifieedBy: Padmini
-- Modified Date:31-01-2015
-- =============================================
ALTER FUNCTION [MASTERS].[fn_BusinessUnitCodeGenerate]()
RETURNS VARCHAR(10)
AS
BEGIN
	
DECLARE @BUCODE INT
		,@BUCODENew VARCHAR(10)
		,@LengthGlobal INT =2
		,@LengthCurrent INT
		
	--GET CURRENT Business Unit Code		
	--SET @BUCODENew= (SELECT TOP 1 BUCode 
	--								FROM Tbl_BussinessUnits 
	--								ORDER BY IdentityBU_ID DESC)	
		
	--New Code (Logic Changed to accept BusinessUnit which is having zero)
	SET @BUCODENew= (SELECT TOP 1 MAX(BUCode) 
								FROM Tbl_BussinessUnits )
																				
	--INCREMENT Business Unit Code BY 1
	--IF(@BUCODENew IS NOT NULL)
	SET @BUCODE =CONVERT(INT,ISNULL(@BUCODENew,0)) + 1
	
	--ELSE 
	--SET @BUCODE=0;

	SET @BUCODENew=@BUCODE
	
	--GET LEGHT OF CURRENT Business Unit Code
	SET @LengthCurrent=LEN(@BUCODENew) 
	
	--NEW Business Unit Code CAN BE GENERATE OR NOT
	IF(@LengthCurrent<=@LengthGlobal)
	BEGIN
		--IF Business Unit Code
		WHILE(@LengthCurrent < @LengthGlobal)
		BEGIN
			SET @BUCODENew= {fn CONCAT('0',@BUCODENew)}
			SET @LengthCurrent=@LengthCurrent+1;
		END
	END
	ELSE
	BEGIN
		SET @BUCODENew=NULL
	END
	
	RETURN @BUCODENew
	--PRINT @BUCODENew
END
--========================================================================================--
GO
/****** Object:  UserDefinedFunction [dbo].[fn_BusinessUnitCodeGenerate]    Script Date: 04/07/2015 03:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 22-DEC-2014
-- Create date: 22-JAN-2014
-- =============================================
ALTER FUNCTION [dbo].[fn_BusinessUnitCodeGenerate]()
RETURNS VARCHAR(10)
AS
BEGIN
	
DECLARE @BUCODE INT
		,@BUCODENew VARCHAR(10)
		,@LengthGlobal INT =2
		,@LengthCurrent INT
		
	--GET CURRENT Business Unit Code		
	SET @BUCODENew= (SELECT TOP 1 BUCode 
									FROM Tbl_BussinessUnits 
									ORDER BY IdentityBU_ID DESC)						
	--INCREMENT Business Unit Code BY 1
	IF(@BUCODENew IS NOT NULL)
	SET @BUCODE =CONVERT(INT,ISNULL(@BUCODENew,0)) + 1
	
	ELSE 
	SET @BUCODE=0;

	SET @BUCODENew=@BUCODE
	
	--GET LEGHT OF CURRENT Business Unit Code
	SET @LengthCurrent=LEN(@BUCODENew) 
	
	--NEW Business Unit Code CAN BE GENERATE OR NOT
	IF(@LengthCurrent<=@LengthGlobal)
	BEGIN
		--IF Business Unit Code
		WHILE(@LengthCurrent < @LengthGlobal)
		BEGIN
			SET @BUCODENew= {fn CONCAT('0',@BUCODENew)}
			SET @LengthCurrent=@LengthCurrent+1;
		END
	END
	ELSE
	BEGIN
		SET @BUCODENew=NULL
	END
	
	RETURN @BUCODENew
	--PRINT @BUCODENew
END
GO
/****** Object:  UserDefinedFunction [MASTERS].[fn_StateCodeGenerate]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 11-DEC-2014
-- Description:	GENERATE STATE CODE
-- =============================================
ALTER FUNCTION [MASTERS].[fn_StateCodeGenerate]()
RETURNS VARCHAR(10)
AS
BEGIN
	
DECLARE @StateCode INT
		,@StateCodeNew VARCHAR(10)
		,@LengthGlobal INT =2
		,@LengthCurrent INT
		
	--GET CURRENT Service Center Code		
	SET @StateCodeNew= (SELECT TOP 1 StateCode 
									FROM Tbl_States 
									ORDER BY StateId DESC)							
	--INCREMENT Service Center Code BY 1
	--IF(@StateCodeNew IS NOT NULL)
	SET @StateCode =CONVERT(INT,ISNULL(@StateCodeNew,0)) + 1
	--ELSE 
	--	SET @StateCode=0;

	SET @StateCodeNew=@StateCode
	
	--GET LEGHT OF CURRENT Service Center Code
	SET @LengthCurrent=LEN(@StateCodeNew) 
	
	--NEW Service Center Code CAN BE GENERATE OR NOT
	IF(@LengthCurrent<=@LengthGlobal)
	BEGIN
		--IF Service Center Code
		WHILE(@LengthCurrent < @LengthGlobal)
		BEGIN
			SET @StateCodeNew= {fn CONCAT('0',@StateCodeNew)}
			SET @LengthCurrent=@LengthCurrent+1;
		END
	END
	ELSE
	BEGIN
		SET @StateCodeNew=NULL
	END
	
	RETURN @StateCodeNew
END
--====================================================================================================
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CaluculateCustomerBill]    Script Date: 04/07/2015 03:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 21-05-2014
-- Description:	The purpose of this function is to Calculate The Bill Amount
-- =============================================
ALTER FUNCTION [dbo].[fn_CaluculateCustomerBill]
(
@AccountNumner VARCHAR(50)
,@Month INT 
,@Year INT
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	DECLARE @Date VARCHAR(50) 
			,@PreviousReading DECIMAL(20)
			,@CurrentReading DECIMAL(20)
			,@Usage DECIMAL(20)
			,@RemaingUsage DECIMAL(20)
			,@TotalAmount DECIMAL(18,2)= 0
			,@PresentCharge INT
			,@FromUnit INT
			,@ToUnit INT
			,@Amount DECIMAL(18,2)
			
	SET @Date = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'
	DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)

	DELETE FROM @ChargeUnits 
	
	INSERT INTO @ChargeUnits 
	SELECT 
		ClassID,FromKW,ToKW,Amount,TaxId FROM Tbl_LEnergyClassCharges 
	WHERE ClassID IN (SELECT TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber = @AccountNumner)
	AND IsActive = 1 AND CONVERT(DATE,@Date) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
	ORDER BY FromKW ASC		
	
	SELECT TOP(1) @PreviousReading = PreviousReading FROM Tbl_CustomerReadings 
	WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0
	ORDER BY CONVERT(DECIMAL(18),PreviousReading) ASC

	SELECT TOP(1) @CurrentReading = PresentReading FROM Tbl_CustomerReadings 
	WHERE GlobalAccountNumber = @AccountNumner AND IsBilled = 0
	ORDER BY  CONVERT(DECIMAL(18),PresentReading) DESC

	SELECT @Usage = @CurrentReading - @PreviousReading
	SET @RemaingUsage = @Usage
	SET @TotalAmount = 0
			
	SELECT TOP(1) @PresentCharge = Id FROM @ChargeUnits ORDER BY Id ASC

	WHILE(EXISTS(SELECT TOP(1) Id FROM @ChargeUnits WHERE Id >= @PresentCharge ORDER BY Id ASC))
		BEGIN
		
		SELECT @FromUnit = FromKW,@ToUnit = ToKW,@Amount = Amount
		FROM @ChargeUnits
		WHERE Id = @PresentCharge
		
		IF(@ToUnit IS NOT NULL)
			BEGIN
				 SET @TotalAmount = @TotalAmount + ((@ToUnit - @FromUnit)+1)* @Amount	
				 SET @RemaingUsage = @RemaingUsage - ((@ToUnit - @FromUnit)+1)
			END
		ELSE
			BEGIN
				SET @TotalAmount = @TotalAmount + @RemaingUsage * @Amount	
				 SET @RemaingUsage = @RemaingUsage - @RemaingUsage
			END			
		
	IF(@PresentCharge = (SELECT TOP(1) Id FROM @ChargeUnits ORDER BY Id DESC))
		BREAK
	ELSE
		BEGIN
			SET @PresentCharge = (SELECT TOP(1) Id FROM @ChargeUnits WHERE  Id > @PresentCharge ORDER BY Id ASC)
			IF(@PresentCharge IS NULL) break;
				Continue
		END
	END

	RETURN @TotalAmount
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CaluculateBill_Consumption]    Script Date: 04/07/2015 03:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 05-06-2014
-- Description:	The purpose of this function is to Calculate The Bill Amount Based on Consumption
-- Modified By : Padmini
-- Modified Date : 26-12-2014  

-- =============================================
ALTER FUNCTION [dbo].[fn_CaluculateBill_Consumption]
(
@AccountNumner VARCHAR(50)
,@Consumption DECIMAL(18,2)
,@Month INT 
,@Year INT
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	DECLARE @Date VARCHAR(50) 
			,@RemaingUsage DECIMAL(20)
			,@TotalAmount DECIMAL(18,2)= 0
			,@PresentCharge INT
			,@FromUnit INT
			,@ToUnit INT
			,@Amount DECIMAL(18,2)
			
	SET @Date = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'
	DECLARE @ChargeUnits TABLE(Id INT IDENTITY(1,1),ClassId INT,FromKW INT,ToKW INT,Amount DECIMAL(18,2),TaxId INT)

	DELETE FROM @ChargeUnits 
	
	INSERT INTO @ChargeUnits 
	SELECT 
		ClassID,FromKW,ToKW,Amount,TaxId FROM Tbl_LEnergyClassCharges 
	WHERE ClassID IN (SELECT TariffClassID FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber = @AccountNumner)
	AND IsActive = 1 AND CONVERT(DATE,@Date) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate) 
	ORDER BY FromKW ASC		
	
	SET @RemaingUsage = @Consumption
	SET @TotalAmount = 0
			
	SELECT TOP(1) @PresentCharge = Id FROM @ChargeUnits ORDER BY Id ASC

	WHILE(EXISTS(SELECT TOP(1) Id FROM @ChargeUnits WHERE Id >= @PresentCharge ORDER BY Id ASC))
		BEGIN
		
		SELECT @FromUnit = FromKW,@ToUnit = ToKW,@Amount = Amount
		FROM @ChargeUnits
		WHERE Id = @PresentCharge
		
		IF(@ToUnit IS NOT NULL)
			BEGIN
				IF(@RemaingUsage > (@ToUnit - @FromUnit)+1)
					BEGIN					
						SET @TotalAmount = @TotalAmount + ((@ToUnit - @FromUnit)+1)* @Amount	
						SET @RemaingUsage = @RemaingUsage - ((@ToUnit - @FromUnit)+1)
					END	
				ELSE
					BEGIN
						SET @TotalAmount = @TotalAmount + @RemaingUsage * @Amount
						SET @RemaingUsage = 0
					END	
			END
		ELSE
			BEGIN
				SET @TotalAmount = @TotalAmount + @RemaingUsage * @Amount	
				 SET @RemaingUsage = @RemaingUsage - @RemaingUsage
			END			
		
	IF(@PresentCharge = (SELECT TOP(1) Id FROM @ChargeUnits ORDER BY Id DESC))
		BREAK
	ELSE
		BEGIN
			SET @PresentCharge = (SELECT TOP(1) Id FROM @ChargeUnits WHERE  Id > @PresentCharge ORDER BY Id ASC)
			IF(@PresentCharge IS NULL) break;
				Continue
		END
	END

	RETURN @TotalAmount
END
GO
/****** Object:  UserDefinedFunction [MASTERS].[fn_BookCodeGenerate]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 31-DEC-2014
-- Description:	GENERATE BOOK CODE
-- ModifieedBy: Padmini
-- Modified Date:31-01-2015
-- =============================================
ALTER FUNCTION [MASTERS].[fn_BookCodeGenerate]
 (
   @ServiceCenterId VARCHAR(20)
 )
RETURNS VARCHAR(10)
AS
BEGIN
	
DECLARE @BOOK INT
		,@BOOKNew VARCHAR(10)
		,@LengthGlobal INT =3
		,@LengthCurrent INT
		
	--GET CURRENT Business Unit Code		
	--SET @BOOKNew=  (SELECT TOP 1 BookCode 
	--				FROM Tbl_BookNumbers
	--				WHERE ServiceCenterId=@ServiceCenterId 
	--				ORDER BY IdentityBookId DESC)						
		
	--New Code (Logic Changed to accept BusinessUnit which is having zero)
	SET @BOOKNew=  (SELECT TOP 1 MAX(BookCode)
					FROM Tbl_BookNumbers BN
					JOIN Tbl_Cycles C ON C.CycleId=BN.CycleId
                    JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
					WHERE SC.ServiceCenterId=@ServiceCenterId)				
	--INCREMENT Business Unit Code BY 1
	--IF(@BOOKNew IS NOT NULL)
	SET @BOOK =CONVERT(INT,ISNULL(@BOOKNew,0)) + 1
	
	--ELSE 
	--SET @BOOK=0;

	SET @BOOKNew=@BOOK
	
	--GET LEGHT OF CURRENT Business Unit Code
	SET @LengthCurrent=LEN(@BOOKNew) 
	
	--NEW Business Unit Code CAN BE GENERATE OR NOT
	IF(@LengthCurrent<=@LengthGlobal)
	BEGIN
		--IF Business Unit Code
		WHILE(@LengthCurrent < @LengthGlobal)
		BEGIN
			SET @BOOKNew= {fn CONCAT('0',@BOOKNew)}
			SET @LengthCurrent=@LengthCurrent+1;
		END
	END
	ELSE
	BEGIN
		SET @BOOKNew=NULL
	END
	
	RETURN @BOOKNew
	--PRINT @BOOKNew
END
--====================================================================================================
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GenerateCustomerAccountNo]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar
-- Create date: 13-08-2014
-- Description:	The purpose of this function is to generate Customer AccountNo
-- =============================================
ALTER FUNCTION [dbo].[fn_GenerateCustomerAccountNo]
(
@BU_ID VARCHAR(20)
,@SU_Id VARCHAR(20)
,@SC_Id VARCHAR(20)
,@BookNo VARCHAR(20)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE 
		--@Count INT
		 @UniqueId VARCHAR(50)
		--,@Prefix VARCHAR(20)
		--,@PrefixLength INT
		--,@Id VARCHAR(50)
		,@BookCode VARCHAR(10)
		,@ServiceUnitCode VARCHAR(10)
		,@ServiceCenterCode VARCHAR(10)
		,@BusinessUnitCode VARCHAR(10)
		
	SELECT @BusinessUnitCode = ISNULL(BUCode,'') FROM Tbl_BussinessUnits WHERE BU_ID = @BU_ID
	SELECT @ServiceUnitCode = ISNULL(SUCode,'') FROM Tbl_ServiceUnits WHERE SU_ID = @SU_Id
	SELECT @ServiceCenterCode = ISNULL(SCCode,'') FROM Tbl_ServiceCenter	 WHERE ServiceCenterId = @SC_Id
	SELECT @BookCode = ISNULL(BookCode,0) FROM Tbl_BookNumbers WHERE BookNo = @BookNo
	
	SET @UniqueId=@BusinessUnitCode+@ServiceUnitCode+@ServiceCenterCode+@BookCode	
	RETURN @UniqueId
	--SELECT @Prefix = RIGHT('0000'+LEFT(@BusinessUnitCode,2),2)+RIGHT('0000'+LEFT(@ServiceUnitCode,2),2)+RIGHT('0000'+LEFT(@ServiceCenterCode,2),2)+RIGHT('0000'+LEFT(@BookCode,3),3)--'BEDC_BU_'
	--SELECT @Count = COUNT(0) FROM [UDV_CustomerDescription] WHERE AccountNo LIKE @Prefix+'%'
	--SELECT @Id = MAX(AccountNo) FROM [UDV_CustomerDescription] WHERE AccountNo LIKE @Prefix+'%'-- retrives last inserted application no
	
	--SELECT @PrefixLength = LEN(@Prefix)

	--IF(@Count!=0)----first if
	--BEGIN
	
	--	DECLARE @Num VARCHAR(50),@Char VARCHAR(50),@Temp INT
	   	   
	--	SET @Num = SUBSTRING(@Id,@PrefixLength+1,5)	
	--	SET @Char = SUBSTRING(@Id,@PrefixLength+1,1)	
	--	SET @Temp = ASCII(@Char)
		
	--	IF(@Temp>=65)-- Contains A,B...
	--		SET @Num = SUBSTRING(@Id,@PrefixLength+2,5)	
						
	--	IF(CONVERT(VARCHAR(10),@Num) = '99999' OR CONVERT(VARCHAR(10),@Num)='9999')----checks ID last 4 digits are equal to 9999
	--		BEGIN---second if
	--			IF(CONVERT(VARCHAR(10),@Char)='9')
	--				SET @Temp=64
	--			ELSE
	--				SET @Temp =ASCII(@Char)
	--			IF((@Temp >= 64 AND @Temp <= 90) OR (@Temp >=97 AND @Temp <= 122))
	--				BEGIN---third if
					
	--					SET @Temp = @Temp+1---assign as next character like a--b b--c
	--					SELECT @UniqueId = @Prefix+ CONVERT(VARCHAR(50),CHAR(@Temp))+'0001'
						 
	--				END---end of third if
				
	--		END----end of second if
	--	ELSE---if ID last 4 digits are not equal to 9999
	--		BEGIN---else for second if
	--			SET @Num = @Num+1--increments by 1 of last 4 digits of ID.
	--			IF(@Temp>=65)--Contains A,B...
	--				SELECT @UniqueId =  @Prefix+@Char+RIGHT('0000'+CONVERT(VARCHAR(50),@Num),4)
	--			ELSE
	--				SELECT @UniqueId =  @Prefix+RIGHT('0000'+CONVERT(VARCHAR(50),@Num),5)
				
	--		END---end of else for second if
	--END
	--ELSE---else for first if
	--BEGIN	
	--	SELECT @UniqueId = @Prefix+'00001'---if ID exists		
	--END	
	
	
	--RETURN @UniqueId

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GenerateBillNo]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 21-05-2014
-- Description:	The purpose of this function is to generate Customer Bill NO
-- Modified By - Padmini
-- Modified Date - 16/02/2015
-- =============================================
ALTER FUNCTION [dbo].[fn_GenerateBillNo]
(
@BillTypeId INT
,@BookNo VARCHAR(50)		
,@Month INT
,@Year INT
)
RETURNS VARCHAR(20)
AS
BEGIN
	DECLARE @Count INT
		,@UniqueId VARCHAR(50)
		,@Prefix VARCHAR(20)
		,@PrefixLength INT
		,@Id VARCHAR(50)
		,@BUCode VARCHAR(10)
		,@BillCode VARCHAR(10)
		,@Constant VARCHAR(10)
		
	SELECT @BillCode = UPPER(BillingType)  FROM Tbl_MBillingTypes WHERE BillingTypeId = @BillTypeId
	--SELECT @BUCode = ISNULL(BUCode,'') FROM Tbl_BookNumbers B INNER JOIN Tbl_BussinessUnits BU ON BU.BU_ID = B.BU_ID  WHERE BookNo = @BookNo	
	
	SELECT @BUCode = ISNULL(BU.BUCode,'') FROM Tbl_BookNumbers B  -- (Modified By - Padmini)
	JOIN Tbl_Cycles C ON C.CycleId=B.CycleId
	JOIN Tbl_ServiceCenter SC ON SC.ServiceCenterId=C.ServiceCenterId
	JOIN Tbl_ServiceUnits SU ON SU.SU_ID=SC.SU_ID
	JOIN Tbl_BussinessUnits BU ON BU.BU_ID=SU.BU_ID
	WHERE BookNo = @BookNo	
	
		IF(LEFT(@BillCode,1) = 'W')
			BEGIN
				SET @Constant = 'BEDC'
			END
		ELSE IF(LEFT(@BillCode,1) = 'M')
			BEGIN
				SET @Constant = 'IMEI'
			END
		ELSE IF(LEFT(@BillCode,1) = 'S')
			BEGIN
				SET @Constant = 'SPOT'
			END		
			
	
	--SELECT @Prefix = 'BEDC_BU_'
	SELECT @Prefix = RIGHT('000'+LEFT(@Constant,4),4)+RIGHT('00'+LEFT(@BUCode ,2),2)+RIGHT('000'+LEFT(@BillCode,1),1)+RIGHT('00'+RIGHT(@Year,2),2)+dbo.fn_GetMonthShortName(@Month)--'BEDC_BU_'
	SELECT @Count = COUNT(*) FROM Tbl_CustomerBills WHERE BillNo LIKE @Prefix+'%'
	SELECT @Id = MAX(BillNo) FROM Tbl_CustomerBills WHERE BillNo LIKE @Prefix+'%'-- retrives last inserted application no
	
	SELECT @PrefixLength = LEN(@Prefix)

	IF(@Count!=0)----first if
	BEGIN
	
		DECLARE @Num VARCHAR(50),@Char VARCHAR(50),@Temp INT
	   	   
		SET @Num = SUBSTRING(@Id,@PrefixLength+1,6)	
		SET @Char = SUBSTRING(@Id,@PrefixLength+1,1)	
		SET @Temp = ASCII(@Char)
		
		IF(@Temp>=65)-- Contains A,B...
			SET @Num = SUBSTRING(@Id,@PrefixLength+2,5)	
						
		IF(CONVERT(VARCHAR(10),@Num) = '999999' OR CONVERT(VARCHAR(10),@Num)='99999')----checks ID last 4 digits are equal to 9999
		BEGIN---second if
			IF(CONVERT(VARCHAR(10),@Char)='9')
				SET @Temp=64
			ELSE
				SET @Temp =ASCII(@Char)
			IF((@Temp >= 64 AND @Temp <= 90) OR (@Temp >=97 AND @Temp <= 122))
			BEGIN---third if
			
				SET @Temp = @Temp+1---assign as next character like a--b b--c
				SELECT @UniqueId = @Prefix+ CONVERT(VARCHAR(50),CHAR(@Temp))+'00001'
				 
			END---end of third if
			
		END----end of second if
		ELSE---if ID last 4 digits are not equal to 9999
		BEGIN---else for second if
			SET @Num = @Num+1--increments by 1 of last 4 digits of ID.
			IF(@Temp>=65)--Contains A,B...
				SELECT @UniqueId =  @Prefix+@Char+RIGHT('00000'+CONVERT(VARCHAR(50),@Num),5)
			ELSE
				SELECT @UniqueId =  @Prefix+RIGHT('00000'+CONVERT(VARCHAR(50),@Num),6)
			
		END---end of else for second if
	END
	ELSE---else for first if
	BEGIN
	
		SELECT @UniqueId = @Prefix+'000001'---if ID exists
		
	END	

	RETURN @UniqueId

END
GO
/****** Object:  UserDefinedFunction [MASTERS].[fn_GenCustomerAccountNo]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		NEERAJ KANOJIYA

-- Create date: 263-DEC-2014

-- DESCRIPTION:	THE PURPOSE OF THIS FUNCTION IS TO GENERATE CUSTOMER ACCOUNTNO IN NEW CUSTOMER TABLE
--Modified By : Bhimaraju V
--ModifiedDate: 13-Feb-15
-- =============================================

ALTER FUNCTION [MASTERS].[fn_GenCustomerAccountNo]

(

@BU_ID VARCHAR(20)

,@SU_Id VARCHAR(20)

,@SC_Id VARCHAR(20)

,@BookNo VARCHAR(20)

)

RETURNS VARCHAR(50)

AS

BEGIN

	DECLARE @Count INT

		,@UniqueId VARCHAR(50)

		,@Prefix VARCHAR(20)

		,@PrefixLength INT

		,@Id VARCHAR(50)

		,@BookCode VARCHAR(10)

		,@ServiceUnitCode VARCHAR(10)

		,@ServiceCenterCode VARCHAR(10)

		,@BusinessUnitCode VARCHAR(10)

		

	SELECT @BusinessUnitCode = ISNULL(BUCode,'') FROM Tbl_BussinessUnits WHERE BU_ID = @BU_ID

	SELECT @ServiceUnitCode = ISNULL(SUCode,'') FROM Tbl_ServiceUnits WHERE SU_ID = @SU_Id

	SELECT @ServiceCenterCode = ISNULL(SCCode,'') FROM Tbl_ServiceCenter	 WHERE ServiceCenterId = @SC_Id

	SELECT @BookCode = ISNULL(BookCode,0) FROM Tbl_BookNumbers WHERE BookNo = @BookNo

	SET @UniqueId=@BusinessUnitCode+@ServiceUnitCode+@ServiceCenterCode+@BookCode
	RETURN @UniqueId	

	

	--SELECT @Prefix = RIGHT('0000'+LEFT(@BusinessUnitCode,2),2)+RIGHT('0000'+LEFT(@ServiceUnitCode,2),2)+RIGHT('0000'+LEFT(@ServiceCenterCode,2),2)+RIGHT('0000'+LEFT(@BookCode,3),3)--'BEDC_BU_'

	--SELECT @Count = COUNT(0) FROM CUSTOMERS.Tbl_CustomersDetail WHERE AccountNo LIKE @Prefix+'%'

	--SELECT @Id = MAX(AccountNo) FROM CUSTOMERS.Tbl_CustomersDetail WHERE AccountNo LIKE @Prefix+'%'-- retrives last inserted application no

	

	--SELECT @PrefixLength = LEN(@Prefix)



	--IF(@Count!=0)----first if

	--BEGIN

	

	--	DECLARE @Num VARCHAR(50),@Char VARCHAR(50),@Temp INT

	   	   

	--	SET @Num = SUBSTRING(@Id,@PrefixLength+1,5)	

	--	SET @Char = SUBSTRING(@Id,@PrefixLength+1,1)	

	--	SET @Temp = ASCII(@Char)

		

	--	IF(@Temp>=65)-- Contains A,B...

	--		SET @Num = SUBSTRING(@Id,@PrefixLength+2,5)	

						

	--	IF(CONVERT(VARCHAR(10),@Num) = '99999' OR CONVERT(VARCHAR(10),@Num)='9999')----checks ID last 4 digits are equal to 9999

	--		BEGIN---second if

	--			IF(CONVERT(VARCHAR(10),@Char)='9')

	--				SET @Temp=64

	--			ELSE

	--				SET @Temp =ASCII(@Char)

	--			IF((@Temp >= 64 AND @Temp <= 90) OR (@Temp >=97 AND @Temp <= 122))

	--				BEGIN---third if

					

	--					SET @Temp = @Temp+1---assign as next character like a--b b--c

	--					SELECT @UniqueId = @Prefix+ CONVERT(VARCHAR(50),CHAR(@Temp))+'0001'

						 

	--				END---end of third if

				

	--		END----end of second if

	--	ELSE---if ID last 4 digits are not equal to 9999

	--		BEGIN---else for second if

	--			SET @Num = @Num+1--increments by 1 of last 4 digits of ID.

	--			IF(@Temp>=65)--Contains A,B...

	--				SELECT @UniqueId =  @Prefix+@Char+RIGHT('0000'+CONVERT(VARCHAR(50),@Num),4)

	--			ELSE

	--				SELECT @UniqueId =  @Prefix+RIGHT('0000'+CONVERT(VARCHAR(50),@Num),5)

				

	--		END---end of else for second if

	--END

	--ELSE---else for first if

	--BEGIN	

	--	SELECT @UniqueId = @Prefix+'00001'---if ID exists		

	--END	

	

	

	--RETURN @UniqueId



END
GO
/****** Object:  UserDefinedFunction [MASTERS].[fn_CycleCodeGenerate]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 30-DEC-2014
-- Description:	GENERATE CYCLE CODE
-- ModifieedBy: Padmini
-- Modified Date:31-01-2015
-- =============================================
ALTER FUNCTION [MASTERS].[fn_CycleCodeGenerate]
(
 @ServiceCenterId VARCHAR(20)
)
RETURNS VARCHAR(10)
AS
BEGIN
	
DECLARE @CycleCode INT
		,@CycleCodeNew VARCHAR(10)
		,@LengthGlobal INT =2
		,@LengthCurrent INT
		
	--GET CURRENT Service Center Code		
	--SET @CycleCodeNew= (SELECT TOP 1 CycleCode 
	--					FROM Tbl_Cycles 
	--					WHERE ServiceCenterId=@ServiceCenterId
	--					ORDER BY IdentityCycleId DESC)
		
	--New Code (Logic Changed to accept BusinessUnit which is having zero)
	SET @CycleCodeNew= (SELECT TOP 1 MAX(CycleCode) 
						FROM Tbl_Cycles 
						WHERE ServiceCenterId=@ServiceCenterId)											
	--INCREMENT Service Center Code BY 1
	--IF(@CycleCodeNew IS NOT NULL)
	SET @CycleCode =CONVERT(INT,ISNULL(@CycleCodeNew,0)) + 1
	--ELSE 
	--	SET @CycleCode=0;

	SET @CycleCodeNew=@CycleCode
	
	--GET LEGHT OF CURRENT Service Center Code
	SET @LengthCurrent=LEN(@CycleCodeNew) 
	
	--NEW Service Center Code CAN BE GENERATE OR NOT
	IF(@LengthCurrent<=@LengthGlobal)
	BEGIN
		--IF Service Center Code
		WHILE(@LengthCurrent < @LengthGlobal)
		BEGIN
			SET @CycleCodeNew= {fn CONCAT('0',@CycleCodeNew)}
			SET @LengthCurrent=@LengthCurrent+1;
		END
	END
	ELSE
	BEGIN
		SET @CycleCodeNew=NULL
	END
	
	RETURN @CycleCodeNew
END
--====================================================================================================
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerAddress]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 30 JULY 2014
-- Description:	The purpose of this function is to get the Customer's Address
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerAddress]
(
@AccountNo VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @CustomerAddress VARCHAR(MAX)

	SELECT @CustomerAddress =LTRIM(RTRIM(
	CASE WHEN HouseNo IS NULL OR HouseNo = '' THEN '' ELSE LTRIM(RTRIM(HouseNo)) + ', '  END + 
CASE WHEN StreetName IS NULL OR StreetName = '' THEN '' ELSE  LTRIM(RTRIM(StreetName)) + ', '  END + 
CASE WHEN Landmark IS NULL OR Landmark = '' THEN '' ELSE LTRIM(RTRIM(Landmark))  + ', ' END + 
CASE WHEN City IS NULL OR City = '' THEN '' ELSE LTRIM(RTRIM(City)) + ', '  END + 
CASE WHEN zipcode IS NULL OR zipcode = '' THEN '' ELSE  LTRIM(RTRIM(zipcode)) + ', '  END ))
	FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]				
	WHERE GlobalAccountNumber = @AccountNo
	AND IsServiceAddress=0
	SET @CustomerAddress = SUBSTRING(@CustomerAddress,0,LEN(@CustomerAddress)-0)
	RETURN @CustomerAddress
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAverageUsageByTariff]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author  : NEERAJ KANOJIYA              
-- Create date  : 28 July 2014                
-- Description  : THIS PROCEDURE WILL GET AVG READING BY TARIFF ID
-- Modified By  : Suresh Kumar Dasi
-- Modified date: 12 Aug 2014
-- =======================================================================   

ALTER FUNCTION [dbo].[fn_GetAverageUsageByTariff]
(
	@Tariff VARCHAR(20)
	,@CycleId VARCHAR(50)
	,@Year INT
	,@Month INT
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @AccountNumner VARCHAR(50),
			@AverageUsage  INT,
			@TotalUsage INT
			,@ReadingsCount INT
			,@Sum DECIMAL(18,2)
			,@InitialAverage DECIMAL(18,2)
			,@MonthStartDate VARCHAR(50)       
			,@Date VARCHAR(50)
	DECLARE @FilteredAccounts TABLE(AccountNo VARCHAR(50)) 
	
    SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
    SET @Date = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate)))   
	
	--INSERT INTO @FilteredAccounts     
	--SELECT DISTINCT AccountNo FROM Tbl_CustomerDetails CDM
	--JOIN Tbl_BookNumbers BC ON CDM.BookNo = BC.BookNo
 --   WHERE TariffId  = @Tariff AND CDM.ActiveStatusId = 1  AND BC.CycleId=@CycleID
	--AND AccountNo NOT IN(SELECT   
	--						  CR.AccountNumber
	--						FROM Tbl_CustomerReadings CR  
	--						JOIN Tbl_CustomerDetails CD ON CR.AccountNumber = CD.AccountNo  
	--						JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo
	--						WHERE TariffId  = @Tariff AND CD.ActiveStatusId = 1  AND BC.CycleId=@CycleID
	--						AND CONVERT(DATE,CONVERT(DATETIME,CR.ReadDate)) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)     
	--					)
	
	
	--SELECT TOP(1) @AccountNumner = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC    

	--WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @AccountNumner ORDER BY AccountNo ASC))    
	--BEGIN  
			
	--		SELECT @ReadingsCount = COUNT(0) FROM Tbl_CustomerReadings CR WHERE AccountNumber = @AccountNumner	
	--		SELECT @Sum = SUM(Usage) FROM Tbl_CustomerReadings CR WHERE AccountNumber = @AccountNumner	
	--		SET @AverageUsage = (CASE WHEN @Sum > 0 THEN @Sum/@ReadingsCount ELSE 0 END)
	--		IF(@AverageUsage IS NULL OR @AverageUsage = 0)
	--			BEGIN
	--				IF EXISTS(SELECT 0 FROM Tbl_CustomerDetails WHERE AccountNo = @AccountNumner AND ReadCodeId = 1)
	--					BEGIN
	--						SELECT @InitialAverage = MinimumReading FROM Tbl_CustomerDetails WHERE AccountNo = @AccountNumner	
	--						SET @TotalUsage = ISNULL(@TotalUsage,0) + ISNULL(@InitialAverage,0)
	--					END
	--				ELSE
	--					BEGIN
	--						SELECT @InitialAverage = AverageReading FROM Tbl_CustomerDetails WHERE AccountNo = @AccountNumner	
	--						SET @TotalUsage = ISNULL(@TotalUsage,0) + ISNULL(@InitialAverage,0)
	--					END	
	--			END
	--		ELSE
	--			BEGIN
	--				SET @TotalUsage = ISNULL(@TotalUsage,0) + ISNULL(@AverageUsage,0)
	--			END	
			 
	      
	--	  IF(@AccountNumner = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))    
	--	    BREAK    
	--	  ELSE    
	--		   BEGIN    
	--			SET @AccountNumner = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @AccountNumner ORDER BY AccountNo ASC)    
	--			IF(@AccountNumner IS NULL) break;    
	--			 Continue    
	--		   END    
	-- END     


	INSERT INTO @FilteredAccounts     
	SELECT DISTINCT CDM.GlobalAccountNumber FROM CUSTOMERS.Tbl_CustomerSDetail CDM
	JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS CPD ON CDM.GlobalAccountNumber=CPD.GlobalAccountNumber
	JOIN Tbl_BookNumbers BC ON CPD.BookNo = BC.BookNo
    WHERE TariffClassID  = @Tariff AND CDM.ActiveStatusId = 1  AND BC.CycleId=@CycleID
	AND CDM.GlobalAccountNumber NOT IN(SELECT   
							  CR.GlobalAccountNumber
							FROM Tbl_CustomerReadings CR  
							JOIN CUSTOMERS.Tbl_CustomerSDetail CD ON CR.GlobalAccountNumber = CD.GlobalAccountNumber  
							JOIN CUSTOMERS.Tbl_CustomerProceduralDetails AS ICPD ON ICPD.GlobalAccountNumber=CD.GlobalAccountNumber
							JOIN Tbl_BookNumbers BC ON ICPD.BookNo=BC.BookNo
							WHERE ICPD.TariffClassID  = @Tariff AND CD.ActiveStatusId = 1  AND BC.CycleId=@CycleID
							AND CONVERT(DATE,CONVERT(DATETIME,CR.ReadDate)) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)     
						)
	
	
	SELECT TOP(1) @AccountNumner = AccountNo FROM @FilteredAccounts ORDER BY AccountNo ASC    

	WHILE(EXISTS(SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE AccountNo >= @AccountNumner ORDER BY AccountNo ASC))    
	BEGIN  
			
			SELECT @ReadingsCount = COUNT(0) FROM Tbl_CustomerReadings CR WHERE GlobalAccountNumber = @AccountNumner	
			SELECT @Sum = SUM(Usage) FROM Tbl_CustomerReadings CR WHERE GlobalAccountNumber = @AccountNumner	
			SET @AverageUsage = (CASE WHEN @Sum > 0 THEN @Sum/@ReadingsCount ELSE 0 END)
			IF(@AverageUsage IS NULL OR @AverageUsage = 0)
				BEGIN
					IF EXISTS(SELECT 0 FROM CUSTOMERS.[Tbl_CustomerProceduralDetails] WHERE GlobalAccountNumber = @AccountNumner AND ReadCodeId = 1)
						BEGIN
							SELECT @InitialAverage = InitialBillingKWh FROM CUSTOMERS.[Tbl_CustomerActiveDetails] WHERE GlobalAccountNumber = @AccountNumner	
							SET @TotalUsage = ISNULL(@TotalUsage,0) + ISNULL(@InitialAverage,0)
						END
					ELSE
						BEGIN
							SELECT @InitialAverage = AvgReading FROM CUSTOMERS.[Tbl_CustomerActiveDetails] WHERE GlobalAccountNumber = @AccountNumner	
							SET @TotalUsage = ISNULL(@TotalUsage,0) + ISNULL(@InitialAverage,0)
						END	
				END
			ELSE
				BEGIN
					SET @TotalUsage = ISNULL(@TotalUsage,0) + ISNULL(@AverageUsage,0)
				END	
			 
	      
		  IF(@AccountNumner = (SELECT TOP(1) AccountNo FROM @FilteredAccounts ORDER BY AccountNo DESC))    
		    BREAK    
		  ELSE    
			   BEGIN    
				SET @AccountNumner = (SELECT TOP(1) AccountNo FROM @FilteredAccounts WHERE  AccountNo > @AccountNumner ORDER BY AccountNo ASC)    
				IF(@AccountNumner IS NULL) break;    
				 Continue    
			   END    
	 END     
	 
	RETURN @TotalUsage;
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetAccountNumbers_ByCycleFeeder]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 14 MAY 2014
-- Description:	The purpose of this function is to get the Account numbers Based on Cycle Id and feederId
-- Modified By: Padmini
-- Modified Date:02/10/2015 (Deleted distinct keyword)
-- =============================================
ALTER FUNCTION [dbo].[fn_GetAccountNumbers_ByCycleFeeder]
(
--@FeederId VARCHAR(50)
@CycleId VARCHAR(MAX)
)
RETURNS @AccountNumbers TABLE(AccountNo VARCHAR(50))
AS
BEGIN
	
	INSERT INTO @AccountNumbers
	SELECT  GlobalAccountNumber 
	FROM [CUSTOMERS].Tbl_CustomerProceduralDetails 
	WHERE BookNo IN(SELECT BookNo FROM Tbl_BookNumbers
	 WHERE CycleId IN(SELECT [com] FROM dbo.fn_Split(@CycleId,',')))
	AND ActiveStatusId IN (1,2)-- Active And DeActive Customers Getting
	--WHERE (FeederId = @FeederId OR(BookNo IN(select BookNo from Tbl_BookCycles where CycleId = @CycleId)))
	 			
	RETURN 

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GenerateUniqueId]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 20-02-2014
-- Description:	The purpose of this function is to generate Id

-- Modified By: Faiz-ID103
-- Modified Date: 18-02-2015
-- Added code for generating unique book No ReferencedId is 11
-- =============================================
ALTER FUNCTION [dbo].[fn_GenerateUniqueId]
(
	@ReferenceId INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Count INT
		,@UniqueId VARCHAR(MAX)
		,@Prefix VARCHAR(20)
		,@PrefixLength INT
		,@Id VARCHAR(50)
		
	IF(@ReferenceId=1)-- Business Unit Id
	BEGIN	
		SELECT @Prefix=KeyCode+'_BU_' FROM Tbl_CompanyDetails WHERE ActiveStatusId=1
		SELECT @Count = COUNT(*) FROM Tbl_BussinessUnits WHERE BU_ID LIKE @Prefix+'%'
		SELECT @Id=MAX(BU_ID) FROM Tbl_BussinessUnits  WHERE BU_ID LIKE @Prefix+'%'-- retrives last inserted application no
	END
	ELSE IF(@ReferenceId=2)-- Service Unit Id
	BEGIN
		SELECT @Prefix=KeyCode+'_SU_' FROM Tbl_CompanyDetails WHERE ActiveStatusId=1
		SELECT @Count = COUNT(*) FROM Tbl_ServiceUnits WHERE SU_ID LIKE @Prefix+'%'
		SELECT @Id=MAX(SU_ID) FROM Tbl_ServiceUnits  WHERE SU_ID LIKE @Prefix+'%'-- retrives last inserted application no
	END
	ELSE IF(@ReferenceId=3)-- Service Center Id
	BEGIN
		SELECT @Prefix=KeyCode+'_SC_' FROM Tbl_CompanyDetails WHERE ActiveStatusId=1
		SELECT @Count = COUNT(*) FROM Tbl_ServiceCenter WHERE ServiceCenterId LIKE @Prefix+'%'
		SELECT @Id=MAX(ServiceCenterId) FROM Tbl_ServiceCenter  WHERE ServiceCenterId LIKE @Prefix+'%'-- retrives last inserted application no
	END
	--ELSE IF(@ReferenceId=4)-- Injection SubStation Id
	--BEGIN
	--	SELECT @Prefix=KeyCode+'_I_' FROM Tbl_CompanyDetails WHERE ActiveStatusId=1
	--	SELECT @Count = COUNT(*) FROM Tbl_InjectionSubStation WHERE SubStationId LIKE @Prefix+'%'
	--	SELECT @Id=MAX(SubStationId) FROM Tbl_InjectionSubStation  WHERE SubStationId LIKE @Prefix+'%'-- retrives last inserted application no
	--END
	--ELSE IF(@ReferenceId=5)-- Feeder Id
	--BEGIN
	--	SELECT @Prefix=KeyCode+'_F_' FROM Tbl_CompanyDetails WHERE ActiveStatusId=1
	--	SELECT @Count = COUNT(*) FROM Tbl_Feeders WHERE FeederId LIKE @Prefix+'%'
	--	SELECT @Id=MAX(FeederId) FROM Tbl_Feeders  WHERE FeederId LIKE @Prefix+'%'-- retrives last inserted application no
	--END
	--ELSE IF(@ReferenceId=6)-- Transformer Id
	--BEGIN
	--	SELECT @Prefix=KeyCode+'_T_' FROM Tbl_CompanyDetails WHERE ActiveStatusId=1
	--	SELECT @Count = COUNT(*) FROM Tbl_TransFormers WHERE TransFormerId LIKE @Prefix+'%'
	--	SELECT @Id=MAX(TransFormerId) FROM Tbl_TransFormers  WHERE TransFormerId LIKE @Prefix+'%'-- retrives last inserted application no
	--END
	--ELSE IF(@ReferenceId=7)-- Pole Id
	--BEGIN
	--	SELECT @Prefix=KeyCode+'_P_' FROM Tbl_CompanyDetails WHERE ActiveStatusId=1
	--	SELECT @Count = COUNT(*) FROM Tbl_Poles WHERE PoleId LIKE @Prefix+'%'
	--	SELECT @Id=MAX(PoleId) FROM Tbl_Poles  WHERE PoleId LIKE @Prefix+'%'-- retrives last inserted application no
	--END
	ELSE IF(@ReferenceId=8)-- Cycle Id
	BEGIN
		SELECT @Prefix=KeyCode+'_C_' FROM Tbl_CompanyDetails WHERE ActiveStatusId=1
		SELECT @Count = COUNT(*) FROM Tbl_Cycles WHERE CycleId LIKE @Prefix+'%'
		SELECT @Id=MAX(CycleId) FROM Tbl_Cycles  WHERE CycleId LIKE @Prefix+'%'-- retrives last inserted application no
	END
	ELSE IF(@ReferenceId=9)-- Customer Unique No
	BEGIN
		SELECT @Prefix=KeyCode+'_CUN_' FROM Tbl_CompanyDetails WHERE ActiveStatusId=1
		SELECT @Count = COUNT(*) FROM CUSTOMERS.Tbl_CustomerSDetail WHERE GlobalAccountNumber LIKE @Prefix+'%'
		SELECT @Id=MAX(GlobalAccountNumber) FROM CUSTOMERS.Tbl_CustomerSDetail  WHERE GlobalAccountNumber LIKE @Prefix+'%'-- retrives last inserted application no
	END
	ELSE IF(@ReferenceId=10)-- Customer Account No
	BEGIN
		SELECT @Prefix=KeyCode+'_CAN_' FROM Tbl_CompanyDetails WHERE ActiveStatusId=1
		SELECT @Count = COUNT(*) FROM CUSTOMERS.Tbl_CustomerSDetail WHERE AccountNo LIKE @Prefix+'%'
		SELECT @Id=MAX(AccountNo) FROM CUSTOMERS.Tbl_CustomerSDetail  WHERE AccountNo LIKE @Prefix+'%'-- retrives last inserted application no
	END
	
	
	-----Faiz - ID103
	ELSE IF(@ReferenceId=11)-- Book Unique No
	BEGIN
		SELECT @Prefix=KeyCode+'_B_' FROM Tbl_CompanyDetails WHERE ActiveStatusId=1
		SELECT @Count = COUNT(*) FROM Tbl_BookNumbers WHERE BookNo LIKE @Prefix+'%'
		SELECT @Id=MAX(BookNo) FROM Tbl_BookNumbers  WHERE BookNo LIKE @Prefix+'%'-- retrives last inserted application no
	END
	
	SELECT @PrefixLength=LEN(@Prefix)

	IF(@Count!=0)----first if
	BEGIN
	
		DECLARE @Num VARCHAR(50),@Char VARCHAR(50),@Temp INT
	   	   
		SET @Num = SUBSTRING(@Id,@PrefixLength+1,4)	
		SET @Char = SUBSTRING(@Id,@PrefixLength+1,1)	
		SET @Temp = ASCII(@Char)
		
		IF(@Temp>=65)-- Contains A,B...
			SET @Num = SUBSTRING(@Id,@PrefixLength+2,3)	
						
		IF(CONVERT(VARCHAR(10),@Num) = '9999' OR CONVERT(VARCHAR(10),@Num)='999')----checks ID last 4 digits are equal to 9999
		BEGIN---second if
			IF(CONVERT(VARCHAR(10),@Char)='9')
				SET @Temp=64
			ELSE
				SET @Temp =ASCII(@Char)
			IF((@Temp >= 64 AND @Temp <= 90) OR (@Temp >=97 AND @Temp <= 122))
			BEGIN---third if
			
				SET @Temp = @Temp+1---assign as next character like a--b b--c
				SELECT @UniqueId = @Prefix+ CONVERT(VARCHAR(50),CHAR(@Temp))+'001'
				 
			END---end of third if
			
		END----end of second if
		ELSE---if ID last 4 digits are not equal to 9999
		BEGIN---else for second if
			SET @Num = @Num+1--increments by 1 of last 4 digits of ID.
			IF(@Temp>=65)--Contains A,B...
				SELECT @UniqueId =  @Prefix+@Char+RIGHT('0000'+CONVERT(VARCHAR(50),@Num),3)
			ELSE
				SELECT @UniqueId =  @Prefix+RIGHT('0000'+CONVERT(VARCHAR(50),@Num),4)
			
		END---end of else for second if
	END
	ELSE---else for first if
	BEGIN
	
		SELECT @UniqueId = @Prefix+'0001'---if ID exists
		
	END	

	RETURN @UniqueId

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GenerateServiceNo]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar
-- Create date: 14-08-2014
-- Description:	The purpose of this function is to generate Service Number
-- =============================================
ALTER FUNCTION [dbo].[fn_GenerateServiceNo]
(
@SU_Id VARCHAR(20)
,@SC_Id VARCHAR(20)
,@BookNo VARCHAR(20)
)
RETURNS VARCHAR(20)
AS
BEGIN
	DECLARE @Count INT
		,@UniqueId VARCHAR(50)
		,@Prefix VARCHAR(20)
		,@PrefixLength INT
		,@Id VARCHAR(50)
		,@BookCode VARCHAR(10)
		,@ServiceUnitCode VARCHAR(10)
		,@ServiceCenterCode VARCHAR(10)
		
	SELECT @ServiceUnitCode = ISNULL(SUCode,'') FROM Tbl_ServiceUnits WHERE SU_ID = @SU_Id
	SELECT @ServiceCenterCode = ISNULL(SCCode,'') FROM Tbl_ServiceCenter WHERE ServiceCenterId = @SC_Id
	SELECT @BookCode = ISNULL(BookCode,'') FROM Tbl_BookNumbers WHERE BookNo = @BookNo
		
	
	SELECT @UniqueId = RIGHT('0000'+LEFT(@ServiceUnitCode,2),2)+RIGHT('0000'+LEFT(@ServiceCenterCode,2),2)+RIGHT('0000'+LEFT(@BookCode,3),3)--'BEDC_BU_'
	
	
	RETURN @UniqueId

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerLastBillAmountWithArrears]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author   : M.Padmini  
-- Create date: 20-Feb-15
-- Description: This function is used to get customer previousbillamountwithArrears.
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerLastBillAmountWithArrears]  
(  
@GlobalAccountNo VARCHAR(50)
,@BillMonth INT
,@BillYear  INT
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
 DECLARE   
   @BillAmountWithTax DECIMAL(18,2)
  ,@MaxCustomerBillId INT 
  ,@BillCount INT
  --,@PresDate DATE
  --,@PrevDate DATE
  
  --      SET @PresDate= CONVERT(DATE,CONVERT(VARCHAR(20),@BillYear)+'-'+CONVERT(VARCHAR(20),@BillMonth)+'-01') 
		--SET @PrevDate= CONVERT(DATE,CONVERT(VARCHAR(20),@BillYear)+'-'+CONVERT(VARCHAR(20),@BillMonth)+'-01') 
		
	 --================ Getting 2nd heighest CustomerBillId===================
	 SET @BillCount= (SELECT COUNT(0) FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo)
	 IF(@BillCount>=2)
	 BEGIN
		 SELECT  TOP 1 @MaxCustomerBillId= MAX(CustomerBillId)
		 FROM Tbl_CustomerBills   
		 WHERE AccountNo =@GlobalAccountNo 
		 AND CustomerBillId <(SELECT MAX(CustomerBillId) FROM Tbl_CustomerBills WHERE AccountNo=@GlobalAccountNo)
		 GROUP BY CustomerBillId
		 ORDER BY CustomerBillId DESC 
		 
		 --================= Getting Previous Bill Amount===============
		 
		 SELECT @BillAmountWithTax= TotalBillAmountWithArrears FROM Tbl_CustomerBills
		 WHERE CustomerBillId=@MaxCustomerBillId
		 AND AccountNo=@GlobalAccountNo
		 --AND BillMonth<DATEPART(MONTH, DATEADD(MONTH,+1,CONVERT(DATE, '2014-'+convert(VARCHAR(2),@BillMonth)+'-01'))) 
		 --AND BillYear=@BillYear
	 END
	 ELSE
	 BEGIN
		SET @BillAmountWithTax=(SELECT OpeningBalance FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber=@GlobalAccountNo)
	 END
 SET @BillAmountWithTax = ISNULL(@BillAmountWithTax,0) 
  
      
 RETURN @BillAmountWithTax   
  
END  
------------------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerFullName]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bhimaraju Vanka
-- Create date: 18-10-2014
-- Description:	To Get FullName Of the Customer Title+Name+SurName
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerFullName]
(
	@AccountNo VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Name VARCHAR(MAX)

		SELECT @Name =	LTRIM(RTRIM(
		CASE WHEN Title IS NULL OR Title = '' THEN '' ELSE LTRIM(RTRIM(Title+'. ')) END + 
		CASE WHEN FirstName IS NULL OR FirstName = '' THEN '' ELSE  LTRIM(RTRIM(FirstName)) END + 
		CASE WHEN MiddleName IS NULL OR MiddleName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(MiddleName)) END +
		CASE WHEN LastName IS NULL OR LastName = '' THEN '' ELSE ' ' + LTRIM(RTRIM(LastName)) END))
	FROM [CUSTOMERS].[Tbl_CustomerSDetail]				
	WHERE GlobalAccountNumber = @AccountNo
	RETURN @Name
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint_Customerwise]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- 
-- =============================================  


CREATE FUNCTION [dbo].[fn_GetCustomerBillsForPrint_Customerwise]
(  
 @GlobalAcountNumber VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (SELECT         
	  CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
           
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat_bill(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  CB.VAT  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(11),CB.PaymentLastDate,106) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(11),CB.PaymentLastDate,101) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS [MonthName]        
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
     ,(CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END)  AS Usage        
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),BD.ReadDate),'--') AS ReadDate        
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(11), CB.PaymentLastDate,106),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'--') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
    FROM Tbl_CustomerBills CB    
    INNER JOIN Tbl_BillDetails BD ON CB.CustomerBillId = BD.CustomerBillID 
    
     where AccountNo=@GlobalAcountNumber and BillMonth=@BillMonth and BillYear=@BillYear
  );
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBillsForPrint]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author		: Satya  
-- Create date	: 15 March 2015
-- Description	: Bill For Print  
-- 
-- =============================================  


ALTER FUNCTION [dbo].[fn_GetCustomerBillsForPrint]
(  
 @ServiceCenter VARCHAR(MAx) , 
 @BillMonth  INT ,
  @BillYear  INT
  
)  
RETURNS  TABLE  
AS
  RETURN (SELECT         
	  CB.CustomerBillId        
     ,CB.BillNo        
     ,CB.AccountNo        
     ,BD.CustomerFullName AS Name      
           
     ,Replace(convert(varchar,convert(Money,  CB.TotalBillAmount),1),'.00','') as  TotalBillAmount        
     ,(dbo.fn_GetCustomerAddressFormat_bill(BD.Service_HouseNo,BD.Service_Street,BD.Service_City,BD.ServiceZipCode) ) AS ServiceAddress
	 ,(CASE WHEN CONVERT(VARCHAR(10),CB.MeterNo) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.MeterNo) END) AS MeterNo
     ,(CASE WHEN CONVERT(VARCHAR(10),CB.Dials) IS NULL THEN '--' ELSE CONVERT(VARCHAR(10),CB.Dials) END )AS Dials
     ,Replace(convert(varchar,convert(Money, CB.NetArrears),1),'.00','')  AS NetArrears
     ,(CASE WHEN CB.NetEnergyCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetEnergyCharges ),1),'.00','')  END) AS NetEnergyCharges     
     ,(CASE WHEN CB.NetFixedCharges IS NULL THEN convert(money, 0.00) ELSE Replace(convert(varchar,convert(Money, CB.NetFixedCharges),1),'.00','')  END)AS NetFixedCharges
     ,Replace(convert(varchar,convert(Money,  CB.VAT  ),1),'.00','') AS  VAT        
     ,Replace(convert(varchar,convert(Money,  CB.VATPercentage  ),1),'.00','')  AS VATPercentage        
     ,(CASE WHEN CB.[Messages] IS NULL THEN 'N/A' ELSE CB.[Messages] END) AS [Messages]      
     ,CB.BillGeneratedBy        
     ,CB.BillGeneratedDate        
     ,CASE WHEN CONVERT(VARCHAR(11),CB.PaymentLastDate,106) IS NULL THEN 'N/A' ELSE CONVERT(VARCHAR(11),CB.PaymentLastDate,101) END AS LastDateOfBill        
     ,CB.BillYear        
     ,CB.BillMonth        
     ,(SELECT dbo.fn_GetMonthName(CB.BillMonth)) AS [MonthName]        
     ,(dbo.fn_GetPreviusMonth(CB.BillYear,CB.BillMonth)) AS PrevMonth     
     ,Replace(convert(varchar,convert(Money, isnull(CB.TotalBillAmountWithArrears,0) ),1),'.00','')   as  TotalBillAmountWithArrears       
     ,(CASE WHEN CB.PreviousReading IS NULL THEN '--' ELSE CB.PreviousReading END)AS PreviousReading
     ,(CASE WHEN CB.PresentReading IS NULL THEN '--' ELSE CB.PresentReading END)AS PresentReading     
     ,(CASE WHEN CONVERT(INT,CB.Usage)IS NULL THEN 0.00 ELSE CONVERT(INT,CB.Usage)END)  AS Usage        
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithTax ),1),'.00','') AS  TotalBillAmountWithTax         
     ,BD.BusinessUnitName        
     ,(CASE WHEN BD.Service_HouseNo IS Null THEN '--' ELSE  BD.Service_HouseNo END )AS ServiceHouseNo    
     ,(CASE WHEN BD.Service_Street IS Null THEN '--' ELSE  BD.Service_Street END )  AS ServiceStreet        
     ,(CASE WHEN BD.Service_City IS Null THEN '--' ELSE  BD.Service_City END )  AS ServiceCity        
     ,(CASE WHEN BD.ServiceZipCode IS Null THEN 'N/A' ELSE  BD.ServiceZipCode END )  AS ServiceZipCode          
     ,CB.TariffId        
     --,CB.AdjustmentAmmount    
     ,case    when  CB.AdjustmentAmmount  < 0 then  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' CR ' when CB.AdjustmentAmmount  =0 then '0.00' ELSE  CONVERT(varchar(20),Replace(convert(varchar,convert(Money, CB.AdjustmentAmmount ),1),'.00','') ) + ' DR ' END  as AdjustmentAmmount
     ,BD.TariffName
     ,ISNULL(BD.OldAccountNumber,'----') AS OldAccountNo    
     ,ISNULL(convert(varchar(50),BD.ReadDate),'--') AS ReadDate        
     ,ISNULL(convert(varchar(50),BD.Multiplier),'--')  AS Multiplier         
     , ISNULL(convert(varchar(11), CB.PaymentLastDate,106),'--')  AS LastPaymentDate        
     ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--')  as LastPaidAmount
     ,ISNULL(convert(varchar(50),CB.PreviousBalance),'--') as PreviousBalance             
     ,CB.CycleId
     ,1 AS RowsEffected
      ,ISNULL(convert(varchar(50),CB.LastPaymentTotal),'--') AS TotalPayment
      ,BD.Postal_ZipCode AS PostalZipCode
     ,isnull((dbo.fn_GetCustomerAddressFormat(BD.Postal_HouseNo,BD.Postal_Street,BD.Postal_City,BD.Postal_ZipCode) ),'--') AS PostalAddress 
     ,Replace(convert(varchar,convert(Money, CB.TotalBillAmountWithArrears ),1),'.00','')  AS TotalDueAmount 
	,CB.TariffRates As EnergyCharges
    FROM Tbl_CustomerBills CB    
    INNER JOIN Tbl_BillDetails BD ON CB.CustomerBillId = BD.CustomerBillID 
    where ServiceCenterId=@ServiceCenter and BillMonth=@BillMonth and BillYear=@BillYear
  );
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerServiceAddress]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 28 MAY 2014
-- Description:	The purpose of this function is to get the Customer Service Address
-- SELECT dbo.[fn_GetCustomerServiceAddress]('BEDC_CAN_0042')

-- Author:		Padmini
-- Modified date: 25 DEC 2014
-- Description:	Getting details from the same postal address table by adding isservice address true.
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerServiceAddress]
(
@GlobalAccountNo VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @ServiceAddress VARCHAR(MAX)

--IF CUSTOMER HAVING TWO ADDRESSES WE ARE SELECTING SERVICE ADDRESS
    IF((SELECT COUNT(0)FROM CUSTOMERS.Tbl_CustomerPostalAddressDetails WHERE GlobalAccountNumber=@GlobalAccountNo)>=2)
		BEGIN
			SELECT @ServiceAddress =	LTRIM(RTRIM(
				CASE WHEN HouseNo IS NULL OR HouseNo = '' THEN '' ELSE LTRIM(RTRIM(HouseNo)) + ', '  END + 
				CASE WHEN StreetName IS NULL OR StreetName = '' THEN '' ELSE  LTRIM(RTRIM(StreetName)) + ', ' END + 
				CASE WHEN Landmark IS NULL OR Landmark = '' THEN '' ELSE LTRIM(RTRIM(Landmark)) + ', '  END + 
				CASE WHEN City IS NULL OR City = '' THEN '' ELSE  LTRIM(RTRIM(City)) + ', ' END  + 
				CASE WHEN Details IS NULL OR Details = '' THEN '' ELSE  LTRIM(RTRIM(Details))+ ', ' END   + 
				CASE WHEN zipcode IS NULL OR zipcode = '' THEN '' ELSE  LTRIM(RTRIM(zipcode))+ ', ' END )) 
			FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]				
			WHERE GlobalAccountNumber = @GlobalAccountNo
			AND IsServiceAddress=1 AND IsActive=1
		END
	ELSE
--IF CUSTOMER HAVING ONLY ONE ADDRESS THAT WILL BE SERVICE ADDRESS	
		BEGIN
			SELECT @ServiceAddress =	LTRIM(RTRIM(
				CASE WHEN HouseNo IS NULL OR HouseNo = '' THEN '' ELSE LTRIM(RTRIM(HouseNo)) + ', '  END + 
				CASE WHEN StreetName IS NULL OR StreetName = '' THEN '' ELSE  LTRIM(RTRIM(StreetName)) + ', ' END + 
				CASE WHEN Landmark IS NULL OR Landmark = '' THEN '' ELSE LTRIM(RTRIM(Landmark)) + ', '  END + 
				CASE WHEN City IS NULL OR City = '' THEN '' ELSE  LTRIM(RTRIM(City)) + ', ' END  + 
				CASE WHEN Details IS NULL OR Details = '' THEN '' ELSE  LTRIM(RTRIM(Details))+ ', ' END   + 
				CASE WHEN zipcode IS NULL OR zipcode = '' THEN '' ELSE  LTRIM(RTRIM(zipcode))+ ', ' END )) 
			FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]				
			WHERE GlobalAccountNumber = @GlobalAccountNo
			AND IsServiceAddress=0 AND IsActive=1
		END	
	--SET @ServiceAddress = CASE WHEN @ServiceAddress IS NOT NULL THEN SUBSTRING(@ServiceAddress,0,LEN(@ServiceAddress)-1) END
	--SET @ServiceAddress = SUBSTRING(@ServiceAddress,0,LEN(@ServiceAddress)-1)
	RETURN @ServiceAddress
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomersCount_Tariff]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                        
-- Author  : Suresh kumar                   
-- Create date  : 31 OCT 2014                        
-- Description  : THIS function is use to get customers under the selecting cycle
-- =======================================================================    
ALTER FUNCTION [dbo].[fn_GetCustomersCount_Tariff]
(    
@TariffId INT    
,@CycleID VARCHAR(50)  
)    
RETURNS INT    
AS    
BEGIN    
   DECLARE @Result INT = 0    
		
		
  SELECT 
	@Result = COUNT(0) 
  FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CD    
  JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo  
  WHERE CD.TariffClassID  = @TariffId AND BC.CycleId = @CycleID   
  AND CD.ActiveStatusId IN(1,2)
       
       
 RETURN @Result    
    
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCycleIdByBook]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 07-08-2014
-- Description:	The purpose of this function is to get CycleId By BookNo
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCycleIdByBook]
(
	@BookNo VARCHAR(50)
)
RETURNS VARCHAR(MAX)
AS
BEGIN	
		 DECLARE @CycleId VARCHAR(MAX)
		SET @CycleId = NULL
		SELECT   @CycleId=C.CycleId
		 FROM Tbl_Cycles C
		 LEFT JOIN Tbl_BookNumbers B ON B.CycleId=C.CycleId
		 WHERE B.BookNo = @BookNo
			
	RETURN @CycleId

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustPrevReadingByReadDate_New]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 03-03-2015
-- Description:	The purpose of this function is to get customer
--				 Previous Reading based on meter billing read date 				
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustPrevReadingByReadDate_New]
(
	@GlobalAccountNumber VARCHAR(50)
	,@ReadDate VARCHAR(20)
	,@MeterNumber VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @PreviousReading VARCHAR(50)=''

	SET @PreviousReading=(SELECT CASE WHEN (SELECT TOP(1) PreviousReading 
							FROM Tbl_CustomerReadings CR 
							WHERE CR.GlobalAccountNumber=@GlobalAccountNumber 
							AND CR.MeterNumber=@MeterNumber
							AND CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  
						 order by CustomerReadingId desc)  
					   IS NULL  
					   THEN  CONVERT(VARCHAR(50),ISNULL((SELECT InitialReading FROM CUSTOMERS.Tbl_CustomerActiveDetails WHERE GlobalAccountNumber=@GlobalAccountNumber),0))  
					   ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber=@GlobalAccountNumber AND   CR.MeterNumber=@MeterNumber AND
					  CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END 
					  )
	RETURN @PreviousReading	

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetDirectCustomersCount]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                        
-- Author  : Suresh kumar                   
-- Create date  : 06 OCT 2014                        
-- Description  : THIS function is use to get direct customers under the selecting cycle
-- Modified By : T.Karthik
-- Modified date: 24-12-2014
-- Modified By : Jeevan Amunuri
-- Modified date: 31-03-2015
-- =======================================================================    
ALTER FUNCTION [dbo].[fn_GetDirectCustomersCount]
(    
@TariffId INT    
,@CycleID VARCHAR(50)  
,@ClusterCategoryId INT
)    
RETURNS INT    
AS    
BEGIN    
   DECLARE @MonthStartDate VARCHAR(50)         
    ,@Date VARCHAR(50)           
    ,@Result INT = 0    
    ,@ReadCustomersCount INT     
    
 IF(@ClusterCategoryId!=0)
	 BEGIN
			SELECT 
			@Result = COUNT(0) 
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CD    
			JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo  
			WHERE CD.TariffClassID  = @TariffId AND BC.CycleId = @CycleID   
			AND CD.ActiveStatusId IN(1,2) AND ReadCodeID = 1 AND ClusterCategoryId=@ClusterCategoryId
	 END
 ELSE
	 BEGIN
			SELECT 
			@Result = COUNT(0) 
			FROM CUSTOMERS.Tbl_CustomerProceduralDetails AS CD    
			JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo  
			WHERE CD.TariffClassID  = @TariffId AND BC.CycleId = @CycleID   
			AND CD.ActiveStatusId IN(1,2) AND ReadCodeID = 1 
	 END 
       
 RETURN @Result    
    
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerPostalAddress]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Padmini
-- Create date: 21/02/2015
-- Description:	This function is unsed to get the customer postal address
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerPostalAddress]
(
@GlobalAccountNo VARCHAR(50)	
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @PostalAddress VARCHAR(MAX)

			SELECT @PostalAddress =	LTRIM(RTRIM(
				CASE WHEN HouseNo IS NULL OR HouseNo = '' THEN '' ELSE LTRIM(RTRIM(HouseNo)) + ', '  END + 
				CASE WHEN StreetName IS NULL OR StreetName = '' THEN '' ELSE  LTRIM(RTRIM(StreetName)) + ', ' END + 
				CASE WHEN Landmark IS NULL OR Landmark = '' THEN '' ELSE LTRIM(RTRIM(Landmark)) + ', '  END + 
				CASE WHEN City IS NULL OR City = '' THEN '' ELSE  LTRIM(RTRIM(City)) + ', ' END  + 
				CASE WHEN Details IS NULL OR Details = '' THEN '' ELSE  LTRIM(RTRIM(Details))+ ', ' END   + 
				CASE WHEN zipcode IS NULL OR zipcode = '' THEN '' ELSE  LTRIM(RTRIM(zipcode))+ ', ' END )) 
			FROM [CUSTOMERS].[Tbl_CustomerPostalAddressDetails]				
			WHERE GlobalAccountNumber = @GlobalAccountNo
			AND IsServiceAddress=0 AND IsActive=1
			
	RETURN @PostalAddress			
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetDueBill]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------
-- =============================================  
-- Author:  NEERAJ KANOJIYA 
-- Create date: 30 JUNE 2014  
-- Description: The purpose of this function is to get the total Due Amonunt for a bill
-- Modified BY: Suresh Kumar Dasi
-- Description:add the Where Condition with BillNo and AdjustmentAmount also diductiong.  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetDueBill]  
(  
@BillNo VARCHAR(50)  
)  
RETURNS DECIMAL(18,2)  
AS  
BEGIN  
DECLARE @DueAmount Decimal(18,2)     

SELECT @DueAmount= TotalBillAmountWithTax - AdjustmentAmmount -(dbo.fn_GetTotalPaidAmountOfBill(@BillNo)) FROM Tbl_CustomerBills
WHERE BillNo = @BillNo
    
RETURN @DueAmount  
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthlyUsage_ByCycle]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 29 July 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READING TOTA BY TARIFF ID
-- Modified By  : Padmini
-- Modified Date: 25-12-2014
-- Modified By  : Jeevan Amunuri
-- Modified Date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetMonthlyUsage_ByCycle]
(
	@CycleID VARCHAR(20)
	,@Year INT
    ,@Month int  
)
RETURNS INT
AS
BEGIN
	DECLARE @TotalUsage INT
	
	SELECT @TotalUsage = SUM(Usage) FROM Tbl_CustomerReadings CR
	JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CPD ON CPD.GlobalAccountNumber = CR.GlobalAccountNumber
	INNER JOIN Tbl_BookNumbers AS BC ON CPD.BookNo=BC.BookNo
	WHERE  BC.CycleId = @CycleID 
	AND (ReadDate > (SELECT MAX(BillGeneratedDate) FROM Tbl_CustomerBills WHERE CycleId=@CycleID AND BillYear=@Year AND BillMonth=@Month)
	OR ReadDate <= dbo.fn_GetCurrentDateTime())
	--AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)
	 
 	RETURN @TotalUsage;
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthlyReadingCustomers_ByCycle]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 03 NOV 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READING TOTA BY TARIFF ID
-- Modified By   : Padmini
-- Modified Date : 25-12-2014
-- Modified By  : Jeevan Amunuri
-- Modified Date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetMonthlyReadingCustomers_ByCycle]
(
	@CycleID VARCHAR(20)
	,@Year INT
    ,@Month int  
)
RETURNS INT
AS
BEGIN
	DECLARE @TotalCount INT
		
	SELECT @TotalCount = COUNT(Distinct CR.GlobalAccountNumber) FROM Tbl_CustomerReadings CR
	JOIN CUSTOMERS.Tbl_CustomerProceduralDetails CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
	INNER JOIN Tbl_BookNumbers AS BC ON CD.BookNo=BC.BookNo	
	WHERE  BC.CycleId = @CycleID
	AND (ReadDate > (SELECT MAX(BillGeneratedDate) FROM Tbl_CustomerBills WHERE CycleId=@CycleID AND BillYear=@Year AND BillMonth=@Month)
	OR ReadDate <=dbo.fn_GetCurrentDateTime())
	--AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)
	 
	RETURN @TotalCount;
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetReadCustomers_ByCycle]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 04 NOV 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READ Customers BY TARIFF Id
-- Modified By  : Jeevan Amunuri
-- Modified Date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetReadCustomers_ByCycle]
(
	@CycleID VARCHAR(20)
)
RETURNS INT
AS
BEGIN
	DECLARE @TotalCount INT
   
	SELECT 
		@TotalCount = COUNT(0) 
	FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CD 
	INNER JOIN Tbl_BookNumbers AS BC ON CD.BookNo = BC.BookNo
	WHERE  BC.CycleId = @CycleID
	AND CD.ActiveStatusId =1 AND ReadCodeId = 2
		 
	RETURN @TotalCount
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetPaidMeterCustomer_Balance]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: <Suresh Kumar Dasi>
-- Create date	: <10-NOV-2014>
-- Description	: This function is used to get the paid meter customer balance 
-- =============================================
ALTER FUNCTION [dbo].[fn_GetPaidMeterCustomer_Balance]
(
@AccountNo VARCHAR(50)
)
RETURNS DECIMAL(18,2)
AS
BEGIN
		DECLARE @Result DECIMAL(18,2)
				,@TotalMeterPrice DECIMAL(18,2)
				,@TotalDiscountedAmount DECIMAL(18,2)
		
		IF((SELECT dbo.fn_IsPaidMeterCustomer(@AccountNo)) = 1)
			BEGIN
				SELECT @TotalMeterPrice = ISNULL(MeterCost,0) FROM Tbl_PaidMeterDetails WHERE AccountNo = @AccountNo 
				SELECT @TotalDiscountedAmount = SUM(ISNULL(Amount,0)) FROM Tbl_PaidMeterPaymentDetails WHERE AccountNo = @AccountNo 
				SET @Result = (ISNULL(@TotalMeterPrice,0) - ISNULL(@TotalDiscountedAmount,0))
			END
		ELSE
			BEGIN
				SET @Result = 0
			END	
			
	RETURN @Result

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetNormalCustomerConsumption]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Rajaiah>
-- Create date: <Create Date,,>
-- Description:	<Description,, Generate Normal customer consumption>
-- =============================================

--SELECT *	FROM fn_GetNormalCustomerConsumption('0001329260')
 
ALTER FUNCTION [dbo].[fn_GetNormalCustomerConsumption]
(
	-- Add the parameters for the function here
	@GlobalAccountNumber VARCHAR(50),
	@OpeningBalance decimal(18,2)	
)
RETURNS @CustomerConsumption TABLE 
(
	-- Add the column definitions for the TABLE variable here
	 GlobalAccountNumber VARCHAR(50)
	,PreviousReading VARCHAR(50)
	,PresentReading VARCHAR(50)
	,Usage DECIMAL(18,2)
	,LastBillGenerated DATETIME
	,BalanceUsage BIT
	,IsFromReading INT
	,CustomerBillId INT
	,PreviousBalance Decimal(18,2)
	,Multiplier INt
	,ReadDate datetime
)
AS
BEGIN
	DECLARE @ResultConsumption VARCHAR(1000),@LastBillGenerated DATETIME,@MeterMultiplier INT, @BalanceUsage INT,@CustomerBillId Int,@PreviousBalance decimal(18,2),@ReadDate Datetime
	
	-- Add the T-SQL statements to compute the return value here
	-- If reading available
	IF EXISTS(SELECT GlobalAccountNumber FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0)
	BEGIN
		
		 
		SELECT @MeterMultiplier=(SELECT top (1)  MeterMultiplier FROM Tbl_MeterInformation(NOLOCK)    
			   WHERE MeterNo=(SELECT CPD.MeterNumber FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD(NOLOCK) WHERE GlobalAccountNumber=@GlobalAccountNumber))    
		
		SELECT TOP(1) @LastBillGenerated =  BillGeneratedDate,@BalanceUsage=BalanceUsage ,@CustomerBillId=CustomerBillId,@PreviousBalance=PreviousBalance
		FROM Tbl_CustomerBills (NOLOCK)    
		WHERE AccountNo = @GlobalAccountNumber     
		ORDER BY CustomerBillId DESC   	
			
			IF @PreviousBalance IS NULL
				BEGIN
					select @PreviousBalance=@OpeningBalance
				END
			if @BalanceUsage IS NULL
				SET @BalanceUsage=0
			if @CustomerBillId IS NULL
				SET @CustomerBillId=0
		-- Fill the table variable with the rows for your result set
		INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,
		LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance,Multiplier,ReadDate)
		SELECT GlobalAccountNumber
			,MIN(PreviousReading) AS PreviousReading
			,MAX(PresentReading) AS PresentReading
			,SUM(Usage)*@MeterMultiplier AS Usage
			,@LastBillGenerated AS LastBillGenerated
			,@BalanceUsage AS BalanceUsage
			,1 AS IsFromReading
			,@CustomerBillId
			,@PreviousBalance
			,@MeterMultiplier
			,MAX(ReadDate)
		FROM Tbl_CustomerReadings(NOLOCK) 
		WHERE GlobalAccountNumber=@GlobalAccountNumber AND ISNULL(IsBilled,0)=0
		GROUP BY GlobalAccountNumber		
	
	END
	ELSE -- If reading not available
	BEGIN
		DECLARE @IsFirstTimeCustomer BIT = 1 
		SELECT @IsFirstTimeCustomer=dbo.fn_IsFirstTimeCustomerForBilling(@GlobalAccountNumber)
		
		IF @IsFirstTimeCustomer=0
		BEGIN
		
		 
 		
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,PresentReading,Usage,LastBillGenerated,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT TOP(1) AccountNo AS GlobalAccountNumber
				,'--' AS PreviousReading
				,'--' AS PresentReading
				,AverageReading AS Usage
				,BillGeneratedDate AS LastBillGenerated
				,isnull(BalanceUsage,0)
				,0 AS IsFromReading
				,CustomerBillId
				,PreviousBalance
				 
			FROM Tbl_CustomerBills (NOLOCK) 
			WHERE AccountNo = @GlobalAccountNumber 
			ORDER BY CustomerBillId DESC
		END
		ELSE
		BEGIN
			INSERT INTO @CustomerConsumption(GlobalAccountNumber,PreviousReading,
			PresentReading,Usage,BalanceUsage,IsFromReading,CustomerBillId,PreviousBalance)
			SELECT GlobalAccountNumber
				,'--' AS PreviousReading
				,'--' AS PresentReading
				,InitialBillingKWh AS Usage
				,0 AS  BalanceUsage 
				,0 AS IsFromReading
				,0
				,@OpeningBalance
				
			FROM [CUSTOMERS].[Tbl_CustomeractiveDetails] CPD(NOLOCK) 
			WHERE GlobalAccountNumber=@GlobalAccountNumber
		END		
	
	END	
	
	RETURN 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMeterDials_ByAccountNO]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 26 JUNE 2014
-- Description:	The purpose of this function is to get the Customer Meter Dials
-- Modified By : Padmini
-- Modified Date : 27-12-2014 
-- =============================================
ALTER FUNCTION [dbo].[fn_GetMeterDials_ByAccountNO]
(
@AccountNo VARCHAR(50)
)
RETURNS INT
AS
BEGIN
	DECLARE @MeterDials INT

	SELECT @MeterDials  = M.MeterDials FROM [CUSTOMERS].[Tbl_CustomerSDetail] CD
	JOIN [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD ON CD.GlobalAccountNumber=CPD.GlobalAccountNumber
	JOIN Tbl_MeterInformation M ON CPD.MeterNumber = M.MeterNo	
	WHERE CD.GlobalAccountNumber = @AccountNo
		
	RETURN @MeterDials 

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsValidCustomerCycle]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: 04-09-2014
-- Description:	The purpose of this function is to check Customer Cycleid IsValid Or Not
-- =============================================
ALTER FUNCTION [dbo].[fn_IsValidCustomerCycle]
(
 @AccountNo VARCHAR(50)
 ,@CycleId VARCHAR(20)
)
RETURNS BIT
AS
BEGIN
	DECLARE @IsValid BIT = 0
			,@CustomerCycleId VARCHAR(20)
		
		SELECT @CustomerCycleId = BC.CycleId
		FROM [CUSTOMERS].[Tbl_CustomerProceduralDetails] CPD
		LEFT JOIN Tbl_BookNumbers BC ON CPD.BookNo = BC.BookNo
		WHERE GlobalAccountNumber = @AccountNo 
		
		IF(@CustomerCycleId = @CycleId)
			BEGIN
				SET @IsValid = 1
			END
		
	RETURN @IsValid
END
GO
/****** Object:  UserDefinedFunction [MASTERS].[fn_ServiceUnitCodeGenerate]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 22-DEC-2014
-- Description:	GENERATE SERVICE CODE
-- =============================================
ALTER FUNCTION [MASTERS].[fn_ServiceUnitCodeGenerate]()
RETURNS VARCHAR(10)
AS
BEGIN
	
DECLARE @SUCODE INT
		,@SUCODENew VARCHAR(10)
		,@LengthGlobal INT =2
		,@LengthCurrent INT
		
	--GET CURRENT Service Unit Code		
	SET @SUCODENew= (SELECT TOP 1 SUCode 
									FROM Tbl_ServiceUnits 
									ORDER BY IdentitySU_ID DESC)							
	--INCREMENT Service Unit Code BY 1
	--IF(@SUCODENew IS NOT NULL)
	SET @SUCODE =CONVERT(INT,ISNULL(@SUCODENew,0)) + 1
	
	--ELSE 
	--SET @SUCODE=0;

	SET @SUCODENew=@SUCODE
	
	--GET LEGHT OF CURRENT Service Unit Code
	SET @LengthCurrent=LEN(@SUCODENew) 
	
	--NEW Service Unit Code CAN BE GENERATE OR NOT
	IF(@LengthCurrent<=@LengthGlobal)
	BEGIN
		--IF Service Unit Code
		WHILE(@LengthCurrent < @LengthGlobal)
		BEGIN
			SET @SUCODENew= {fn CONCAT('0',@SUCODENew)}
			SET @LengthCurrent=@LengthCurrent+1;
		END
	END
	ELSE
	BEGIN
		SET @SUCODENew=NULL
	END
	
	RETURN @SUCODENew
	--PRINT @SUCODENew
END
--========================================================================================--
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ServiceUnitCodeGenerate]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 22-DEC-2014
-- Description:	GENERATE SERVICE CODE
-- ModifieedBy: Padmini
-- Modified Date:14-02-2015
-- =============================================
ALTER FUNCTION [dbo].[fn_ServiceUnitCodeGenerate]
(
  @BU_ID VARCHAR(20)
)
RETURNS VARCHAR(10)
AS
BEGIN
	
DECLARE @SUCODE INT
		,@SUCODENew VARCHAR(10)
		,@LengthGlobal INT =2
		,@LengthCurrent INT
		
	----GET CURRENT Service Unit Code		
	--SET @SUCODENew= (SELECT TOP 1 SUCode 
	--								FROM Tbl_ServiceUnits 
	--								WHERE BU_ID=@BU_ID
	--								ORDER BY IdentitySU_ID DESC)
			
	--New Code (Logic Changed to accept BusinessUnit which is having zero)
	SET @SUCODENew= (SELECT MAX(SUCode)
									FROM Tbl_ServiceUnits 
									WHERE BU_ID=@BU_ID
									)																						
	--INCREMENT Service Unit Code BY 1
	--IF(@SUCODENew IS NOT NULL)
	SET @SUCODE =CONVERT(INT,ISNULL(@SUCODENew,0)) + 1
	
	--ELSE 
	--SET @SUCODE=0;
	--SET @SUCODE=1; --(Modified By Padmini : It should generate from 1 not by 0)

	SET @SUCODENew=@SUCODE
	
	--GET LEGHT OF CURRENT Service Unit Code
	SET @LengthCurrent=LEN(@SUCODENew) 
	
	--NEW Service Unit Code CAN BE GENERATE OR NOT
	IF(@LengthCurrent<=@LengthGlobal)
	BEGIN
		--IF Service Unit Code
		WHILE(@LengthCurrent < @LengthGlobal)
		BEGIN
			SET @SUCODENew= {fn CONCAT('0',@SUCODENew)}
			SET @LengthCurrent=@LengthCurrent+1;
		END
	END
	ELSE
	BEGIN
		SET @SUCODENew=NULL
	END
	
	RETURN @SUCODENew
	--PRINT @SUCODENew
END
GO
/****** Object:  UserDefinedFunction [MASTERS].[fn_ServiceCenterCodeGenerate]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 22-DEC-2014
-- Description:	GENERATE SERVICE CODE
-- =============================================
ALTER FUNCTION [MASTERS].[fn_ServiceCenterCodeGenerate]()
RETURNS VARCHAR(10)
AS
BEGIN
	
DECLARE @SCCODE INT
		,@SCCODENew VARCHAR(10)
		,@LengthGlobal INT =2
		,@LengthCurrent INT
		
	--GET CURRENT Service Center Code		
	SET @SCCODENew= (SELECT TOP 1 SCCode 
									FROM Tbl_ServiceCenter 
									ORDER BY IdentitySC_ID DESC)							
	--INCREMENT Service Center Code BY 1
	--IF(@SCCODENew IS NOT NULL)
	SET @SCCODE =CONVERT(INT,ISNULL(@SCCODENew,0)) + 1
	--ELSE 
	--	SET @SCCODE=0;

	SET @SCCODENew=@SCCODE
	
	--GET LEGHT OF CURRENT Service Center Code
	SET @LengthCurrent=LEN(@SCCODENew) 
	
	--NEW Service Center Code CAN BE GENERATE OR NOT
	IF(@LengthCurrent<=@LengthGlobal)
	BEGIN
		--IF Service Center Code
		WHILE(@LengthCurrent < @LengthGlobal)
		BEGIN
			SET @SCCODENew= {fn CONCAT('0',@SCCODENew)}
			SET @LengthCurrent=@LengthCurrent+1;
		END
	END
	ELSE
	BEGIN
		SET @SCCODENew=NULL
	END
	
	RETURN @SCCODENew
	--PRINT @SCCODENew
END
--====================================================================================================
GO
/****** Object:  UserDefinedFunction [dbo].[fn_ServiceCenterCodeGenerate]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 22-DEC-2014
-- Description:	GENERATE SERVICE CODE
-- Modified By: Padmini
-- Modified Date: 14-02-2015
-- =============================================
ALTER FUNCTION [dbo].[fn_ServiceCenterCodeGenerate]
(
  @SU_ID VARCHAR(50)
)
RETURNS VARCHAR(10)
AS
BEGIN
	
DECLARE @SCCODE INT
		,@SCCODENew VARCHAR(10)
		,@LengthGlobal INT =2
		,@LengthCurrent INT
		
	--GET CURRENT Service Center Code		
	--SET @SCCODENew= (SELECT TOP 1 SCCode 
	--								FROM Tbl_ServiceCenter
	--								WHERE SU_ID=@SU_ID
	--								ORDER BY IdentitySC_ID DESC)
	
	--New Code (Logic Changed to accept BusinessUnit which is having zero)
	SET @SCCODENew= (SELECT MAX(SCCode) 
									FROM Tbl_ServiceCenter
									WHERE SU_ID=@SU_ID)													
	--INCREMENT Service Center Code BY 1
	--IF(@SCCODENew IS NOT NULL)
	SET @SCCODE =CONVERT(INT,ISNULL(@SCCODENew,0)) + 1
	--ELSE 
	    --SET @SCCODE=0;
		--SET @SCCODE=1; -- (Modified By : Padmini - Should not generate by 0)

	SET @SCCODENew=@SCCODE
	
	--GET LEGHT OF CURRENT Service Center Code
	SET @LengthCurrent=LEN(@SCCODENew) 
	
	--NEW Service Center Code CAN BE GENERATE OR NOT
	IF(@LengthCurrent<=@LengthGlobal)
	BEGIN
		--IF Service Center Code
		WHILE(@LengthCurrent < @LengthGlobal)
		BEGIN
			SET @SCCODENew= {fn CONCAT('0',@SCCODENew)}
			SET @LengthCurrent=@LengthCurrent+1;
		END
	END
	ELSE
	BEGIN
		SET @SCCODENew=NULL
	END
	
	RETURN @SCCODENew
	--PRINT @SCCODENew
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsStateExists]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Bhimaraju Vanka>
-- Create date: <13-FEB-2014>
-- Description	: This function is used to Check the StateName,StateCode is existing or not
--Select dbo.fn_IsStateExists
-- =============================================
ALTER FUNCTION [dbo].[fn_IsStateExists]
(
	 @StateName VARCHAR(50)
	,@StateCode VARCHAR(20)
)
RETURNS BIT
AS
BEGIN
		DECLARE @Result BIT				
		IF EXISTs(select 0 from Tbl_States where StateName= @StateName AND StateCode=@StateCode)
			BEGIN
				SET @Result = 1
			END
		ELSE
			BEGIN
				SET @Result = 0
			END	
	RETURN @Result

END
GO
/****** Object:  UserDefinedFunction [CUSTOMERS].[fn_GlobalAccountNumberGenerate]    Script Date: 04/07/2015 03:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		NEERAJ KANOJIYA
-- Create date: 25-DEC-2014
-- Description:	GENERATE GLOBAL ACCOUNT NUMBER
-- =============================================
ALTER FUNCTION [CUSTOMERS].[fn_GlobalAccountNumberGenerate]()
RETURNS VARCHAR(10)
AS
BEGIN
	DECLARE @GlobalAccountNumber INT
		,@GlobalAccountNumberNew VARCHAR(10)
		,@LengthGlobal INT =10
		,@LengthCurrent INT
		
	--GET CURRENT GAN		
	SET @GlobalAccountNumberNew= (SELECT TOP 1 GlobalAccountNumber 
									FROM CUSTOMERS.Tbl_CustomersDetail 
									ORDER BY GlobalAccountNumber DESC)
								
	--INCREMENT GAN BY 1
	SET @GlobalAccountNumber =CONVERT(INT,ISNULL(@GlobalAccountNumberNew,0)) + 1
	
	SET @GlobalAccountNumberNew=@GlobalAccountNumber
	
	--GET LEGHT OF CURRENT GAN
	SET @LengthCurrent=LEN(@GlobalAccountNumberNew) 
	
	--NEW GAN CAN BE GENERATE OR NOT
	IF(@LengthCurrent<=@LengthGlobal)
	BEGIN
		--IF GAN
		WHILE(@LengthCurrent < @LengthGlobal)
		BEGIN
			SET @GlobalAccountNumberNew= {fn CONCAT('0',@GlobalAccountNumberNew)}
			SET @LengthCurrent=@LengthCurrent+1;
		END
	END
	ELSE
	BEGIN
		SET @GlobalAccountNumberNew=NULL
	END
	
	RETURN @GlobalAccountNumberNew
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetZeroMinimumCustomers_ByCycle]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 04 NOV 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READ Customers BY TARIFF Id
-- Modified By  : Jeevan Amunuri
-- Modified Date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetZeroMinimumCustomers_ByCycle]
(
	@CycleID VARCHAR(20)
)
RETURNS INT
AS
BEGIN
	DECLARE @TotalCount INT
   
	SELECT 
		@TotalCount = COUNT(0) 
	FROM CUSTOMERS.Tbl_CustomerProceduralDetails CD 
	JOIN CUSTOMERS.Tbl_CustomerActiveDetails CA ON CD.GlobalAccountNumber=CA.GlobalAccountNumber
	INNER JOIN Tbl_BookNumbers AS BC ON CD.BookNo = BC.BookNo
	WHERE  BC.CycleId = @CycleID
	AND CD.ActiveStatusId = 1 AND ReadCodeId = 1 AND ISNULL(InitialBillingKWh,0) = 0
		 
	RETURN @TotalCount
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsCustomerAccountNoExists]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 29-03-2014
-- Modified date: 30-JULY-2014
-- Modified By	: NEERAJ KANOJIYA
-- Description:	The purpose of this function is to check customer account no exists or not
-- =============================================
ALTER FUNCTION [dbo].[fn_IsCustomerAccountNoExists] 
(
	@AccountNo VARCHAR(20)
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @IsAccountNoExists BIT=1
	
	IF NOT EXISTS(SELECT 0 FROM [CUSTOMERS].[Tbl_CustomerSDetail] WHERE GlobalAccountNumber=@AccountNo AND ActiveStatusId=1)
		SET @IsAccountNoExists=0
		
	RETURN @IsAccountNoExists
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsExistsTariffEnergies_Cycle]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		: Suresh Kumar Dasi
-- Create date	: <01-NOV-2014>
-- Description	: This function is used to Check the cycle havings Tariff Energies for all tarifs
-- =============================================
ALTER FUNCTION [dbo].[fn_IsExistsTariffEnergies_Cycle]
(
@Year INT 
,@Month INT 
,@CycleId VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
		DECLARE @PresentId INT
				,@IsValid BIT = 1
				,@MonthStart DATETIME
				,@MonthEnd DATETIME
				,@FromDate DATETIME
				,@ToDate DATETIME
				,@CurrentDate DATETIME = dbo.fn_GetCurrentDateTime()
				
		DECLARE @ValidTariffsTable TABLE(TariffId INT, CustomersCount INT)		
			
			SET @MonthStart = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
		SET @MonthEnd = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStart))))   

		IF(@MonthEnd > @CurrentDate)
			SELECT @ToDate = @CurrentDate
		ELSE
			SET @ToDate = @MonthEnd					
				
		
		DELETE FROM @ValidTariffsTable		

		INSERT INTO @ValidTariffsTable(TariffId,CustomersCount)
		SELECT       
			ClassID
			,dbo.fn_GetCustomersCount_Tariff(ClassID,@CycleId) AS CustomersCount  
		FROM Tbl_MTariffClasses T
		WHERE RefClassID IS NOT NULL AND IsActiveClass = 1  

		DELETE FROM @ValidTariffsTable WHERE CustomersCount = 0
		
		SELECT TOP(1) @PresentId = TariffId FROM @ValidTariffsTable ORDER BY TariffId ASC    
			     
			 WHILE(EXISTS(SELECT TOP(1) TariffId FROM @ValidTariffsTable WHERE TariffId >= @PresentId ORDER BY TariffId ASC))    
			  BEGIN 
						
				IF NOT EXISTS(SELECT 0 FROM Tbl_LEnergyClassCharges WHERE IsActive = 1
								AND ClassID = @PresentId AND @ToDate BETWEEN FromDate AND Todate)
						BEGIN
							SET @IsValid = 0
						END		
			  
			  IF(@PresentId = (SELECT TOP(1) TariffId FROM @ValidTariffsTable ORDER BY TariffId DESC))    
					   BREAK    
				  ELSE    
					   BEGIN    
							SET @PresentId = (SELECT TOP(1) TariffId FROM @ValidTariffsTable WHERE  TariffId > @PresentId ORDER BY TariffId ASC)    
							IF(@PresentId IS NULL) break;    
							  Continue    
					   END    
			  END	  
		
	RETURN @IsValid

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsEstimationsCompleted_Cycle]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Suresh Kumar Dasi
-- Create date: <28-OCT-2014>
-- Description	: This function is used to Check the cycle havings estimations for all tarifs
-- Modified By: T.Karthik
-- Modified date : 29-12-2014
-- =============================================
ALTER FUNCTION [dbo].[fn_IsEstimationsCompleted_Cycle]
(
@Year INT 
,@Month INT 
,@CycleId VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
		DECLARE @PresentId INT
				,@IsValid BIT = 1
		DECLARE @ValidTariffsTable TABLE(TariffId INT, CustomersCount INT)		
		
		DELETE FROM @ValidTariffsTable
		
		INSERT INTO @ValidTariffsTable(TariffId,CustomersCount)
		SELECT       
			ClassID
			,dbo.fn_GetDirectCustomersCount(ClassID,@CycleId,0) AS CustomersCount  
		FROM Tbl_MTariffClasses T
		WHERE RefClassID IS NOT NULL AND IsActiveClass = 1  

		DELETE FROM @ValidTariffsTable WHERE CustomersCount = 0
		
		SELECT TOP(1) @PresentId = TariffId FROM @ValidTariffsTable ORDER BY TariffId ASC    
			     
			 WHILE(EXISTS(SELECT TOP(1) TariffId FROM @ValidTariffsTable WHERE TariffId >= @PresentId ORDER BY TariffId ASC))    
			  BEGIN 
						
				IF NOT EXISTS(SELECT 0 FROM Tbl_EstimationSettings WHERE BillingMonth = @Month 
								AND BillingYear = @Year AND CycleId = @CycleId AND ActiveStatusId = 1
								AND TariffId = @PresentId
								AND BillingRule = (CASE WHEN EnergytoCalculate > 0 THEN 1 ELSE 2 END))
						BEGIN
							SET @IsValid = 0
							BREAK
						END		
			  
			  IF(@PresentId = (SELECT TOP(1) TariffId FROM @ValidTariffsTable ORDER BY TariffId DESC))    
					   BREAK    
				  ELSE    
					   BEGIN    
							SET @PresentId = (SELECT TOP(1) TariffId FROM @ValidTariffsTable WHERE  TariffId > @PresentId ORDER BY TariffId ASC)    
							IF(@PresentId IS NULL) break;    
							  Continue    
					   END    
			  END	  
		
	RETURN @IsValid

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsAccountNoExists_BU_Id]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author      : Suresh Kumar D
-- Create date : 29-10-2014
-- Description : to check the Account exists under the given BU_Id
-- =============================================
ALTER FUNCTION [dbo].[fn_IsAccountNoExists_BU_Id]
(
@AccountNo VARCHAR(20)
,@BU_ID VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	
	DECLARE @IsAccountNoExists BIT=1
	
	IF NOT EXISTS(SELECT 0 FROM [UDV_CustomerDescription] 
                  WHERE (GlobalAccountNumber=@AccountNo OR OldAccountNo=@AccountNo)
                  AND ActiveStatusId=1 
                  AND (BU_ID = @BU_ID OR @BU_ID = ''))
		SET @IsAccountNoExists=0
		
	RETURN @IsAccountNoExists
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetTotalMinimumUsage_Tariff]    Script Date: 04/07/2015 03:06:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                        
-- Author  : Suresh kumar                   
-- Create date  : 06 OCT 2014                        
-- Description  : THIS function is use to get direct customers under the selecting cycle
-- Modified date: 24-12-2014
-- Modified By : T.Karthik
-- Modified By : Jeevan Amunuri
-- Modified date: 31-03-2015
-- =======================================================================    
ALTER FUNCTION [dbo].[fn_GetTotalMinimumUsage_Tariff]
(    
@TariffId INT    
,@CycleID VARCHAR(50)  
,@ClusterCategoryId INT
)    
RETURNS INT    
AS    
BEGIN    
   DECLARE @Result DECIMAL(18,2) = 0    

SELECT 
		@Result =SUM(AverageReading) 
  FROM Tbl_DirectCustomersAvgReadings DCA
  INNER JOIN UDV_RptCustomerBookInfo AS CD  ON CD.GlobalAccountNumber= DCA.GlobalAccountNumber
  JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo  
  WHERE CD.TariffId  = @TariffId AND BC.CycleId = @CycleID   
  AND CD.ClusterCategoryId = @ClusterCategoryId
  AND CD.ActiveStatusId IN(1,2) AND ReadCodeId = 1
  GROUP BY BC.CycleId
 RETURN @Result    
    
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthlyUsage_ByTariff]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 29 July 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READING TOTA BY TARIFF ID
-- Modified By : Padmini
-- Modified Date :25-12-2014
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetMonthlyUsage_ByTariff]
(
	@CycleID VARCHAR(20)
	,@TariffId  VARCHAR(20)
	,@Year INT
    ,@Month int  
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @TotalUsage INT
			,@Date DATETIME     
			,@MonthStartDate VARCHAR(50)     
   
	SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
    SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   
   
	
	--SELECT @TotalUsage = SUM(Usage) FROM Tbl_CustomerReadings CR
	--JOIN Tbl_CustomerDetails CD ON CD.AccountNo = CR.AccountNumber
	--LEFT JOIN Tbl_BookNumbers AS BC ON CD.BookNo=BC.BookNo
	--WHERE  BC.CycleId = @CycleID AND CD.TariffId=@TariffId
	--AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)
	 
	SELECT @TotalUsage = SUM(Usage) FROM Tbl_CustomerReadings CR
	JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
	LEFT JOIN Tbl_BookNumbers AS BC ON CD.BookNo=BC.BookNo
	WHERE  BC.CycleId = @CycleID AND CD.TariffId=@TariffId
	AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)
	 
	RETURN @TotalUsage;
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthlyUsage_ByCycles]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: Padmini
-- Create date  : 13/01/2015             
-- Description  : THIS FUNCTION WILL GET TOTAL READING TOTA BY Cycles
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetMonthlyUsage_ByCycles]
(
	@CycleID VARCHAR(20)
	,@CycleIds VARCHAR(MAX)
	,@Year INT
    ,@Month int  
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @TotalUsage INT
			,@ReadCustTotalUsage INT
			,@DirectCustTotalUsage INT
			,@Date DATETIME     
			,@MonthStartDate VARCHAR(50)     
   
	SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
    SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   
	 
	SELECT @ReadCustTotalUsage = SUM(Usage) FROM Tbl_CustomerReadings CR
	JOIN [UDV_CustomerDescription] CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
	LEFT JOIN Tbl_BookNumbers AS BC ON CD.BookNo=BC.BookNo
	WHERE  BC.CycleId = @CycleID AND CD.TariffId IN (SELECT [com] FROM dbo.fn_Split(@CycleIds,','))
	AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)

	SELECT @DirectCustTotalUsage=SUM(EnergytoCalculate) FROM Tbl_EstimationSettings 
	WHERE CycleId=@CycleID AND TariffId IN (SELECT [com] FROM dbo.fn_Split(@CycleIds,','))
	AND BillingYear=@Year AND BillingMonth=@Month

	SET @TotalUsage=ISNULL(@ReadCustTotalUsage,0)+ISNULL(@DirectCustTotalUsage,0)
	 
	RETURN @TotalUsage;
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthlyReadCustomers_ByTariff]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 04 NOV 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READING TOTA BY TARIFF ID
-- Modified By : T.Karthik
-- Modified date: 24-12-2014
-- Modified By : Jeevan Amunuri
-- Modified date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetMonthlyReadCustomers_ByTariff]
(
	@CycleID VARCHAR(20)
	,@Year INT
    ,@Month int  
    ,@TariffId INT
	,@ClusterCategoryId INT)
RETURNS INT
AS
BEGIN
	DECLARE @TotalCount INT
	--		,@Date DATETIME     
	--		,@MonthStartDate VARCHAR(50)     
   
	--SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
 --   SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   

	SELECT @TotalCount = COUNT(Distinct CR.GlobalAccountNumber) FROM Tbl_CustomerReadings CR
	JOIN UDV_RptCustomerBookInfo CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
	LEFT JOIN Tbl_BookNumbers AS BC ON CD.BookNo=BC.BookNo
	WHERE  BC.CycleId = @CycleID AND CD.TariffId = @TariffId
	AND CD.ClusterCategoryId = @ClusterCategoryId
	AND (ReadDate > (SELECT MAX(BillGeneratedDate) FROM Tbl_CustomerBills WHERE CycleId=@CycleID AND BillYear=@Year AND BillMonth=@Month)
	OR ReadDate <=dbo.fn_GetCurrentDateTime())
	--AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)
	 
	RETURN @TotalCount;
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetMonthlyConsumption_ByTariff]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                
-- Author		: SURESH Kumar D           
-- Create date  : 04 NOV 2014                
-- Description  : THIS FUNCTION WILL GET TOTAL READING TOTAL COnsumptin BY TARIFF ID
-- Modified By : T.Karthik
-- Modified date: 24-12-2014
-- Modified By : Jeevan Amunuri
-- Modified date: 31-03-2015
-- =======================================================================   
ALTER FUNCTION [dbo].[fn_GetMonthlyConsumption_ByTariff]
(
	@CycleID VARCHAR(20)
	,@Year INT
    ,@Month int  
    ,@TariffId INT
	,@ClusterCategoryId INT
)
RETURNS INT
AS
BEGIN
	DECLARE @TotalUsage INT
			,@Date DATETIME     
			,@MonthStartDate VARCHAR(50)     
   
	SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
    SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   

	SELECT @TotalUsage = SUM(Usage) FROM Tbl_CustomerReadings CR
	JOIN UDV_RptCustomerBookInfo CD ON CD.GlobalAccountNumber = CR.GlobalAccountNumber
	LEFT JOIN Tbl_BookNumbers AS BC ON CD.BookNo=BC.BookNo
	WHERE  BC.CycleId = @CycleID AND CD.TariffId = @TariffId
	AND CD.ClusterCategoryId = @ClusterCategoryId
	AND (ReadDate > (SELECT MAX(BillGeneratedDate) FROM Tbl_CustomerBills WHERE CycleId=@CycleID AND BillYear=@Year AND BillMonth=@Month)
	OR ReadDate <=dbo.fn_GetCurrentDateTime())
	--AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)
	 
	RETURN @TotalUsage;
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetFixedCharges_PaidMeters]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 10 NOV 2014
-- Description:	The purpose of this function is to get the fixed charges for Paid Meter Customers
--=============================================
ALTER FUNCTION [dbo].[fn_GetFixedCharges_PaidMeters]
(
@AccountNo VARCHAR(50)
,@Date DATE
,@PaidMeterAmount DECIMAL(18,2)
)
RETURNS @ResultedTable TABLE(ChargeId INT,ChargeAmount DECIMAL(18,2),EffectedAmount DECIMAL(18,2),GlobalAccountNumber VARCHAR(50),AccountNo VARCHAR(50),ClassID INT)
AS
BEGIN
	DECLARE @RemainingAmount DECIMAL(18,2)= @PaidMeterAmount
			,@ChargeAmount DECIMAL(18,2)
			,@PresentChargeId INT
			
	DECLARE @TempFixedTable TABLE(ChargeId INT,Amount DECIMAL(18,2),GlobalAccountNumber VARCHAR(50),AccountNo VARCHAR(50),ClassID INT)
	
	INSERT INTO @TempFixedTable 
	SELECT 
		ChargeId,A.Amount,C.GlobalAccountNumber,C.AccountNo,ClassID
	FROM Tbl_LAdditionalClassCharges A    
  --JOIN Tbl_CustomerDetails C ON A.ClassID = C.TariffId 
	JOIN [UDV_CustomerDescription] C ON A.ClassID = C.TariffId    
	WHERE A.IsActive = 1 AND C.GlobalAccountNumber = @AccountNo
	AND A.ChargeId IN (SELECT ChargeId FROM Tbl_MChargeIds CI WHERE CI.IsAcitve = 1)
	AND @Date BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)  

	SELECT TOP(1) @PresentChargeId  = ChargeId FROM @TempFixedTable ORDER BY ChargeId ASC    
     
	WHILE(EXISTS(SELECT TOP(1) ChargeId FROM @TempFixedTable WHERE ChargeId >= @PresentChargeId  ORDER BY ChargeId ASC))    
		BEGIN  
			SELECT @ChargeAmount = Amount FROM @TempFixedTable WHERE ChargeId = @PresentChargeId
			
			IF(@ChargeAmount <= @RemainingAmount)
				BEGIN
					INSERT INTO @ResultedTable
					SELECT 
						@PresentChargeId,@ChargeAmount,@ChargeAmount,GlobalAccountNumber,AccountNo,ClassID
					FROM @TempFixedTable WHERE ChargeId = @PresentChargeId
					
					SET @RemainingAmount = (@RemainingAmount - @ChargeAmount)
				END	
			ELSE 
				BEGIN
					INSERT INTO @ResultedTable
					SELECT 
						@PresentChargeId,@ChargeAmount,@RemainingAmount,GlobalAccountNumber,AccountNo,ClassID
					FROM @TempFixedTable WHERE ChargeId = @PresentChargeId
					
					SET @RemainingAmount = 0	
				END		  
		  
			IF(@PresentChargeId  = (SELECT TOP(1) ChargeId FROM @TempFixedTable ORDER BY ChargeId DESC))    
				   BREAK    
			  ELSE    
				   BEGIN    
						SET @PresentChargeId  = (SELECT TOP(1) ChargeId FROM @TempFixedTable WHERE  ChargeId > @PresentChargeId  ORDER BY ChargeId ASC)    
						IF(@PresentChargeId  IS NULL) break;    
						  Continue    
				   END 
		END    
		
	RETURN 

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustPrevReadingByReadDate]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 13-10-2014
-- Description:	The purpose of this function is to get customer
--				 Previous Reading based on meter billing read date 				
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustPrevReadingByReadDate]
(
	@GlobalAccountNumber VARCHAR(50)
	,@ReadDate VARCHAR(20)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @PreviousReading VARCHAR(50)=''

	SET @PreviousReading=(SELECT CASE WHEN (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber=@GlobalAccountNumber AND   
						 CONVERT(DATE,CR.ReadDate)<CONVERT(DATE, @ReadDate)  order by CustomerReadingId desc)  
					   IS NULL  
					   THEN  CONVERT(VARCHAR(50),ISNULL((SELECT InitialReading FROM [UDV_CustomerDescription] WHERE GlobalAccountNumber=@GlobalAccountNumber),0))  
					   ELSE (SELECT TOP(1) PresentReading FROM Tbl_CustomerReadings CR WHERE CR.GlobalAccountNumber=@GlobalAccountNumber AND   
					  CONVERT(DATE,CR.ReadDate )<CONVERT(DATE, @ReadDate)  order by CR.CustomerReadingId desc)END 
					  )
	RETURN @PreviousReading	

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerPreviousReading]    Script Date: 04/07/2015 03:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author   : Suresh Kumar Dasi  
-- Create date: 12-08-2014  
-- Description: To Customer previous reading
--SELECT dbo.fn_GetCustomerPreviousReading('BEDC_CUN_0015',2014,07)  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerPreviousReading]
(  
 @AccountNo VARCHAR(50)  
 ,@Year INT
 ,@Month INT
)  
RETURNS VARCHAR(20)  
AS  
BEGIN  
 DECLARE @PreviousReading VARCHAR(20)
		 ,@MonthStartDate  VARCHAR(20)
		 ,@Date  VARCHAR(20)
		 ,@Dails INT
		
	SELECT  
		@Dails = ISNULL(MI.MeterDials,0) 
	FROM [UDV_CustomerDescription] CD
	JOIN Tbl_MeterInformation MI ON CD.MeterNumber = MI.MeterNo				
	
   SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'    
   SET @Date = CONVERT(DATETIME,CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate))))   
   
   IF EXISTS(SELECT 0 FROM Tbl_CustomerReadings WHERE GlobalAccountNumber = @AccountNo AND IsBilled = 1)
		BEGIN
			SELECT TOP(1)@PreviousReading = PresentReading FROM Tbl_CustomerReadings 
			WHERE GlobalAccountNumber = @AccountNo
			AND CONVERT(DATE,ReadDate) < CONVERT(DATE,@Date)
			ORDER BY ReadDate DESC
		END
	ELSE
		BEGIN
			SELECT @PreviousReading = InitialReading FROM [UDV_CustomerDescription] WHERE GlobalAccountNumber = @AccountNo
		END
	
 RETURN RIGHT('00000000000'+ISNULL(@PreviousReading,''),@Dails) 
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerCycleId]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 25-08-2014
-- Description:	The purpose of this function is to get CycleId By AccountNo
-- =============================================
ALTER FUNCTION [dbo].[fn_GetCustomerCycleId]
(
	@AccountNo VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @CycleId VARCHAR(50)
		SET @CycleId = NULL
		
	SELECT 
		@CycleId  = BN.CycleId 
	from [UDV_CustomerDescription] CD
	LEFT JOIN Tbl_BookNumbers BN  ON CD.BookNo = BN.BookNo
	WHERE GlobalAccountNumber = @AccountNo
				
	RETURN @CycleId

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerBookDisable_Details]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author	  :	Suresh Kumar Dasi
-- Create date: 15 OCT 2014
-- Description:	The purpose of this function is to get Customer book disable Details
--=============================================
ALTER FUNCTION [dbo].[fn_GetCustomerBookDisable_Details]
(
@AccountNo VARCHAR(50)
,@BillingMonth INT 
,@BillingYear INT
)
RETURNS @ResultedTable TABLE(IsDisable BIT,MonthId INT,YearId INT,BookNo VARCHAR(50)
						,DisableTypeId INT,IsPartialBill BIT,FromDate DATETIME,ToDate DATETIME,IsActive BIT)
AS
BEGIN

	DECLARE @BookNo VARCHAR(50)
	SELECT @BookNo = BookNo FROM [UDV_CustomerDescription] WHERE GlobalAccountNumber = @AccountNo
	
	IF NOT EXISTS(SELECT 0 FROM Tbl_BillingDisabledBooks WHERE BookNo = @BookNo 
				AND MonthId = @BillingMonth AND YearId = @BillingYear)
		BEGIN
			INSERT INTO @ResultedTable(IsDisable)VALUES(0)	
		END
	ELSE
		BEGIN
			IF NOT EXISTS(SELECT 0 FROM Tbl_BillingDisabledBooks WHERE BookNo = @BookNo 
					AND IsActive = 1 AND MonthId = @BillingMonth AND YearId = @BillingYear)
				BEGIN
					DECLARE @DisableBookId INT
					SELECT TOP(1)
						@DisableBookId = DisabledBookId FROM Tbl_BillingDisabledBooks 
					WHERE BookNo = @BookNo 
					AND MonthId = @BillingMonth
					AND YearId = @BillingYear					
					ORDER BY DisabledBookId DESC
					
					INSERT INTO @ResultedTable(IsDisable,MonthId,YearId,BookNo,DisableTypeId,IsPartialBill,FromDate,ToDate,IsActive) 	
					SELECT 
						1,MonthId,YearId,BookNo,DisableTypeId,IsPartialBill,FromDate,ToDate,IsActive  
					FROM Tbl_BillingDisabledBooks 
					WHERE DisabledBookId = @DisableBookId
									
					
				END	
			ELSE
				BEGIN
					INSERT INTO @ResultedTable(IsDisable,MonthId,YearId,BookNo,DisableTypeId,IsPartialBill,FromDate,ToDate,IsActive) 	
					SELECT 
						1,MonthId,YearId,BookNo,DisableTypeId,IsPartialBill,FromDate,ToDate,IsActive 
					FROM Tbl_BillingDisabledBooks 
					WHERE BookNo = @BookNo 
					AND IsActive = 1
					AND MonthId = @BillingMonth
					AND YearId = @BillingYear
				END	
		END	
			
		
	RETURN 

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCustomerAverageReading]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author   : Suresh Kumar Dasi  
-- Create date: 06-05-2014  
-- Description: To Customer Average Reading Calculations Based on Reading Date  
--SELECT dbo.fn_GetAverageReading('BEDC_CUN_0005',25)  
-- =============================================  
ALTER FUNCTION [dbo].[fn_GetCustomerAverageReading]  
(  
 @GlobalAccountNumber VARCHAR(50)  
 ,@CurrentReading NUMERIC  
 ,@ReadDate DATETIME  
)  
RETURNS VARCHAR(50)  
AS  
BEGIN  
 DECLARE @FinalReading VARCHAR(50)  
   ,@ReadingsCount INT  
   ,@PreviousReading NUMERIC  
   ,@PreviousUsage NUMERIC  
     
 SELECT @ReadingsCount = (COUNT(0)+ 1) FROM Tbl_CustomerReadings WHERE GlobalAccountNumber=@GlobalAccountNumber 
        AND CONVERT(DATE,ReadDate) < CONVERT(DATE,@ReadDate)  
 SELECT   
  @PreviousUsage = (CASE WHEN SUM(Usage) IS NULL THEN 0 ELSE SUM(Usage) END)  
 FROM Tbl_CustomerReadings   
 WHERE GlobalAccountNumber=@GlobalAccountNumber 
 AND CONVERT(DATE,ReadDate) < CONVERT(DATE,@ReadDate)  
                     
 SELECT   
  TOP(1) @PreviousReading = (CASE WHEN PresentReading IS NULL THEN 0 ELSE CONVERT(NUMERIC,PresentReading) END)  
 FROM Tbl_CustomerReadings   
 WHERE GlobalAccountNumber=@GlobalAccountNumber 
 AND CONVERT(DATE,ReadDate) < CONVERT(DATE,@ReadDate) ORDER BY CustomerReadingId DESC  
   
 DECLARE @UsageValue DECIMAL(18,2)   
   ,@Dails INT  
   ,@DialsCount INT = 0  
   ,@DialMaxReading VARCHAR(20)= ''  
     
 SELECT   
  @Dails = MeterDials from [UDV_CustomerDescription] C  
 JOIN Tbl_MeterInformation M ON C.MeterNumber = M.MeterNo  
 WHERE GlobalAccountNumber =@GlobalAccountNumber   
   
 WHILE(@DialsCount < @Dails)  
  BEGIN  
   SET @DialMaxReading  = @DialMaxReading  + '9'  
   SET @DialsCount = @DialsCount + 1  
  END  
    
 IF(@PreviousReading > @CurrentReading)  
  BEGIN  
     
   SET @UsageValue= (ISNULL(@CurrentReading,0) + 1)+(CONVERT(DECIMAL(18,2),@DialMaxReading ) - ISNULL(@PreviousReading,0))  
  END  
 ELSE  
  BEGIN  
   SET @UsageValue= ISNULL(@CurrentReading,0) - ISNULL(@PreviousReading,0)     
  END  
   
    SET @FinalReading = CONVERT(VARCHAR(50),CONVERT(INT,(@PreviousUsage + @UsageValue )/@ReadingsCount))  
  
     
 RETURN @FinalReading  
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CheckOldAccNoExists]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 22-07-2014
-- Description:	The purpose of this function is to check OldAccountNo Exists
-- Modified By : Padmini
-- Modified Date : 27-12-2014 
-- =============================================
ALTER FUNCTION [dbo].[fn_CheckOldAccNoExists]
(
	 @OldAccountNo VARCHAR(50)
	,@GlobalAccountNo VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	DECLARE @IsExists BIT=0
		IF(@GlobalAccountNo='')
		BEGIN
			IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE OldAccountNo=@OldAccountNo)
				SET @IsExists=1
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE OldAccountNo=@OldAccountNo AND GlobalAccountNumber!=@GlobalAccountNo)
				SET @IsExists=1
		END
	RETURN @IsExists
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetCountOfEstimatedCustomers]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================================                      
-- Author  : NEERAJ KANOJIYA                    
-- Create date  : 25 July 2014                      
-- Description  : THIS PROCEDURE WILL GET ESTIMATION DETAILS OF READING 
 -- Modified By : Padmini
-- Modified Date : 27-12-2014       
 -- Modified By : T.karthik
-- Modified Date : 29-12-2014             
-- =======================================================================  
ALTER FUNCTION [dbo].[fn_GetCountOfEstimatedCustomers]  
(  
@Year INT   
,@Month INT   
,@TariffId INT  
,@CycleID VARCHAR(50)
,@ClusterCategoryId INT
)  
RETURNS INT  
AS  
BEGIN  
   DECLARE @MonthStartDate VARCHAR(50)       
    ,@Date VARCHAR(50)         
    ,@Result INT = 0  
    ,@ReadCustomersCount INT  

  SET @MonthStartDate = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-01'        
  SET @Date = CONVERT(VARCHAR(20),@Year)+'-'+CONVERT(VARCHAR(20),@Month)+'-'+CONVERT(VARCHAR(3),dbo.fn_GetDaysInMonth(CONVERT(DATETIME,@MonthStartDate)))   
  
  SELECT   
   @ReadCustomersCount = COUNT(DISTINCT AccountNo)   
  FROM Tbl_CustomerReadings CR  
  JOIN [UDV_CustomerDescription] CD ON CR.GlobalAccountNumber = CD.AccountNo  
 -- JOIN Tbl_LEnergyClassCharges EC ON EC.ClassID = CD.TariffId  
  JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo
  WHERE TariffId  = @TariffId AND CD.ActiveStatusId = 1  AND BC.CycleId=@CycleID
  AND CONVERT(DATE,CONVERT(DATETIME,CR.ReadDate)) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)     
 -- AND IsActive = 1   
  --AND CONVERT(DATE,CONVERT(DATETIME,@MonthStartDate)) BETWEEN CONVERT(DATE,FromDate) AND CONVERT(DATE,ToDate)     
  --AND CONVERT(DATE,ReadDate) BETWEEN CONVERT(DATE,@MonthStartDate) AND CONVERT(DATE,@Date)   
     
  SELECT @Result = COUNT(0)- @ReadCustomersCount FROM [UDV_CustomerDescription] AS CD  
  JOIN Tbl_BookNumbers BC ON CD.BookNo=BC.BookNo
  WHERE CD.TariffId  = @TariffId AND CD.ClusterCategoryId=@ClusterCategoryId AND BC.CycleId=@CycleID 
  AND BC.ActiveStatusId = 1   
     
 RETURN @Result  
  
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CheckDocumentNoExists]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		T.Karthik
-- Create date: 06-03-2014
-- Description:	The purpose of this function is to check Document No Exists
-- =============================================
ALTER FUNCTION [dbo].[fn_CheckDocumentNoExists]
(
	@DocumentNo VARCHAR(100)
   ,@GlobalAccountNumber VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	DECLARE @IsExists BIT=0
		IF(@GlobalAccountNumber='')
		BEGIN
			IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE DocumentNo=@DocumentNo)
				SET @IsExists=1
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE DocumentNo=@DocumentNo AND GlobalAccountNumber!=@GlobalAccountNumber)
				SET @IsExists=1
		END
	RETURN @IsExists
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CheckCustomerMeterSerialNoExists]    Script Date: 04/07/2015 03:06:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 11-03-2014
-- Description:	The purpose of this function is to check MeterSerialNo Exists
-- =============================================
ALTER FUNCTION [dbo].[fn_CheckCustomerMeterSerialNoExists]
(
	@MeterSerialNo VARCHAR(100)
	,@GlobalAccountNo VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	DECLARE @IsExists BIT=0
		IF(@GlobalAccountNo='')
		BEGIN
			IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE MeterNumber = (SELECT MeterNo FROM Tbl_MeterInformation WHERE MeterSerialNo=@MeterSerialNo))
				SET @IsExists=1
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT 0 FROM UDV_CustomerDescription WHERE MeterNumber = (SELECT MeterNo FROM Tbl_MeterInformation WHERE MeterSerialNo=@MeterSerialNo AND GlobalAccountNumber=@GlobalAccountNo))
				SET @IsExists=1
		END
	RETURN @IsExists
END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_CheckCustomerMeterNoExists]    Script Date: 04/07/2015 03:06:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		V.Bhimaraju
-- Create date: 11-03-2014
-- Description:	The purpose of this function is to check Meter No Exists
-- =============================================
ALTER FUNCTION [dbo].[fn_CheckCustomerMeterNoExists]
(
	@MeterNo VARCHAR(100)
	,@GlobalAccountNo VARCHAR(50)
)
RETURNS BIT
AS
BEGIN
	DECLARE @IsExists BIT=0
		IF(@GlobalAccountNo='')
		BEGIN
			IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE MeterNumber=@MeterNo)
				SET @IsExists=1
			ELSE IF NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
				SET @IsExists=1			
		END
		ELSE
		BEGIN
			IF EXISTS(SELECT 0 FROM [UDV_CustomerDescription] WHERE MeterNumber=@MeterNo AND GlobalAccountNumber!=@GlobalAccountNo)
				SET @IsExists=1
			ELSE IF NOT EXISTS(SELECT 0 FROM Tbl_MeterInformation WHERE MeterNo=@MeterNo)
				SET @IsExists=1
			
		END
	RETURN @IsExists
END
GO
